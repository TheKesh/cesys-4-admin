var gulp = require('gulp');
var debug = require('gulp-debug');
var php = require('gulp-connect-php');

//var browserSync = require('browser-sync').create();     // for Others
var browserSync = require('browser-sync');

var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var pixrem = require('gulp-pixrem');

var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var saveLicense = require('uglify-save-license');

var svgSprite = require("gulp-svg-sprites");
var svgmin = require('gulp-svgmin');

var critical = require('critical')/*.stream*/;

// Critical
// 
gulp.task('critical', function () {
    critical.generate({
        base: './',
        //src: 'http://localhost:3000/',
        src: 'criticaltest.html',
        //css: ['css/global.css'],
        dest: 'critical/critical.min.css',
        minify: true,
        width: 1920,
        height: 1080,
        ignore: ['@font-face', ':root'],
        //include: [/^\.nav/]
    });
});

// Sprites

gulp.task('svg', function () {
    return gulp.src(/*'svgtest/svgmin/*.svg'*/)
        .pipe(svgSprite({
            mode: "symbols",
            selector: "flag-%f",
            baseSize: 16
        }))
        .pipe(gulp.dest(/*"svgtest/symbols"*/));
});


/// generování iconfontu
/* var iconfont = require('gulp-iconfont');
var runTimestamp = Math.round(Date.now() / 1000);

gulp.task('iconfont', function () {
    return gulp.src(['svgicons/svgmin/*.svg'])
    .pipe(iconfont({
        fontName: 'c4f', // required
        prependUnicode: false, // recommended option
        formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'], // default, 'woff2' and 'svg' are available
        timestamp: runTimestamp, // recommended to get consistent builds when watching files
    }))
    .on('glyphs', function (glyphs, options) {
        // CSS templating, e.g.
        console.log(glyphs, options);nish', cb);
    })
    .pipe(gulp.dest('svgicons/fonts/'));
}); */

// SVGmin
gulp.task('svgmin', function () {
    return gulp.src('svgicons/svg/*.svg')
        .pipe(svgmin({
            plugins: [{
                removeDoctype: true
            }, {
                removeTitle: true
            }, {
                cleanupAttrs: true
            }, {
                removeMetadata: true
            }, {
                removeDesc: true
            }, {
                removeUselessDefs: true
            }, {
                removeXMLNS: true
            },  {
                removeEmptyAttrs: true
            },  {
                removeHiddenElems: true
            },  {
                removeEmptyText: true
            },  {
                removeViewBox: true
            },  {
                minifyStyles: true
            },  {
                mergePaths: true
            },  {
                removeEditorsNSData: true
            },   {
                removeEmptyContainers: true
            },   {
                removeEnableBackground: true
            },   {
                convertStyleToAttrs: true
            },   {
                convertPathData: true
            },   {
                convertTransform: true
            },   {
                removeUnknownsAndDefaults: true
            },   {
                removeNonInheritableGroupAttrs: true
            },   {
                removeUselessStrokeAndFill: true
            },   {
                removeUnusedNS: true
            },   {
                cleanupIDs: true
            },   {
                cleanupNumericValues: true
            },   {
                cleanupListOfValues: true
            },   {
                moveElemsAttrsToGroup: true
            },   {
                moveGroupAttrsToElems: true
            },   {
                removeRasterImages: true
            },   {
                convertShapeToPath: true
            },   {
                sortAttrs: true
            },   {
                removeDimensions: true
            },/*   {
                removeAttrs: true
            },*/   {
                removeStyleElement: true
            },   {
                removeScriptElement: true
            },   {
                removeXMLNS: true
            }, {
                removeDoctype: true
            }, {
                removeComments: true
            }, {
                cleanupNumericValues: {
                    floatPrecision: 1
                }
            }, {
                convertColors: {
                    names2hex: true,
                    rgb2hex: true
                }
            }]
        }))
        .pipe(gulp.dest('svgicons/svgmin'));
});

//var svgstore = require('gulp-svgstore');
//var svgmin = require('gulp-svgmin');
//var path = require('path');
// 
//gulp.task('svgstore', function () {
//    return gulp
//        .src('icons/f/svg/*.svg')
//        .pipe(svgmin(function (file) {
//            var prefix = path.basename(file.relative, path.extname(file.relative));
//            return {
//                plugins: [{
//                    cleanupIDs: {
//                        prefix: prefix + '-',
//                        minify: true
//                    }
//                }]
//            }
//        }))
//        .pipe(svgstore())
//        .pipe(gulp.dest('icons/f'));
//});


// SASS, JS

////// DEFINITIONS

var cssFiles = './assets/sass/**/*.scss',
    cssDest = './css/';

var jsFiles = './assets/js/**/*.js',
    jsDest = './js/';

var allHTML = './**/*.html';
var allPHP = './**/*.php';
var allJS = './**/*.js';

//// BROWSERSYNC
gulp.task('brsync', ['br-sass', 'br-js'], function() {
    //browserSync.init({
    //    server: "./"
    //});
    php.server({
        base: './'
    }, function() {
        browserSync({
            proxy: 'localhost:8000',
            //port: 4000
        });
    });
    gulp.watch(cssFiles, ['br-sass']);
    gulp.watch(jsFiles, ['br-js']);
    gulp.watch(allHTML).on('change', browserSync.reload);
    gulp.watch(allPHP).on('change', browserSync.reload);
    //gulp.watch(allJS).on('change', browserSync.reload);
});

//// CSS
// Default
gulp.task('css', function() {
    gulp.src(cssFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(pixrem())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(cssDest));
});

// Default for BrowserSync
gulp.task('br-sass', function() {
    gulp.src(cssFiles)
        .pipe(sass().on('error', sass.logError))
        //.pipe(pixrem())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(sass(/*{outputStyle: 'compressed'}*/).on('error', sass.logError))
        .pipe(gulp.dest(cssDest))
        .pipe(browserSync.stream());
});

// Production
gulp.task('sass', function () {
    gulp.src(cssFiles)
        .pipe(sass().on('error', sass.logError))
        //.pipe(pixrem())
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest(cssDest));
});

//// JS
// default
gulp.task('js', function () {
    gulp.src(jsFiles)
        .pipe(concat('js.js'))
        .pipe(gulp.dest(jsDest));
});
// Uglify
gulp.task('uglify', function () {
    gulp.src(jsFiles)
        .pipe(concat('js.js'))
        .pipe(gulp.dest(jsDest))
        .pipe(rename('js.min.js'))
        .pipe(uglify({
            output: {
                comments: saveLicense
            }
        }))
        .pipe(gulp.dest(jsDest));
});

// Default for BrowserSync
gulp.task('br-js', function() {
    gulp.src(jsFiles)
        .pipe(concat('globals.js'))
        .pipe(gulp.dest(jsDest))
        //.pipe(rename('js.min.js'))
        /* .pipe(uglify({
            output: {
                comments: saveLicense
            }
        }))
        .pipe(gulp.dest(jsDest)) */
        .pipe(browserSync.stream());
});

////// TASKS
//// BROWSERSYNC
gulp.task('sync', function() {
    gulp.start('brsync');
});

//// CSS
// Default task
gulp.task('cssdefault',function() {
    gulp.start('defstyles');
});

// Production task
gulp.task('cssproduction', function () {
    gulp.start('sass');
});

//// JS
// Watch task
gulp.task('jswatch', function() {  
    gulp.watch(jsFiles,['uglify']);
});

// Production
gulp.task('jsproduction', function() {
    gulp.start('uglify');
});

//// ALL
// Watch
gulp.task('watch', function() {
    gulp.watch(cssFiles,['defstyles'])
        .watch(jsFiles,['uglify']);
});

// Production
gulp.task('production', function() {
    gulp.start('sass')
        .start('uglify');
});





//////

// Move Bootstrap SASS
gulp.task('movebootstrapsass', function() {
    var BTscssFiles = './node_modules/bootstrap/scss/**/*.scss';
    var BTscssDest = './assets/sass/vendor/bootstrap/';

    gulp.src(BTscssFiles)
        .pipe(debug())
        .pipe(gulp.dest(BTscssDest));
});
// Move Bootstrap JS
gulp.task('movebootstrapjs', function() {
    var BTjsFiles = './node_modules/bootstrap/js/src/**/*.js';
    var BTjsDest = './assets/js/vendor/bootstrap/';

    gulp.src(BTjsFiles)
        .pipe(debug())
        .pipe(gulp.dest(BTjsDest));
});
// Move Bootstrap
gulp.task('movebootstrap', function() {
    gulp.start('movebootstrapsass')
        .start('movebootstrapjs');
});