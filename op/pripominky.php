<?php include('parts/header.html'); ?>

<h2 class="small-headline">
    <i class="material-icons">beach_access</i>&nbsp;Nabídky zájezdů
</h2>

<div class="table-wrapper">
    <table class="table table-multirow">
        <thead>
            <tr>
                <th colspan="2"><input type="checkbox" name="" id=""></th>
                <th>Ubytování a CK</th>
                <th>Termín</th>
                <th>Datum</th>
                <th>Doprava</th>
                <th colspan="2">Osoba a suma</th>
            </tr>
        </thead>
        <tbody>
            <tr class="has-checkbox checked">
                <td><input type="checkbox" name="" id="" checked></td>
                <td><img src="https://i.ccdn.cz/acm/45/316512/l0.jpg" class="table-image"></td>
                <td>
                    <a href="">Solaris Resort mobilní domy &#8226; 98070</a><br>
                    Kompas
                </td>
                <td>
                    107281449<br>
                    CS
                </td>
                <td>
                    Od&nbsp;14.&nbsp;10.&nbsp;2017<br>
                    Do&nbsp;14.&nbsp;11.&nbsp;2017
                </td>
                <td>
                    <i class="material-icons">directions_bus</i>&nbsp;PRG
                </td>
                <td>
                    8 610 ×2<br>
                    <strong>17&nbsp;220&nbsp;Kč</strong>
                </td>
                <td class="actions text-right">
                    <i class="material-icons text-success">check</i>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <span class="inline-note">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                    </span>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td><img src="https://i.ccdn.cz/acm/45/316512/l0.jpg" class="table-image"></td>
                <td>
                    <a href="">Kanegra &#8226; 4270</a><br>
                    Neckermann
                </td>
                <td>
                    107281449<br>
                    CS
                </td>
                <td>
                    Od&nbsp;14.&nbsp;10.&nbsp;2017<br>
                    Do&nbsp;14.&nbsp;11.&nbsp;2017
                </td>
                <td>
                    <i class="material-icons">directions_car</i>&nbsp;Vlastní
                </td>
                <td>
                    6 457 ×4<br>
                    <strong>25&nbsp;828&nbsp;Kč</strong>
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="7">
                    <span class="inline-note">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                    </span>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="9" class="text-right">
                    <strong class="mr-3">1 vybraná položka</strong>
                    <button class="btn btn-secondary">
                        <i class="material-icons">check</i> &nbsp; Předáno
                    </button>
                    <button class="btn btn-secondary">
                        <i class="material-icons">send</i> &nbsp; Emailem
                    </button>
                    <button class="btn btn-secondary">
                        <i class="material-icons">picture_as_pdf</i> &nbsp; PDF
                    </button>
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">folder_shared</i> &nbsp; Veřejná schránka
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
    <a href="" class="btn btn-circle btn-success table-add-button">
        <i class="material-icons">add</i>
    </a>
</div>




<h2 class="small-headline">
    <i class="material-icons">book</i>&nbsp;Rezervace
</h2>

<div class="table-wrapper">
    <table class="table table-multirow">
        <thead>
            <tr>
                <th></th>
                <th colspan="2">Ubytování, CK a číslo</th>
                <th>Rezervace do</th>
                <th>Termín</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><img src="https://i.ccdn.cz/acm/45/316512/l0.jpg" class="table-image"></td>
                <td>
                    <a href="">Solaris Resort mobilní domy</a><br>
                    <a href="">Kompas</a> &#8226; 98070
                </td>
                <td class="text-right">
                    <span class="label label-info">Nepotvrzeno</span>
                </td>
                <td>14.&nbsp;12.&nbsp;2017</td>
                <td>
                    <a href="">107281449</a><br>
                    CS
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-secondary"><i class="material-icons">check</i>&nbsp;Smlouvu</button>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="inline-note">
                        poznámka
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr>
                <td><img src="https://i.ccdn.cz/acm/45/316512/l0.jpg" class="table-image"></td>
                <td>
                    <a href="">Solaris Resort mobilní domy</a><br>
                    <a href="">Kompas</a> &#8226; 98070
                </td>
                <td class="text-right">
                    <span class="label label-success">Potvrzeno</span>
                </td>
                <td>14.&nbsp;12.&nbsp;2017</td>
                <td>
                    <a href="">107281449</a><br>
                    CS
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-secondary"><i class="material-icons">check</i>&nbsp;Smlouvu</button>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <a href="">přidat poznámku</a>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7"></td>
            </tr>
        </tfoot>
    </table>
    <a href="" class="btn btn-circle btn-success table-add-button">
        <i class="material-icons">add</i>
    </a>
</div>

<?php include('parts/footer.html'); ?>