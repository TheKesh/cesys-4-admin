<?php include('parts/header.html'); ?>

<h2 class="small-headline">
    <i class="material-icons">work</i>&nbsp;Odbavení
</h2>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Přijato
                </div>
                <div class="panel panel-default-dark">
                    <div class="panel-body d-flex align-items-center">
                        <div class="display-4">
                            <i class="material-icons text-danger">help</i>
                        </div>
                        <div class="display-2 ml-3">
                            Čekáme
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-secondary">Potvrdit přijetí</button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Předáno klientovi
                </div>
                <div class="panel panel-default-dark">
                    <div class="panel-body d-flex align-items-center">
                        <div class="display-4">
                            <i class="material-icons text-danger">help</i>
                        </div>
                        <div class="display-2 ml-3">
                            Nepředáno
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-secondary">Potvrdit předání</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Přijato
                    <div class="pull-right">
                        <a href="">Zrušit přijetí</a>
                    </div>
                </div>
                <div class="panel panel-default-dark">
                    <div class="panel-body d-flex align-items-center">
                        <div class="display-4">
                            <i class="material-icons text-success">check</i>
                        </div>
                        <div class="ml-3">
                            <div class="display-2">23. 10. 2017</div>
                            <div class="display-4">Pondělí &#8226; 13:57</div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-outline-secondary"><i class="material-icons">notifications</i>&nbsp;Přidat připomínku</button>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Předáno klientovi
                </div>
                <div class="panel panel-default-dark edit-active">
                    <div class="list-group">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="display-4">
                                <input type="checkbox">
                            </div>
                            <div class="ml-3 text-muted">
                                <div class="display-2">23. 10. 2017</div>
                                <div class="display-4">Pondělí &#8226; 13:57</div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="row d-flex">
                                <div class="col-xs-4 d-flex align-items-end">
                                    <label for="">Způsob</label>
                                </div>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="row d-flex">
                                <div class="col-xs-4">
                                    <label for="">Poznámka</label>
                                </div>
                                <div class="col-xs-8">
                                    <textarea class="form-control" style="resize: vertical">Vyzvedl syn pana Nováka</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-success">Uložit</button>
                <button type="button" class="btn btn-outline-secondary">Zrušit</button>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Přijato
                    <div class="pull-right">
                        <a href="">Zrušit přijetí</a>
                    </div>
                </div>
                <div class="panel panel-default-dark">
                    <div class="panel-body d-flex align-items-center">
                        <div class="display-4">
                            <i class="material-icons text-success">check</i>
                        </div>
                        <div class="ml-3">
                            <div class="display-2">23. 10. 2017</div>
                            <div class="display-4">Pondělí &#8226; 13:57</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
                <div class="mb-3">
                    Předáno klientovi
                </div>
                <div class="panel panel-default-dark">
                    <div class="list-group">
                        <div class="list-group-item d-flex align-items-center">
                            <div class="display-4">
                                <i class="material-icons text-success">check</i>
                            </div>
                            <div class="ml-3">
                                <div class="display-2">23. 10. 2017</div>
                                <div class="display-4">Pondělí &#8226; 13:57</div>
                            </div>
                        </div>
                        <div class="list-group-item">
                            Osobně
                        </div>
                        <div class="list-group-item">
                            Vyzvedl syn pana Nováka
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-outline-secondary"><i class="material-icons">edit</i>&nbsp;Upravit</button>
            </div>
        </div>
    </div>
</div>

<?php include('parts/footer.html'); ?>