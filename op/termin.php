<?php include('parts/header.html'); ?>

<h2 class="small-headline">
    <i class="material-icons">bookmark_border</i>&nbsp;Objednaný termín zájezdu
</h2>

<div class="panel panel-default bc-date">
    <div class="list-group">
        <div class="list-group-item text-center">
            <a href="" class="btn btn-outline-secondary pull-right">
                <i class="material-icons">edit</i>&nbsp;Upravit
            </a>
            <div class="bc-date-image" style="background-image: url(https://i.ccdn.cz/acm/45/316512/l0.jpg)"></div>
            <div class="display-2 bc-accommodation mb-3">
                <a href="">Hotel Luxury Paradise</a>
            </div>
            <div class="mb-4">
                4210 &#8226; Kanegra &#8226; Chorvatsko, Umag
            </div>
            <div class="panel panel-default-dark bc-date-term">
                <div class="panel-body">
                    <div class="display-2">
                        10.10. - 23.10.2017
                    </div>
                    <div>
                        8 dní &#8226; od Soboty do Neděle
                    </div>
                </div>
            </div>
            <div class="display-2">
                <strong>
                    22 500 Kč
                </strong>
            </div>
            <div class="mb-3">
                6 457 Kč za osobu &#8226; 4 osoby
            </div>
        </div>
        <div class="list-group-item d-flex">
            <div class="col-lg-3 col-md-6 col-sm-12">
                Číslo termínu
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>107266974 / CeSYS</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                Typ zájezdu
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>U moře</strong>
            </div>
        </div>
        <div class="list-group-item d-flex">
            <div class="col-lg-3 col-md-6 col-sm-12">
                Cestovní kancelář
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>Neckermann</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                Last / First minute
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>First minute</strong>
            </div>
        </div>
        <div class="list-group-item d-flex">
            <div class="col-lg-3 col-md-6 col-sm-12">
                Doprava
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>
                    <i class="material-icons">directions_car</i>&nbsp;Vlastní
                </strong>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                Strava
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>Polopenze</strong>
            </div>
        </div>
    </div>
</div>

<?php include('parts/footer.html'); ?>