<?php include('parts/header.html'); ?>

<div>
    <button class="btn btn-success">
        <i class="material-icons">add_circle</i>&nbsp;Přidat platbu za zájezd
    </button>
    <button class="btn btn-success">
        <i class="material-icons">add_circle</i>&nbsp;Přidat platbu za službu
    </button>
</div>

<h2 class="small-headline">
    <i class="material-icons">arrow_forward</i>&nbsp;Příchozí platby
</h2>

<div class="table-wrapper">
    <table class="table table-multirow">
        <thead>
            <tr>
                <th><input type="checkbox" name="" id=""></th>
                <th>Typ</th>
                <th>Částka</th>
                <th>Splatnost &#8226; Úhrada</th>
                <th colspan="2">Dokumenty</th>
            </tr>
        </thead>
        <tbody>
            <tr class="has-checkbox checked">
                <td><input type="checkbox" name="" id="" checked></td>
                <td>Příjem</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017 &#8226; 15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-success">Uhrazeno</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Faktura
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="6">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>Inkaso:</strong> Prodejce,
                        <strong>VS:</strong> 20170101
                    </div>
                    <div class="text-muted">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime illo quae eum adipisci tempora temporibus veritatis, ipsa nam quaerat esse?
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Záloha od klienta</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-danger">Po splatnosti</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Pokladní doklad
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">check</i>&nbsp;Odbaveno
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <span class="caret"></span>
                        </button>
                    </div><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="6">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Záloha od klienta</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2019<br>
                    <span class="label label-warning">Neuhrazeno</span>
                </td>
                <td>
                    <!-- // -->
                </td>
                <td class="actions text-right">
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">check</i>&nbsp;Odbaveno
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <span class="caret"></span>
                        </button>
                    </div><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="6">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>VS:</strong> 20170101
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td colspan="8">
                    <span>
                        K úhradě: <strong>25&nbsp;100&nbsp;Kč</strong>
                    </span>
                    &#8226;
                    <span>
                        Uhrazeno: <strong>25&nbsp;100&nbsp;Kč</strong>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="9" class="text-right">
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit pokladní doklad
                    </button>
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit fakturu
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
    <a href="" class="btn btn-circle btn-success table-add-button">
        <i class="material-icons">add</i>
    </a>
</div>





<h2 class="small-headline">
    <i class="material-icons">arrow_back</i>&nbsp;Odchozí platby
</h2>

<div class="table-wrapper">
    <table class="table table-multirow">
        <thead>
            <tr>
                <th><input type="checkbox" name="" id=""></th>
                <th>Typ</th>
                <th>Částka</th>
                <th>Splatnost &#8226; Úhrada</th>
                <th colspan="2">Dokumenty</th>
            </tr>
        </thead>
        <tbody>
            <tr class="has-checkbox checked">
                <td><input type="checkbox" name="" id="" checked></td>
                <td>Výdaj</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017 &#8226; 15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-success">Uhrazeno</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Faktura
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>VS:</strong> 20170101
                    </div>
                    <div class="text-muted">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis velit consectetur odio sint aliquid exercitationem. Minima, voluptatem! Rerum, libero commodi. Nulla ipsam dolore error iste a obcaecati cum...
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Záloha CK</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-danger">Po splatnosti</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Pokladní doklad
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <button class="btn btn-xs btn-outline-secondary">
                        <i class="material-icons">check</i>&nbsp;Odbaveno
                    </button><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>VS:</strong> 20170101
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Záloha CK</td>
                <td>25&nbsp;100&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2019<br>
                    <span class="label label-warning">Neuhrazeno</span>
                </td>
                <td>
                    <!-- // -->
                </td>
                <td class="actions text-right">
                    <button class="btn btn-xs btn-outline-secondary">
                        <i class="material-icons">check</i>&nbsp;Odbaveno
                    </button><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>VS:</strong> 20170101
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td colspan="8">
                    <span>
                        K úhradě: <strong>25&nbsp;100&nbsp;Kč</strong>
                    </span>
                    &#8226;
                    <span>
                        Uhrazeno: <strong>25&nbsp;100&nbsp;Kč</strong>
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="9" class="text-right">
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit pokladní doklad
                    </button>
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit fakturu
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
    <a href="" class="btn btn-circle btn-success table-add-button">
        <i class="material-icons">add</i>
    </a>
</div>





<h2 class="small-headline">
    <i class="material-icons">account_balance_wallet</i>&nbsp;Provize
</h2>

<div class="table-wrapper">
    <table class="table table-multirow">
        <thead>
            <tr>
                <th><input type="checkbox" name="" id=""></th>
                <th>Typ</th>
                <th>Částka</th>
                <th>Splatnost &#8226; Úhrada</th>
                <th colspan="2">Dokumenty</th>
            </tr>
        </thead>
        <tbody>
            <tr class="has-checkbox checked">
                <td><input type="checkbox" name="" id="" checked></td>
                <td>Provize (zájezd)</td>
                <td>1&nbsp;000&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017 &#8226; 15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-success">Uhrazeno</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Faktura
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>Daň:</strong> Ne,
                        <strong>Dodavatel:</strong> TopFly s.r.o.
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Provize (doprava)</td>
                <td>500&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2017<br>
                    <span class="label label-danger">Po splatnosti</span>
                </td>
                <td>
                    <div class="btn-group">
                        <button class="btn btn-xs btn-outline-secondary">
                            Pokladní doklad
                        </button>
                        <button class="btn btn-xs btn-outline-secondary">
                            <i class="material-icons">get_app</i>
                        </button>
                    </div>
                </td>
                <td class="actions text-right">
                    <button class="btn btn-xs btn-outline-secondary">
                        <i class="material-icons">check</i>&nbsp;Odbaveno
                    </button><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>Daň:</strong> Ne,
                        <strong>Dodavatel:</strong> TopFly s.r.o.
                    </div>
                </td>
            </tr>
        </tbody>
        <tbody>
            <tr class="has-checkbox">
                <td><input type="checkbox" name="" id=""></td>
                <td>Provize (pojištění)</td>
                <td>150&nbsp;Kč</td>
                <td>
                    15.&nbsp;11.&nbsp;2019<br>
                    <span class="label label-warning">Neuhrazeno</span>
                </td>
                <td>
                    <!-- // -->
                </td>
                <td class="actions text-right">
                    <button class="btn btn-xs btn-outline-secondary">
                        <i class="material-icons">check</i>&nbsp;Odbaveno
                    </button><br>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">edit</i></button>
                    <button type="button" class="btn btn-link btn-icon"><i class="material-icons">call_split</i></button>
                    <button type="button" class="btn btn-link btn-link-light btn-icon"><i class="material-icons">delete</i></button>
                </td>
            </tr>
            <tr>
                <td></td>
                <td colspan="5">
                    <div class="text-muted">
                        <strong>Forma:</strong> Bankovní převod,
                        <strong>Daň:</strong> Ne,
                        <strong>Dodavatel:</strong> TopFly s.r.o.
                    </div>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td></td>
                <td class="text-right">
                    Součet:
                </td>
                <td colspan="7">
                    <strong>1&nbsp;750&nbsp;Kč</strong>
                </td>
            </tr>
            <tr>
                <td colspan="9" class="text-right">
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit pokladní doklad
                    </button>
                    <button class="btn btn-outline-secondary">
                        <i class="material-icons">note_add</i>&nbsp;Vytvořit fakturu
                    </button>
                </td>
            </tr>
        </tfoot>
    </table>
    <a href="" class="btn btn-circle btn-success table-add-button">
        <i class="material-icons">add</i>
    </a>
</div>

<div class="text-center">
    <a href="" class="btn btn-danger">
        <i class="material-icons">delete</i>&nbsp;Smazat všechny platby
    </a>
</div>

<?php include('parts/footer.html'); ?>