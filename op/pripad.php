<?php include('parts/header.html'); ?>

<div class="row">
    <!-- střední blok -->
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="panel panel-info-dark note">
            <div class="panel-body">
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vel vitae fugiat eos veniam fuga ipsam dicta atque, earum quisquam aliquam dolorem dignissimos nesciunt molestiae sapiente voluptas.</p>
                <p>Mollitia laborum magnam laudantium aliquid reprehenderit ad laboriosam est tempora. Animi ullam unde nam quas deserunt ipsa velit itaque fugit nobis iusto totam, quasi laudantium id expedita incidunt nostrum. Sunt illum libero optio sit incidunt exercitationem temporibus cupiditate. Doloribus, voluptatibus ratione expedita officia aliquam rerum reprehenderit dolorem consequuntur cupiditate, culpa perferendis, eius sed veritatis dignissimos inventore aliquid facere itaque quam.</p>
                <p>Tempora, pariatur quia ullam, nemo velit, nihil molestias natus magnam deserunt cupiditate vel! Nesciunt voluptatibus, corporis accusantium recusandae possimus repellendus illum dolorum sunt labore? A debitis corrupti inventore deserunt quae ea. Nisi, in ut!<p>
            </div>
        </div>
        <div class="mb-5">
            <h2 class="small-headline">
                <i class="material-icons">event</i>&nbsp;Události
            </h2>
            <div class="table-wrapper">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Termín a typ</th>
                            <th colspan="2">Popis</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <strong>Pondělí 11.10. 15:45</strong><br>
                                Schůzka
                            </td>
                            <td>
                                Schůzka s klientkou na Vyšehradě u kostela
                            </td>
                            <td class="actions text-right">
                                <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                                <a href="" class="btn btn-link btn-link-light btn-icon">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tfoot>
                </table>
                <a href="" class="btn btn-circle btn-success table-add-button">
                    <i class="material-icons">add</i>
                </a>
            </div>
        </div>
        <div class="mb-5">
            <h2 class="small-headline">
                <i class="material-icons">assignment</i>&nbsp;Úkoly
            </h2>
            <div class="table-wrapper">
                <table class="table">
                    <thead>
                        <tr>
                            <th class="wp15">Priorita</th>
                            <th class="wp40">Úkol</th>
                            <th class="wp20">Dokončení</th>
                            <th class="wp25" colspan="2">Uživatel</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Nízká
                            </td>
                            <td>
                                <div>
                                    <strong>
                                        Udělat něco důležitého
                                    </strong>
                                </div>
                                <div class="text-muted">
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Quae, nostrum doloribus! Non ad porro vero magni illum obcaecati libero! Cupiditate.
                                </div>
                            </td>
                            <td>
                                20.10.2018
                            </td>
                            <td>
                                Cestovka
                            </td>
                            <td class="actions text-right">
                                <button type="button" class="btn btn-link btn-icon"><i class="material-icons">notifications</i><span class="caret"></span></button>
                                <a href="" class="btn btn-link btn-icon">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="" class="btn btn-link btn-link-light btn-icon">
                                    <i class="material-icons">delete</i>
                                </a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="5"></td>
                        </tr>
                    </tfoot>
                </table>
                <a href="" class="btn btn-circle btn-success table-add-button">
                    <i class="material-icons">add</i>
                </a>
            </div>
        </div>
        <div class="mb-5">
            <h2 class="small-headline">
                <i class="material-icons">event_note</i>&nbsp;Krátké poznámky
            </h2>
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-info-dark note">
                        <div class="panel-body">
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore asperiores minima deleniti obcaecati omnis officia.
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                    <div class="panel panel-default-dark note">
                        <div class="panel-body centered-content">
                            <button type="button" class="btn btn-circle btn-success">
                                <i class="material-icons">add</i>
                            </button>
                            <div>
                                <strong class="text-success">
                                    Nová
                                </strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-5">
            <div class="text-center">
                <div class="mb-3">
                    Předčasné ukončení případu
                </div>
                <div class="row">
                    <div class="col-xs-6 text-right">
                        <a href="" class="btn btn-outline-danger">Storno</a><br>
                        <small>dle podmínek</small>
                    </div>
                    <div class="col-xs-6 text-left">
                        <a href="" class="btn btn-danger"><i class="material-icons">delete</i>&nbsp;Zahodit</a><br>
                        <small>a založit do Archivu</small>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- pravý blok -->
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="panel panel-default-dark">
            <div class="list-group">
                <div class="list-group-item text-center">
                    <div class="h1 mb-0">2</div>
                    <strong>Dospělí</strong>
                </div>
                <div class="list-group-item text-center">
                    <div class="h1 mb-0">3</div>
                    <strong>Děti</strong>
                    <div>5 let, 8 let, 12 let</div>
                </div>
            </div>
        </div>
        <div class="panel panel-default-dark">
            <div class="panel-body text-center">
                <div class="h1 mb-0">
                    <i class="material-icons">email</i>
                </div>
                <strong>Email</strong>
                <div>
                    Na základě poptávky z&nbsp;webového formuláře
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="list-group">
                <div class="list-group-item">
                    <div>
                        <strong>Vytvořen</strong>
                    </div>
                    <div>31.10.2016 10:01</div>
                </div>
                <div class="list-group-item">
                    <div>
                        <strong>Poslední změna</strong>
                    </div>
                    <div>Včera 16:35</div>
                </div>
                <div class="list-group-item">
                    <div>
                        <strong>Uživatel</strong>
                    </div>
                    <div>Cestovka</div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('parts/footer.html'); ?>