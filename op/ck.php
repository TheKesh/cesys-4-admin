<?php include('parts/header.html'); ?>

<h2 class="small-headline">
    <i class="material-icons">extension</i>&nbsp;Neckermann
</h2>

<div class="panel panel-default bc-ck">
    <div class="list-group">
        <div class="list-group-item">
            <div class="bc-ck-image pull-right">
                <img src="https://press.ckneckermann.cz/wp-content/uploads/2018/02/ckn_logo_vertical_max-width50mm_rgb.jpg" alt="CK Neckermann">
            </div>
            <a href="" class="btn btn-outline-secondary pull-right">
                <i class="material-icons">edit</i>&nbsp;Upravit
            </a>
            <div class="display-2 bc-name mb-3">
                Thomas Cook s.r.o.
            </div>
            <div class="mb-4 display-4">
                <span class="mr-4 text-success"><i class="material-icons">check</i>&nbsp;Kancelář je aktivní</span>
                <span><a href="">www.ckneckermann.cz</a></span>
            </div>
            <div class="row d-flex">
                <div class="col col-auto">
                    <div class="panel panel-default-dark">
                        <div class="panel-body text-center">
                            <div class="display-3">
                                <strong>
                                    1 919
                                </strong>
                            </div>
                            <div>
                                zájezdů v nabídce
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-auto">
                    <div class="panel panel-default-dark">
                        <div class="panel-body text-center">
                            <div class="display-3">
                                1 284 778
                            </div>
                            <div>
                                platných termínů
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-auto">
                    <div class="panel panel-default-dark">
                        <div class="panel-body text-center">
                            <div class="display-3">
                                0
                            </div>
                            <div>
                                last momentů
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="h4">
                Státy
            </div>
            <div class="mb-3">
                Bulharsko, Egypt, Indie, Izrael, Kuba, Kypr, Maroko, Portugalsko, Řecko, Spojené arabské emiráty, Turecko
            </div>
        </div>
        <div class="list-group-item d-flex">
            <div class="col-lg-3 col-md-6 col-sm-12">
                ID
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>40</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                Vytvořeno
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>Pá 27. března 2009 15:49</strong>
            </div>
        </div>
        <div class="list-group-item d-flex">
            <div class="col-lg-3 col-md-6 col-sm-12">
                IČ
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>52773283</strong>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                Změněno
            </div>
            <div class="col-lg-3 col-md-6 col-sm-12">
                <strong>Pá 27. března 2009 15:49</strong>
            </div>
        </div>
    </div>
</div>

<?php include('parts/footer.html'); ?>