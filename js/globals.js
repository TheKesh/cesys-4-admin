$(function () {
    // FIXME: replace custom
    $.widget("custom.dataTable", {

        options: {
            ajax: null,
            header: null,
            content: null
        },

        // The constructor
        _create: function () {
            var self = this,
                element = self.element,
                options = self.options;

            console.log(element);

            if (options.ajax === null) {
                element.html(self._initFromTable());
            }

            self._rowElements = self._bodyElement.find('.datatable__row');

            self._headerSortElements = self._headerElement.find('.datatable__col').on('click', function(event) {
                self._sortTable(event);
            });
        },

        _initFromTable: function () {
            var self = this,
                element = self.element,
                options = self.options;

            self._tableElement = $('<div />', {
                class: 'datatable'
            });
            self._headerElement = $('<div />', {
                class: 'datatable__header'
            });
            self._bodyElement = $('<div />', {
                class: 'datatable__body'
            });
            self._footerElement = $('<div />', {
                class: 'datatable__footer'
            });

            if (options.header === null) {
                var h = element.find('tr th');

                if (h.length > 0) {
                    var headerRow = $('<div />', {
                        class: 'datatable__row datatable__row--header'
                    });

                    $.each(h, function(k, v) {
                        $('<div />', {
                            class: 'datatable__col',
                            'data-sort': k,
                            'data-sortby': 'asc'
                        }).text($(v).text()).appendTo(headerRow);
                    });

                    self._headerElement.append(headerRow);
                }
            } else {
                // TODO:
            }

            var rows = element.find('tbody tr');

            if (rows.length > 0) {

                $.each(rows, function() {
                    var cols = $(this).find('td');

                    var row = $('<div />', {
                        class: 'datatable__row'
                    });

                    $.each(cols, function(k, v) {
                        $('<div />', {
                            class: 'datatable__col',
                            'data-col': k
                        }).text(
                            $(v).text()
                        ).appendTo(row);
                    });

                    self._bodyElement.append(row);
                });
            }

            self._tableElement.append(self._headerElement.add(self._bodyElement.add(self._footerElement)));

            return self._tableElement;
        },

        _sortTable: function (event) {
            var self = this;

                var n, rows, switching, x, y, shouldSwitch, dir;
                table = self._tableElement;

                n = $(event.currentTarget).data('sort');

                dir = $(event.currentTarget).attr('data-sortby');

                $(event.currentTarget).attr('data-sortby', 
                    $(event.currentTarget).attr('data-sortby') == 'asc' ? 'desc' : 'asc'
                );

                switching = true;
                /* Make a loop that will continue until
                no switching has been done: */
                while (switching) {
                  // Start by saying: no switching is done:
                  switching = false;
                  rows = self._rowElements;
                  /* Loop through all table rows: */
                  $.each (rows, function() {
                    // Start by saying there should be no switching:
                    shouldSwitch = false;
                    /* Get the two elements you want to compare,
                    one from current row and one from the next: */
                    x = $(this).find('div[data-col="' + n + '"]:first');
                    y = $(this).next().find('div[data-col="' + n + '"]:first');
                    /* Check if the two rows should switch place,
                    based on the direction, asc or desc: */
                    if (y.length) {
                        if (dir == "asc") {
                          if (x.html().toLowerCase() > y.html().toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            return false;
                          }
                        } else if (dir == "desc") {
                          if (x.html().toLowerCase() < y.html().toLowerCase()) {
                            // If so, mark as a switch and break the loop:
                            shouldSwitch = true;
                            return false;
                          }
                        }
                    }
                  });
                  
                  if (shouldSwitch) {
                    /* If a switch has been marked, make the switch
                    and mark that a switch has been done: */
                    y.parent().insertBefore(x.parent());
                    switching = true;
                  }
                }
        }
    });
});