<?php

$files = array(
		'./admin/farbtastic.css',
		'./admin/jquery-calendar_admin.css',
		'./admin/jquery.tooltip.css',
		'./admin/jquery-ui-1.10.3.css',			
		'./admin/jquery.multiselect.css',
		'./admin/ui.jqgrid.4.5.2.css',		
		'./shared/colorbox-1.5.14.css',
		'./shared/country-flag.css',
		'./admin/forum.css',
		'./admin/tooltip.css',
		'./admin/api.css',
		'./admin/customer.css',
		'./admin/jquery-autocomplete-custom.css',
		'./admin/mollio_extension.css',
		'./admin/bootstrap-editable.css',
		'./admin/simple-accordion.css',
		'./admin/select2-4.0.3.min.css',
		'./admin/toastr-2.1.3.min.css',

		// sdílené css k ceníku + jeden soubor k dolazení vzhledu pro adminu
		'./shared/occupancy-form.css',
		'./shared/price-list.css',
		'./shared/svgsizes.css',
		'./shared/parameters.css',
		'./shared/searchmask.css',
		'./shared/order-form.css',
		'./admin/local-booking-form.css',

		'./shared/cesys3-icon-font.css',
		'./admin/file-upload.css',				//ajax uploadování souborů
		);

require_once './css.inc.php';
