<?php

switch (@$_GET['v']) {
	case 2:
		//	Nová verze.
		$files = array(
			'./public/main-screen.css',
			'./shared/country-flag.css',
			'./public/parameters-public.css',
			'./shared/parameters.css',
			'./shared/colorbox-1.5.14.css',
			'./public/jquery-ui-1.8.16.custom.css',
			'./shared/jquery-ui.jqgrid-4.4.1.css',
			'./public/jquery-ui-extension.css',
			'./public/superfish.css',
			'./public/superfish-vertical.css',
			'./public/expecta.css',
			'./public/countryTable.css',
			'./public/customer.css',
			'./public/jquery.selectBox.css',
			'./public/dialog-destinations-selector.css',
			'./public/search-result-templates.css',
			'./public/template-components.css',
			'./public/accommodation-date-templates.css',
			'./shared/price-list.css',
			'./shared/svgsizes.css',
			'./shared/occupancy-form.css',
			'./shared/order-form.css',
			'./public/local-booking-form.css',
			'./public/jquery.tooltip.css',
			'./public/swiper.min.css',	
			'./shared/cesys3-icon-font.css',
			'./shared/searchmask.css',
			'./shared/gdpr.css',
			);
		break;
	default:
		//	Původní verze.
		$files = array(
			'./public/main-screen.css',
			'./shared/country-flag.css',
			'./shared/colorbox-1.5.14.css',
			'./shared/svgsizes.css',
			'./public/jquery-ui-1.8.16.custom.css',
			'./shared/jquery-ui.jqgrid-4.4.1.css',
			'./public/jquery-ui-extension.css',
			'./public/superfish.css',
			'./public/superfish-vertical.css',
			'./public/expecta.css',
			'./public/jquery.selectBox.css',
			);
}

require_once './css.inc.php';
