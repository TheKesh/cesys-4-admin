<?php 
	
	/**
	 * Kompilace bootstrapu s kesovanim vystupu
	 * Kompiluje znovu jen pokud doslo ke zmene v nekterem ze vstupnich souboru
	 * @author Vladimír Starec
	 * Dokumentace kompilatoru http://leafo.net/lessphp/docs/
	 * Dokumentace Less http://lesscss.org/#docs
	 */
	
	require_once 'lessc.inc.php'; 

	$inputFile = "admin/bootstrap-less/bootstrap.less";
	$cacheFile = './cache/admin-bootstrap.cache';
	$compiler = new lessc;

	//pokud neni nastaven debug, komprimuje vystup
	$cache = $inputFile;

	//pokud je nastaven clear, smaze cache
	if (isset($_GET['c'])) {
		unlink($cacheFile);
	}

	//kdyz nejsme v debug modu, nastavime komprimovany vystup
	//a nacteme obsah cache = pole s kompilovanym css a timestampy jednotlivych souboru
	if (!isset($_GET['d'])){
		$compiler->setFormatter("compressed");
		//pokusime se o nacteni cache
		if (file_exists($cacheFile)) {
			$cache = unserialize(file_get_contents($cacheFile));
		}
	}
	
	//cachedCompile rekompiluje vystup jen pri zmenach zdrojových souborů
	//na vstupu buď adresu k vstupnimu souboru, nebo cache objekt
	//v obou případech je cache objekt na výstupu
	try {
		$newCache = $compiler->cachedCompile($cache);
	}
	catch (Exception $ex) {
		header('HTTP/1.1 500 Internal Server Error');
		header("Content-Type: text/css");
	    echo "lessphp fatal error: ".$ex->getMessage();
	    exit;
	}

	//nebyl k dispozici cachovany soubor = $cache neni pole
	//nebo je k dispozici ale je expirovany - vracime prekompilovane css
	if (!is_array($cache) || $newCache["updated"] > $cache["updated"]){
		//cache updatujeme jen pokud nejsme v debug rezimu
		if (!isset($_GET['d'])){
			file_put_contents($cacheFile, serialize($newCache));
		}

		//vypocteme etag a lastmodified
		$etag = md5($newCache['compiled']);
		$lastModified = filemtime($cacheFile);

		//predame klientovi spolu s hlavickami
		header('HTTP/1.1 200 OK');
		header("Content-Type: text/css");
		header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
		header("Content-Type: text/css;");
		header("Cache-Control: cache"); // HTTP/1.1
		header("Pragma: cache");		// HTTP/1.0
		header("ETag: $etag");
	    echo $newCache['compiled'];
	}else{
		//jinak pouzijeme cachovanou verzi ze souboru
		// - podle hlavicek od klienta se rozhodujeme, jestli posleme hlavicku Not Modified, nebo plny obsah souboru
		
		//nacteme hlavicky requestu
		$ifNoneMatch = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'].'' : false; 
		$ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;

		//zjistime udaje ze souboru
		$lastModified = filemtime($cacheFile);
		$etag = md5($cache['compiled']);

		//pokud ma klient neexpirovanou verzi souboru, vracime not modified
		if (($etag === $ifNoneMatch) OR ($ifModifiedSince AND strtotime($ifModifiedSince) >= $lastModified)) {
			header('HTTP/1.1 304 Not Modified');
			header("ETag: $etag");
			exit; // stop processing
		}
		
		//ma expirovanou verzi, vratime css z cache
		header('HTTP/1.1 200 OK');
		header("Content-Type: text/css");
		header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
		header("Content-Type: text/css;");
		header("Cache-Control: cache"); // HTTP/1.1
		header("Pragma: cache");		// HTTP/1.0
		header("ETag: $etag");

		echo $cache['compiled'];
	}
