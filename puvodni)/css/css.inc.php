<?php
date_default_timezone_set('Europe/Prague');

function compress($buffer) {
    /* remove comments */
    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
    /* remove tabs, spaces, newlines, etc. */
    $buffer = str_replace(array("\r\n", "\r", "\n", "\t"), '', $buffer);
    $buffer = str_replace(array('    ', '   ', '  '), ' ', $buffer);
    $buffer = str_replace(array('    ', '   ', '  '), ' ', $buffer);
    return $buffer;
}

$version = isset($_GET['v']) ? $_GET['v'] : Null;
$cache = true;
$debug = 0;
if (isset($_GET['d']) AND in_array($_GET['d'], array('1', '2'))){
        $debug = $_GET['d'] + 0;
        $cache = false;
}

if (!isset($files)) $files = array();

$filename = basename($_SERVER["SCRIPT_NAME"], '.php');
$filepath = './cache/' . $filename . '.css' . $version;

$content = '';

if ($debug == 0 AND file_exists($filepath)){
    $content = file_get_contents($filepath);
    $lastModified = filemtime($filepath);
} else {

    foreach ($files as $file){

            if ($debug > 0) $content .= "\n" . '/* file: ' . $file . ' */' . "\n";
           
            if ($debug > 1){
                $content .= file_get_contents($file);
            } else {
                $content .= compress(file_get_contents($file));
            }

    }

    if ($debug == 0 AND strlen($content) > 0 AND (($fp = @fopen($filepath, 'w')) !== false)){ //musi byt povolen zapis do adresare cache, jinak nebude cache zapisovat
		fwrite($fp, $content);
		fclose($fp);
    }
    $lastModified = time();
}

if ($cache){
    $ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
    if ($ifModifiedSince && strtotime($ifModifiedSince) >= $lastModified) {
        header('HTTP/1.1 304 Not Modified');
        exit; // stop processing
    }
    header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
    header("Content-Type: text/css");
    header("Expires: " . gmdate("D, j M Y H:i:s", time() + 60*60*24) . " GMT");
    header("Cache-Control: cache"); // HTTP/1.1
    header("Pragma: cache");        // HTTP/1.0
} else {
    header("Content-Type: text/css");
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 10 Jan 2000 05:00:00 GMT"); // Date in the past
    header('Pragma: no-cache');
}
echo $content;

