(function ($) {
    $.fn.simpleSpinner = function (callback) {
        this.each(function () {
            var el = $(this);

            // add elements
            el.wrap('<span class="spinner"></span>');
            el.before('<span class="sub">&nbsp;</span>');
            el.after('<span class="add">&nbsp;</span>');

            // substract
            el.parent().on('click', '.sub', function () {
                if (el.val() > parseInt(el.attr('min')))
                    el.val(function (i, oldval) {
                        return --oldval;
                    });
            });

            // increment
            el.parent().on('click', '.add', function () {
                if (el.val() < parseInt(el.attr('max')) && el.attr('data-add-disabled') != 'true')
                    el.val(function (i, oldval) {
                        return ++oldval;
                    });
            });


            el.parent().on('click', function () {
                if (typeof callback === 'function')
                    callback();

            });
        });
    };

})(jQuery);
