/*
 * jQuery-Mobile-DateBox 
 * Date: Wed Nov 19 2014 21:05:33 UTC
 * http://dev.jtsage.com/jQM-DateBox/
 * https://github.com/jtsage/jquery-mobile-datebox
 *
 * Copyright 2010, 2014 JTSage. and other contributors
 * Released under the MIT license.
 * https://github.com/jtsage/jquery-mobile-datebox/blob/master/LICENSE.txt
 *
 */
jQuery.extend(jQuery.mobile.datebox.prototype.options.lang, { "sk": {
	"setDateButtonLabel": "Nastaviť dátum",
	"setTimeButtonLabel": "Nastaviť čas",
	"setDurationButtonLabel": "Nastaviť dobu trvania",
	"calTodayButtonLabel": "Teraz",
	"titleDateDialogLabel": "Vyberte dátum",
	"titleTimeDialogLabel": "Vyberte čas",
	"daysOfWeek": [
		"Nedeľa",
		"Pondelok",
		"Utorok",
		"Streda",
		"Štvrtok",
		"Piatok",
		"Sobota"
	],
	"daysOfWeekShort": [
		"Ne",
		"Po",
		"Ut",
		"St",
		"Št",
		"Pi",
		"So"
	],
	"monthsOfYear": [
		"Január",
		"Február",
		"Marec",
		"Apríl",
		"Máj",
		"Jún",
		"Júl",
		"August",
		"September",
		"Október",
		"November",
		"December"
	],
	"monthsOfYearShort": [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"Máj",
		"Jún",
		"Júl",
		"Aug",
		"Sep",
		"Okt",
		"Nov",
		"Dec"
	],
	"durationLabel": [
		"Dni",
		"Hodín",
		"Minút",
		"Sekúnd"
	],
	"durationDays": [
		"Deň",
		"Dni"
	],
	"tooltip": "Otvoriť výber dátumu",
	"nextMonth": "Neskôr",
	"prevMonth": "Skôr",
	"timeFormat": 24,
	"headerFormat": "%A, %B %-d, %Y",
	"dateFieldOrder": [
		"d",
		"m",
		"y"
	],
	"timeFieldOrder": [
		"h",
		"i",
		"a"
	],
	"slideFieldOrder": [
		"y",
		"m",
		"d"
	],
	"dateFormat": "%d.%m.%Y",
	"useArabicIndic": false,
	"isRTL": false,
	"calStartDay": 0,
	"clearButton": "Vymazat",
	"durationOrder": [
		"d",
		"h",
		"i",
		"s"
	],
	"meridiem": [
		"AM",
		"PM"
	],
	"timeOutput": "%l:%M %p",
	"durationFormat": "%Dd %DA, %Dl:%DM:%DS",
	"calDateListLabel": "Ďaľšie termíny",
	"calHeaderFormat": "%B %Y",
	"calTomorrowButtonLabel": "Prejsť na zajtra"
}});
jQuery.extend(jQuery.mobile.datebox.prototype.options, {
	useLang: "sk"
});