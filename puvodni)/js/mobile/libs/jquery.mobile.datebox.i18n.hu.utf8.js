/*
 * jQuery-Mobile-DateBox 
 * Date: Wed Nov 19 2014 21:05:33 UTC
 * http://dev.jtsage.com/jQM-DateBox/
 * https://github.com/jtsage/jquery-mobile-datebox
 *
 * Copyright 2010, 2014 JTSage. and other contributors
 * Released under the MIT license.
 * https://github.com/jtsage/jquery-mobile-datebox/blob/master/LICENSE.txt
 *
 */
jQuery.extend(jQuery.mobile.datebox.prototype.options.lang, { "hu": {
	"setDateButtonLabel": "Dátum választása",
	"setTimeButtonLabel": "Idő választása",
	"setDurationButtonLabel": "Időtartam beállítása",
	"calTodayButtonLabel": "Ugrás a mai napra",
	"titleDateDialogLabel": "Dátum kiválasztása",
	"titleTimeDialogLabel": "Idő kiválasztása",
	"daysOfWeek": [
		"Vasárnap",
		"Hétfő",
		"Kedd",
		"Szerda",
		"Csütörtök",
		"Péntek",
		"Szombat"
	],
	"daysOfWeekShort": [
		"V",
		"H",
		"K",
		"Sze",
		"Cs",
		"P",
		"Szo"
	],
	"monthsOfYear": [
		"Január",
		"Február",
		"Március",
		"Április",
		"Május",
		"Június",
		"Július",
		"Augusztus",
		"Szeptember",
		"Október",
		"November",
		"December"
	],
	"monthsOfYearShort": [
		"Jan.",
		"Febr.",
		"Márc.",
		"Ápr.",
		"Máj.",
		"Jún.",
		"Júl.",
		"Aug.",
		"Szept.",
		"Okt.",
		"Nov.",
		"Dec."
	],
	"durationLabel": [
		"Napok",
		"Óra",
		"Perc",
		"Másodperc"
	],
	"durationDays": [
		"Nap",
		"Napok"
	],
	"tooltip": "Dátumválasztó megnyitása",
	"nextMonth": "Köv. hónap",
	"prevMonth": "Előző hónap",
	"timeFormat": 24,
	"headerFormat": "%A, %B %-d, %Y",
	"dateFieldOrder": [
		"y",
		"m",
		"d"
	],
	"timeFieldOrder": [
		"h",
		"i",
		"a"
	],
	"slideFieldOrder": [
		"y",
		"m",
		"d"
	],
	"dateFormat": "%Y-%m-%d",
	"useArabicIndic": false,
	"isRTL": false,
	"calStartDay": 1,
	"clearButton": "Törlés",
	"durationOrder": [
		"d",
		"h",
		"i",
		"s"
	],
	"meridiem": [
		"de.",
		"du."
	],
	"timeOutput": "%l:%M %p",
	"durationFormat": "%Dd %DA, %Dl:%DM:%DS",
	"calDateListLabel": "Más időpontok",
	"calHeaderFormat": "%B %Y",
	"calTomorrowButtonLabel": "Ugrás a holnap"
}});
jQuery.extend(jQuery.mobile.datebox.prototype.options, {
	useLang: "hu"
});
