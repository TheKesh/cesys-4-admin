/*
 * jQuery-Mobile-DateBox 
 * Date: Wed Nov 19 2014 21:05:33 UTC
 * http://dev.jtsage.com/jQM-DateBox/
 * https://github.com/jtsage/jquery-mobile-datebox
 *
 * Copyright 2010, 2014 JTSage. and other contributors
 * Released under the MIT license.
 * https://github.com/jtsage/jquery-mobile-datebox/blob/master/LICENSE.txt
 *
 */
jQuery.extend(jQuery.mobile.datebox.prototype.options.lang, { "cs": {
	"setDateButtonLabel": "Nastavit datum",
	"setTimeButtonLabel": "Nastavit čas",
	"setDurationButtonLabel": "Nastavit dobu trvání",
	"calTodayButtonLabel": "Nyní",
	"titleDateDialogLabel": "Zvolte datum",
	"titleTimeDialogLabel": "Zvolit čas",
	"daysOfWeek": [
		"Neděle",
		"Pondělí",
		"Úterý",
		"Středa",
		"Čtvrtek",
		"Pátek",
		"Sobota"
	],
	"daysOfWeekShort": [
		"Ne",
		"Po",
		"Út",
		"St",
		"Čt",
		"Pá",
		"So"
	],
	"monthsOfYear": [
		"Leden",
		"Únor",
		"Březen",
		"Duben",
		"Květen",
		"Červen",
		"Červenec",
		"Srpen",
		"Září",
		"Říjen",
		"Listopad",
		"Prosinec"
	],
	"monthsOfYearShort": [
		"Led",
		"Úno",
		"Bře",
		"Dub",
		"Kvě",
		"Čer",
		"Čvc",
		"Srp",
		"Zář",
		"Říj",
		"Lis",
		"Pro"
	],
	"durationLabel": [
		"Dny",
		"Hodin",
		"Minut",
		"Sekundy"
	],
	"durationDays": [
		"Den",
		"Dny"
	],
	"tooltip": "Otevřít výběr data",
	"nextMonth": "Později",
	"prevMonth": "Dříve",
	"timeFormat": 24,
	"headerFormat": "%A, %B %-d, %Y",
	"dateFieldOrder": [
		"d",
		"m",
		"y"
	],
	"timeFieldOrder": [
		"h",
		"i",
		"a"
	],
	"slideFieldOrder": [
		"y",
		"m",
		"d"
	],
	"dateFormat": "%d.%m.%Y",
	"useArabicIndic": false,
	"isRTL": false,
	"calStartDay": 0,
	"clearButton": "Vymazat",
	"durationOrder": [
		"d",
		"h",
		"i",
		"s"
	],
	"meridiem": [
		"AM",
		"PM"
	],
	"timeOutput": "%l:%M %p",
	"durationFormat": "%Dd %DA, %Dl:%DM:%DS",
	"calDateListLabel": "Další termíny",
	"calHeaderFormat": "%B %Y",
	"calTomorrowButtonLabel": "Přejít na zítřek"
}});
jQuery.extend(jQuery.mobile.datebox.prototype.options, {
	useLang: "cs"
});
