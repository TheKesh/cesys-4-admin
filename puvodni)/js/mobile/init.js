/*
 * Zakázání animací a teoreticky delay 300ms po tapnutí
 */
$.mobile.defaultPageTransition = 'none';
$.mobile.defaultDialogTransition = 'none';
$.mobile.buttonMarkup.hoverDelay = 0;


/**
 * Přetížení výchozích hodnot popupu
 */
$.mobile.popup.prototype.options.overlayTheme = 'a';

/**
 * Přetížení výchozích hodnot loaderu
 */
$.mobile.loader.prototype.options.text = cesys.ts('Loading...');
$.mobile.loader.prototype.options.textVisible = true;


/**
 * Spuštění aplikace
 */
$('html').app();
