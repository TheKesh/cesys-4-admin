var cesys = cesys || {};
cesys.mobile = cesys.mobile || {};
cesys.mobile.utils = cesys.mobile.utils || {};

/**
 * formát na cenu v kč (1 000 000 Kč)
 * @param string string ceny
 * @returns string formátovaná cena
 */
cesys.mobile.utils.formatPrice = function(price, currency) {
	//nemáme id měny, nastavíme hlavní
	if (!currency) {
		currency = window.config.currencyId;
	}

	//cenu naformátujeme s přesností na dvě místa
	price = parseFloat(price);
	price = price.toFixed(2);

	//rozdělíme na celou část a desetinnou
	price = price.toString();
	price = price.split('.');
	var wholePart = price[0];
	var decimalPart = price[1];

	//celá část - mezeru mezi tisíce
	wholePart = wholePart.replace(/./g, function(c, i, a) {
        return i > 0 && (a.length - i) % 3 === 0 ? " " + c : c;
    });

	//sestavení výsledné ceny - pokud je měna v seznamu měn s desetinnou přesností, přidáme desetinnou část
    var finalPrice = wholePart;
    if (window.config.currencieWithPrecision.indexOf(currency) !== -1){
    	finalPrice = finalPrice + ',' + decimalPart;
    }

    //vracíme výsledek s měnou
	return finalPrice + ' ' + window.config.currencies[currency];
}

/**
 * Vrátí správně formulované číslo a podstatné jméno pro dny
 * @param duration počet dní
 * @returns string 1 den, 2 dny ...
 */
cesys.mobile.utils.addDayWord = function(duration) {
	switch (parseInt(duration)) {
		case 1:
			duration += ' ' + cesys.ts('den');
			break;
		case 2:
		case 3:
		case 4:
			duration += ' ' + cesys.ts('dny');
			break;
		default:
			duration += ' ' + cesys.ts('dní');
			break;
	}

	return duration;
}

/**
 * Získání hodnoty parametru
 * @param name jméno paramatru který chceme získat
 * @returns string hodnota parametru
 */
cesys.mobile.utils.urlParam = function (name) {
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results == null) {
		return null;
	}
	else {
		return results[1] || 0;
	}
};


/**
 * Vrátí parametry url jako objekt
 * @returns {{}} parametr = hodnota
 */
cesys.mobile.utils.getUrlVars = function () {
	var queries = {};
	$.each(decodeURI(document.location.search.substr(1)).split('&'), function (c, q) {
		var i = q.split('=');
		if (i[0] && i[1]){
			var key = i[0].toString();
			var value = i[1].toString();
			var type = typeof queries[key];

			switch (type) {
				//nový parametr - není v seznamu - přidáme
				case 'undefined':
					queries[key] = value;
					break;
				//parametr už v seznamu je, překopírujeme do pole s novou hodnotou
				case 'string':
					var tmp = [
						queries[key],
						value
					];
					queries[key] = tmp;
					break;
				//parametr už tam jako pole je a má více hodnot, novou hodnotu do pole přidáme
				case 'object': 
					queries[key].push(value);
					break;
			}
		}
	});
	return queries;
};

/**
 * Vyplní element obrázkem dostupnosti a volitelně popiskem
 * @param object element     Objekt s elementem
 * @param string status      Stav z ověřováku   
 * @param bool   appendText  Přidat i text ?   
 */
cesys.mobile.utils.setAvailabilityStatus = function(element, status, appendText) {
	var element = $(element),
		message,
		newClass,
		icon;

	//reset elementu
	element.html('');

	//ikona
	var icon = $('<span>')
		.addClass('ui-btn ui-btn-inline ui-corner-all ui-btn-icon-notext')
	    .css('cursor', 'help');

	switch (status) {
		case 'OK':
			newClass = 'ui-icon-check';
			message = cesys.ts('Volná kapacita');
			break;

		case 'SO':
		case 'NK':
			newClass = 'ui-icon-delete';
			message = cesys.ts('Obsazená kapacita');
			break;

		case 'RQ':
		case 'ER':
			newClass = 'ui-icon-phone';
			message = cesys.ts('Na dotaz');
			break;

		case 'NS':
			newClass = 'ui-icon-phone';
			message = cesys.ts('Can not be verified');
			break;

			/* <?php if (AVAILABILITY_ALLOW_ORDER_SOLD_OUT != 1){ ?>
							case 'NK':
							case 'SO':
								image = '<?php echo $availability->soldOutImage(); ?>';
								title = '<?php __('Obsazená kapacita'); ?>';
								text = '<?php __('OBSAZENO'); ?>';
								break;
						<?php } ?> */
	}
	
	//nastavení výsledného stavu ikoně
	icon.attr('title', message)
	    .addClass(newClass);
	
	//přidání ikony do elementu
	element.append(icon);
	
	//přidat i text ?
	if (appendText) {
		element.append(message);
	}


}


