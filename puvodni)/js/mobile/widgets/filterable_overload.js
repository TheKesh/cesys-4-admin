/**
 * Override filtrovacího widgetu aby filtroval bez diakritiky
 */
;( function ($, window, document, undefined) {
	$.widget("mobile.filterable", $.mobile.filterable, {
		options: {

			filterCallback: function (index, searchValue) {
				var accentDictionary = {
					"à": "a", "â": "a", "é": "e", "è": "e", "ê": "e", "ë": "e",
					"ï": "i", "î": "i", "ô": "o", "ö": "o", "û": "u", "ù": "u",
					"á": "a", "č": "c", "ď": "d", "ě": "e", "í": "i", "ň": "n",
					"ó": "o", "ř": "r", "š": "s", "ť": "t", "ú": "u", "ů": "u",
					"ý": "y", "ž": "z"
				};
				var normalizeDiacritics = function (term) {
					var ret = "";
					for (var i = 0; i < term.length; i++) {
						ret += accentDictionary[term.charAt(i)] || term.charAt(i);
					}
					return ret;
				};

				searchValue = normalizeDiacritics(searchValue);

				//prorovnání regulárem case insensitive
				return !new RegExp(searchValue, 'i').test(normalizeDiacritics(this.textContent.toLowerCase()));
			}

		}
	});
})(jQuery, window, document);