/**
 * Seznam / selectmenu v dialogu
 * - přidá filtrovací textové pole, ouško pro zavření seznamu (jen pokud je to dlouhý seznam v pagecontaineru) a tlačítko pro vymazání volby
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.filterableselectmenu", $.mobile.selectmenu, {
		initSelector: "",

		_create: function () {
			this._super();
			this._dialogInit();
		},

		_afterClose: function () {
			var that = this;

			this._resetSearch();
			
			//pokud je vybráno více platných voleb, přidáme čistící tlačítko
			if (this.element.find(':selected:not([data-placeholder=true])').length > 0) {
				if (this.element.parent().find('.clearIt').length < 1) {
					this.element.parent()
						.append('<a href="#" class="clearIt ui-icon-delete ui-btn-icon-notext"></a>')
						.find('a.clearIt')
						.on('tap', function (e) {
							e.preventDefault();
							that._trigger('listclear');
							that.element.val([]);
							that.refresh(true);
							$(this).remove();
						});
				}
			} else {
				//jinak čistící tlačítko odebereme
				this.element.parent().find('.clearIt').remove();
			}

			this._trigger('afterclose');

		},

		/**
		 * Inicializace widgetu - vytvoření filtrovacího formu a nabindování událostí - musí ošetřit přepnutí pagecontaineru a popupu
		 * https://github.com/jquery/demos.jquerymobile.com/blob/master/1.4.0-beta.1/selectmenu-custom-filter/filter-inside-selectmenu.html
		 */
		_dialogInit: function () {
			var idToFilter = this.element.attr('id');
			var that = this;
			$.mobile.document
				.on("listviewcreate", "#" + idToFilter + "-menu", function (event) {
						var input, list = $(event.target), form = list.jqmData("filter-form");

						if (!form) {
							input = $("<input data-type='search' placeholder='" + cesys.ts('Filter records') + "'>");
							form = $("<form></form>").append(input);

							input.textinput();
							list.before(form).jqmData("filter-form", form);
							form.jqmData("listview", list);
						}


						list.filterable({
							input   : input,
							children: "> li:not(:jqmData(placeholder='true'))"
						});

					})
				.on("pagecontainerbeforeshow", function (event, data) {
						var listview, form, id = data.toPage && data.toPage.attr("id");
						if (!(id === "" + idToFilter + "-dialog")) {
							return;
						}
						listview = data.toPage.find("ul");

						data.toPage.jqmData("listview", listview);

						listview.before(form);
					})

				// Otevření dialogu (akce po skrytí stránky/popupu - podle délky seznamu)
				.on("pagecontainershow", function (event, data) {
						var listview, form, id = data.toPage && data.toPage.attr("id");
						if (!(id === "" + idToFilter + "-dialog" )) {
							return;
						}
						listview = data.toPage.jqmData("listview");
						form = listview.jqmData("filter-form");

						listview.before(form);

						//pokud ještě není ouško na zavření, přidáme ho
						if (!$("#" + id + ' .earWrap').length) {
							$("#" + id + ' .ui-dialog-contain').append(
									  '<div class="earWrap">'
									+ '<a data-rel="back" class="ear ui-btn ui-icon-check ui-btn-icon-notext"></a>'
									+ '</div>'
								);
						}
					})
				.on("popupafteropen", function (event, data) {
						if (event.target.id == (idToFilter + '-listbox')) {
							$('#' + idToFilter + '-listbox-popup')
								.append(
								'<div class="earWrap">'
								+ '<a data-rel="back" class="ear ui-btn ui-icon-check ui-btn-icon-notext"></a>'
								+ '</div>'
							);
						}
					})

				// Skrytí dialogu (akce po skrytí stránky/popupu - podle délky seznamu)
				.on("pagecontainerhide", function (event, data) {
						var id = data.prevPage.attr("id") && data.prevPage.attr("id");
						if (id === "" + idToFilter + "-dialog") {
							that._afterClose();
						}
					})
				.on("popupafterclose", function (event, data) {
						if (event.target.id == (idToFilter + '-listbox')) {
							that._afterClose();
						}
					});


		},

		_resetSearch: function () {
			$('input[data-type="search"]')
				.val("")
				.trigger("keyup");
		}


	});


})(jQuery, window, document);


