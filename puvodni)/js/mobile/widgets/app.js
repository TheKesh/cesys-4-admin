/**
 * Celá aplikace mobilního webu v jednom widgetu
 *  - při přechodu mezi stránkami se inicializují a naplní příslušné widgety
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.app", {

		// nepoužité
		options: {},

		/**
		 * Spuštění aplikace - nabindování přechodů mezi jednotlivými stránkami maska -> výsledky -> detail
		 */
		_create: function () {
			// načítat výsledky vyhledávání ? 
			// - při přechodu z hotelu zpět na nabídky ne, jinak ano
			this._reloadOffers = true; 

			// pomocné proměnné aby se vědělo jestli zobrazit tl. zpět
			// načtený index s maskou ?
			this._indexLoaded = false;
			// načtený seznam nabídek ?
			this._offersLoaded = false; 

			//tlačítko zpět
			var self = this;

			/**
			 * Na custom události nabidnujeme zobrazení / skrytí loaderu - zevnitř widgetů lze ovlivňovat zobrazení
			 * + volitelným parametrem události lze změnit titulek
			 */
			$(document)
				.on("beforedataload", function(event, data) {
					self._showLoader();
				})
				.on("afterdataload", function(event, data) {
					self._hideLoader();
					if (data && data.header) {
						self._setHeader(data.header);
					}
				});

			/**
			 * Inicializace vyhledávací masky
			 */
			$(document).on("pageshow", "#search", function () {
				self._setHeader(cesys.ts('Search'));
				self._handleBackButtonVisibility(self._offersLoaded);

				$('#searchForm').searchmask();
				self._indexLoaded = true;
			});

			/**
			 * Inicializace výsledků vyhledávání
			 */
			$(document).on("pageshow", "#offers", function (event, data) {
				self._setHeader(cesys.ts('Search result'));
				self._handleBackButtonVisibility(self._indexLoaded); 

				// vracíme se z detailu nabídky ?
				var backFromDetail = Boolean(data && data.prevPage && $(data.prevPage).attr('id') === 'offerDetail');

				//byl inicializovaný widget ? - pokud použijeme po reloadu tlačítko zpět v prohlížeči, není widget inicalizován
				var widgetInitialized = Boolean(self.element.data('offers'));

				//načtení jen pokud nejdeme zpátky z detailu a není inicializován widget
				//pokud jdeme zpět na stránku již načtenou, jqm se nějak postará aby se to znovu nenačítalo
				if (!backFromDetail || !widgetInitialized) { 
					$(event.currentTarget).offers();
					self._offersLoaded = true;
				}

				//po zobrazení vždy refresh výšek
				$(event.currentTarget).offers('handleOffersHeight');

			});
		
			/**
			 * Inicializace detailu nabídky - musí být na pageshow, při pageinit ještě není dostupné dateId v adrese
			 */
			$(document).on("pageshow", "#offerDetail", function (event) {
				//zobrazit odkaz na přechod na předchozí stránku pokud jsme nepřišli přímo
				self._handleBackButtonVisibility(self._offersLoaded);

				//id termínu z url parametrů 
				var dateId = cesys.mobile.utils.urlParam('id');
				
				//vytvoření detailu nabídky
				$(event.currentTarget).offerDetail({
					dateId   : dateId
				});

			});

			/**
			 * Klik na odkaz na plnou verzi delegujeme
			 */
			$(document).on('tap', '#displayFullVersion', function(){
				//nastavit cookie
				var d = new Date();
			    d.setTime(d.getTime() + (365*24*60*60*1000)); //365 dni vpred
			    var expires = "expires="+d.toUTCString();
			    var name = 'viewversion';
			    var value = 'full';
			    document.cookie = name + "=" + value + "; " + expires + "; path=/";

			    //presmerovat
			    window.location = '/';
			});

			/**
			 * Otevírání postranního panelu
			 */
			$(document).on('tap', '#navpanelOpener', function(){
				//přepínáme jen panel uvnitř aktuální stránky
				$(this).closest('.ui-page').find(".navpanel").panel('toggle');
			});

		},

		/**
		 * Skrytí tlačítka zpět
		 */
		_hideBackButton : function () {
			$(this.element).find('.back-button').hide();
		},

		/**
		 * Zobrazení tlačítka zpět
		 */
		_showBackButton : function () {
			$(this.element).find('.back-button').show();
		},

		/**
		 * Skrytí / zobrazení tlačítka zpět na základě předané hodnoty
		 */
		_handleBackButtonVisibility : function (value){
			if (value) {
				this._showBackButton();
			}else{
				this._hideBackButton();
			}
		},

		/**
		 * Zobrazení loaderu
		 * - občas se asi bije s automatickými událostmi jqm, proto volání přes setTimeout
		 */
		_showLoader : function () {
			setTimeout(function () {
				$.mobile.loading('show');
			}, 1);
		},

		/**
		 * Skrytí loaderu
		 * - občas se asi bije s automatickými událostmi jqm, proto volání přes setTimeout
		 */
		_hideLoader : function () {
			setTimeout(function () {
				$.mobile.loading('hide');
			}, 1);
		},

		/**
		 * Změna textu nadpisu na liště
		 */
		_setHeader : function (text) {
			//hlavička jquery mobile
			$(this.element).find('#mainHeader h1').html(text);

			//title webu
			var title = window.config.title;
			title = title.replace('%TITULEK%', text);
			$('title').html(title);
		}


	});
})(jQuery, window, document);

