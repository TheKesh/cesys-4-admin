/**
 * Widget na výběr lokací
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.locationselect", $.mobile.filterableselectmenu, {

		options: {
			//seznam lokací
			locations: []
		},

		/**
		 * Konstruktor
		 */
		_create: function () {
			var that = this;
			this._super();

			// při vlastní události contentchange/změna obsahu selektu sám plugin ošetří vlastní viditelnost
			$.mobile.document.on("locationselectcontentchanged", function () {
				that._switchParentVisibility();
			});
		},

		/**
		 * Naplnění selectmenu lokacemi
		 * @param  array forDestinations seznam id destinací ke kterým vyplňujeme lokace
		 */
		fill: function (forDestinations) {
			var self = this;
			var firstOption = '';
			var options = [];

			if (this.options.doesnotmattertext) {
				firstOption = '<option value="0" selected>' + this.options.doesnotmattertext + '</option>';
			}else if (this.options.placeholdertext) {
				firstOption = '<option data-placeholder="true" value="0" >' + this.options.placeholdertext + '</option>';
			}

			//pro vsechny vybrané destiance pridám i lokace
			if (Array.isArray(forDestinations) && forDestinations.length > 0) {
				var options = [];
				$.each(self.options.locations, function (key, value) {
					if ($.inArray(value.parent_id, forDestinations) > -1) {
						options.push("<option value='" + value.id + "'>" + value.name + "</option>");
					}
				});
			}

			this.element.html(options.join(''));

			//placeholder / nullOption
			this.element.prepend(firstOption);

			this.refresh(true);
			this._trigger('contentchanged');
		}

	});
})(jQuery, window, document);