/**
 * Widget pro view nabídek, použití - zavolat nad kořenovým divem view nabídek a předat data z ajaxu
 * 
 * $('#offers').offers({
 * 	data : data
 * });
 *
 * - widget vyplní nabídky, zajistí zobrzaení listovacích tlačítek a vyplní dostupnost
 * 
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.offers", {

		options: {
			//layout sloupců - potřeba pro označení bloku {@link http://demos.jquerymobile.com/1.4.1/grids/}
			colLayout : [
				'a',
				'b',
				'c',
				'd'
			],

			//text když nejsou nabídky
			noOffersText : cesys.ts('No offers were found, try to change your search criteria')
		},

		/**
		 * Konstruktor
		 */
		_create: function () {
			var self = this;
			self._offersWrapper = self.element.find('.ui-responsive');
			self._buttonsWrapper = self.element.find('.buttons');

			//privátní proměnná, do ketré se ajaxem načtou data nabídek
			self._data = null;

			//naplnění přes callback fce loadOffers
			$(self).on('dataLoaded', function(){
				//vyplnit nabídky
				self._fill();

				//nabidnovat přechod na detail na klik
				self._bindClickHandlers();

				//tlačítka prev/next
				self._handlePrevNext();

				//nakonec vyplníme dostupnost
				self._fillAvailability();

			});

			self._loadOffers();

			//na resize přepočítáme výšku nabídek - ve speciálním eventu jquery mobile
			$(window).on('throttledresize', function(){
				$(self.element).offers('handleOffersHeight');
			});
		},

		/**
		 * Z předaných dat vyplní nabídky + srovná velikost na řádku
		 */
		_fill: function (){
			var colLayout = this.options.colLayout;
			var data = this._data;
			var self = this;

			if (!data || !data.offers || !data.offers.length) {
				self._offersWrapper.html(
					$('<div>', {
						text : self.options.noOffersText,
						'class' : 'nothing-found'
					})
				);
			}else{
				$.each(data.offers, function (key, value) {
					var subContent = [],
						price_min = cesys.mobile.utils.formatPrice(value.price_min.value, value.price_min.currency),
						name = value.name,
						country = value.country,
						rating = value.rating,
						dateId = value.date_id,
						dateHash = value.hash,
						pic = value.image;

					self._offersWrapper.append(
						'<div class="offer ui-block-' + colLayout[key % colLayout.length] + '" data-id="' + dateId + '" data-hash="' + dateHash + '">' + 
							'<div class="img">' +
								'<img data-id="' + dateId + '" src="' + pic + '">' +
							'</div>' + 
							'<h3>' + name + '</h3>' + 
							'<strong>' + country + '</strong>' + 
							'<span class="price">' + price_min + '<span class="price-from">' + cesys.ts('Price from') + ':</span></span>' + 
							'<a href="#" data-id="' + dateId + '" class="detail-link ui-link ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-info" href="#">' + cesys.ts('Offer detail') + '</a>' + 
						'</div>'
					);
				});
			}

			self._sameOffersHeight();
		},

		/**
		 * Nabinduje všechny click handlery
		 *  - odkazy na detail hotelu
		 *  - stránkovací odkazy
		 */
		_bindClickHandlers : function (){
			/**
			 * Po kliknutí na obrázek nebo odkaz na ubytování (podle data-id)
			 */
			this._offersWrapper.find('.offer a, .offer .img img').click(function (event) {
				var id = event.target.attributes['data-id'].value,
					params = cesys.mobile.utils.getUrlVars();
				
				params.id = id;

				//do odkazu i vyhledávací parametry
				$.mobile.pageContainer.pagecontainer('change', '/mobile/offer_detail' + document.location.search, {
					data : params
				});
			});
		},


		/**
		 * Nastaví listovací tlačítka - upraví adresy url a zobrazí / skryje tlačítka dle výsledků
		 */
		_handlePrevNext : function() {

			//tlačítka další / předchozí
			var prevBtn = this._buttonsWrapper.find('.prevPage').closest('.ui-btn');
			var nextBtn = this._buttonsWrapper.find('.nextPage').closest('.ui-btn');

			//obě skryjeme
			prevBtn.hide();
			nextBtn.hide();

			var currentPage = this._data.page,
				pageCount = this._data.pageCount,
				searchUrlParams = cesys.mobile.utils.getUrlVars();
				
			if (currentPage > 1) {
				searchUrlParams.page = currentPage - 1;
				prevBtn.attr('href', '/mobile/offers/?' + $.param(searchUrlParams)).show();
			}

			if (currentPage < pageCount) {
				searchUrlParams.page = currentPage + 1;
				nextBtn.attr('href', '/mobile/offers/?' + $.param(searchUrlParams)).show();
			}
		},

		/**
		 * Srovná velikost nabídek na řádku
		 */
		_sameOffersHeight : function () {
			var currentTallest = 0,
				currentRowStart = 0,
				rowDivs = [],
				currentOfferElement,
				topPosition = 0;

			this._offersWrapper.find('.offer').each(function () {
				currentOfferElement = $(this).height('auto'); //reset nastavené výšky
				topPosition = currentOfferElement.position().top;

				if (currentRowStart != topPosition) {

					// we just came to a new row.  Set all the heights on the completed row
					for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
						rowDivs[currentDiv].height(currentTallest);
					}

					// set the variables for the new row
					rowDivs.length = 0; // empty the array
					currentRowStart = topPosition;
					currentTallest = currentOfferElement.height();
					rowDivs.push(currentOfferElement);

				} else {

					// another div on the current row.  Add it to the list and check if it's taller
					rowDivs.push(currentOfferElement);
					currentTallest = (currentTallest < currentOfferElement.height()) ? (currentOfferElement.height()) : (currentTallest);

				}

				// do the last row
				for (var currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}

			})
		},

		/**
		 * Ajax pro načtení dat nabídek - po dokončení uloží výsledek do proměnné pluginu a informuje o dokončení událostí 'dataLoaded'
		 */
		_loadOffers : function () {
			var self = this;

			//informujeme aplikaci o spuštění nového ajaxu
			$(document).trigger('beforedataload');

			//načtení nabídky- data uložíme do privátní proměnné
			var dataString = window.location.search.substring(1) + '&init=true';
			$.ajax({
				type    : "GET",
				url     : "/mobile/ajax_offers",
				data    : dataString,
				dataType: "json",
				success : function (data) {
					//data do proměnné pluginu
					self._data = data;
					$(self).trigger('dataLoaded');
				},
				error   : function (result, data) {
					console.log('Chyba při stahování nabídek');
				},
				complete : function () {
					//informujeme aplikaci o dokončení načítání
					$(document).trigger('afterdataload');
				}
			});
		},

		/**
		 * Vyplnění dostupností k temrínům
		 */
		_fillAvailability : function () {
			this._offersWrapper.find('.offer').each(function () {
				var id = $(this).attr('data-id');
				var hash = $(this).attr('data-hash');
				var thisSpan = $(this).find('.ui-link');

				if (!hash || !hash.length) {
					return;
				}
				
				$.post(
					"/online/availability/cesys/check_date.php",
					{
						//lang : 'cs',
						cid : id,
						hash : hash
					}
				)
				.done(function (data) {
					var status = ($.parseJSON(data).status);
					if (status == 'OK') {
						setAvailabilityContent(thisSpan, status, false, true);
					}
				});
			});
		},

		/**
		 * Přepočítání velikosti nabídek například při resizu okna
		 */
		handleOffersHeight : function(){
			return this._sameOffersHeight();
		},

	});
})(jQuery, window, document);