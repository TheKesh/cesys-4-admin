/**
 * Widget na vylistování dostupných termínů v detailu nabídky
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.availabledatesselect", $.mobile.selectmenu, {

		/**
		 * Naplnění selectu
		 * @param  array of objects dates Seznam termínů
		 * @param  int              total Celkový počet termínů
		 */
		fill: function (dates, total) {
			var self = this;

			self.element.empty();

			//naplnění termíny
			$.each(dates, function (key, date) {
				var optionText = (new XDate(date.date_from).toString("dd.MM.yyyy")) + ' – ' +	(new XDate(date.date_to).toString("dd.MM.yyyy"));
				optionText = optionText + ' (' + cesys.mobile.utils.formatPrice(date.price) + ')';
				self.element.append(
					$('<option>', {
						value : date.id,
						html  : optionText,
						'data-date-from' : date.date_from,
						'data-date-to' : date.date_to
					})
				);
			});

			//pokud je víc termínů
			if (dates.length < total) {
				var lastOptionText = cesys.ts('...and %d other dates');
				lastOptionText = lastOptionText.replace('%d', total - dates.length);

				self.element.append(
					$('<option>', {
						value : 0,
						disabled : 'disabled',
						text  : lastOptionText
					})
				);
			}
		}


	});
})(jQuery, window, document);