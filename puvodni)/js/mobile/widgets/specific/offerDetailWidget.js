/**
 * Widget pro detail nabídky
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.offerDetail", {

		options: {
			//parametr s id termínu
			dateId : null,

			//hláška nejsou-li termíny
			noDatesMessage : cesys.ts('No other dates were found')
		},

		/**
		 * Konstruktor
		 */
		_create: function () {
			var self = this,
				currentView = $(this.element);

			//widgety na seznam termínů a booking form
			self._availableDates = currentView.find('#availableDates').availabledatesselect(),
			self._bookingForm = currentView.find('#bookingForm').bookingform();

			//privátní proměnné s údaji nabídky
			self._initialized = false;
			self._pageContent = currentView.find('.ui-content');
			self._hotelName = currentView.find('.hotelName');
			self._destination = currentView.find('.destination');
			self._slider = currentView.find('.swiper-wrapper');
			self._fromTo = currentView.find('.offer-info .fromTo');
			self._boarding = currentView.find('.offer-info .boarding');
			self._transport = currentView.find('.offer-info .transport');
			self._price = currentView.find('.offer-info .price');
			self._descriptions = currentView.find('.offer-info .content');
			self._rating = currentView.find('.offer-info .stars');
			self._availability = currentView.find('.offer-info .availability');

			//data nabídky - načtou se ajaxem
			self._data = null;

			//naplnění přes callback fce loadOffer
			$(self).on('dataLoaded', function(){
					//máme data - naplníme detail
					if (self._data) {
						if (!self._initialized){
							self._fillMainInfo();
						}
						self._fillDateInfo();
					}else{
						//404 - chyba při načítání
						self._pageContent.children().hide();
						self._pageContent.append($('<h3>', {
							'class' : 'notFound',
							'text' : cesys.ts('Not found')
						}));
					}
				});

			self._loadOffer();
		},

		/**
		 * Část co se musí načíst po změně termínu
		 */
		_fillDateInfo : function() {
			var self = this,
			    offerDetail  = this._data,
			    dateFrom     = offerDetail.date_from,
			    dateTo       = offerDetail.date_to,
			    id           = offerDetail.id,
			    hash         = offerDetail.hash,
			    duration     = offerDetail.duration,
			    priceFrom    = offerDetail.price.value,
			    boarding     = offerDetail.boarding,
			    transport    = offerDetail.transport,
			    tourOperator = offerDetail.tour_operator_id,
			    descriptions = offerDetail.descriptions;

			//vyplnění ceníku
			if (offerDetail.erased) {
				self._bookingForm.hide();
				self._showSoldOut();
			}else{
				self._removeSoldOut();
				self._bookingForm.show();
				self._bookingForm.bookingform('option', 'offerDetail', offerDetail);
				self._bookingForm.bookingform('fill', offerDetail.prices);
			}


			//ověření dostupnosti ?
			if (hash) {
				self._availability.closest('tr').show();
				window.cesys.service.availability.doCheckRoom(
					{
						'cid'  : id,
						'hash' : hash,
						'mode' : 'rooms'
					},
					function(status, json) {
						cesys.mobile.utils.setAvailabilityStatus(self._availability, status, true);
						if (json && json.rooms && json.rooms.length) {
							self._bookingForm.bookingform('fillRoomAvailabilities', json.rooms);
						}
					}
				);
			}else{
				self._availability.closest('tr').hide();
			}

			//transport + odlet/odjezd
			if (offerDetail.departures) {
				var departures = offerDetail.departures;
				$.each(departures, function (key, value) {
					transport += key == 0 ? ' - ' + value : ', ' + value;
				});
			} else if (offerDetail.airport) {
				transport += ' - ' + offerDetail.airport;
			}
			this._transport.html(transport);

			//naplnění popisků
			self._fillDescriptions();
			
			//datumy
			if (offerDetail.erased) {
				this._fromTo.html(cesys.ts('Sold out'));
			} else {
				var from = new XDate(dateFrom).toString("dd.MM.yyyy");
				var to = new XDate(dateTo).toString("dd.MM.yyyy");
				this._fromTo.html(from + ' - ' + to + ' (' + cesys.mobile.utils.addDayWord(duration) + ')');
			}

			//strava
			this._boarding.html(boarding);

			//cena za termíny
			this._price.html(cesys.mobile.utils.formatPrice(priceFrom));
			
		
		},


		/**
		 * Hlavní info - vyplní se jen jednou po načtení detailu - poté již jen obnova údajů závislých na termínu
		 */
		_fillMainInfo : function(){
			var offerDetail = this._data,
				name        = offerDetail.accommodation,
				country     = offerDetail.country,
				destination = offerDetail.destination,
				rating      = offerDetail.rating,
				images      = offerDetail.images,
				self        = this;


			//select s dostupnými termíny + bind akce při změně termínu
			if (offerDetail.dates_count <= 1) {
				var message = $('<strong>', {
					id : 'noDatesMessage',
					text : self.options.noDatesMessage
				});

				self._availableDates.availabledatesselect('destroy');
				self._availableDates.hide();
				message.insertBefore(self._availableDates);
			}else{
				self._availableDates
					.availabledatesselect('fill', offerDetail.dates, offerDetail.dates_count)
					.val(offerDetail.id)
					.availabledatesselect('refresh', true)
					.availabledatesselect('enable')
					.on('change', function () {
						self.options.dateId = $(this).val();
						self._loadOffer();
					});
			}

			//vyplňování stránky daty
			this._hotelName.html(name);
			this._destination.html(country + ' - ' + destination);

			$.each(images, function (key, image) {
				var slide = $('<div>', {
					'class' : 'swiper-slide'
				});
				var img = $('<img>', {
					src : image.large
				});
				slide.append(img);
				self._slider.append(slide);
			});

			//slider jen pro více obrázků
			if (images && images.length && images.length > 1) {
				self._slider.parent().parent().addClass('init');
				self._slider.parent().swiper({
					pagination : '.swiper-pagination',
					autoplay : 6000,
					loop : true,
					paginationClickable : true
				});
			}

	
			
			//rating zobrazíme jen nenulový
			if (rating > 0) {
				this._rating.closest('tr').css('display', 'table-row');
				this._rating.raty({
					path    : window.config.fileServer+'/img/mobile/libs/raty/',
					score   : rating,
					readOnly: true
				});
			}else{
				this._rating.closest('tr').hide();
			}

			this._initialized = true;
		},


		/**
		 * Vloží popisky a inicializuje nad nimi widget collapsible
		 */
		_fillDescriptions : function() {
			var self = this,
			    offerDetail  = this._data,
			    descriptions = offerDetail.descriptions,
			    mainDescriptions = '',
			    otherDescriptions = [];

			//popisky - do accordionu, popisky bez validního názvu bloku umístíme do společného lbo "ostatní"
			$.each(descriptions, function (key, description) {
				if (description.text) {
					//obsah umístíme do dočasného kontejneru
					var tmp = $('<div>', {
						html : description.text
					});
					//pokud má popisek po odstranění html značek délku alesoň 5 znaků, umístíme ho do popisků (snaha o odstranění zmetků v popiscích)
					if (tmp.text().length > 5) {
						//popisky s názvek do vlastního bloku, popisky bez názvu do společného pole "Ostatní"
						if (description.name) {
							mainDescriptions += '<div data-role="collapsible"' + (key == 0 ? 'data-collapsed="false">' : '>') +
											'<h2>' + description.name + '</h2>' +
											'<p>' + description.text.split('\n').join("<br />") + '</p>' +
										'</div>';
						}else{
							otherDescriptions.push(description.text.split('\n').join("<br />"));
						}
					}
				}
			});

			//pokud něco zbylo do bloku "ostatní", přidáme tento blok
			if (otherDescriptions.length) {
				var otherContentElement = $('<div>', {
					html : otherDescriptions.join('<br />')
				});
				//pokud je obsah bez hmtl značek delší než pět znaků - umístíme ho do popisků (snaha o odstranění zmetků v popiscích)
				if (otherContentElement.text().length > 5) {
					mainDescriptions += '<div data-role="collapsible">' +
										'<h2>' + cesys.ts('Other') + '</h2>' +
										'<p>' + otherContentElement.html() + '</p>' +
									'</div>';
				}
			}

			//vložíme a inicializujeme
			this._descriptions.html('<div data-role="collapsible-set">' + mainDescriptions +	'</div>');
			this._descriptions.find('[data-role=collapsible]').collapsible();
		},


		/**
		 * Ajax pro načtení dat nabídky - po dokončení uloží výsledek do proměnné pluginu a informuje o dokončení událostí 'dataLoaded'
		 */
		_loadOffer : function (){
			//informujeme aplikaci o spuštění nového ajaxu
			$(document).trigger('beforedataload');

			//načtení nabídky- data uložíme do privátní proměnné
			var self = this;
			$.ajax({
				url  : '/mobile/ajax_offer_detail' + document.location.search, 
				type : 'POST',
				data : {
					id : self.options.dateId,
					init : !self._initialized
				},
				dataType: 'json',
				success : function (result) {
					//data do proměnné pluginu
					self._data = result;
					$(self).trigger('dataLoaded');

					//změna historie
					var urlParams = cesys.mobile.utils.getUrlVars();
					urlParams.id = self.options.dateId;
				 	history.replaceState(history.state, null, "/mobile/offer_detail?" + $.param(urlParams));
				},
				error : function () {
					console.log("Chyba stahování dat termínu");
					$(self).trigger('dataLoaded');
				},
				complete : function () {
					//informujeme aplikaci o dokončení načítání - druhým parametrem pošleme text nového titulku
					var data = null;
					if (self._data && self._data.accommodation) {
						data =  {
							header : self._data.accommodation
						};
					}
					$(document).trigger('afterdataload', data);
				}
			});
		},

		/**
		 * Zobrazí zprávu o vyprodanosti zájezdu
		 */
		_showSoldOut : function() {
			if (!this._pageContent.find('#soldOut').length) {
				this._bookingForm.before($('<strong>', {
					id : 'soldOut',
					html : cesys.ts('The date was sold out, order form not available.')
				}));
			}
		},

		/**
		 * Skryje zpravu o vyprodanosti zájezdu
		 */
		_removeSoldOut : function() {
			this._pageContent.find('#soldOut').remove();
		}




	});
})(jQuery, window, document);