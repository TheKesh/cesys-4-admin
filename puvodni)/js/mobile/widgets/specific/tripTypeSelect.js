/**
 * WIdget pro výběr typu zájezdu - ošetřuje stromové zobrazení dle hodnoty id
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.triptypeselect", $.mobile.filterableselectmenu, {


		fill: function (tripTypes) {
			//placeholder nebo volba nerozhoduje ? 
			var firstoption = '';
			if (this.options.doesnotmattertext) {
				firstoption = '<option value="" selected>' + this.options.doesnotmattertext + '</option>';
			}else if (this.options.placeholdertext) {
				firstoption = '<option data-placeholder="true" value="null" >' + this.options.placeholdertext + '</option>';
			}

			//seznam options pro select
			var tripTypeOptions = '';
			$.each(tripTypes, function (key, tripType) {
				//ty, které mají parent odsadíme pomlčkou
				if (tripType.parent_id != 0) { 
					tripType.name = '&#151; ' + tripType.name;
				}
				tripTypeOptions += "<option value='" + tripType.id + "'>" + tripType.name + "</option>\n";
			});

			this.element.html(firstoption + tripTypeOptions);
			this.refresh();
		}


	});
})(jQuery, window, document);