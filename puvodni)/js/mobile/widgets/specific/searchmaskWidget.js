/**
 * Widget pro view nabídek, použití - zavolat nad kořenovým divem view nabídek a předat data z ajaxu
 * 
 * $('#offers').offers({
 * 	data : data
 * });
 *
 * - widget vyplní nabídky, zajistí zobrzaení listovacích tlačítek a vyplní dostupnost
 * 
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.searchmask", {

		_create: function () {
			this._searchForm = $(this.element);
			this._countrySelect        = this._searchForm.find('#country-select'),
			this._destinationSelect    = this._searchForm.find('#destination-select'),
			this._locationSelect       = this._searchForm.find('#location-select'),
			this._boardingTypesSelect  = this._searchForm.find('#dietType'),
			this._flyFromSelect        = this._searchForm.find('#flyFrom'),
			this._driveFromSelect      = this._searchForm.find('#rideFrom'),
			this._transportationSelect = this._searchForm.find('#transportationSelect'),
			this._tripTypeSelect       = this._searchForm.find('#tripType');
			this._dateFrom             = this._searchForm.find('#dateFrom');
			this._dateTo               = this._searchForm.find('#dateTo');

			this._createSearchmask();
			this._fillSearchmask();
		},

		/**
		 * Inicializace pluginů + pospojování callbacků
		 */
		_createSearchmask : function (){
			var self = this;     

			//datepickery - po zavření datumu od, nastavíme minimální datum do a otevřeme datepicker
			self._dateFrom.add(self._dateTo).datebox();
			self._dateFrom.datebox({
				'closeCallback' : function(date){
					//vyčteme datum a zvýšíme ho jeden den
					minDate = date.date;
					minDate.adj(2,1);
					minDate = this.callFormat('%Y-%m-%d', date.date);

					//nastavíme ho druhému datepickeru jako minumum a otevřeme ho
					self._dateTo.attr('min', minDate);
					self._dateTo.datebox('applyMinMax');
					self._dateTo.datebox('open');
				}
			})


			//inicializace země + destinace + lokace
			self._countrySelect.countryselect({
				'disabled' : true,
				'preferredText' : cesys.ts('Nejžádanější země:'),
				'allText' : cesys.ts('Abecední seznam:')
			});

			self._destinationSelect.destinationselect({
				'disabled' : true
			});

			self._locationSelect.locationselect({
				'disabled' : true
			});

			//na události reagují ostatní widgety
			self._countrySelect
				.on('countryselectafterclose', function () {
					selectedCountries = $(this).val();
					self._destinationSelect.destinationselect('fill', selectedCountries);

					selectedDestinations = self._destinationSelect.destinationselect('selectedDestinations');
					self._locationSelect.locationselect('fill', selectedDestinations);
				})
				.on('countryselectlistclear', function () {
					self._destinationSelect.destinationselect('clear');
					self._locationSelect.locationselect('clear');
				});

			//widget s výběrem destinace
			//na události reagují ostatní widgety
			self._destinationSelect
				.on('destinationselectafterclose', function () {
					//vybrané destinace získáme vlastní fcí, která vrací celý vybraný strom
					selectedDestinations = $(this).destinationselect('selectedDestinations');
					self._locationSelect.locationselect('fill', selectedDestinations);

				})
				.on('destinationselectlistclear', function () {
					self._locationSelect.locationselect('clear');
				});

			//widget s výběrem dopravy - načteme disablovaný, po načtení dat ajaxem povolíme
			//na události reagují ostatní widgety
			self._transportationSelect
				.selectmenu({
					'disabled' : true
				})
				.on('change', function () {
					self._searchForm.find('#flyFromWrap, #rideFromFromWrap')
						.addClass('hiddenContain');
					self._flyFromSelect.val(0).selectmenu('refresh');
					self._driveFromSelect.val(0).filterableselectmenu('refresh');
					switch ($(this).val()) {
						//Letecky
						case '1' :
							self._searchForm.find('#flyFromWrap').removeClass('hiddenContain');
							break;
						//Busem
						case '2' :
							self._searchForm.find('#rideFromFromWrap').removeClass('hiddenContain');
							break
					}
				});

			//ostatní specifické widgety
			self._tripTypeSelect.triptypeselect({
				'disabled' : true
			});

			//ostatní generické widgety
			self._flyFromSelect.selectmenu({
				'disabled' : true
			});
			self._driveFromSelect.filterableselectmenu({
				'disabled' : true
			});
			self._boardingTypesSelect.selectmenu({
				'disabled' : true
			});

			//zamezení výběru fm/lm zároveň
			self._searchForm.find('#firstMinuteOnly').on('change', function () {
				$('#lastMinuteOnly')
					.val('')
					.slider("refresh");
			});
			self._searchForm.find('#lastMinuteOnly').on('change', function () {
				$('#firstMinuteOnly')
					.val('')
					.slider("refresh");
			});


			/**
			 * Odeslání formuláře
			 */
			self._searchForm.on('submit', function (e) {
				e.preventDefault();

				// serializace parametrů formuláře - prázdné vyhodíme (není úplně nutné, ale url je přehlednější)
				var dataString = (self._searchForm.serialize().replace(/[^&]+=(&|$)/g, '')); 

				// pokud jsou vybrány lokace, vypíše se jejich id destinace a vymažou se destinace
				if (dataString.indexOf('&location') > -1) {
					dataString = dataString.replace(/di.+?&/g, '');
					dataString = dataString.replace(/&location/g, '&di');
				}

				$.mobile.pageContainer.pagecontainer('change', '/mobile/offers', {
					data  : dataString
				});
			});
		},


		/**
		 * Načtení číselníků a vyplnění formuláře
		 */
		_fillSearchmask : function(){
			var self = this;
			
			$(document).trigger('beforedataload');
			
			$.ajax({
				url     : '/mobile/ajax_static_data',
				type    : 'POST',
				dataType: 'json',
				success : function (result) {
					self._destinationSelect
						.destinationselect('option', 'destinations', result['Destination'])
						.destinationselect('enable');

					self._locationSelect
						.locationselect('option', 'locations', result['Location'])
						.locationselect('enable');

					self._boardingTypesSelect
						.selectmenu('fill', result['Boarding'])
						.selectmenu('enable');

					self._flyFromSelect
						.selectmenu('fill', result['Airport'])
						.selectmenu('enable');

					self._driveFromSelect
						.filterableselectmenu('fill', result['Departure'])
						.filterableselectmenu('enable');

					self._transportationSelect
						.selectmenu('fill', result['Transport'])
						.selectmenu('enable');

					self._countrySelect
						.countryselect('option', 'preferred', result['CountryPreferred'])
						.countryselect('fill', result['Country'])
						.countryselect('enable');

					self._tripTypeSelect
						.triptypeselect('fill', result['TripType'])
						.triptypeselect('enable');

				},
				error   : function (result, data) {
					console.log("Chyba při plnění vyhledávací masky");
					console.log(result);
					console.log(data);
				},
				complete : function () {
					$(document).trigger('afterdataload');
				}
			});
		}


	});

})(jQuery, window, document);;