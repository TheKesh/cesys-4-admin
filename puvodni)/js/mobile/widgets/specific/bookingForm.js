/**
 * Widget obsluhující objednávkový formulář
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.bookingform", $.mobile.widget, {

		options: {
			//text pro koncovou cenu
			finalPriceText : cesys.ts('Price:') + ' %d',

			//detail nabídky
			offerDetail : null
		},

		/**
		 * Konstruktor
		 */
		_create: function () {
			var self = this;

			//lokální proměnné -------------------------------
			//wrapper okolo výběru pokoje
			self._roomWrapper = self.element.find('#room');

			//selekt výběru pokoje
			self._roomSelect = self.element.find('#roomSelect');

			//element pro zobrazení názvu pokoje, pokud je jen jeden
			self._oneRoom = self.element.find('#oneRoom');

			//kam se vkládají ceny pro konkrétní pokoj
			self._roomPrices = self.element.find('#roomPrices');

			//dostupnost pokoje
			self._roomAvailability = self.element.find('#roomAvailability');

			//kam se vkládají ceny nezávislé na pokoji
			self._noroomPrices = self.element.find('#noroomPrices');

			//kam se vkládá vypočtená cena
			self._finalPrice = self.element.find('#finalPrice');

			//element kam se vloží výsledný stav po odeslán objednávky
			self._orderStatus = self.element.find('#orderResult');

			//pole s dostupností pokojů - nastaví se zvenčí
			self._roomAvailabilities = null;

			//aktuální ceník - nastaví se zvenčí
			self._prices = null;
			// end / lokální proměnné -------------------------------
			
			//na výběr čísla spinner plugin
			self.element.find('input[type=text].number').simpleSpinner();

			//Odeslání formuláře ajaxem
			self.element.on('submit', function (event) {
				event.preventDefault();
				self._submitForm();
			});


			//kliknutí na odkaz v checkboxovém wrapperu otevře odkaz v popupu - obsah nahraje ajaxem
			// např. odkaz na obchodní podmínky
			self.element
				.find('.ui-checkbox a, .lightbox')
				.bind("tap", function( event, data ){
					event.preventDefault();
				   	event.stopPropagation();

				   	//odkaz na soubor do noveho okna, jinak do popupu
					if ($(this).attr('href').match(/\.(pdf|doc|docx|odt|ods|jpg|jpeg|png)$/i)) {
						window.open($(this).attr('href'));
					}else{
						var wrapper = $('<div>', {
							'class' : 'ui-content ui-body-a'
						});
						var closeBtn = $('<a href="#" data-rel="back" data-role="button" data-theme="b" data-icon="delete" data-iconpos="notext" class="ui-btn-right">Close</a>');
						var url = $(this).attr('href');

						// odkaz na personal_info - přidáme id operatora
						if ( $(this).hasClass('personal-information-link')) {
							url = url + '/' + self.options.offerDetail.tour_operator_id;
						}
						//načteme obsah a zobrazíme v popupu
						$(document).trigger('beforedataload');
						wrapper.load(url, function(){
							$(document).trigger('afterdataload');

							// pozice k oknu, okraje 60px, na zavření se smaže
							$(wrapper)
								.prepend(closeBtn)
								.popup({
									'positionTo' : 'window',
									'tolerance'  : "60"
								})
								.on('popupafterclose', function() {
							        $(this).remove();
							    })
							    .popup('open')
							    .trigger("create");
							//pokud je v systémové stránce odkaz načte se do tohoto okna místo nové stránky (kvůli správné funkci poučení správce)
							$('.systemPage a:not(.ui-btn)').on('tap', function(e) {
								e.preventDefault();
								$.ajax({
									url: $(this).attr('href'),
									method: 'GET'
								  }).done(function(data) {
									$('.main-column').html(data);
								  });
							});
						});
					}

				});
		},

		/**
		 * Naplnení ceníku pro konkrétní date
		 * @param  array of objects prices Seznam cen rozdělených na 'rooms' - pokoje s cenami a 'noroom' - ceny bez pokojů
		 */
		_fill: function (prices) {
			var self = this,
				rooms = $.makeArray(prices.rooms),
				norooms = $.makeArray(prices.noroom);


			self._prices = prices;

			//vyčistíme ceník
			self._roomSelect.empty(); //selekt s pokoji
			self._oneRoom.hide();
			self._roomWrapper.hide(); //wrapper s pokoji sryjeme
			self._noroomPrices.hide(); //ostatní ceny skryjeme

			//máme pokoje - vyplníme ceník s pokoji
			if (rooms && rooms.length) {
				//pokud je pokoj jen jeden, zobrazíme jeho název mimo select, select skryjeme 
				if (rooms.length === 1) {
					//musíme odstranit selectmenu widget pokud uz byl inicializovan
					if (self._roomSelect.data('mobile-selectmenu')){
						self._roomSelect.selectmenu('destroy');
					}

					//zobrazíme text, naplníme ceník
					var roomOptionText = self._getRoomName(rooms[0]);
					self._roomSelect.hide();
					self._oneRoom.html(roomOptionText).show();
					self._fillRoomPrices(rooms[0].room_id);
				}else{
					//pro více pokojů select naplníme
					$.each(rooms, function (index, room) {
						//název pokoje
						var roomOptionText = self._getRoomName(room);

						//přidáme volbu do selectu
						self._roomSelect.append(
							$('<option>', {
								value : room.room_id,
								text : roomOptionText
							})
						);
					});

					// inicializujeme plugin + change handler
					self._roomSelect
						.selectmenu()
						.on('change', function () {
							//přenačteme ceny
							self._fillRoomPrices($(this).val());

							//přenačteme dostupnost pokoje
							self._setRoomAvailability();
						})
						.trigger('change'); //spustí handler a naplní ceny k prvnímu pokoji
				}
			}

			//vyplníme ceny bez pokojů
			if (norooms && norooms.length) {
				self._fillNoRoomPrices(norooms);
			} 

			//počáteční výpočet ceny
			self._calculateFinalPrice();

		},

		/**
		 * Vyplnění cen k pokoji
		 * @param  int room_id Id pokoje pro který vyplňujeme
		 */
		_fillRoomPrices: function (room_id) {
			var self = this,
				prices = self._prices, 
				pricesContent = '';
			
			//vymažeme seznam cen
			self._roomPrices.empty();

			//najdeme pokoj, k nemu projdeme ceny a pridame je do ceníku
			$.each(prices.rooms, function (index, room) {
				//našli jsme v cenách požadovaný pokoj
				if (room.room_id == room_id) {
					//projdeme ceny
					$.each(room.Prices, function (index, price){
						var priceElement = self._createOnePrice(price);
						priceElement.appendTo(self._roomPrices);	
					});
					self._roomWrapper.show();
				}
			});

		},

		/**
		 * Vyplnění cen nezávislých na pokoji
		 * @param  array noroomPrices Seznam cen
		 */
		_fillNoRoomPrices : function (noroomPrices) {
			var self = this;
			if (noroomPrices && noroomPrices.length) {
				$.each(noroomPrices, function (index, price){
					var priceElement = self._createOnePrice(price);
					priceElement.appendTo(self._noroomPrices);	
				});
				self._noroomPrices.show();
			}
		},

		/**
		 * Vytvoření elementu pro výběr ceny (pokojové i nepokojové)
		 * - nabidnuje spinner plugin a vrátí celý element
		 * @param  object price Jedna cenam
		 */
		_createOnePrice : function (price) {
			var self = this;
			var wrapper = $('<div>');
			var priceLabelText = price.description;
			var additionalLabelText = '';

			//příplatky - označení
			if (price.fee) {
				if (price.required) {
					additionalLabelText += cesys.ts('Mandatory surcharge');					
				}else{
					additionalLabelText += cesys.ts('Optional supplement');					
				}
				priceLabelText += ' (' + additionalLabelText + ') ';
			}
			priceLabelText += '<strong>' + price.price + '</strong>'

			var priceLabel = $('<label>', {
				'html'  : priceLabelText
			})
			var priceInput = $('<input>', {
				type : 'text',
				'class' : 'number',
				'data-price' : price.price_value,
				'data-per-day' : price.per_day,
				'value' : 0,
				'min' : 0,
				'max' : 100,
			});

			//sestavení elementů
			wrapper
				.append(priceLabel)
				.append(priceInput);

			//po vložení do wrapperu inicializace spinneru (pokud by se inicializovalo před vložením, zůstal by kód spineru viset mimo wrapper)
			priceInput.simpleSpinner(
				function () {
					self._calculateFinalPrice();
				}
			)

			return wrapper;
		},


		/**
		 * Sestaví název pokoje pro label
		 */
		_getRoomName : function (room) {
			//název pokoje
			var roomOptionText = room.description;

			//přidáme počty lůžek 
			if(room.beds > 0) {
				roomOptionText += ' (' + cesys.ts('lůžka') + ' : ' + room.beds;
				if(room.extra_beds > 0) {
					roomOptionText += ', ' + cesys.ts('přistýlky') + ' : ' + room.extra_beds;
				}
				roomOptionText += ')';
			}

			return roomOptionText;
		},


		/**
		 * Zvenčí injectnutí seznamu dostupností pokojů - ověřuje se ve widgetu detailu nabídky
		 * @param  array roomAvailabilities dostupnosti pokojů
		 */
		_fillRoomAvailabilities: function (roomAvailabilities) {
			var self = this;

			//pokud jsou dostupnosti platné
			if (Array.isArray(roomAvailabilities) && roomAvailabilities.length) {
				//uložíme do privátní proměnné
				self._roomAvailabilities = roomAvailabilities;

				//vyplníme dostupnost u aktuálního pokoje
				self._setRoomAvailability();
			}
		},

		/**
		 * Aktualizace dostupnosti pro aktuální pokoj
		 */
		_setRoomAvailability : function () {
			var self = this;

			//pokud jsou dostupnosti platné
			if (Array.isArray(self._roomAvailabilities) && self._roomAvailabilities.length) {
				//id vybraného pokoje
				var currentRoom = self._roomSelect.val();

				//najdeme pokoj v dostupnostech
				var status = null;
				$.each(self._roomAvailabilities, function (index, value) {
					if (value.id === currentRoom) {
						status = value.status;
					}
				});

				//pokud jsme našli ověření
				if (status) {
					self._roomAvailability.show();
					cesys.mobile.utils.setAvailabilityStatus(self._roomAvailability.find('span.result'), status, true);
				}else{
					self._roomAvailability.hide();
				}
			}
		},

		/**
		 * Pokus odeslání zabookování
		 */
		_submitForm: function () {
			var self = this;

			//zpracování dat k odeslání
			var inputsToSend = self.element.find('#firstname')
				.add(self.element.find('#surname'))
				.add(self.element.find('#phone'))
				.add(self.element.find('#email'))
				.add(self.element.find('#adultCount'))
				.add(self.element.find('#childCount'))
				.add(self.element.find('#localBookingAccept'))
				.add(self.element.find('#localBookingAcceptMarketing'))
				.add($('<input>', {
					'name' : 'data[LocalBooking][date_id]',
					'value' : self.options.offerDetail.id
				}));
			var dataToSend = inputsToSend.serialize();

			//pokud je aktivní zaškrtávací pole - přidáme ho ručně, protože serialize() nepřidá nezaškrtnutý checkbox
			var checkCondition = self.element.find('#LocalBookingCheckCondition');
			if (checkCondition.length) {
				dataToSend = dataToSend + '&' + encodeURIComponent(checkCondition.attr('name')) + '=' +  encodeURIComponent(checkCondition.prop('checked') === true ? 1 : 0);
			}

			//tlačítko zneaktivnit
			self.element.find('input[type=submit]').button('disable');

			//informovat apliakci že se bude provádět ajax
			$(document).trigger('beforedataload');

			//uložit nabídku
			$.ajax({
				url     : '/mobile/ajax_booking',
				type    : 'POST',
				data    : dataToSend,
				success : function (data) {
					self._setOrderStatus(data);
				},
				error   : function (xhr, status, error) {
					self._setOrderStatusError(cesys.ts('Server or parse error'));
				},
				complete: function () {
					$(document).trigger('afterdataload');
					self.element.find('input[type=submit]').button('enable');
				}
			});
		},

		/**
		 * Nastavení vráceného stavu objednávky
		 * @param array data = array(
		 *         error : int 0 = bez chyby | > 0 = chyba,
		 *         status : string textový popis výsledku,
		 *         data : dodatečná data - třeba seznam validačních chyb
		 * 		)
		 */
		_setOrderStatus : function (data) {
			if (data.error) {
				this._setOrderStatusError(data.status, data.data);
			}else{
				this._setOrderStatusSuccess(data.status, data.data);

				//po úspěchu reset formu
				this.element[0].reset();
			}
		},

		/**
		 * Nastavení chybového stavu
		 * @param string status Textový status
		 * @param array  data   Dodatečná data chyby
		 */
		_setOrderStatusError : function (status, data) {
			var self = this;

			self._orderStatus
				.empty()
				.removeClass('success')
				.addClass('error')
				.append('<h3>' + status + '</h3>');

			//jsou li data, projdeme a vypiseme
			if (data && data.length) {
				$.each(data, function(index, error){
					self._orderStatus.append('<p>' + error + '</p>');
				});
			}

			self._orderStatus.show();
		},

		/**
		 * Nastavení úspěšného stavu vytváření objednávky
		 * @param string status Tetxový status
		 * @param array  data   Dodatečná data - u úspěšného stavu zatím nepoužité
		 */
		_setOrderStatusSuccess : function(status, data) {
			this._orderStatus
				.empty()
				.removeClass('error')
				.addClass('success')
				.append('<h3>' + status + '</h3>')
				.show();
		},

		/**
		 * Výpočet výsledné ceny
		 */
		_calculateFinalPrice : function () {
			var self = this,
				price = 0,
				priceQuantities = self._roomPrices.add(self._noroomPrices).find('input');

			priceQuantities.each(function (index, element) {
				if (parseInt(element.attributes['data-per-day'].value)){
					price += (element.attributes['data-price'].value * element.value * self.options.offerDetail.duration);	
				}else{
					price += (element.attributes['data-price'].value * element.value);
				}
			});

			self._fillTotalPrice(price);
		},

		/**
		 * Naformátování a zobrazení výsledné ceny
		 */
		_fillTotalPrice : function(price){
			var price = cesys.mobile.utils.formatPrice(price);
			var text = this.options.finalPriceText;
			text = text.replace('%d', price);
			this._finalPrice.html(text);
		},


		// public rozhraní ------------------
		fill : function (prices){
			this._fill(prices);
		},

		fillRoomAvailabilities : function (roomAvailabilities){
			this._fillRoomAvailabilities(roomAvailabilities);
		}

	});
})(jQuery, window, document);
