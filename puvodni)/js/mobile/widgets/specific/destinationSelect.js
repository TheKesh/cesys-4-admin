/**
 * Widget na výběr destinací
 */
;(function ($, window, document, undefined) {

	$.widget("mobile.destinationselect", $.mobile.filterableselectmenu, {
		
		options: {
			//seznam destinací
			destinations: []
		},

		/**
		 * Konstruktor
		 */
		_create: function () {
			this._super();

			// při vlastní události contentchange/změna obsahu selektu sám plugin ošetří vlastní viditelnost
			var self = this;
			$.mobile.document.on("destinationselectcontentchanged", function () {
				self._switchParentVisibility();
			});
		},

		/**
		 * Utilitka pro opakované vypsání stringu
		 */
		_repeatString : function (count, string) {
			count = parseInt(count);
			return count == 0 ? '' : new Array(count + 1).join(string);
		},

		/**
		 * Naplnění selectmenu destinacemi ve stromovém formátu - destinace obalíme do optgroup s názvem země
		 */
		fill: function (selectedCountries) {
			var self = this;
			var selectOptions = '';
			var firstOption = '';

			if (this.options.doesnotmattertext) {
				firstOption = '<option value="0" selected>' + this.options.doesnotmattertext + '</option>';
			}else if (this.options.placeholdertext) {
				firstOption = '<option data-placeholder="true" value="0" >' + this.options.placeholdertext + '</option>';
			}

			//pomocná proměnná, jestli byla otevřená nějaká skupina <optgroup>
			var optGroupOpened = false;

			//pokud jsou nějaké země vybrané
			if (Array.isArray(selectedCountries) && selectedCountries.length > 0) {
				$.each(this.options.destinations, function (key, destination) {
					//pokud destinace spadá pod zemi, přidáme ji
					if (selectedCountries.indexOf(destination.country_id) !== -1 ){
						//main destinace s hloubkou nula přidáme jako optgroup
						if (!destination.depth){
							//pokud byl nějaký optGroup již otevřen, uzavřeme ho
							if (optGroupOpened) {
								selectOptions = selectOptions + '</optgroup>';
							}
							selectOptions = selectOptions + '<optgroup label="' + destination.name + '">';
							optGroupOpened = true;
						}else{
							//přidání destinace pokud spadá pod zemi
							if (selectedCountries.indexOf(destination.country_id) !== -1){
								selectOptions = selectOptions + '<option value="' + destination.id + '">' + self._repeatString(destination.depth-1, '&#151;') + ' ' + destination.name + '</option>';
							}
						}
					}
				});
				//uzavření poslední optgroup pokud jsme nějaké vložili
				if (optGroupOpened) {
					selectOptions = selectOptions + '</optgroup>';
				}
			}

			this.element.html(selectOptions);

			//placeholder / nullOption
			this.element.prepend(firstOption);

			this.refresh(true);
			this._trigger('contentchanged');
		},

		/**
		 * Získá seznam všech vybraných destinací - v selectu je vybraná jen jedna rodičovská, ale pro list lokací potřebujeme i všechny poddestinace
		 *  - vracíme všechna id celého podstromu
		 */
		selectedDestinations : function() {
			var self = this,
				destinations = self.options.destinations,
				values = $(self.element).val();

			//je li neco vybraneho 
			if (values) {
				//destinace, ktere maji za rodice nekterou z vybranych destinaci pridame do vysledku
				$.each(destinations, function(index, destination) {
					if ($.inArray(destination.parent_id, values) !== -1) {
						values.push(destination.id);
					}
				});
			}

			return values;
		}

	});
})(jQuery, window, document);