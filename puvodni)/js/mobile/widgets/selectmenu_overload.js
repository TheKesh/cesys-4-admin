/**
 * Přetížení select listu, přidání metody pro naplnění selectu
 */
;(function ($, window, document, undefined) {
	$.widget("mobile.selectmenu", $.mobile.selectmenu, {
		//auto inicializace vypnutá prázdným selectorem
		initSelector: '',

		options: {
			preferred       : [],
			preferredText   : cesys.ts('Preferred'),
			allText         : cesys.ts('All'),
			idtohidewhenemty: null,
			doesnotmattertext: false,
			placeholdertext: false
		},

		_create : function(){
			//nejprve konstruktor rodiče - jako první nabinduje obsluhu událostí jquery selectmenu
			this._super();

			//poté vlastní handler, který z labelu odstraní pomlčky, pokud je obsah selectu stromový
			var self = this;
			this.element.on('change', function(self) {
				var parent = $(this).closest('.ui-select');
				if (parent.length){
					var label = parent.find('.ui-btn > span:not(.ui-li-count)');
					label.html(label.text().replace(/—/g, ""));
				}
			});
		},


		/**
		 * Vyčištění selectmenu
		 */
		clear: function () {
			this.element.html('');
			this.refresh();
			this._switchParentVisibility();
		},


		/**
		 * Vyplní volby selectu z pole
		 * 
		 * @param items seznam položek id > název
		 */
		fill: function (items) {
			var self = this,
			    options = [],
			    preferredOptions = [],
			    firstOption = '';

			if (this.options.doesnotmattertext) {
				firstOption = '<option value="0" selected>' + this.options.doesnotmattertext + '</option>';
			}else if (this.options.placeholdertext) {
				firstOption = '<option data-placeholder="true" value="0" >' + this.options.placeholdertext + '</option>';
			}

			//příprava options
			var preferred  = self.options.preferred;
			$.each(items, function (id, name) {				
				//přidáme option do seznamu
				if (typeof preferred[id] !== 'undefined') {
					//volba existuje i v preferovaných
					preferredOptions.push('<option value="' + id + '">' + name + '</option>');
				}
				options.push('<option value="' + id + '">' + name + '</option>');
			});

			//doplníme volby do selectu
			if (preferredOptions.length > 0) {
				this.element.html(
					'<optgroup label="' + this.options.preferredText + '">' +
						preferredOptions.join("\n") +
					'</optgroup>' +

					'<optgroup label="' + this.options.allText + '">' +
						options.join("\n") +
					'</optgroup>'
				);
			} else {
				this.element.html(options.join("\n"));
			}

			//placeholder / nullOption
			this.element.prepend(firstOption);
			this.refresh();
		},

		/**
		 * Pokud má select nějaký obsah, bude zobrazený, jinak se skryje
		 */
		_switchParentVisibility: function () {
			if (this.options.idtohidewhenemty != null) {
				var selectoptions = $(this.element).find('option:not([data-placeholder]):not([value=0])');
				if (selectoptions.length > 0) {
					$('#' + this.options.idtohidewhenemty).removeClass('hiddenContain');
				} else {
					$('#' + this.options.idtohidewhenemty).addClass('hiddenContain');
				}
			}
		}
	});
})(jQuery, window, document);