/**
 * Přetížení popup tak aby se za ním zobrazovala mlha, definice v CSS
 */
;(function ($, window, document, undefined) {
	$.widget("mobile.popup", $.mobile.popup, {

		_create: function () {
			this._super();
			if ($('#overlay').length == 0)
				this._page.append(
					'<div id="overlay"></div>'
				);
		},
		open   : function () {
			this._super();
			$('#overlay').addClass('overlayShowed');
		},
		close  : function () {
			this._super();
			$('#overlay').removeClass('overlayShowed');
		}
	});
})(jQuery, window, document);