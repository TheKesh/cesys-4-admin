<?php
/**
 * Provider for gettext translations
 *
 * @author Vojta Tůma<ja@vojta-tuma.cz>
 */

namespace Yustme\Translations\I18n;

class GettextProvider implements ProviderInterface
{
	/**
	 * Init Gettext
	 * @param String $catalogName     name of catalog (i.e. messages)
	 * @param String $path            path to folder with l10ns (i.e. __DIR__ . '/locale')
	 * @param String $locale          iso code of locale (i.e. cs_CZ)
	 * @param string $codeset         codeset of translations
	 * @param string $catalogSuffix   suffix of calatalog ('mo' or 'po')
	 * @param string $localeDirectory if name of folder is different from $locale
	 */
	public function __construct($catalogName, $path, $locale, $codeset ="utf-8", $catalogSuffix = "mo", $localeDirectory = "")
	{
		$realPath = realpath($path);

		//check if target directory is readable
		if(!is_readable($path)){
			throw new \Exception("Path $path is nr readable");
		}

		//set custom localeDirecotry if is set
		$localeDirectory = ( empty( $localeDirectory ) ) ? $locale : $localeDirectory ;

		//get full path
		$absolutePath = $path . '/' . $localeDirectory . '/' . 'LC_MESSAGES' . '/' . $catalogName . '.' . $catalogSuffix;

		//var_dump($absolutePath);

		//check existing of targer catalog
		if(!file_exists($absolutePath)){
			throw new \Exception("Catalog file does not exists on path : $absolutePath" );
		}
		//catalog name in ISO format
		$catalogDef = $locale . '.' . $codeset;
		
		clearstatcache(); // reset gettext cache
		
		//sets locale of app
		$this->checkOutput(setlocale(LC_ALL, $catalogDef),"setlocale(LC_ALL, $catalogDef)");
		$this->checkOutput(setlocale(LC_CTYPE, $catalogDef),"setlocale(LC_CTYPE, $catalogDef)");

		//says where catalog is stored
		$this->checkOutput(bindtextdomain($catalogName, $path),"$catalogName, $path)");

		//run domain with translates
		$this->checkOutput(textdomain($catalogName),"(textdomain($catalogName)");

	}

	/**
	 * Check if value has been set
	 * @param  String $value       returned string from func
	 * @param  String $description what has been done
	 */
	private static function checkOutput($value, $description)
	{
		if(empty($value)){
			throw new \Exception('Variable cannot be set : ' . $description);
		}
	}

	/**
	 * Return translated content
	 * @param  String $content original string
	 * @return String          translated string from catalog
	 */
	public function translate($content)
	{
		return "'" . _($content) . "'";
	}
}