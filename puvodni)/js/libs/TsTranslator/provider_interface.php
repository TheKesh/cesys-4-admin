<?php
/**
 * Interface for providing translation
 */

namespace Yustme\Translations\I18n;

interface ProviderInterface
{
	/**
	 * Translations
	 * @param  String $content original string
	 * @return String          translated string
	 */
	public function translate($content);
}