<?php
/**
 * TsTranslator for text translations
 *
 * @author Vojta Tůma<ja@vojta-tuma.cz>
 */

namespace Yustme\Translations\I18n;

class TsTranslator{
	
	/**
	 * Perex for "gettext" function in content
	 */
	const DEFAULT_METHOD_PEREX = "~cesys\.ts\(\'([^\']+)\'\)~";

	
	/**
	 * Provider for translations
	 * @var ProviderInterface
	 */
	private $provider = null;


	/**
	 * Setting default provider for translation
	 * @param ProviderInterface $translationProvider provider for translations
	 */
	public function __construct(ProviderInterface $translationProvider)
	{
		$this->provider = $translationProvider;
	}



	/**
	 * Proccess content and replace custom defined function by gettext translate
	 * @param  $content 		content where translated function will be replaced by translation
	 * @param  String 	$perex  regular expression for replacing
	 * @return String           translated content
	 */
	public function processContent($content, $perex = self::DEFAULT_METHOD_PEREX)
	{	
		$Provider = $this->provider;
		$callback = function ($match) use ($Provider){
			$word = trim((string)$match[1]);
			return $Provider->translate($word);
		};

		return  preg_replace_callback($perex, $callback, $content);
	}
}