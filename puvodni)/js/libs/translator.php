<?php
/**
 *  Init Gettext and provide method to translate text
 *
 * @author Vojta Tůma <ja@vojta-tuma.cz>
 */

use Yustme\Translations\I18n;

//libraries
require_once __DIR__ . '/TsTranslator/provider_interface.php';
require_once __DIR__ . '/TsTranslator/gettext_provider.php';
require_once __DIR__ . '/TsTranslator/ts_translator.php';

$localeCodes = array(
	 "cs" => "cs_CZ"
	,"sk" => "sk_SK"
	,"hu" => "hu_HU"
	,"hr" => "hr_HR"
	,"sr" => "sr_RS"
	,"en" => "en_GB"
	,"vi" => "vi_VN"
);

$code = (isset($localeCodes[$lang])) ? $localeCodes[$lang] : 'cs_CZ';



//settings for gettext
$path = __DIR__ . '/../../../core/locale';
$locale = $code;
$catalogName = 'core';
$codeset = "utf-8";
$catalogSuffix = "mo";
$localeDirectory = $code;

//init of translations
$Provider = new I18n\GettextProvider($catalogName, $path, $locale, $codeset, $catalogSuffix, $localeDirectory);
$Translator = new I18n\TsTranslator($Provider);
