<?php
/**
 * Rozlišení, a nastavení jazykové mutace stránek. Fixně podle názvu domény.
 *
 * @author Martin Takáč <martin@takac.name>
 */
$locale = 'cs';
switch ($_SERVER['HTTP_HOST']) {
	case 'f.ccdn.sk':
	case 'f-p.ccdn.sk':
	case 'files.cesys-sk.lc':
		$locale = 'sk';
		break;
	case 'f.ccdn.hu':
	case 'files.utazasbongeszo.eu': //docasna adresa
		$locale = 'hu';
		break;
	case 'vie.f.ccdn.cz':
		$locale = 'vi';
		break;
	case 'scc.f.ccdn.cz':
		$locale = 'sr';
		break;
	case 'hrv.f.ccdn.cz':
		$locale = 'hr';
		break;
	case 'eng.f.ccdn.cz':
		$locale = 'en';
		break;
	case 'f.ccdn.cz':
	case 'f-p.ccdn.cz':
	case 'files.cesys-cz.lc':
	default:
		$locale = 'cs';
}

return $locale;
