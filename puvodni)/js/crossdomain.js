/**
 *  CrossDomain Javascript Function Execute Server Side 1.00
 *  Changelog:
 *  - 1.0: project started
 * Coded by: Maxim Voldachinsky <admin@emposha.com> 
 * Coded by: Martin Takáč <martin@takac.name> 
 *
 * Copyright: Emposha.com <http://www.emposha.com/> - Distributed under MIT - Keep this message!
 */


/**
 *	Namespace
 */
var crossdomain = crossdomain || {};


/**
 *	Kontroler na straně hostující stránky.
 */
crossdomain.controller = crossdomain.controller || function (idframe)
{
	var url = window.parent.parent.location.hash;
	var iframe = window.parent.parent.document.getElementById(idframe);
	var src = iframe.src.match(/[^\:]+\:\/\/[^/]+/g);
	if (src.length) {
		if (iframe.src != src[0] + url.substr(1)) {
			iframe.src = src[0] + url.substr(1);
		}
	}
	return this;
};



/**
 *	Akce pro změnu hotujícího url.
 */
crossdomain.controller.hash = function (context, text)
{
	text = '' + text
	text = text.replace(/@/g,'&');
	context.host.location.hash = '' + text;
};



/**
 *	Část pro komunikaci, umístěná v proxy souboru na stejné doméně.
 */
crossdomain.server = crossdomain.server || {
	object : null,
	server: function() {
		if (this.object == null) {
			this.object = new this.fc();
		}
		return this.object;
	}
};


crossdomain.server.fc = function()
{

	/**
	 *	Hostující stránka.
	 */
	this.host = window.parent.parent;



	/**
	 *	Hostovaná stránka. Iframe
	 */
	this.client = window.parent;



	/**
	 *	Server zajištující komunikaci.
	 */
	this.server = window;


	//	Injektnout hostující stránce kontroler.
	this.host.crossdomain = this.host.crossdomain || {};
//	this.host.crossdomain.controller = this.host.crossdomain.controller || crossdomain.controller('idIframe');

};




crossdomain.server.fc.prototype =
{
	init: function(idframe)
	{
		//	Injektnout hostující stránce kontroler.
//		this.host.crossdomain = this.host.crossdomain || {};
//		this.host.crossdomain.controller = this.host.crossdomain.controller || crossdomain.controller('idIframe');

		if (!this.host.crossdomain.controller) {
			this.host.crossdomain.controller = crossdomain.controller(idframe);
			return this;
		}
		try {
			var func = this.parseGet("func");
			var attr = this.unserialize(this.base64_decode(this.parseGet("attr")));
			var params = '';
			if (typeof(attr) == 'object') {
				for (var key in attr) {
					params += attr[key] + ",";
				}
			}
		}
		catch (e) {
		//	console.error('Exception 1', e);
		}
		try {
			params = this.substr(params, 0, -1);

//				eval("this.host.crossdomain.controller.controller." + func + "(this, " + params + ")");
			eval("this." + func + "(this, " + params + ")");
//			eval("window.parent.parent." + func + "(" + params + ")");
		}
		catch (e) {
		//	console.error('Exception 2', e.message, window.parent.parent, "this.host.crossdomain.controller.controller." + func + "(this, " + params + ")");
		}
		return this;
	},




	/**
	 *	Akce pro změnu hostujícího url.
	 */
	hash: function (context, text)
	{
		text = text.replace(/@/g,'&');
		this.host.location.hash = '' + text;
	},



	parseGet: function (key)
	{
		var query = (location.search ? location.search.substring(1) : "");
		var pairs = query.split("&");
		for (var i = 0; i < pairs.length; i++) {
			var pos = pairs[i].indexOf('=');
			if (pos >= 0) {
				var argname = pairs[i].substring(0, pos);
				var value = pairs[i].substring(pos + 1);
				if (argname == key) {
					return value;
				}
			}
		}
		return "";
	},
	
	
	substr: function (f_string, f_start, f_length) {
		f_string += '';
		if (f_start < 0) {
			f_start += f_string.length;
		}
		if (f_length == undefined) {
			f_length = f_string.length;
		} else if (f_length < 0) {
			f_length += f_string.length;
		} else {
			f_length += f_start;
		}
		if (f_length < f_start) {
			f_length = f_start;
		}
		return f_string.substring(f_start, f_length);
	},
	
	
	base64_decode: function (data)
	{
		var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0, dec = "", tmp_arr = [];
		if (!data) {
			return data;
		}
		data += '';
		do {
			h1 = b64.indexOf(data.charAt(i++));
			h2 = b64.indexOf(data.charAt(i++));
			h3 = b64.indexOf(data.charAt(i++));
			h4 = b64.indexOf(data.charAt(i++));
			bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
			o1 = bits >> 16 & 0xff;
			o2 = bits >> 8 & 0xff;
			o3 = bits & 0xff;
			if (h3 == 64) {
				tmp_arr[ac++] = String.fromCharCode(o1);
			} else if (h4 == 64) {
				tmp_arr[ac++] = String.fromCharCode(o1, o2);
			} else {
				tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
			}
		} while (i < data.length);
		dec = tmp_arr.join('');
		dec = this.utf8_decode(dec);
		return dec;
	},


	utf8_decode: function (str_data)
	{
		var tmp_arr = [], i = ac = c1 = c2 = c3 = 0;
		str_data += '';
		while (i < str_data.length) {
			c1 = str_data.charCodeAt(i);
			if (c1 < 128) {
				tmp_arr[ac++] = String.fromCharCode(c1);
				i++;
			} else if ((c1 > 191) && (c1 < 224)) {
				c2 = str_data.charCodeAt(i + 1);
				tmp_arr[ac++] = String.fromCharCode(((c1 & 31) << 6) | (c2 & 63));
				i += 2;
			} else {
				c2 = str_data.charCodeAt(i + 1);
				c3 = str_data.charCodeAt(i + 2);
				tmp_arr[ac++] = String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
		}
		return tmp_arr.join('');
	},
	unserialize: function (data) {
		var error = function (type, msg, filename, line) {
			throw new window[type](msg, filename, line);
		};
		var read_until = function (data, offset, stopchr) {
			var buf = [];
			var chr = data.slice(offset, offset + 1);
			var i = 2;
			while (chr != stopchr) {
				if ((i + offset) > data.length) {
					error('Error', 'Invalid');
				}
				buf.push(chr);
				chr = data.slice(offset + (i - 1), offset + i);
				i += 1;
			}
			return [buf.length, buf.join('')];
		};
		var read_chrs = function (data, offset, length) {
			buf = [];
			for (var i = 0; i < length; i++) {
				var chr = data.slice(offset + (i - 1), offset + i);
				buf.push(chr);
			}
			return [buf.length, buf.join('')];
		};
		var _unserialize = function (data, offset) {
			if (!offset) offset = 0;
			var buf = [];
			var dtype = (data.slice(offset, offset + 1)).toLowerCase();
			var dataoffset = offset + 2;
			var typeconvert = new Function('x', 'return x');
			var chrs = 0;
			var datalength = 0;
			switch (dtype) {
				case "i":
					typeconvert = new Function('x', 'return parseInt(x)');
					var readData = read_until(data, dataoffset, ';');
					var chrs = readData[0];
					var readdata = readData[1];
					dataoffset += chrs + 1;
					break;
				case "b":
					typeconvert = new Function('x', 'return (parseInt(x) == 1)');
					var readData = read_until(data, dataoffset, ';');
					var chrs = readData[0];
					var readdata = readData[1];
					dataoffset += chrs + 1;
					break;
				case "d":
					typeconvert = new Function('x', 'return parseFloat(x)');
					var readData = read_until(data, dataoffset, ';');
					var chrs = readData[0];
					var readdata = readData[1];
					dataoffset += chrs + 1;
					break;
				case "n":
					readdata = null;
					break;
				case "s":
					var ccount = read_until(data, dataoffset, ':');
					var chrs = ccount[0];
					var stringlength = ccount[1];
					dataoffset += chrs + 2;
					var readData = read_chrs(data, dataoffset + 1, parseInt(stringlength));
					var chrs = readData[0];
					var readdata = readData[1];
					dataoffset += chrs + 2;
					if (chrs != parseInt(stringlength) && chrs != readdata.length) {
						error('SyntaxError', 'String length mismatch');
					}
					break;
				case "a":
					var readdata = {};
					var keyandchrs = read_until(data, dataoffset, ':');
					var chrs = keyandchrs[0];
					var keys = keyandchrs[1];
					dataoffset += chrs + 2;
					for (var i = 0; i < parseInt(keys); i++) {
						var kprops = _unserialize(data, dataoffset);
						var kchrs = kprops[1];
						var key = kprops[2];
						dataoffset += kchrs;
						var vprops = _unserialize(data, dataoffset);
						var vchrs = vprops[1];
						var value = vprops[2];
						dataoffset += vchrs;
						readdata[key] = value;
					}
					dataoffset += 1;
					break;
				default:
					error('SyntaxError', 'Unknown / Unhandled data type(s): ' + dtype);
					break;
			}
			return [dtype, dataoffset - offset, typeconvert(readdata)];
		};
		return _unserialize(data, 0)[2];
	}
};



/**
 *	Obslužný klient, který ovlivnuje hostující stránku.
 */
crossdomain.client = crossdomain.client || {
	archive : {},
	client : function(frame_id) {
		if (typeof this.archive[frame_id] == "undefined") {
			this.archive[frame_id] = new this.fc(frame_id);
		}
		return this.archive[frame_id];
	}
};



crossdomain.client.fc = function()
{

	/**
	 *	Proxy, překládající naše požadavky.
	 */
	this.proxy = null;
	this.path = null;
	this.done = false;
	this.timer = 0;
	this.ready = null;
	this.frame_id = arguments[0];
};



crossdomain.client.fc.prototype =
{


	init: function(path)
	{
		this.path = path;
		this.domready(this.createProxyFrame);
		return this;
	},



	/**
	 * Vytvoří komunikační frame.
	 */
	createProxyFrame: function (caller)
	{
		try {
			var frame = document.createElement("iframe");
			frame.setAttribute("id", this.frame_id);
			frame.setAttribute("src", 'jav' + 'a' + 'scr' + 'ipt:false;');
			frame.style.display = "none";
			document.getElementsByTagName("body")[0].appendChild(frame);
			this.proxy = frame

//			this.addEvent(frame, "load", function() {
//				console.info('client.createProxyFrame.frame.eventload')
//			});

		}
		catch (e) {
		//	console.error('createProxyFrame.exception', e)
		}
	},
	


	/**
	 *	Zavolá akci nad serverem.
	 */
	remote: function (action, attr)
	{
		this.domready(function(context) {
			if (context.path != null) {
				var url = ((context.path.indexOf("?") != -1) ? (context.path + "&") : (context.path + "?")) + "func=" + action + "&attr=" + context.base64_encode(context.serialize(attr));
				document.getElementById(context.frame_id).setAttribute("src", url);
			}
		});
	},
	
	
	
	/**
	 *	Vykoná funkci, až bude připraveny a načteny zdroje.
	 */
	domready: function (func)
	{
		if (this.done) {
			return func(this);
		}
		if (this.timer) {
			this.ready.push(func);
		}
		else {
			var context = this;
			this.addEvent(window, "load", function() {
				context.flushFront(context);
			});
			this.ready = [ func ];
			this.timer = setInterval(function() {
				context.flushFront(context);
			}, 13);
		}
	},



	/**
	 *	Vykonat čekající akce ve frontě.
	 */
	flushFront: function (context)
	{
		if (context.done) {
			return this;
		}
		if (document && document.getElementById && document.body) {
			clearInterval(context.timer);
			context.timer = null;
			if (context.ready != null && context.ready.length > 0) {
				for (var i = 0; i < context.ready.length; i++) {
					try {
						if (typeof context.ready[i] == "function") {
							context.ready[i].call(this, context);
						}
						else {
						//	console.warn('Očekávána funkce:', context.ready[i]);
//							eval("context." + context.ready[i] + "();");
						}
					}
					catch(e) {
					//	console.warn('client.flushFront.exception', e, context.ready[i], i)
						throw 'Eval fail X';
					}
				}
			}
			context.ready = null;
			context.done = true;
		}
		return this;
	},



	/**
	 * Pověsit událost name na objekt obj.
	 */
	addEvent: function (obj, name, func, attribute)
	{
		if (obj.addEventListener) {
			obj.addEventListener(name, function() {
				func(attribute)
			}, false);
		}
		else if (obj.attachEvent) {
			obj.attachEvent('on' + name, function() {
				func(attribute)
			});
		}
		else {
			throw 'AddEvent Error'
		}
	},



	base64_encode: function (data) {
		var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
		var o1, o2, o3, h1, h2, h3, h4, bits, i = ac = 0, enc = "", tmp_arr = [];
		if (!data) {
			return data;
		}
		data = this.utf8_encode(data + '');
		do {
			o1 = data.charCodeAt(i++);
			o2 = data.charCodeAt(i++);
			o3 = data.charCodeAt(i++);
			bits = o1 << 16 | o2 << 8 | o3;
			h1 = bits >> 18 & 0x3f;
			h2 = bits >> 12 & 0x3f;
			h3 = bits >> 6 & 0x3f;
			h4 = bits & 0x3f;
			tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
		} while (i < data.length);
		enc = tmp_arr.join('');
		switch (data.length % 3) {
			case 1:
				enc = enc.slice(0, -2) + '==';
				break;
			case 2:
				enc = enc.slice(0, -1) + '=';
				break;
		}
		return enc;
	},



	utf8_encode: function (string) {
		string = (string + '').replace(/\r\n/g, "\n").replace(/\r/g, "\n");
		var utftext = "";
		var start, end;
		var stringl = 0;
		start = end = 0;
		stringl = string.length;
		for (var n = 0; n < stringl; n++) {
			var c1 = string.charCodeAt(n);
			var enc = null;
			if (c1 < 128) {
				end++;
			} else if ((c1 > 127) && (c1 < 2048)) {
				enc = String.fromCharCode((c1 >> 6) | 192) + String.fromCharCode((c1 & 63) | 128);
			} else {
				enc = String.fromCharCode((c1 >> 12) | 224) + String.fromCharCode(((c1 >> 6) & 63) | 128) + String.fromCharCode((c1 & 63) | 128);
			}
			if (enc != null) {
				if (end > start) {
					utftext += string.substring(start, end);
				}
				utftext += enc;
				start = end = n + 1;
			}
		}
		if (end > start) {
			utftext += string.substring(start, string.length);
		}
		return utftext;
	},



	serialize: function (mixed_value) {
		var _getType = function(inp) {
			var type = typeof inp, match;
			var key;
			if (type == 'object' && !inp) {
				return 'null';
			}
			if (type == "object") {
				if (!inp.constructor) {
					return 'object';
				}
				var cons = inp.constructor.toString();
				if (match = cons.match(/(\w+)\(/)) {
					cons = match[1].toLowerCase();
				}
				var types = ["boolean", "number", "string", "array"];
				for (key in types) {
					if (cons == types[key]) {
						type = types[key];
						break;
					}
				}
			}
			return type;
		};
		var type = _getType(mixed_value);
		var val, ktype = '';
		switch (type) {
			case "function":
				val = "";
				break;
			case "undefined":
				val = "N";
				break;
			case "boolean":
				val = "b:" + (mixed_value ? "1" : "0");
				break;
			case "number":
				val = (Math.round(mixed_value) == mixed_value ? "i" : "d") + ":" + mixed_value;
				break;
			case "string":
				val = "s:" + mixed_value.length + ":\"" + mixed_value + "\"";
				break;
			case "array":
			case "object":
				val = "a";
				var count = 0;
				var vals = "";
				var okey;
				var key;
				for (key in mixed_value) {
					ktype = _getType(mixed_value[key]);
					if (ktype == "function") {
						continue;
					}
					okey = (key.match(/^[0-9]+$/) ? parseInt(key) : key);
					vals += this.serialize(okey) +
							this.serialize(mixed_value[key]);
					count++;
				}
				val += ":" + count + ":{" + vals + "}";
				break;
		}
		if (type != "object" && type != "array") val += ";";
		return val;
	}
};


