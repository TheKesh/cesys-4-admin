<?php
/**
 *	Podavač pro js.
 *
 *	@param $_GET['d'] Uroven ladeni. 
 *		NULL: žádné/product - kód se komrimuje
 *		1: kód se komprimuje, v komentáři se uvádí které soubory jsou použity.
 *		2: kód se NEkomprimuje, v komentáři se uvádí které soubory jsou použity.
 *		3: devel
 *	@param $_GET['r'] flag jen pro prohížeče
 *	@param $_GET['v'] Verze scriptů.
 *	@param $files Seznam scriptů, které se mají použít.
 *	@param $filename_dynamic seznam souboru na dynamicke nacitani
 *
 *	@dependence ./jsmin.php
 */


 
date_default_timezone_set('Europe/Prague');

//fwrite(STDERR, memory_get_peak_usage(true)."\n");

$cache = true;
$debug = 0;
$try_catch = false; //'try-catch-block.js';


if (isset($_GET['d']) AND in_array($_GET['d'], array('1', '2'))) {
	$debug = $_GET['d'] + 0;
	$cache = false;
}

//jiz existuje v admin.php, zjistime jazyk
$lang = require __dir__ . '/libs/locale.php';

//inicializace překladače do proměnné $Translator - spolehá na existenci proměnné lang
require_once __DIR__ . '/libs/translator.php';

if (!isset($files)) {
	$files = array();
}

if (!isset($version)) {
	$version = '';
}

if (!isset($filename_dynamic)) {
	$filename_dynamic = '';
}

$filename = basename($_SERVER["SCRIPT_NAME"], '.php');
$filepath = './cache/' . strtoupper($lang) . $filename . $version .  '.js' . $filename_dynamic;

if (!isset($content)) {
	$content = '';
}

//	Pokusíme se použít kešovanou verzi.
if ($debug == 0 AND file_exists($filepath)) {
	$content = file_get_contents($filepath);
	$lastModified = filemtime($filepath);
	$etag = md5($content);
}
else {
	//	Script sloužící k minifikaci js.
	require_once './jsmin.php';
//	require_once __dir__ . '/../../core/tools/jsmin.php';

	foreach ($files as $file) {

		if ($debug > 0) {
			$content .= "\n\n\n/* file: $file */\n";
		}
		if ($debug == 2){
			$content .= $Translator->processContent(file_get_contents($file));
		}
		else {
			$content .= JSMin::minify($Translator->processContent(file_get_contents($file)));
		}
	}
	//	Zabalit do super bloku.
	if ($try_catch && ($context = @file_get_contents($try_catch))) {
		$content = strtr($context, array(
				'%try-catch-content%' => $content,
				));
	}

	if ($debug == 0 
			AND strlen($content) > 0 
			AND (($fp = @fopen($filepath, 'w')) !== false)) {
		fwrite($fp, $content);
		fclose($fp);
	}
	$lastModified = time();
	$etag = md5($content);
}

if ($cache) {
	
	$ifNoneMatch = isset($_SERVER['HTTP_IF_NONE_MATCH']) ? $_SERVER['HTTP_IF_NONE_MATCH'].'' : false; 
	$ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
	
	if (($etag === $ifNoneMatch) OR ($ifModifiedSince AND strtotime($ifModifiedSince) >= $lastModified)) {
		header('HTTP/1.1 304 Not Modified');
		header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
		header("Cache-Control: no-transform,public,max-age=86400,s-maxage=93600"); // http://www.mobify.com/blog/beginners-guide-to-http-cache-headers/
		header("ETag: $etag");
		exit; // stop processing
	}
	header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
	header("Content-Type: text/javascript; charset=utf-8");
//	header("Expires: " . gmdate("D, j M Y H:i:s", time() + 60*60*24) . " GMT");
//	header("Cache-Control: cache"); // HTTP/1.1
//	header("Pragma: cache");		// HTTP/1.0
	header("Cache-Control: no-transform,public,max-age=86400,s-maxage=93600"); // http://www.mobify.com/blog/beginners-guide-to-http-cache-headers/
	header("ETag: $etag");
}
else {
	header("Content-Type: text/javascript; charset=utf-8");
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 10 Jan 2000 05:00:00 GMT"); // Date in the past
	header('Pragma: no-cache');
}



echo $content;

//fwrite(STDERR, memory_get_peak_usage(true)."\n");

