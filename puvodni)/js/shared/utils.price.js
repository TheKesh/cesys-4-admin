var cesys = cesys || {};
$.extend(true, cesys, { 
	utils: {

		/**
		 * Formátování ceny
		 * @param  {float} priceValue  Hodnota ceny
		 * @param  {int} currency      Id měny
		 * @return {string}
		 */
		formatPrice : function(priceValue, currency) {
			//nemáme id měny, nastavíme hlavní
			if (!currency) {
				currency = window.config.currencyId;
			}

			//cenu naformátujeme s přesností na dvě místa
			var price = parseFloat(priceValue);
			price = price.toFixed(2);

			//rozdělíme na celou část a desetinnou
			price = price.toString();
			price = price.split('.');
			var wholePart = price[0];
			var decimalPart = price[1];

			//celá část - mezeru mezi tisíce
			wholePart = wholePart.replace(/./g, function(c, i, a) {
		        return i > 0 && (a.length - i) % 3 === 0 ? '&nbsp;' + c : c;
		    });

			//sestavení výsledné ceny - pokud je měna v seznamu měn s desetinnou přesností, přidáme desetinnou část
		    var finalPrice = wholePart;
		    if (window.config.currencieWithPrecision.indexOf(currency) !== -1 && decimalPart != '00'){
		    	finalPrice = finalPrice + ',' + decimalPart;
		    }

		    //vracíme výsledek s měnou
			return finalPrice + '&nbsp;' + window.config.currencies[currency];
		},


		/**
		 * Naformátování ceny do alternativní měny
		 * @param  {float} priceValue  Hodnota ceny
		 * @param  {int} currency      Id měny ve které cena je
		 * @param  {string} dataSource Zdroj dat
		 * @return {string}
		 */
		formatAltPrice : function(priceValue, currency, dataSource)
		{	
			var price = parseFloat(priceValue);
			var altPrice = null;

			//kurz alternativní měny je nastaven vzhledem k hlavní měně jazyka, pokud zadaná cena není v hlavní měně, nelze spočítat
			if (currency != window.config.currencyId){
				return altPrice;
			}

			//je-li cena v eurech, nemáme alternativní měnu
			if (currency == 2) {
				return altPrice;
			}

            if (window.config.settings.euroExchangeRate) {
                switch (dataSource) {
					case 'traffics_connector':
                        var exchangeRate = parseFloat(window.config.settings.euroExchangeRateTraffics);
						break;
					default:
                        var exchangeRate = parseFloat(window.config.settings.euroExchangeRate);
						break;
				}
                //jen pokud je nastaven kurz
                if (exchangeRate > 0){
					altPrice = price / exchangeRate;
					altPrice = cesys.utils.formatPrice(altPrice, 2); //2 = id eura
				}
			}

			return altPrice;
		}
	}
});


