/**
 *	Kontrola dostupnosti. Asynchroně.
 *	@author Martin Takáč
 */
var cesys = cesys || {};
$.extend(true, cesys, { 
	service: {
		availability: {
			/**
			 *	Konfigurace:
			 *
			 *	@var url Adresa služby, které se budem dotazovat.
			 *	@var lang Parametr jazyku - nutno nastavit zvenčí dle aktuálního jazyka
			 */
			configuration: {
				url: {
					check: '/online/availability/cesys/check_date.php'
				},
				lang: 'cs'
			},



			/**
			 *	Ověří jeden konkrétní zájezd.
			 *
			 *	@param data Vstupní data pro ajax, možnosti:
			 *	{
			 *		'cid': date id, 
			 *		'hash': zahashované id, 
			 *		'mode': mód ověřování, 
			 *		'rid': id pokoje, 
			 *		'cache': kešování true/false, defaultně true, 
			 *		'lang': jazyk, z konfigu - defaultně cs
			 *	}
			 *	@param parser callback(status, data) pro zpracování odpovědi 
			 */
			doCheckRoom: function(data, parser, ajax)
			{
				//ošetření vstupu - merge s defaultními hodnotami
				var defaults = {
					lang : window.cesys.service.availability.configuration.lang,
					cache : true
				};
				var handledInput = $.extend({}, defaults, data);

				//defaultní ajax
				if (!ajax) {
					ajax = $.ajax;
				}

				try {
					ajax({
						url : window.cesys.service.availability.configuration.url.check,
						data : handledInput,
						type: 'GET',
						dataType: 'json',
						success: function(json) {
                            if (typeof json.total_price !== "undefined") {
                                parser.call(this, json.status, json.total_price, json)
                            } else {
                                parser.call(this, json.status, json)
                            }
						},
						error: function() {
							parser.call(this, 'ER');
						}
					});
				}
				catch(e) {
					console.log('doCheckRoom', e)
				}
			},
            
            doCheckGroupRooms: function(dataGroup, whileCycle, checkReturnStatus)
            {
                $.each(dataGroup, function (k, data) {
                    setTimeout(function () {
                        data.check(window.cesys.service.availability.configuration.url.check);
                    }, k*1000);
                });

                whileCycle(dataGroup, checkReturnStatus);
            }

		}
	}
});

