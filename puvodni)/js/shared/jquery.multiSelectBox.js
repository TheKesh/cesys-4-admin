 /*
 *  jQuery selectBox - A cosmetic, styleable replacement for SELECT elements
 *
 *  Copyright 2012 Cory LaViska for A Beautiful Site, LLC.
 *
 *  https://github.com/claviska/jquery-selectBox
 *
 *  Licensed under both the MIT license and the GNU GPLv2 (same as jQuery: http://jquery.org/license)
 *
 */
if (jQuery)(function($) {
	$.extend($.fn, {
		multiSelectBox: function(method, data) {
			var typeTimer, typeSearch = '',
				isMac = navigator.platform.match(/mac/i);
			/*
			 * na nami zvoleny select pripoji knihovnu "selectBox" a dale doda checkboxy 
			 * 
			 * @param HTMLElement select - aktualni select se kterym pracujeme
			 * 
			 */
			var init = function(select, data) {
				$(select).data("description", data)
				$(select).data("multiselectCheckbox", new Array());
				$(select).data("number", 0);
				$(select).selectBox();
				var id = $(select).attr('id');
				$(".selectBox-"+id+" li").not(".selectBox-optgroup", ".remove").removeClass('selectBox-selected selectBox-hover').each(function(){
					var idCheck = id+'-'+$(this).find('a').attr('rel');
					$(this).html('<label><input type="checkbox" class="'+id+'" value="' + $(this).find('a').attr('rel') + '" id="'+idCheck+'" />' + $.trim($(this).find("a").text()) + '</label>');
					$(this).find('input').change(function(){
						check($(this).is(':checked'), $(this).val(), select);
					});
				}).unbind();
			}
			
			/*
			 * Kontrola zaskrtnuti checkboxu 
			 * Pokud je zaskrtnuty prida se hodnota do pole, pokud zaskrtnuta neni tak se z pole odebere
			 * Pokud je hodnota == 0 pole se vyprazdni a odskrtne vsechny checkboxy v aktualnim selectu
			 * 
			 * @param bool check - urcuje zda je/neni zaskrtnuty checkbox
			 * @param enum(0,1) value - hodnota checkboxu - potrebna pro plneni pole hodnot
			 * @param HTMLElement select - aktualni select se kterym pracujeme
			 * 
			 */
			var check = function(check, value, select) {
				if (check === true){
					$('input#'+$(select).attr('id')+'-'+value).attr('checked', true);
					var d = new Array();
					d = $(select).data("multiselectCheckbox");
					d[value] = value;
					$(select).data("multiselectCheckbox", d);
					if (value != 0){
						$('#'+$(select).attr('id')+'-0').attr('checked',false);
						$(select).data("number", stringData(select).split(",").length);
					}else{
						$('ul.selectBox-'+$(select).attr('id')).find('input').each(function(){
							$(this).attr('checked',false);
							$(this).attr('disabled',false);
						});
						$('#'+$(select).attr('id')+'-0').attr('checked',true);
						$(select).data("multiselectCheckbox", new Array());
						$(select).data("number", 0);
					}
				}else{
					var d = $(select).data("multiselectCheckbox");
					$('input#'+$(select).attr('id')+'-'+value).attr('checked', false);
					if (d[value] !== undefined){
						var number = $(select).data("number")-1;
					}
					delete d[value];
					$(select).data("multiselectCheckbox", d);
					if (number < 0){
						$(select).data("number", 0);
						number = 0;
					}
					$(select).data("number", number);
				}

				var descriptions = $(select).data("description"),
					description = descriptions['select0'];

				if ($(select).data("number") > 0) {
					//	Do tří slovně, pak číselně.
					if ($(select).data("number") < 3) {
						var control = $(select).data('selectBox-control'),
							options = control.data('selectBox-options');

						description = $.map(stringData(select).split(","), function(m) {
							var el = $(options).find('INPUT[value="' + m + '"]').parent();
							var temp = el.clone();
							temp.find('input').remove();
							var s = $.trim(temp.html());
							if (s == 'nerozhoduje') {
								var base = $(el).parents('li');
								return base.parent().find('[id="' + base.attr('id') + '"]').first().text();
							}
							return s;
						})
						.join(", ");
					}
					else {
						description = descriptions['selects'] + ': ' + $(select).data("number");
					}
				}
				$(select).next().find('.selectBox-label').html(description);
			}



			/*
			 * Nacte pole zaskrtnutych hodnot a vrati je zpet jako string
			 * 
			 * @param HTMLElement select - aktualni select se kterym pracujeme 
			 * 
			 * @return - vraci string vsech hodnot z pole oddeleny carkou
			 */
			var stringData = function(select){
				var data = [];
				$.each($.makeArray($(select).data("multiselectCheckbox")), function(i, j){
					if (j) {
						data.push(j);
					}
				});

				return data.join(',');
			}
			
			/*
			 * Zaskrtne vsechny checkboxy poslane v parametru data
			 * 
			 * @param HTMLElement select - aktualni select se kterym pracujeme 
			 * @param String data - vstupni hodnoty ktere se maji zaskrtnout (string oddeleny carkou)
			 * 
			 */
			var inputCheck = function(select, data){
				var idSelect = $(select).attr('id');
				var d = $(select).data("multiselectCheckbox");
				var inputData = data.split(',');
				$.each($.makeArray(inputData), function(i, j){
					if (j) {
						check(true, j, select);
					}
				});
			}

			/**
			 * Zavře menu zavoláním metody hideMenus na instanci selectBoxu
			 * @param HTMLElement select - aktualni select se kterym pracujeme 
			 */
			var hideMenu = function(select){
				$(select).selectBox('hideMenus');
			}
			
			switch (method) {
				case 'array':
					return $(this).data("multiselectCheckbox");
					break;
				case 'stringData':
					return stringData(this);
					break;
				case 'inputCheck':
					inputCheck(this, data);
					break;
				case 'check':
					var checked = false;
					if (data['check'] == 1){
						checked = true;
					}
					check(checked, data['value'], this);
					break;
				case 'hideMenu':
					hideMenu(this);
					break;
				default:
					$(this).each(function() {
						init(this, data);
					});
					break;
			}
			return $(this);
		}
	});
})(jQuery);