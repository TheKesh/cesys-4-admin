/*
 *  jQuery selectBox - A cosmetic, styleable replacement for SELECT elements
 *
 *  Copyright 2012 Cory LaViska for A Beautiful Site, LLC.
 *
 *  https://github.com/claviska/jquery-selectBox
 *
 *  Licensed under both the MIT license and the GNU GPLv2 (same as jQuery: http://jquery.org/license)
 *
 */
if (jQuery)(function($) {


	/**
	 *	Nahrazení chybějícího indexOf u <MSIE9
	 */
	if (Array.prototype.indexOf == undefined) {
		Array.prototype.indexOf = function(m)
		{
			for (var i=0; i<this.length; i++) {
				if (this[i] == m) {
					return i;
				}
			}
			return -1;
		}
	}



	$.fn.selectBox = (function ()
	{

		/**
		 * Construct Function
		 * @param string | object method Název akce, nebo nastavení konstruktoru.
		 * @param object data Nastavení akce.
		 * @param object context Přenášená konfigurace pro instanci.
		 */
		function selectBox(method, data, context) {
			var typeTimer,
				typeSearch = '',
				isMac = navigator.platform.match(/mac/i);



			/**
			 * Pokud v poli prvek je, tak jej vyjme, pokud není, tak jej přidá.
			 *
			 * @param Array arr
			 * @param mixin m
			 * @return Array
			 */
			function arrayToggle(arr, m)
			{
			    if (arr.indexOf(m) < 0) {
			        arr.push(m);
			    }
			    else {
			    	while(arr.indexOf(m) >= 0) {
			    		arr.splice(arr.indexOf(m), 1);
			    	}
			    }

			    return arr;
			}



			/**
			 * Odstranění duplicit
			 *
			 * @param Array a
			 * @return Array
			 */
			function arrayUnique(a)
			{
				var unique = [];
				//	pro jQuery > 1.9.1 nutné ošetření vstupu do $.each -> nesmí být string
				if (typeof a == 'string'){
					a = [a];
				}
				$.each(a, function(i, el){
				    if($.inArray(el, unique) === -1) unique.push(el);
				});

				return unique;
			}



			/**
			 * Init Function
			 */
			var init = function(select, data)
			{
				// Disable for iOS devices (their native controls are more suitable for a touch device)
				/*if (navigator.userAgent.match(/iPad|iPhone|Android|IEMobile|BlackBerry/i)) {
					return false;
				}*/

				// Element must be a select control
				if (select.tagName.toLowerCase() !== 'select') {
					return false;
				}

				select = $(select);
				//	Exists element.
				if (select.data('selectBox-control')) {
					return false;
				}

				var control = $('<a class="selectBox" />'),
					inline = select.attr('multiple__') || parseInt(select.attr('size')) > 1,
					settings = data || {};

				control.width(select.outerWidth())
					.addClass(select.attr('class'))
					.attr('title', select.attr('title') || '')
					.attr('tabindex', parseInt(select.attr('tabindex')))
					.css('display', 'inline-block')
					.bind('focus.selectBox', function() {
						if (this !== document.activeElement && document.body !== document.activeElement) $(document.activeElement).blur();
						if (control.hasClass('selectBox-active')) return;
						control.addClass('selectBox-active');
						select.trigger('focus');
					})
					.bind('blur.selectBox', function() {
						if (!control.hasClass('selectBox-active')) return;
						control.removeClass('selectBox-active');
						select.trigger('blur');
					});

				/*if (!$(window).data('selectBox-bindings')) {
					$(window).data('selectBox-bindings', true).bind('scroll.selectBox', hideMenus).bind('resize.selectBox', hideMenus);
				}*/

				if (select.attr('disabled')) {
					control.addClass('selectBox-disabled');
				}

				// Focus on control when label is clicked
				select.bind('click.selectBox', function(event) {
					control.focus();
					event.preventDefault();
				});

				// Generate control
				if (inline) {
					//
					// Inline controls
					//
					var optionControls = getOptions(select, 'inline', settings);
					control
						.append(optionControls)
						.data('selectBox-options', optionControls)
						.addClass('selectBox-inline selectBox-menuShowing')
						.bind('keydown.selectBox', function(event) {
							handleKeyDown(select, event);
						})
						.bind('keypress.selectBox', function(event) {
							handleKeyPress(select, event);
						})
						.bind('mousedown.selectBox', function(event) {
							if ($(event.target).is('A.selectBox-inline')) event.preventDefault();
							if (!control.hasClass('selectBox-focus')) control.focus();
						})
						.insertAfter(select);
					
					// Auto-height based on size attribute
					if (!select[0].style.height) {
						var size = select.attr('size') ? parseInt(select.attr('size')) : 5;
						// Draw a dummy control off-screen, measure, and remove it
						var tmp = control.clone().removeAttr('id').css({
							position: 'absolute',
							top: '-9999em'
						})
						.show()
						.appendTo('body');
						tmp.find('.selectBox-options').html('<li><a>\u00A0</a></li>');
						var optionHeight = parseInt(tmp.find('.selectBox-options A:first').html('&nbsp;').outerHeight());
						tmp.remove();
						control.height(optionHeight * size);
					}
					disableSelection(control);
				}
				else {
					//
					// Dropdown controls
					//
					var label = $('<span class="selectBox-label" />'),
						arrow = $('<span class="selectBox-arrow" />');

					// Update label
					label.attr('class', getLabelClass(select));

					var optionControls = getOptions(select, 'dropdown', settings);
					optionControls.appendTo('BODY');
					control.data('selectBox-options', optionControls)
						.addClass('selectBox-dropdown')
						.append(label)
						.append(arrow)
						.bind('mousedown.selectBox', function(event) {
							if (control.hasClass('selectBox-menuShowing')) {
								hideMenus();
							}
							else {
								event.stopPropagation();
								// Webkit fix to prevent premature selection of optionControls
								//původně event.screenX a event.screenY - nefungovalo na nadroidu
								optionControls.data('selectBox-down-at-x', event.pageX).data('selectBox-down-at-y', event.pageY);
								showMenu(select); 
							}
						})
						.bind('keydown.selectBox', function(event) {
							handleKeyDown(select, event);
						})
						.bind('keypress.selectBox', function(event) {
							handleKeyPress(select, event);
						})
						.bind('open.selectBox', function(event, triggerData) {
							if (triggerData && triggerData._selectBox === true) return;
							showMenu(select);
						})
						.bind('close.selectBox', function(event, triggerData) {
							if (triggerData && triggerData._selectBox === true) return;
							hideMenus();
						})
						.insertAfter(select);
					
					// Set label width
					var labelWidth = control.width() - arrow.outerWidth() - parseInt(label.css('paddingLeft')) - parseInt(label.css('paddingLeft'));
					label.width(labelWidth);
					disableSelection(control);
				}
				// Store data for later use and show the control
				select.addClass('selectBox')
					.data('selectBox-control', control)
					.data('selectBox-settings', settings)
					.hide();
				
				//	Redraw own list.
				select.bind('update', function() {
					$(this).val(settings.filterData(select, null, null, $(this).val()));
					refresh(this, settings)
				});

				settings.updateLabel(label, select);
				settings.updateOptions(optionControls, select);
			};



			/**
			 *	Create complete options list.
			 *	@access private
			 *	@param HTMLElementSelect
			 *	@param enum inline | dropdown
			 *	@param array
			 */
			var getOptions = function(select, type, context)
			{
				var _this = this;
				
					/**
					 * Private function to handle recursion in the getOptions function.
					 * Create one option element.
					 */
					var _getOptions = function(select, options) {
						// Loop through the set in order of element children.
						select.children('OPTION, OPTGROUP').each(function() {
							// If the element is an option, add it to the list.
							if ($(this).is('OPTION')) {
								// Check for a value in the option found.
								if ($(this).length > 0) {
									// Create an option form the found element.
									options.append(context.buildOption($(this)));
								}
								else {
									// No option information found, so add an empty.
									options.append('<li>\u00A0</li>');
								}
							}
							else {
								// If the element is an option group, add the group and call this function on it.
								var optgroup = $('<li class="selectBox-optgroup" />');
								optgroup.text($(this).attr('label'));
								options.append(optgroup);
								options = _getOptions($(this), options);
							}
						});
						// Return the built string
						return options;
					};

					switch (type) {
						//	nějaký inline seznam..??
						case 'inline':
							var optionControls = $('<ul class="selectBox-options ' + context.klass + '" />');
							optionControls = _getOptions(select, optionControls);
							optionControls
								.find('A')
								.bind('mouseover.selectBox', function(event) {
									addHover(select, $(this).parent());
								})
								.bind('mouseout.selectBox', function(event) {
									removeHover(select, $(this).parent());
								})
								.bind('mousedown.selectBox', function(event) {
									event.preventDefault(); // Prevent optionControls from being "dragged"
									if (!select.selectBox('control').hasClass('selectBox-active')) select.selectBox('control').focus();
								})
								.bind('mouseup.selectBox', function(event) {
									/// ???
									hideMenus();
									dispatch(select, $(this).parent(), event);
								});
							disableSelection(optionControls);
							return optionControls;
						
						//	dropdown seznam
						case 'dropdown':
							var optionControls = $('<ul class="selectBox-dropdown-menu selectBox-options selectBox-' + select.attr('id') + ' ' + context.klass + '" />');
							optionControls = _getOptions(select, optionControls);
							optionControls
								.data('selectBox-select', select)
								.css('display', 'none')
								.appendTo('BODY')
								.find('A')
								.bind('mousedown.selectBox', function(event) {
									event.preventDefault(); // Prevent optionControls from being "dragged"
									//původně event.screenX a event.screenY - nefungovalo na nadroidu
									if (event.pageX === optionControls.data('selectBox-down-at-x') && event.pageY === optionControls.data('selectBox-down-at-y')) {
										optionControls.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y');
										hideMenus();
									}

								})
								.bind('mouseup.selectBox', function(event) {
									//původně event.screenX a event.screenY - nefungovalo na nadroidu
									if (event.pageX === optionControls.data('selectBox-down-at-x') && event.pageY === optionControls.data('selectBox-down-at-y')) {
										return;
									}
									else {
										optionControls.removeData('selectBox-down-at-x').removeData('selectBox-down-at-y');
									}
									
									if (!dispatch(select, $(this).parents('li'), event)) {
										if (! select.attr('multiple')) {
											hideMenus();
										}
									}
								})
								.bind('mouseover.selectBox', function(event) {
									addHover(select, $(this).parent());
								})
								.bind('mouseout.selectBox', function(event) {
									removeHover(select, $(this).parent());
								})
								//	Zprava, že byl zaškrtnut řádek.
								.bind('select.selectBox', function(event, option) {
									event.ctrlKey = option.ctrlKey;
									dispatch(select, $(this).parents('li'), event);
								});

							//	Má se překreslit prvek li.
							optionControls
								.find('li')
								.bind('toggled.selectBox', function(event, options) {
									context.toggleOption(select, this, options);
								});


							//	Překreslení podle stavu modelu.
							select.bind('change', function() {
								if (!$(this).val()){
									$(this).val(0);
								}
								//	Nic, nebo zvolený null prvek.
								if (! $(this).val()) {
									optionControls.find('li').each(function() {
										if ($(this).find('a').attr('rel') == 0) {
											$(this).addClass('selectBox-selected');
										}
										else {
											$(this).removeClass('selectBox-selected');											
										}
									});
								}
								else if ($(this).val().length < 2) {
									optionControls.find('li').each(function() {
										if ($(this).find('a').attr('rel') == 0) {
											$(this).removeClass('selectBox-selected');
										}
									});
								}

							});


							// Inherit classes for dropdown menu
							var classes = select.attr('class') || '';
							if (classes !== '') {
								classes = classes.split(' ');
								for (var i in classes) optionControls.addClass(classes[i] + '-selectBox-dropdown-menu');
							}
							disableSelection(optionControls);
							return optionControls;

					}
				};
			
			

			var getLabelClass = function(select)
			{
				var selected = $(select).find('OPTION:selected');
				return ('selectBox-label ' + (selected.attr('class') || '')).replace(/\s+$/, '');
			};
				
				
				
			var destroy = function(select) {
				select = $(select);
				var control = select.data('selectBox-control');
				if (!control) return;
				var optionControls = control.data('selectBox-options');
				optionControls.remove();
				control.remove();
				select.removeClass('selectBox')
					.removeData('selectBox-control')
					.data('selectBox-control', null)
					.removeData('selectBox-settings')
					.data('selectBox-settings', null)
					.show();
			};


			/**
			 *	Aktualizeuje selectbox podle předlohy nativního selectu.
			 *	@access private
			 *	@param HTMLElementSelect
			 *	@param array
			 */
			var refresh = function(select, settings)
			{
				select = $(select);

				//	Je zajímavé, že když zapíšeme do .val() klíče bez 0 prvku, tak v OPTION bude stejně prvek 0 zaškrtnut.
				var v = select.val() || [];
				if (v.indexOf("0") < 0) {
					select.find('OPTION[value="0"]').attr("selected", null);
				}

				select.selectBox('options', select.html(), settings);
			};


			var showMenu = function(select) {
				select = $(select);
				var control = select.data('selectBox-control'),
					settings = select.data('selectBox-settings'),
					options = control.data('selectBox-options');
				if (control.hasClass('selectBox-disabled')) return false;
				hideMenus();
				var borderBottomWidth = isNaN(control.css('borderBottomWidth')) ? 0 : parseInt(control.css('borderBottomWidth'));
				// Menu position
				
				options
					.css({
						'min-width': control.innerWidth(),
						top: control.offset().top + control.outerHeight() - borderBottomWidth,
						left: control.offset().left
					});

				if (select.triggerHandler('beforeopen')) return false;
				function dispatchOpenEvent() {
					select.triggerHandler('open', {
						_selectBox: true
					});
				};

				// Show menu
				switch (settings.menuTransition) {
					case 'fade':
						options.fadeIn(settings.menuSpeed, dispatchOpenEvent);
						break;
					case 'slide':
						options.slideDown(settings.menuSpeed, dispatchOpenEvent);
						break;
					default:
						options.show(settings.menuSpeed, dispatchOpenEvent);
						break;
				}

				if (!settings.menuSpeed) {
					dispatchOpenEvent();
				}

				// Center on selected option
				var li = options.find('.selectBox-selected:first');
				keepOptionInView(select, li, true);
				addHover(select, li);
				control.addClass('selectBox-menuShowing');
				$(document).bind('mousedown.selectBox', function(event) {
					if ($(event.target).parents().andSelf().hasClass('selectBox-options')) {
						return;
					}
					hideMenus();
				});
			};


			/**
			 *	Hide js generated menu.
			 *	@access private
			 */
			var hideMenus = function() {
				if ($(".selectBox-dropdown-menu:visible").length === 0) {
					return;
				}
				
				$(document).unbind('mousedown.selectBox');
				
				$(".selectBox-dropdown-menu").each(function() {
					var options = $(this),
						select = options.data('selectBox-select'),
						control = select.data('selectBox-control'),
						settings = select.data('selectBox-settings');
					if (select.triggerHandler('beforeclose')) {
						return false;
					}
					var dispatchCloseEvent = function() {
							select.triggerHandler('close', {
								_selectBox: true
							});
						};
					if (settings) {
						switch (settings.menuTransition) {
							case 'fade':
								options.fadeOut(settings.menuSpeed, dispatchCloseEvent);
								break;
							case 'slide':
								options.slideUp(settings.menuSpeed, dispatchCloseEvent);
								break;
							default:
								options.hide(settings.menuSpeed, dispatchCloseEvent);
								break;
						}
						if (!settings.menuSpeed) dispatchCloseEvent();
						control.removeClass('selectBox-menuShowing');
					}
					else {
						$(this).hide();
						$(this).triggerHandler('close', {
							_selectBox: true
						});
						$(this).removeClass('selectBox-menuShowing');
					}
				});
			};



			/**
			 * Vybere řádek. Rozhodne, zda bude přidán, nebo odebrán. Rozhoduje jakým způsobem se pracuje.
			 * Zpracování nativních událostí.
			 *
			 * @param HTMLSelectElement
			 * @param HTMLLiElement li Řádek, na který bylo kliknuto.
			 * @param Event
			 */
			function dispatch(select, li, event)
			{
				select = $(select);
				li = $(li);
				var control = select.data('selectBox-control'),
					settings = select.data('selectBox-settings'),
					//	Zda po zvolení prvku zavřít menu.
					letOpenedMenu = false,
					selection = arrayUnique(select.val() || []),
					curr = li.find('A').attr('rel'),
					triggerData = {
						'option': li
					};

				if (control.hasClass('selectBox-disabled')) {
					return false;
				}
				
				if (li.length === 0 || li.hasClass('selectBox-disabled')) {
					return false;
				}
					
				if (select.attr('multiple')) {
					// If event.shiftKey is true, this will select all options between li and the last li selected
					if (event.shiftKey && control.data('selectBox-last-selected')) {
						selection = arrayToggle(selection, curr);
						letOpenedMenu = true
						triggerData.ctrlKey = true;
						triggerData.shiftKey = true;
					}
					//	Zaškrtnutí se stisknutým <ctrl>
					else if ((isMac && event.metaKey) || (!isMac && event.ctrlKey) || settings.permanentCtrlKey) {
						selection = arrayToggle(selection, curr);
						letOpenedMenu = true
						triggerData.ctrlKey = true;
					}
					//	Nestisknut <ctrl>
					else {
						selection = []
						selection.push(curr);
						triggerData.ctrlKey = false;
					}
				}
				else {
					selection = []
					selection.push(curr);
				}

				// Update original control's value
				if (select.attr('multiple')) {
					if (curr > 0) {
						while (selection.indexOf("0") >= 0) {
							selection.splice(selection.indexOf("0"), 1);
						}
					}
					else {
						selection = ["0"]
						triggerData.ctrlKey = false;
					}
				}

				//	Profiltrovat, které prvky zaškrtneme.
				selection = settings.filterData(select, curr, triggerData, selection);

				triggerData.selection = selection;

				// Remember most recently selected itemreturn false;
				control.data('selectBox-last-selected', li);

				// Change callback Propsání do inputu.
				updateData(select, selection);

				//	Zapsání do labelu selectBoxu - TODO přesunout do posluchače toggled
				if (control.hasClass('selectBox-dropdown')) {
					settings.updateLabel(control.find('.selectBox-label'), select);
					settings.updateOptions(control.find('.selectBox-options'), select);
				}
				
				//	Rozšířit informaci o tom, že se element změnil. Například za učelem překreslení.
				li.trigger('toggled.selectBox', triggerData);

				return letOpenedMenu;
			};



			/**
			 * Zapíše data do mateřského selectu. Emituje zprávu 'change'.
			 *
			 * @param HTMLSelectElement control Cílový element.
			 * @param array selection Jeden nebo více prvků zapsaných do selectu.
			 *
			 * @return boolean
			 */
			var updateData = function(control, selection)
			{
				control = $(control);
				if (control.val() !== selection) {
					control.val(selection);

					//	Vytvoříme si vlastní událost, a odešleme klasickou.
					control.trigger('change.selectBox');
					control.trigger('change');
				}

				return true;
			};



			var addHover = function(select, li)	{
				select = $(select);
				li = $(li);
				var control = select.data('selectBox-control'),
					options = control.data('selectBox-options');
				options.find('.selectBox-hover').removeClass('selectBox-hover');
				li.addClass('selectBox-hover');
			};



			var removeHover = function(select, li) {
				select = $(select);
				li = $(li);
				var control = select.data('selectBox-control'),
					options = control.data('selectBox-options');
				options.find('.selectBox-hover').removeClass('selectBox-hover');
			};



			/**
			 * ???
			 */
			var keepOptionInView = function(select, li, center)	{
				if (!li || li.length === 0) return;
				select = $(select);
				var control = select.data('selectBox-control'),
					options = control.data('selectBox-options'),
					scrollBox = control.hasClass('selectBox-dropdown') ? options : options.parent(),
					top = parseInt(li.offset().top - scrollBox.position().top),
					bottom = parseInt(top + li.outerHeight());
				if (center) {
					scrollBox.scrollTop(li.offset().top - scrollBox.offset().top + scrollBox.scrollTop() - (scrollBox.height() / 2));
				} else {
					if (top < 0) {
						scrollBox.scrollTop(li.offset().top - scrollBox.offset().top + scrollBox.scrollTop());
					}
					if (bottom > scrollBox.height()) {
						scrollBox.scrollTop((li.offset().top + li.outerHeight()) - scrollBox.offset().top + scrollBox.scrollTop() - scrollBox.height());
					}
				}
			};



			var handleKeyDown = function(select, event) {
					//
					// Handles open/close and arrow key functionality
					//
					select = $(select);
					var control = select.data('selectBox-control'),
						options = control.data('selectBox-options'),
						settings = select.data('selectBox-settings'),
						totalOptions = 0,
						i = 0;
					if (control.hasClass('selectBox-disabled')) return;
					switch (event.keyCode) {
					case 8:
						// backspace
						event.preventDefault();
						typeSearch = '';
						break;
					case 9:
						// tab
					case 27:
						// esc
						hideMenus();
						removeHover(select);
						break;
					case 13:
						// enter
						if (control.hasClass('selectBox-menuShowing')) {
							if (!dispatch(select, options.find('LI.selectBox-hover:first'), event) && control.hasClass('selectBox-dropdown')) {
								hideMenus();
							}
						}
						else {
							showMenu(select);
						}
						break;
					case 38:
						// up
					case 37:
						// left
						event.preventDefault();
						if (control.hasClass('selectBox-menuShowing')) {
							var prev = options.find('.selectBox-hover').prev('LI');
							totalOptions = options.find('LI:not(.selectBox-optgroup)').length;
							i = 0;
							while (prev.length === 0 || prev.hasClass('selectBox-disabled') || prev.hasClass('selectBox-optgroup')) {
								prev = prev.prev('LI');
								if (prev.length === 0) {
									if (settings.loopOptions) {
										prev = options.find('LI:last');
									} else {
										prev = options.find('LI:first');
									}
								}
								if (++i >= totalOptions) break;
							}
							addHover(select, prev);
							dispatch(select, prev, event);
							keepOptionInView(select, prev);
						} else {
							showMenu(select);
						}
						break;
					case 40:
						// down
					case 39:
						// right
						event.preventDefault();
						if (control.hasClass('selectBox-menuShowing')) {
							var next = options.find('.selectBox-hover').next('LI');
							totalOptions = options.find('LI:not(.selectBox-optgroup)').length;
							i = 0;
							while (next.length === 0 || next.hasClass('selectBox-disabled') || next.hasClass('selectBox-optgroup')) {
								next = next.next('LI');
								if (next.length === 0) {
									if (settings.loopOptions) {
										next = options.find('LI:first');
									} else {
										next = options.find('LI:last');
									}
								}
								if (++i >= totalOptions) break;
							}
							addHover(select, next);
							dispatch(select, next, event);
							keepOptionInView(select, next);
						} else {
							showMenu(select);
						}
						break;
					}
				};



			var handleKeyPress = function(select, event) {
					//
					// Handles type-to-find functionality
					//
					select = $(select);
					var control = select.data('selectBox-control'),
						options = control.data('selectBox-options');
					if (control.hasClass('selectBox-disabled')) {
						return;
					}
					switch (event.keyCode) {
						case 9:
							// tab
						case 27:
							// esc
						case 13:
							// enter
						case 38:
							// up
						case 37:
							// left
						case 40:
							// down
						case 39:
							// right
							// Don't interfere with the keydown event!
							break;
						default:
							// Type to find
							if (!control.hasClass('selectBox-menuShowing')) showMenu(select);
							event.preventDefault();
							clearTimeout(typeTimer);
							typeSearch += String.fromCharCode(event.charCode || event.keyCode);
							options.find('A').each(function() {
								if ($(this).text().substr(0, typeSearch.length).toLowerCase() === typeSearch.toLowerCase()) {
									addHover(select, $(this).parent());
									keepOptionInView(select, $(this).parent());
									return false;
								}
							});

							// Clear after a brief pause
							typeTimer = setTimeout(function() {
								typeSearch = '';
							}, 1000);
							break;
					}
				};

				
				
			var enable = function(select) {
				select = $(select);
				select.attr('disabled', false);
				var control = select.data('selectBox-control');
				if (!control) return;
				control.removeClass('selectBox-disabled');
			};
				

				
			var disable = function(select) {
				select = $(select);
				select.attr('disabled', true);
				var control = select.data('selectBox-control');
				if (!control) return;
				control.addClass('selectBox-disabled');
			};



			/**
			 * Přiřazení hodnoty.
			 * @param ?? select
			 * @param ?? value
			 */
			var setValue = function(select, value)
			{
				select = $(select);
				select.val(value);
				value = select.val(); // IE9's select would be null if it was set with a non-exist options value
				if (value === null) { // So check it here and set it with the first option's value if possible
					value = select.children().first().val();
					select.val(value);
				}
				var control = select.data('selectBox-control');
				if (!control) return;
				var settings = select.data('selectBox-settings'),
					options = control.data('selectBox-options');

				// Update label
				context.updateLabel(control.find('.selectBox-label'), select);
				context.updateOptions(control.data('selectBox-options'), select);

				// Update control values
				options.find('.selectBox-selected').removeClass('selectBox-selected');
				options.find('A').each(function() {
					if (typeof(value) === 'object') {
						for (var i = 0; i < value.length; i++) {
							if ($(this).attr('rel') == value[i]) {
								$(this).parent().addClass('selectBox-selected');
							}
						}
					} else {
						if ($(this).attr('rel') == value) {
							$(this).parent().addClass('selectBox-selected');
						}
					}
				});

				if (context.change) {
					context.change.call(select);
				}
			};



			/**
			 * @param select Ctrl SELECT
			 * @param data 
			 * @param this Context
			 */
			var buildOptions = function(select, data, context)
			{
				var select = $(select),
					control = select.data('selectBox-control'),
					context = select.data('selectBox-settings');
					
				if (!control) {
					return;
				}

				switch (typeof(data)) {
					case 'string':
						select.html(data);
						break;

					case 'object':
						select.html('');
						for (var i in data) {
							if (data[i] === null) {
								continue;
							}

							if (typeof(data[i]) === 'object') {
								var optgroup = $('<optgroup label="' + i + '" />');
								for (var j in data[i]) {
									optgroup.append('<option value="' + j + '">' + data[i][j] + '</option>');
								}
								select.append(optgroup);
							}
							else {
								select.append($('<option value="' + i + '">' + data[i] + '</option>'));
							}
						}
						break;
				}

				// Remove old options
				control.data('selectBox-options').remove();
				
				// Generate new options
				var type = control.hasClass('selectBox-dropdown') ? 'dropdown' : 'inline',
					options = getOptions(select, type, context);

				control.data('selectBox-options', options);
				switch (type) {
					case 'inline':
						control.append(options);
						break;

					case 'dropdown':
						// Update label
						$("BODY").append(options);
						context.updateLabel(control.find('.selectBox-label'), select);
						context.updateOptions(control.data('selectBox-options'), select);
						break;
				}
				//context._doneInit(select);
			};



			var disableSelection = function(selector) {
				$(selector).css('MozUserSelect', 'none').bind('selectstart', function(event) {
					event.preventDefault();
				});
			};




			/**
			 *	Defaultní konfigurace pluginu. Obsahuje zároveň předefinovatelné methody.
			 */
			this.defaults = {
				version: '0.1-selectBox',
				updateLabel: selectBox.prototype.updateLabel,
				updateOptions: selectBox.prototype.updateOptions,
				toggleOption: selectBox.prototype.toggleOption,
				getLabelText: selectBox.prototype.getLabelText,
				filterData: selectBox.prototype.filterData,
				buildOption: selectBox.prototype.buildOption
			};






			//
			// Public methods
			//
			var _this = this;
			switch (method) {
			case 'control':
				return $(this).data('selectBox-control');
			case 'settings':
				if (!data) return $(this).data('selectBox-settings');
				$(this).each(function() {
					$(this).data('selectBox-settings', $.extend(true, $(this).data('selectBox-settings'), data));
				});
				break;
			case 'options':
				// Getter
				if (data === undefined) {
					return $(this).data('selectBox-control').data('selectBox-options');
				}
				// Setter
				$(this).each(function() {
					buildOptions(this, data, context);
				});
				break;
			case 'value':
				// Empty string is a valid value
				if (data === undefined) return $(this).val();
				$(this).each(function() {
					setValue(this, data);
				});
				break;
			case 'refresh':
				$(this).each(function() {
					refresh(this);
				});
				break;
			case 'enable':
				$(this).each(function() {
					enable(this);
				});
				break;
			case 'disable':
				$(this).each(function() {
					disable(this);
				});
				break;
			case 'destroy':
				$(this).each(function() {
					destroy(this);
				});
				break;
			case 'hideMenus':
				$(this).each(function() {
					hideMenus(this);
				});
				break;
			default:
				$(this).each(function() {
					init(this, $.fn.extend(_this.defaults, method || {}), data);
//					selectBox.prototype.init.call(this, $.fn.extend(_this.defaults, method || {}), data);
				});
				break;
			}
			return $(this);
		}



		/**
		 * Zapíše hlavičku select boxu podle modelu.
		 *
		 * @param HTMLElement label
		 * @param HTMLSelectElement select
		 */
		selectBox.prototype.updateLabel = function(label, select)
		{
			var a = [];
			$(select).find("option:selected").each(function () {
				a[a.length] = $(this).text();
			});
			label.text(a.join(', ') || '\u00A0');
		};



		/**
		 * Zaktualizuje options select boxu podle modelu. Zaškrtnuté položky jsou zaškrtnuté jen vizuelně, nemusí odpovídat modelu.
		 *
		 * @param HTMLElement options Wraper selecboxu
		 * @param HTMLSelectElement select Original select.
		 */
		selectBox.prototype.updateOptions = function(options, select)
		{	
			
			//	Reset původních hodnot
			options.find('A').removeClass('selectBox-selected');

			//	Nastavení podle modelu
			//	pro jQuery > 1.9.1 nutné ošetření vstupu do $.each -> nesmí být string
			$elms = $(select).val() || [];
			if (typeof $elms == 'string'){
				$elms = [$elms];
			}
			$.each($elms, function(i, m) {			
				options.find('A[rel="' + m + '"]').parents('LI').addClass('selectBox-selected');
			})

		};



		/**
		 * Vizuelní přepnutí stavu optionu.
		 *
		 * @param HTMLElementSelect control Zdrojový model.
		 * @param HTMLLiElement li
		 * @param object opts
		 * @deprecated
		 */
		selectBox.prototype.toggleOption = function(control, li, opts)
		{
			select = $(control);
			li = $(li);

			var control = select.data('selectBox-control');

			if (select.attr('multiple')) {
				// If event.shiftKey is true, this will select all opts between li and the last li selected
				if (opts.shiftKey && control.data('selectBox-last-selected')) {
					li.toggleClass('selectBox-selected');

					var affectedOptions;
					
					if (li.index() > control.data('selectBox-last-selected').index()) {
						affectedOptions = li.siblings().slice(control.data('selectBox-last-selected').index(), li.index());
					}
					else {
						affectedOptions = li.siblings().slice(li.index(), control.data('selectBox-last-selected').index());
					}
					
					affectedOptions = affectedOptions.not('.selectBox-optgroup, .selectBox-disabled');
					
					if (li.hasClass('selectBox-selected')) {
						affectedOptions.addClass('selectBox-selected');
					}
					else {
						affectedOptions.removeClass('selectBox-selected');
					}
				}
				//	Zaškrtnutí se stisknutým <ctrl>
				else if (opts.ctrlKey) {
					var v = li.find('a').attr('rel');
					li.parent().find('li a[rel="' + v + '"]').each(function() {
						$(this).parents('li').toggleClass('selectBox-selected');
					});

				}
				//	Nestisknut <ctrl>
				else {
					li.siblings().removeClass('selectBox-selected');
					li.addClass('selectBox-selected');
				}
				
				//	Odstranit 0, pokud není v datech.
				if (opts.selection.length > 0 && opts.selection.indexOf('0') < 0) {
					li.parent().find('li a[rel="0"]').each(function() {
						$(this).parents('li').removeClass('selectBox-selected');
					});
				}
			}
			else {
				li.siblings().removeClass('selectBox-selected');
				li.addClass('selectBox-selected');
			}
		};



		/**
		 * Text labelu. 
		 *
		 * @param HTMLSelectElement 
		 *
		 * @return string
		 */
		selectBox.prototype.getLabelText = function(select)
		{
			var selected = $(select).find('OPTION:selected');
			return selected.text() || '\u00A0';
		};



		/**
		 * Vytvoření položky seznamu.
		 *
		 * @param HTMLOptionElement option
		 *
		 * @return HTMLLiELement Element of select row.
		 */
		selectBox.prototype.buildOption = function(option)
		{
			var li = $('<li />'),
				a = $('<a />');
			li.addClass(option.attr('class'));
			li.data(option.data());
			a.attr('rel', option.val()).text(option.text());
			li.append(a);
			if (option.attr('disabled')) li.addClass('selectBox-disabled');
			if (option.attr('selected')) li.addClass('selectBox-selected');
			return li;
		};



		/**
		 * Rozhodujem, které hodnoty skutečně propíšeme do modelu. Například když zapisujem 0, tak všechny ostatní
		 * odškrtnu. A naopak, když zaškrtnu jinou než 0, tak 0 vyhodím ze seznamu. V multiSelectu je to ještě složitější.
		 *
		 * @param control Model, do kterého zapisujeme prvky.
		 * @param option int Který prvek byl změněn.
		 * @param triggerData 
		 * @param selection Hodnoty zvolených prvků.
		 *
		 * @return [] of ids
		 */
		selectBox.prototype.filterData = function(control, option, triggerData, selection)
		{
			if (selection.indexOf("0") >= 0) {
				selection.splice(selection.indexOf("0"), 1)
			}
			return selection;	
		};



		return selectBox;
	})();



})(jQuery);
