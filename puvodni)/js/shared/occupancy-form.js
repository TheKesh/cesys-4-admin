
;(function ($, window, document, undefined) {
	$.widget('cesys.occupancyForm', {
		/**
		 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
		 * $(selector).occupancyForm({
		 * 		optionName : optionValue
		 * });
		 */
		options: {
			//zamknutí změn
			disableChanges : false,

			//výchozí počty dospělých a dětí
			adultCount : 2,
			childCount : 0,
			ageChild : [2, 2, 2, 2]
		},

		/**
		 * Konstruktor - načtení seznamů, bindování funkcí
		 */
		_create : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn

			//options z atributů
			if (typeof element.data('disable-changes') != "undefined"){
				options.disableChanges = (element.data('disable-changes') == "1");
			}
			if (typeof element.data('adult-count') != "undefined"){
				options.adultCount = parseInt(element.data('adult-count'));
			}
			if (typeof element.data('child-count') != "undefined"){
				options.childCount = parseInt(element.data('child-count'));
			}
			for (var i = 1; i <= 4; i++) {
				if (typeof element.data('age-child-'+i) != "undefined"){
					options.ageChild[i] = parseInt(element.data('age-child-'+i));
				}
			};

			//počet dospělých
			self._adultCount = $(element).find('#count_adult').val(self.options.adultCount);

			//počet dětí
			self._childCount = $(element).find('#count_child').val(self.options.childCount);

			//věky dětí
			self._ageChild = [];
			for (var i = 1; i <= 4; i++) {
				self._ageChild[i] = $(element).find('#age_child_'+i).val(self.options.ageChild[i]);
			};

			//sekce s věky dětí
			self._childAgesSection = $(element).find('.child-ages');

			//zobrazení selektů věků dle počtu dětí
			self._childCount.bind('change', function(){
				self._displayChildAges($(this).val());
			});

			//na změnu počtů a věků dětí spustíme událost a předáme data ven
			self._childCount
				.add(self._adultCount)
				.add(self._ageChild[1])
				.add(self._ageChild[2])
				.add(self._ageChild[3])
				.add(self._ageChild[4])
				.bind(
					'change',
					function(){
						element.trigger('occupancyChanged', self.getData());
					}
				);

			//spustíme change u dětí - správně zobrazí selekty a zároveň pošle ven z pluginu info o vybraných počtech při načtení stránky
			self._childCount.trigger('change');

			//dle options zakážeme změny
			if (options.disableChanges) {
				self._disableChanges();
			}
		},


		/**
		 * Vyresetuje nastavené věky dětí
		 * /
		_resetChildAges : function(){
			var self = this;

			self._ageChild1.val(0);
			self._ageChild2.val(0);
			self._ageChild3.val(0);
			self._ageChild4.val(0);
		},

		/**
		 * Zobrazí správný počet selektů pro výběr věku dětí, ostatní Vyresetuje
		 */
		_displayChildAges : function(childCount){
			var self = this;

			for (var i = 1; i <= 4; i++) {
				var select = self._ageChild[i];

				if (i <= childCount) {
					select.closest('.cell').show();
				}else{
					select.closest('.cell').hide();
					select.val(2);
				}
			}

			if (childCount == 0){
				self._childAgesSection.hide();
			}else{
				self._childAgesSection.show();
			}
		},

		/**
		 * Zakázat úpravy
		 */
		_disableChanges : function(){
			var self = this;

			self._adultCount.prop('disabled', 'disabled');
			self._childCount.prop('disabled', 'disabled');

			$.each(self._ageChild, function(index, element){
				$(element).prop('disabled', 'disabled');
			})

			self.element.prepend($('<p>', {
				'class' : 'fixed-occupancy',
				text : cesys.ts('The date and prices are valid only for selected occupancy. To change the occupancy, please return to previous steps.')
			}));
		},


		/**
		 * Vytvoření objektu s aktuálně nastavenou obsazeností
		 */
		getData : function(shortcut){
			var self = this;
			var data = {};
			if (typeof shortcut !== "undefined" && shortcut === true) {
                data.ac = parseInt(self._adultCount.val());
                data.cc = parseInt(self._childCount.val());
                data.ca = null;
                if (data.cc > 0){
                    data.ca = {};
                    for (var i = 1; i <= data.cc; i++) {
                        data.ca[i] = parseInt(self._ageChild[i].val());
                    }
                }
			} else {
                data.adultCount = parseInt(self._adultCount.val());
                data.childCount = parseInt(self._childCount.val());
                data.childAges = null;
                if (data.childCount > 0){
                    data.childAges = {};
                    for (var i = 1; i <= data.childCount; i++) {
                        data.childAges[i] = parseInt(self._ageChild[i].val());
                    }
                }
			}

			return data;
		}

	});

})(jQuery, window, document);
