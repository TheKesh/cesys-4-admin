/**
 * Plugin pro výběr zemí a destiancí ve vyhledávači
 * - zaškrtnuté checkboxy u zemí odpovídají vybraným zemím, kdežto zaškrtnuté checkboxy u destinací zobrazují jen v čem se bude na serveru hledat, ve skutečnosti se posílají
 *   jen některá id destinací - vybrané ukládáme pomocí .data('checked', true) do elementů <li>
 * - předáním url a selektorů v options se dá zprovoznit nad téměř libovolným html - není závislý na dialogu ani na tom, kam se výběr propisuje
 * - seznam věcí, které se dají nastavit je v komentářích v options
 * 
 * @author Vladimír Starec
 * 
 * Formát dat pro načtení destinací:
 * [
 * 	 {
 * 	 	"Destination":{
 * 	 		"id":"1",
 * 	 		"name":"Alb\u00e1nie",
 * 	 		"parent_id":null,
 * 	 		"country_id":"3",
 * 	 		"depth":0
 * 	 		}
 * 	 	},
 * 	 	{
 * 	 	"Destination":{
 * 	 		"id":"1571",
 * 	 		"name":"Anguilla",
 * 	 		"parent_id":null,
 * 	 		"country_id":"9",
 * 	 		"depth":0
 * 	 	},
 * 	 	...
 * 	 }
 * ]
 *
 *
 * Formát dat pro načtení zemí:
 * {
 * 	 "top_countries":[
 * 	 	{
 * 	 		"id":15,
 * 	 		"name":"Austr\u00e1lie"
 * 	 	},
 * 	 	{
 * 	 		"id":31,
 * 	 		"name":"Braz\u00edlie"
 * 	 	}
 * 	 ],
 * 	 "all_countries":[
 * 	 	{
 * 	 		"id":3,
 * 	 		"name":"Alb\u00e1nie"
 * 	 	},
 * 	 	{
 * 	 		"id":9,
 * 	 		"name":"Anguilla"
 * 	 	}
 * 	 ]
 * } 
 */


(function($) {
$.widget('cesys.destinationsSelector', {
	/**
	 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
	 * $(selector).destinationsSelector({
	 * 		optionName : optionValue
	 * });
	 */
	options: {
		//url pro nahrání destinací
		destinationsListUrl	: '/admin/Searches/js_destination_list/',

		//url pro nahrání zemí
		countriesListUrl : '/admin/Searches/js_countries_list/',

		//selektor textového pole filtru
		filterElement : '#modalSelectorFilter',

		//selektor elementu pro seznam zemí
		countriesListElement : '#countriesList',

		//selektor elementu pro seznam destinací
		destinationsListElement : '#destinationsList',

		//selektor elementu pro zobrazování seznamu zaškrtlých zemí
		selectedCountriesElement : '#selectedCountriesList',

		//selektor tlačítka pro vyčištění filtru
		clearFilterElement : '#clearFilter',

		//selektor elementu pro zobrazení info o počtu vybraných zemí
		labelElement  : '.modalDestinationSelectorOpener',

		//text labelu
		//#C je nahrazeno buď názvy zemí, pokud jich je vybraných méně než 'labelCountriesCount', nebo počtem vybraných zemí
		//#D je nahrazeno počtem vybraných destinací
		labelText : 'Vybrané země: #C, dest: #D',

		//text pro zobrazení počtu vybraných u sbalené hlavní destinace
		//znak # je nahrazen počtem zaškrtnutých
		selectedCountText : '(vybráno: #)',

		//text, který se bude zobrazovat před seznamem vybraných zemí
		selectedCountriesText : 'Vybrané země: ',

		//tento text se zobrazí, pokud se pomocí filtru nenajde žádná destinace
		nothingFoundText : 'Zadanému výrazu neodpovídá žádná destinace. Pro zobrazení destinací odeberte slovo z filtru.',

		//počet zemí do kterého se budou země na labelu zobrazovat pomocí názvů, potom jen počtem
		labelCountriesCount : 2,

		//zapnut/vypnutí sbalování na destinacích
		useDestinationsAccordion : true,

		//předvybrané země
		// - seznam ID v poli, např. ["41","48","183","195"]	
		preselectedCountries : [],

		//předvybrané destinace
		// - seznam ID v poli, např. ["60","142","539","549"]
		preselectedDestinations : [],

		//názvy oddílů v seznamu zemí - oblíbené, abecední seznam apod.
		countriesListsNames : []
	},

	/**
	 * Konstruktor - načtení seznamů, bindování funkcí
	 */
	_create : function(){
		var self = this,  //instance pluginu
			o = this.options, //options
			parentElement = this.element, //element, na kterém je plugin spuštěn
			filter = parentElement.find(o.filterElement), //textové pole filtru
			selectedList = $(o.selectedCountriesElement), //seznam vybraných zemí - neni hledan skrze parentElement, protoze neni uvnitr nej
			clearFilter = parentElement.find(o.clearFilterElement); //tlačítko pro vyčištění filtru

		//stav sestavení seznamů
		this._destinationsListBuilt = false;
		this._countriesListBuilt = false;

		//stav předvybrání zemí/destinací
		this._preselectDone = false;

		//pomocná proměnná s uloženou maximální délkou některého ze vstupního seznamu zemí
		//nastaveno ve funkci buildCountriesMenu
		//použito ve funkci _preselect - pokud je délka jedna, víme, že je v seznamu jen jedna země a můžeme ji vybrat
		//nelze předat parametrem kvůli asynchronímu volání _preselect, nenapadl mě jiný jednoduchý způsob aniž bych musel vždy procházet všechny země
		this._maxCountriesListLength = 0 ;
		
		//přidání tetxu do seznamu vybraných zemí
		selectedList.prepend(o.selectedCountriesText);

		// stavba menu
		self._buildDestinationsMenu();
		self._buildCountriesMenu();

		//update labelu
		self._updateLabel();

		//timeout pro keyup handler ve filtru
		self._filterTimeout = null;

		//zprovoznění filtru - spuštění filtrování s prodlevou 150ms
		//když píše člověk rychle, spustí filtr až nad celým textem
		filter.keyup(function(e){
			if(self._filterTimeout != null){
				clearTimeout(self._filterTimeout);  
			}

  			self._filterTimeout = setTimeout(
				function(){	self._runFilter(); },
				150
			); 			
		})
		//počáteční vyčištění políčka filtru - po reloadu tam zůstává text
		.val('');
		

		//bind vyčištění filtru na tlačítko
		clearFilter.click(function(event){
			self._clearFilter();
			event.preventDefault();
		});
	},



	/**
	 * Sestaví seznam zemí
	 *  - data načte v JSON z url z options
	 */
	_buildCountriesMenu : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement);

			//vyčištění seznamu - kvůli validitě XHTML je nutné renderovat s jednou prázdnou položkou
			countriesList.empty();

			//ukládáme délku nejdelšího seznamu zemí
			maxListLength = 0;

			$.ajax({
				type:'POST',
				url: o.countriesListUrl,
				dataType: "json",
				success : function(data) {
					//vracíme objekt s jedním nebo více seznamy zemí (oblíbené + všechny země)
					//projdeme seznamy
					var i = 0;
					$.each(data, function(key, list){
						//ukládáme délku nejdelšího seznamu
						if (list.length && list.length > maxListLength){
							maxListLength = list.length;
						}

						//nejprve přidáme název listu = oddělovač
						if (o.countriesListsNames.length && i < o.countriesListsNames.length){
							var name = o.countriesListsNames[i];
							countriesList.append($('<div>',{
								'class' : 'listSeparator',
								'text' : name
							}));
						}
						//pro jednotlivé seznamy projdeme jednotlivé země
						//seznam sestavíme jako jeden string a vložíme najednou kvůli výkonu
						var stringToAppend = '';
						for (var j = 0; j < list.length; j++) {
							var country = list[j];
							var id = country.id;
							var name = country.name;

							//input
							stringToAppend += '<li data-country="' + id + '" data-search-text="' +  self._prepareSearchText(name) + '" data-orig-text="' + name + '">';
							stringToAppend += 	'<label>';
							stringToAppend += 		'<input type="checkbox" value="' + id + '" data-country="' + id + '" />';
							stringToAppend += 		'<span class="itemText">' + name + '</span>';
							stringToAppend += 	'</label>';
							stringToAppend += '</li>';

						};//end for each item in list
						
						//vložení + nabindování událostí
						countriesList
							.append(stringToAppend)
							.find('input').bind('change', function(){
								self._checkCountryHandler(this);
							});

						i = i + 1;

					});//end for each list in data
					
					//uložení maximálního počtu zemí
					self._maxCountriesListLength = maxListLength;

					//uložení informace, že byl seznam sestaven
					self._countriesListBuilt = true;

					//nastavení předvybraných hodnot
					self._preselect();

				}//success end
			});
	},


	/**
	 * Sestaví seznam destinací
	 *  - data načte v JSON z url z options
	 */
	_buildDestinationsMenu : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			destinationsList = parentElement.find(o.destinationsListElement);

			//vyčištění seznamu - kvůli validitě XHTML je nutné renderovat s jednou prázdnou položkou
			destinationsList.empty();
			
			$.ajax({
				type:'POST',
				url: o.destinationsListUrl,
				dataType: "json",
				success : function(data) {
					//destinace sestavíme jako jeden string a vložíme najednou kvůli výkonu
					var stringToAppend = '';
					for (var j = 0; j < data.length; j++) {
						var destination = data[j];
						var destination = destination['Destination'],
							destDepth = parseInt(destination['depth']),
							destName = destination['name'],
							destId = parseInt(destination['id']),
							countryId = parseInt(destination['country_id']),
							parentId = parseInt(destination['parent_id']);

						var extraParams = '';
						var input = '';
						if (destDepth === 0){
							extraParams = 'class="masterDestination"';
						}else{
							extraParams = 'class="slaveDestination" data-slave-depth="' + destDepth + '" data-slave-parent="' + parentId +'"';
							input = '<input type="checkbox" value="' + destId + '" data-destination="' + destId + '" data-country="' + countryId + '" />';
						}

						stringToAppend += '<li '+ extraParams +' data-destination="' + destId + '" data-country="' + countryId + '" data-search-text="'+ self._prepareSearchText(destName) + '" data-orig-text="'+ destName + '" style="display:none;" >';
						stringToAppend += 	'<label>';
						stringToAppend += 		input;
						stringToAppend += 		'<span class="itemText" >' + destName + '</span>';
						stringToAppend += 	'</label>';
						stringToAppend += '</li>';


					} //end for
					
					//vložíme do seznamu, nabindujeme událsoti
					destinationsList
						.html(stringToAppend)
						.find('input').bind('change', function(){
							self._checkDestinationHandler(this);
						});

					destinationsList.find('li.masterDestination label').bind('click', function(e){
						e.preventDefault();
					});

					//přidání třídy 'hasSlaveDestinations' všem destinacím, které mají nějaké poddestinace
					var slaves = destinationsList.find('li').filter(function(){
						var id = $(this).attr('data-destination');
						if ($(this).next().hasClass('slaveDestination') && $(this).next().attr('data-slave-parent') == id){
							return true;
						}else{
							return false;
						}
					});
					for (var i = 0; i < slaves.length; i++) {
						$(slaves[i]).addClass('hasSlaveDestinations');
					};

					//pokud je v option zapnuté sbalování destinací, přidáme accordion
					if (o.useDestinationsAccordion){
						destinationsList.find('.masterDestination.hasSlaveDestinations')
							.addClass('hasAccordion')
							//na click proběhne accordion
							.click(function(event){
								self._runMasterDestinationsAccordion(this);
							})
							//přidáme ikonu sbalení
							.find('label').append(
								$('<span />', {
									'class' : 'ui-icon ui-icon-circle-triangle-s'
								})
							);
					}

					//uložení informace, že byl seznam sestaven
					self._destinationsListBuilt = true;

					//nastavení předvybraných hodnot
					self._preselect();

				}//end of success
			});
	},


	/**
	 * Zaškrtnutí předvybraných hodnot - proběhne až po sestavení seznamu zemí i destinací
	 */
	_preselect : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			selectedCountriesList = parentElement.find(o.selectedCountriesElement),
			destinationsList = parentElement.find(o.destinationsListElement),
			destinations = o.preselectedDestinations,
			countries = o.preselectedCountries;

		//ošetření vstupních hodnot - pokud je pole ve stringu
		if(typeof destinations === 'string'){
			destinations = jQuery.parseJSON(destinations);
		}
		if(typeof countries === 'string'){
			countries = jQuery.parseJSON(countries);
		}

		//provede se jen pokud byly oba seznamy již sestavené
		if (self._destinationsListBuilt && self._countriesListBuilt){
			//není-li co přednastavovat, označíme přednastavení jako hotové a skončíme
			if ((!countries.length || !destinations.length) && self._maxCountriesListLength !== 1){
				self._preselectDone = true;
				return;
			}

			//pokud je v zemích jen jedna země, vyberem ji a zobrazíme její destinace
			//počet zemí získáme z maximální délky seznamu uložené v privátní proměnné _maxCountriesListLength
			if (self._maxCountriesListLength === 1){
				//id poslední země
				var oneCountryId = countriesList.find('li[data-country]').last().attr('data-country');

				//ještě pro jistotu kontrola, zda v seznamu opravdu neexistuje  žádná další 
				var otherCountriesCount = countriesList.find('li[data-country!='+ oneCountryId +']').not('.listSeparator').length;

				//pokud žádná další neexistuje, vyberu zemi a zobrazím její destinace
				if (!otherCountriesCount){
					countriesList.find('li[data-country=' + oneCountryId + '] input').prop('checked', true);
					destinationsList.find('li[data-country=' + oneCountryId + ']').show();
				}
			}

			//pokud jsou nějaké předvybrané země, vybereme je a zobrazíme jejich destinace
			if (countries.length){
				$.each(countries, function(index, value){
					countriesList.find('li[data-country=' + value + '] input').prop('checked', true);
					destinationsList.find('li[data-country=' + value + ']').show();
				});
			}

			//pokud jsou nějaké předvybrané destinace, vybereme je
			if (destinations.length){
				for (var i = 0; i < destinations.length; i++) {
					var value = destinations[i];
					//aktuální destinace
					var currentDestination = destinationsList.find('li[data-destination=' + value + ']');

					//vybereme destinaci a viditelně zaškrtneme
					currentDestination.data('checked', true).find('input').prop('checked', true);

					//její poddestinace jen viditelně zaškrtneme, ale nevybereme (neuložíme $(<li>).data('checked', true))
					// - ale jen pokud to není master
					if (!currentDestination.hasClass('masterDestination')){
						var childDestinations = self._getAllChildDestinations(value);
						for (var j = 0; j < childDestinations.length; j++) {
							var childDestination = childDestinations[j];
							$(childDestination).find('input').prop('checked', true);
						}
					}
				};
			}

			//vybraným zemím přidáme label
			//mohlo by být již ve for cyklu kde vybíráme země, ale tam se teoreticky může objevit i země, která nebyla v seznamu
			countriesList.find('li input:checked').each(function(index, element){
				self._addSelectedCountryLabel($(element).attr('data-country'));
			});
			
			//po dokončení spustíme ručně change událost
			self._trigger('change');

			//Přednastavení je hotové
			self._preselectDone = true;

			self._updateLabel();
		}
	},


	/**
	 * Handler pro change událost inputu v seznamu zemí
	 * @param  object element Input nad kterám proběhla změna
	 */
	_checkCountryHandler : function(element){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			destinationsList = parentElement.find(o.destinationsListElement);

		var changedId = $(element).attr('data-country');
		var items = destinationsList.find('li[data-country='+changedId+']');


		if ($(element).is(':checked')){
			//vybereme masteru destinaci
			items.first().data('checked', true);

			//destinace zobrazíme a appendem přidáme nakonec seznamu
			destinationsList.append(items.show());

			//přidáme label
			self._addSelectedCountryLabel(changedId);

			//země může být ve více seznamech - oblíbené + všechny
			//zemi pro jistotu znovu zaškrtneme v celém seznamu
			countriesList.find('li[data-country=' + changedId + '] input:not(:checked)').prop('checked', true);
		}else{
			//odškrtneme checkboxy
			items.data('checked', false).find('input').prop('checked', false);

			//skryjeme destinace
			items.hide();

			//odstraníme label s odškrtnutou zemí
			self._removeSelectedCountryLabel(changedId);

			//země může být ve více seznamech - oblíbené + všechny
			//zemi pro jistotu znovu odškrtneme v celém seznamu
			countriesList.find('li[data-country=' + changedId + '] input:checked').prop('checked', false);
		}

		if (self._preselectDone){
			//updatu textu labelu
			self._updateLabel();

			//událost, která informuje o změně vybraných hodnot
			self._trigger('change');
		}
	},


	/**
	 * Handler pro change událost na inputu v seznamu destinací
	 * - vybranou destinaci neurčuje to, jestli je zaškrtlý input, ale jestli má <li> element destinace uloženo property checked na true (.data('checked', true))
	 * 
	 * @param  object element Input nad kterám proběhla změna
	 */
	_checkDestinationHandler : function(element){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement);

		var changedId = $(element).attr('data-destination'),
			changedCountryId = $(element).attr('data-country'),
			liElement = $(element).closest('.slaveDestination'),
			changedIdParent = $(liElement).attr('data-slave-parent'),
			destinationsList = parentElement.find(o.destinationsListElement).find('li[data-country=' + changedCountryId + ']'),
			depth = liElement.attr('data-slave-depth');


		//zaškrtnutí destinace/inputu
		if ($(element).is(':checked')){

			//vybereme destinaci nastavením "checked" na <li>
			$(liElement).data('checked', true);

			//odebereme master destinaci
			destinationsList.filter('.masterDestination').data('checked', false);

			//zaškrtnutí všech poddestinací - jen vizuálně
			var childDestinations = self._getAllChildDestinations(changedId);
			$.each(childDestinations, function(){
				//všechny poddestinace viditelně zaškrtneme, ale na pozadí odškrtneme - jsou zahrnuté pod parentem
				$(this).data('checked', false).find('input').prop('checked', true);
			});

			//zaškrtneme zemi k destinaci
			countriesList.find('li[data-country=' + changedCountryId + '] input').prop('checked', true);

			//přidáme k zemi label
			self._addSelectedCountryLabel(changedCountryId);

			//všechny destinace k zemi zobrazíme
			destinationsList.show();
		}
		//odškrtnutí inputu destinace
		else{
			//odvybereme i na <li>
			$(liElement).data('checked', false);

			//musíme odškrtnout i odebrat všechny rodičovské destinace
			var parentDestinations = self._getAllParentDestinations(changedId);
			$.each(parentDestinations, function(){
				$(this).data('checked', false).find('input').prop('checked', false);
			});

			//vsechny destinace se stejnou hloubkou, ktere jsou zaskrtle, musime dozaskrtnout na pozadi
			//- pred tim byly vybrane tim, ze nalezely pod stejneho vybraneho parenta
			var sameLevelDestinations = destinationsList.filter(function(){
				var sameParent = ($(this).attr('data-slave-parent') == changedIdParent); //stejny rodic
				var notSameId = ($(this).attr('data-destination') != changedId); //ale neni to stejna destinace jakou prave odskrtavame
				var isVisiblyChecked = ($(this).find('input').is(':checked')); // a zaroven byla pred tim zaskrtla
				return sameParent &&  notSameId && isVisiblyChecked;
			});
			$.each(sameLevelDestinations, function(){
				$(this).data('checked', true);
			});

			//kdyz se odebere nejaky hlavni rodic, ktery zahrnoval potomky, tak ty potomky musime přidat do vyběru i když jsou vizuálně zaškrtnuté
			//- procházíme odspodu
			var nearestParent = parentDestinations.slice(0, 1)[0];
			parentDestinations = parentDestinations.slice(1);
			while (nearestParent){
				//rodič rodiče odškrtnuté destinace
				var nearestParentParent = $(nearestParent).attr('data-slave-parent');

				//vybereme destinace, které jsou na stejné úrovni jako rodič, vezmeme z nich jen ty zaškrtlé a ty vybereme
				var sameLevelDestinations = destinationsList.filter('[data-slave-parent=' + nearestParentParent + ']').filter(
						function(){
							return $(this).find('input').is(':checked');
						}
					);

				sameLevelDestinations.data('checked', true);
				
				//posuneme se o další úroveň výš
				nearestParent = parentDestinations.slice(0, 1)[0];
				parentDestinations = parentDestinations.slice(1);
			}

			//odškrtnutí všech poddestinací
			var childDestinations = self._getAllChildDestinations(changedId);
			$.each(childDestinations, function(){
				$(this).data('checked', false).find('input').prop('checked', false);
			});

			//pokud to byla poslední zaškrtlá destinace v zemi
			//musíme zašrtnout master destinaci
			var checked = destinationsList.filter(
				function(){
					return $(this).find('input').is(':checked');
				}
			);
			if (!checked.length){
				destinationsList.filter('.masterDestination').data('checked', true).find('input').prop('checked', true);
			}
		}

		if (self._preselectDone){
			//update textu labelu
			self._updateLabel();

			//událost, která informuje o změně vybraných hodnot
			self._trigger('change');
		}
	},


	/**
	 * Získání pole ID vybraných zemí
	 * @return array 
	 */
	_getCheckedCountries : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement);

		var checked =  [];
		countriesList.find('li input:checked').each(function(index, element){
			//přidáme jen pokud ještě není v poli
			//jednou může být v oblíbených, jednou v kompletním seznamu
			var id = $(element).val();
			if (checked.indexOf(id) == -1){
				checked.push(id);
			}
		});
		
		return checked;
	},


	/**
	 * Získání pole ID vybraných destinací
	 * @param  bool forLabelUpdate Nastavením na true získáváme počet pro update labelu - počítáme zaškrtlé checkboxy
	 * @return array
	 */
	_getCheckedDestinations : function(forLabelUpdate){
		var self = this,
			o = this.options,
			parentElement = this.element,
			destinationsList = parentElement.find(o.destinationsListElement);

			var checked =  [];
			if (forLabelUpdate){
				var items = destinationsList.find('li input:checked');
			}else{
				var items = destinationsList.find('li').filter(function() { return $.data(this, "checked") == true; });
			}
			$.each(items, function(index, element){
				checked.push($(element).attr('data-destination'));
			});
			return checked;
	},


	/**
	 * Vyčistí filtr zemí a destinací
	 */
	_clearFilter : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			filter = parentElement.find(o.filterElement),
			countriesList = parentElement.find(o.countriesListElement),
			destinationsList = parentElement.find(o.destinationsListElement);
		
		//odstranění textu, že nic nebylo nalezeno
		destinationsList.find('#nothingFound').remove();

		//výběr všech položek
		var li = $(countriesList).add(destinationsList).find('li');

		//odstranění všech tříd filtru
		li.removeClass('hiddenByFilter').removeClass('shownByFilter');

		//odstranění spanů ze všech textů
		li.find('.itemText').each(function(index, element){
			var newText = $(element).text();
			$(element).html(newText);
		});

		//vymazání textu
		filter.val('');
	},



	/**
	 * Funkce accordionu na hlavních destinacích - spouštěna událostí click
	 * @param  object element Element <li> na kterém accordion spouštíme
	 */
	_runMasterDestinationsAccordion : function(element){
		var o = this.options;
	
		//sbalujeme, jen pokud není zrovna aktivní filtr
		if (!$(element).hasClass('shownByFilter')){
			//vybereme všechny slave destinace
			var slaves = $(element).nextUntil('.masterDestination');
			
			//nejde pustit animaci na jednotlivé slave destinace, kvůli sekání v pomalejších prohlížečí
			// - nejprve je obalíme do div s třídou 'hider'
			// - animaci provedeme na hideru a zobrazení destinací uvnitř divu, ale bez animace
			// - hider poté zase odstraníme
			var hider = $('<div>',{
				'class' : 'hider'
			});

			//přidáme destinace do divu a ten umístíme za hlavní destinaci, na kterou bylo kliknuto
			slaves.appendTo(hider);
			$(element).after(hider);

			//pokud jsme na sbalené destinace
			if ($(element).hasClass('collapsed')){
				//hider skryjeme
				hider.css('display', 'none');
				//uvnitř skrytého hideru zobrazíme bez animace destinace
				slaves.css('display', 'block');
				//na hideru provedeme animaci
				//po dokončení hider odstraníme
				hider.slideDown('fast', function(){
					$(element).after(slaves);
					hider.remove();
				});
			}
			//jsme na rozbalené destinaci
			else{
				//na hideru provedeme animaci
				hider.slideUp('fast', function(){
					//po skrytí hideru skryjeme destinace bez animace
					slaves.css('display', 'none');
					//destinace přesuneme zpět mimo hider a ten odstraníme
					$(element).after(slaves);
					hider.remove();
				});
			}

			//přepneme třídu na položce a ikonku sbalení
			$(element).toggleClass('collapsed').find('.ui-icon').toggleClass('ui-icon-circle-triangle-e').toggleClass('ui-icon-circle-triangle-s');
			//přidáme počet vybraných, pokud je položka sbalená
			if ($(element).hasClass('collapsed')){
				var count = slaves.find('input:checked').length;
				var text = o.selectedCountText.replace('#', count);
				$(element).find('label').append(
					$('<span>',{
						'class' : 'itemCount',
						'text' : text
					})
				);
			}
			//jinak počet sbalených odstraníme
			else{
				$(element).find('label .itemCount').fadeOut('fast', function(){
					$(this).remove();
				});
			}
		}
		
	},


	/**
	 * Získání pole všech rodičovských destinací - projde strom přes parent_id až do hlavní destinace
	 * @param  int destId Id destinace, ke které hledáme rodiče
	 * @return array Pole elementů rodičů
	 */
	_getAllParentDestinations : function(destId){
		var self = this, 
			o = this.options, 
			parentElement = this.element, 
			destinationsList = parentElement.find(o.destinationsListElement); 

		var current = destinationsList.find('li[data-destination=' + destId +']'), //aktuální položka <li>
			currentParentId = current.attr('data-slave-parent'), //id rodič položky
			parent = destinationsList.find('li[data-destination=' + currentParentId + ']'), //element rodiče položky
			parentDestinations = []; //návratová hodnota

		//dokud nacházíme rodiče, přidáváme je do návratového pole a posouvme se na další rodiče
		while (parent.length && parent.length > 0){
			parentDestinations.push(parent);
			//posun na rodiče
			currentParentId = parent.attr('data-slave-parent');
			parent = destinationsList.find('li[data-destination=' + currentParentId + ']');
		}

		return parentDestinations;
	},


	/**
	 * Získání pole všech poddestinací
	 *  - vyhledá přímé poddestinace tzn. o jednu úroveň ve stormu níže
	 *  - na nalezené použije rekurzivní volání funkce
	 * @param  int destId ID destinace, ke které hledáme potomky
	 * @return array Pole elementů poddestinací
	 */
	_getAllChildDestinations : function(destId){
		var self = this, //instance pluginu
			o = this.options, //options
			parentElement = this.element, //html element
			destinationsList = parentElement.find(o.destinationsListElement); //html list s destinacemi

		//návratová hodnota
		var childDestinations = [];

		//ziḯskání všech přímých potomků destinace
		var slaves = destinationsList.find('li[data-slave-parent=' + destId + ']');

		//pro každého potomka nalezneme rekurzivním voláním i jeho poddestinace
		slaves.each(function(){
			//každého potomka přidáme
			childDestinations.push($(this));

			//rekurzivne získání potomků všech potomků
			var tmp = self._getAllChildDestinations(
					$(this).attr('data-destination')
				);
			
			//přidáváme, jen pokud není pole prázdné
			if (tmp && tmp.length > 0){
				childDestinations = childDestinations.concat(tmp);				
			}
		});

		return childDestinations;
	},


	/**
	 * Update textu na labelu
	 * - #C je nahrazeno buď názvy zemí, pokud jich je vybraných méně než 'labelCountriesCount', nebo počtem vybraných zemí
	 * - #D je nahrazeno počtem vybraných destinací
	 */
	_updateLabel : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			label = $(o.labelElement);

		var countries = self._getCheckedCountries();
		var destinations = self._getCheckedDestinations(true).length; //počet vybraných destinací bez hlavních destinací

		//pokud jsme pod limitem na počet zemí, zobrazíme názvy zemí
		if (countries.length !== 0 && countries.length <= o.labelCountriesCount){
			var tmp = [];
			$.each(countries, function(key, id){
				tmp.push(
					countriesList.find('li[data-country=' + id + '] .itemText').first().text()
				);
			});
			countries = tmp.join(', ');
		}
		//nad limitem zobrazíme číslo stejně jako u destinací
		else{
			countries = countries.length;
		}

		//aktualizace textu
		label.text(
			o.labelText.replace('#C', countries).replace('#D', destinations)
		);
	},


	/**
	 * Přidá položku do seznamu zaškrtnutých zemí 
	 * @param int id Id země
	 */
	_addSelectedCountryLabel : function(id){
		if (!id){
			return;
		}
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			list = $(o.selectedCountriesElement);

		if (!list.length){
			return;
		}

		//nepřidává labely, které již existují
		if (list.find('.countryListItem[data-country=' + id + ']').length){
			return;
		}

		//název země
		var name = countriesList.find('li[data-country=' + id + '] .itemText').first().text();

		//element položky
		var span = $('<span>', {
			'class' : 'countryListItem',
			'data-country' : id,
			'text' : name
		});

		//tlačítko pro odstranění
		var closer = $('<span>', {
			//'class' : 'ui-icon ui-icon-circle-close',
			'text' : 'X',
			'data-country' : id,
		})
		//bind funkce při odstranění země
		.bind('click', function(){
			var input = countriesList.find('li[data-country=' + id + '] input').prop('checked', false);
			self._checkCountryHandler(input);
			$(this).parent().remove();
		});

		//přidání do seznamu
		list.append(
			span.append(
				closer
			)
		);
	},


	/**
	 * Odebere položky ze seznamu zaškrtnutých zemí
	 * @param  int Id země
	 */
	_removeSelectedCountryLabel : function(id){
		if (!id){
			return;
		}

		var self = this,
			o = this.options,
			list = $(o.selectedCountriesElement);

		list.find('.countryListItem[data-country=' + id + ']').remove();
	},

	/**
	 * Zruší výběr všech zemí a všech destinací
	 */
	_clearSelection : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			destinationsList = parentElement.find(o.destinationsListElement),
			list = $(o.selectedCountriesElement);

		//destinace
		destinationsList.find('li').data('checked', false).hide().find('input').prop('checked', false);

		//země
		countriesList.find('li input').prop('checked', false);
		list.find('.countryListItem').remove();

		//filtr
		self._clearFilter();

		//label
		self._updateLabel();

		//spuštění události pro vnější svět
		self._trigger('change');
	},


	/**
	 * Filtrování textu na událost keyup ve filtru
	 */
	_runFilter : function(){
		var self = this,
			o = this.options,
			parentElement = this.element,
			countriesList = parentElement.find(o.countriesListElement),
			destinationsList = parentElement.find(o.destinationsListElement),
			filter = parentElement.find(o.filterElement);

		var searchedText = self._prepareSearchText(filter.val());

		//odstranění textu, že nic nebylo nalezeno - dříve než vytvoříme seznam k prohledávání
		destinationsList.find('#nothingFound').remove();

		//seznam k prohledávání
		var querySelector = o.destinationsListElement + ' li' + ',' + o.countriesListElement + ' li';
		var listElements = parentElement.find(querySelector);

		//pokud je text hledání prázdný, spustíme kompletní vyčištění filtru
		if (searchedText.length === 0){
			self._clearFilter();
			return;
		}

		//proměnná, kam budeme ukládat id zemí u kterých již je zpětně zobrazená hlavní destinace
		//pokud v takové zemi najdeme shodu u nějaké poddestinace, nebudeme znova hlavní destinaci hledat a řešit její zobrazení
		var showedCountryMasters = [];

		//projdeme všechny položky v obou seznamech
		for (var i = 0; i < listElements.length; i++) {
			//pokud není hledaný text substringem textu labelu, skryjeme ho, jinak zobrazíme
			var element = listElements[i];
			var item = $(element).find('.itemText');
			var itemOrigText = $(element).attr('data-orig-text');
			var itemSearchText = $(element).attr('data-search-text');
			var searchedIndex = itemSearchText.indexOf(searchedText);

			if ( searchedIndex < 0 || $(element).hasClass('masterDestination')){
				//skryti
				$(element).addClass('hiddenByFilter').removeClass('shownByFilter');
				$(item).html(itemOrigText);
			}else{
				//zobrazeni
				$(element).removeClass('hiddenByFilter').addClass('shownByFilter');

				//pro danou zemi zkontrolujeme, zda je zobrazena hlavní destinace - kontrola jen v destinacích
				var country = $(element).attr('data-country');
				if ($(element).hasClass('slaveDestination') && showedCountryMasters.indexOf(country) === -1){
					//hlavní destinace není zobrazena, najdeme ji v seznamu
					var parentMaster = destinationsList.find('.masterDestination[data-country='+ country +']').first();
					//zobrazíme 
					parentMaster.removeClass('hiddenByFilter').addClass('shownByFilter');
					//přidáme do seznamu zemí, kde je hlavní destinace zobrazena
					showedCountryMasters.push(country);
				}

				//zvyrazneni spravneho substringu
				var searchedIndexEnd = searchedIndex + searchedText.length;
				var toReplace = itemOrigText.substring(searchedIndex, searchedIndexEnd); //kopirujeme z originalniho
				var newText = itemOrigText.slice(0, searchedIndex) + '<span>' + toReplace + '</span>' + itemOrigText.slice(searchedIndexEnd);
				$(item).html(newText);
			}
		}

		//pokud v destinacích nebylo nic nalezeno, přidáme text
		if (destinationsList.find('li.shownByFilter').length === 0){
			destinationsList.append($('<li>', {
				'id' : 'nothingFound',
				'text' : o.nothingFoundText
			}));
		}
	},


	/**
	 * Příprava textu pro filtrování - odstranění diakritiky a převedení na lowercase
	 * - pro odstranění diakritky využívá funkci v globálním JS balíku (ať se nemnoží duplicity), v případě nedostupnosti funkce vrací lowercase původního textu
	 * @param  String str Vstupní text
	 * @return String
	 */
	_prepareSearchText : function(str){
		var result;
		if(typeof removeDiacriticsFromString == 'function'){ 
 			result = removeDiacriticsFromString(str);
 		}else{
 			result = str;
 		}
 		return result.toLowerCase(); 
	},

	
	// ------ Public funkce --------
	 
	/**
	 * Zrušení výběru zemí a destinací
	 */
	clear : function(){
		this._clearSelection();
		this._trigger('clear');
	},

	/**
	 * Získání vybraných zemí
	 * @return array ID zemí
	 */
	selectedCountries : function(){
		return this._getCheckedCountries();
	},

	/**
	 * Získání vybraných destinací
	 * @return array ID destinací
	 */
	selectedDestinations : function(){
		console.log(this._getCheckedDestinations());
		return this._getCheckedDestinations();
	}

});

})(jQuery);

