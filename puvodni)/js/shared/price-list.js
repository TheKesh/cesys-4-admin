
;(function ($, window, document, undefined) {
	$.widget('cesys.priceList', {
		MIN_SURCHARGES_COUNT : 3,

		/**
		 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
		 * $(selector).priceList({
		 * 		optionName : optionValue
		 * });
		 */
		options: {
			//url pro nahrání ceníku
			dataUrl	: '/Dates/load_pricelist/',

			//výchozí počet dospělých
			adultCount : 2,

			//výchozí počet dětí
			childCount : 0,

			//věky dětí - pro přenačítání ceníku
			childAges : null,

			//načíst ajaxem ceník při načtení pluginu ?
			loadData : true
		},

		/**
		 * Konstruktor - načtení seznamů, bindování funkcí
		 */
		_create : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn

			//id termínu
			self._dateId = $(element).data('date-id');

			//url ze které se budou načítat data lze přepsat atributem
			if ($(element).data('data-url')){
				options.dataUrl = $(element).data('data-url');
			}

            //options z atributů
            if (typeof element.data('disable-changes') != "undefined"){
                options.disableChanges = (element.data('disable-changes') == "1");
            }

            // zdroj dat
            self._dataSource = $(element).data('data-source');

			//délka zájezdu
			self._duration = parseInt($(element).data('date-duration'));

			//hash na ověřování
			self._hashAvailability = $(element).data('hash-availability');

			//hash na stahování dat ceníku
			self._hashSecurity = $(element).data('hash-security');

			//ověřovat ?
			self._availability = $(element).data('date-availability');

            //zobrazit všechny příplatky?
            self._show_all_surcharges = $(element).data('show-all-surcharges');

			//měna ve které bychom se měli pohybovat
			self._targetCurrencyId = $(element).data('target-currency-id');

			//uložení konkrétních html elementů
			//sekce s pokojem
			self._elementRoom = $(element).find('.section.room');

			//hlavička výběru pokoje
			self._elementRoomHeader = $(element).find('.section.room .section-header');

			//sekce povinných příplatků
			self._elementSurcharges = $(element).find('.section.surcharges');

			//sekce příplatků
			self._elementSurcharges = $(element).find('.section.surcharges');

			//tabulka s ceníkem povinných příplatků
			self._elementSurchargesTable = $(element).find('.section.surcharges table');

			//hlavička příplatků
			self._elementSurchargesHeader = $(element).find('.section.surcharges .section-header');

			//tabulka s ceníkem k pokoji
			self._elementRoomTable = $(element).find('.section.room table');

			//span s výsledkem ověřování dostupnosti pokoje
			self._elementRoomAvailability = $(element).find('.section.room .room-availability');

			//název pokoje
			self._elementRoomName = $(element).find('.section.room .room-name');

			//element s vypočtenou cenou
			self._elementTotalPrice = $(element).find('.total-price-value');

			//element s vypočteným storno poplatkem
			self._elementStornoPrice = $(element).find('.storno-price-value');

			//element s cheboxem storno ano / ne
			self._elementStornoPriceCheckbox = $(element).find('.storno-price input#storno')
														 .bind(
														 	'change',
														 	function(){
														 		self._calculate();
														 	}
														 );

			//data načtená ajaxem
			self._data = null;

			//odkaz na selekt s pokoji - vytvoří se ajaxem
			self._roomSelect = null;

			//odkaz na selekt s příplatky - vytvoří se ajaxem
			self._surchargeSelect = null;

			//načteme data ceníku
			element.bind('dataLoaded', function(){
				//vyplníme pokoje, pokud jsou
				if (self._data && self._data.rooms) {
					self._handleRooms(self._data.rooms);
					
					//vybraný pokoj
					if (self._data.selectedRoom && self._data.selectedRoom > 0){
						self._selectRoomById(self._data.selectedRoom);
					}else{
						self._selectRoomByIndex(0);
					}
				}

				//vendor služby - pojištění, víza atd
				if (self._data && self._data.services && self._data.services.length) {
					self._fillServices(self._data.services);
				}

				//ihned přepočítat
				self._calculate();

			});

			//načteme data
			if (options.loadData){
				self._loadData();
			}

		},

		/**
		 * Načtení dat ceníku ajaxem
		 */
		_loadData : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn
			
			//dáme navenek vědět, že budeme načítat data ajaxem
			element.trigger('dataLoading');

			$.ajax({
				type: "POST",
				url: options.dataUrl + self._dateId,
				dataType : 'json',
				data : {
					hash : self._hashSecurity,
					occupancy : {
						adultCount : options.adultCount,
						childCount : options.childCount,
						childAges : options.childAges
					}
				}
			})
			.done(function(data){
				self._data = data;
			})
			.fail(function(jqXHR, textStatus, errorThrown){
				console.log('Error while loading data: ' + textStatus);
			})
			.always(function(){
				//událostí oznámíme že se ukončilo načítání dat
				element.trigger('dataLoaded');
			});

		},

		/**
		 * Zpracuje seznam pokojů
		 * @param  {array} rooms Seznam pokojů
		 */
		_handleRooms : function(rooms){
			var self = this;
			
			//selekt s pokoji vymažeme
			if (self._roomSelect && self._roomSelect.length){
				self._roomSelect.remove();
			}

			//víc pokojů - přidáme select
			if (rooms.length > 1) {
				self._roomSelect = $('<select>', {
					id : 'roomSelect'
				})
				.bind('change', function(){
					self._fillRoomPriceList($(this).val());
					self._calculate();

					//při změně pokoje si ukládáme do data.surcharges jeho příplatky
					self._data.surcharges = self._data.rooms[$(this).val()].surcharges;
					
					//ověření dostupnosti pro roomId
					var room = self._getRoomByIndex($(this).val());
					self._checkRoomAvailability(room.id);
				});

				//naplnění selektu pokoji - rozdělujeme podle toho jestli vyhovují obsazenosti
				var compliesOccupancy = $('<optgroup>', {
					label : cesys.ts('Complies occupancy')
				});
				var notCompliesOccupancy = $('<optgroup>', {
					label : cesys.ts('Not complies occupancy')
				});
				$.each(rooms, function(index, room){
					//sestavené názvu pokoje
					var optionText = room.name;
					if (room.beds){
						optionText += ' (' + cesys.ts('beds') + ': ' + room.beds;
						if (room.extraBeds) {
							optionText += ', ' + cesys.ts('extra beds') + ': ' + room.extraBeds;
						}
						optionText += ')';
					}	
					//debug přidáme pokud je vyplněn
					if (typeof room.debug == "string" && room.debug.length > 1){
						optionText += ' --- ' +room.debug;
					}
					
					var option = $('<option>', {
						value : index,
						text : optionText
					});

					//podle toho jestli vyhovují obsazenosti přidáme do správného seznamu
					if (room.compliesOccupancy && room.compliesOccupancy === true){
						compliesOccupancy.append(option);
					}else{
						notCompliesOccupancy.append(option);
					}
				});

				//pokud v pokojích vyhovujících obsazenosti není nic, vložíme rovnou všechny pokoje bez rozdělení
				if (compliesOccupancy.find('option').length === 0){
					self._roomSelect.append(notCompliesOccupancy.html());
				}else{
					self._roomSelect.append(compliesOccupancy);
					self._roomSelect.append(notCompliesOccupancy);
				}

				//vložení selektu
				self._elementRoomAvailability.before(self._roomSelect);

			}else if (rooms.length == 1){
				//jen jeden pokoj - buď standardní pokoj, nebo virtual room
				self._fillRoomPriceList(0);
				self._data.surcharges = rooms[0].surcharges;

				if (rooms[0].id > 0) {
					//máme jen jeden pokoj, ale je to standardní vyplněný pokoj, ne virtual room
					//ověřit dostupnost, vyplnit název pokoje
					self._checkRoomAvailability(rooms[0].id);
					if (cesys.ts('Room') != rooms[0].name){
						// poskládáme název pokoje včetně obsazenosti (lůžka, přistýlky)
                        var roomText = cesys.ts('Room') + ': ' + rooms[0].name;
                        if (rooms[0].beds){
                            roomText += ' (' + cesys.ts('beds') + ': ' + rooms[0].beds; // počet lůžek
                            if (rooms[0].extraBeds) {
                                roomText += ', ' + cesys.ts('extra beds') + ': ' + rooms[0].extraBeds; // počet přistýlek
                            }
                            roomText += ')';
                        }
                        //debug přidáme pokud je vyplněn
                        if (typeof rooms[0].debug === "string" && rooms[0].debug.length > 1){
                            roomText += '<br /><span class="debug"> --- ' +rooms[0].debug+ '</span>';
                        }
                        self._elementRoomName.html(roomText);
					}
				}else{
					//je jeden pokoj a je to virtual room - název změníme na "ceník"
					self._elementRoomName.html(cesys.ts('Price list'));
				}

			}else{
				//ceník bez pokojů
				self._elementRoom.hide();
				self._elementSurcharges.hide();
			}


		},

		/**
		 * Vrátí příplatek podle jeho indexu v poli surcharges 
		 * @param  {int} index Index příplatku v seznamu
		 */
		_getSurchargeByIndex : function(surchargeIndex){
			var self = this;
			
			//pokud existuje příplatek na daném indexu, vrátíme ho
			if (self._data.surcharges && self._data.surcharges.length && self._data.surcharges[surchargeIndex]) {
				return self._data.surcharges[surchargeIndex];
			}

			return false;
		},

		/**
		 * Vrátí příplatek podle jeho id v poli surcharges 
		 * @param  {int} index Index příplatku v seznamu
		 */
		_getSurchargeById : function(surchargeId){
			var self = this;
			
			var result = false;

			//pokud select existuje, vybereme index
			if (self._data.surcharges && self._data.surcharges.length) {

				$.each(self._data.surcharges, function(index, surcharge){
					if (surcharge.id == surchargeId) {
						result = surcharge;
						return false; //konec each
					}
				});
			}

			return result;
		},

		/**
		 * Vrátí pokoj podle jeho indexu v poli rooms 
		 * @param  {int} index Index pokoje v seznamu
		 */
		_getRoomByIndex : function(index) {
			var self = this;

			//pokud existuje pokoj na daném indexu, vrátíme ho
			if (self._data.rooms && self._data.rooms.length && self._data.rooms[index]) {
				return self._data.rooms[index];
			}

			return false;
			
		},

		/**
		 * Vybere pokoj podle jeho indexu v poli rooms / hodnotě option v selectu
		 * @param  {int} index Index pokoje v seznamu
		 */
		_selectRoomByIndex : function(index) {
			var self = this;

			//pokud select existuje, vybereme index
			if (self._roomSelect) {
				self._roomSelect.val(index);
				self._roomSelect.trigger('change');
			}
			
		},

		/**
		 * Vybere pokoj podle jeho indexu v poli rooms / hodnotě option v selectu
		 * @param  {int} id Id pokoje
		 */
		_selectRoomById : function(id) {
			var self = this;
			var findIndex = null;

			if (self._data.rooms && self._data.rooms.length){
				$.each(self._data.rooms, function(index, element){
					if (element.id == id){
						findIndex = index;
						return false;
					}
				});
			}
			
			if (self._roomSelect && findIndex !== null) {
				self._selectRoomByIndex(findIndex);
			}
		},

		/**
		 * Vyplní tabulku s ceníkem ke konkrétnímu pokoji - včetně příplatků k pokoji
		 */
		_fillRoomPriceList : function(index) {
			var self = this;
		
			if (self._data && self._data.rooms && self._data.rooms[index]) {
				var room = self._data.rooms[index];
				var prices = self._data.rooms[index].prices;

				//na začátku tabulku skryjeme
				self._elementRoomTable.hide();

				//standardní ceníkové položky k pokoji
				self._elementRoomTable.empty().append('<tr> \
					<th class="description">' + cesys.ts('Description') + '</th> \
					<th class="price">'       + cesys.ts('Price')       + '</th> \
					<th class="count">'       + cesys.ts('Count')       + '</th> \
					<th class="total">'       + cesys.ts('Total')      + '</th> \
					</tr>');
				$.each(prices, function(index, element){
					self._elementRoomTable.append(self._createPriceRow(element));
				});
				
				//příplatky
				var surcharges = self._data.rooms[index].surcharges;
				self._fillSurcharges(surcharges);

				//po vyplnění zobrazení fadeinem
				self._elementRoomTable.fadeIn('slow');
			}
			
		},

		/**
		 * Vyplní příplatky
		 * @param {array} surcharges Seznam příplatků z dat
		 */
		_fillSurcharges : function(surcharges) {
			var self = this;

			//starý selekt smažeme, tbaulku vyistíme, skryjeme
			if (self._surchargeSelect && self._surchargeSelect.length){
				self._surchargeSelect.remove();
				self._surchargeSelect = null;
			}
			self._elementSurchargesTable.empty();
			self._elementSurcharges.hide();

			//máme-li vůbec nějaké příplatky
			if (surcharges && surcharges.length) {
				//vybrané rovnou vylistujeme, zbytek třídíme na povinné/nepovinné pro pozdější vložneí do selektu
				var otherRequiredSurcharges = [];
				var otherOptionalSurcharges = [];
				$.each(surcharges, function(index, surcharge){
					if (surcharge.display){
						var priceRow = self._createPriceRow(surcharge, false);
						self._elementSurchargesTable.append(priceRow);
					}else{
						if (surcharge.required){
							otherRequiredSurcharges.push(surcharge);
						}else{
							otherOptionalSurcharges.push(surcharge);
						}
					}
				});

				//nechceme vypsat všechny příplatky a máme jich dost zbylých abychom zobrazovali select
				if ( (otherRequiredSurcharges.length + otherOptionalSurcharges.length) >= self.MIN_SURCHARGES_COUNT && self._show_all_surcharges == 0) {
					//selekt pro výběr příplatků - při změně přidáme příplatek a vybere zpět první položku
					self._surchargeSelect = $('<select>').bind('change', function(event){
						var selectedOption = $("option:selected", this);
						var optionOptGroup = selectedOption.parent('optgroup');

						//přidáme příplatek do tabulky
						var surchargeId = selectedOption.val();
						self._addSurchargeRow(self._getSurchargeById(surchargeId), true);

						//smažeme ze selektu volbu
						selectedOption.remove();

						//pokud zbyla prázdná skupina příplatků povinné/nepovinné, skryjeme
						if (!optionOptGroup.children().length) {
							optionOptGroup.hide();
						}

						//vrátíme se na volbu "přidat příplatek"
						$(this).val(-1); //pozor, v IE8 chybně spouští change event
					});

					//naplnění a vložení selektu
					self._fillSurchargesInSelect(otherRequiredSurcharges, otherOptionalSurcharges, self._surchargeSelect);
					self._elementSurchargesHeader.append(self._surchargeSelect);
				}else{
					//v selectu by bylo málo příplatků, nemá význam ho použít - ceny rovnou vylistujeme do tabulky
					$(otherRequiredSurcharges).add(otherOptionalSurcharges).each(function(index, element){
						self._addSurchargeRow(element, false);
					});
					
				}
				
				self._elementSurcharges.fadeIn('slow');
			}

		},

		/**
		 * Naplnění selektu příplatky - třídíme do skupin
		 * @param  {array} requiredSurcharges Povinné příplatky
		 * @param  {array} optionalSurcharges Nepovinné příplatky
		 * @param  {object} select            Selekt kam vkládáme
		 */
		_fillSurchargesInSelect : function(requiredSurcharges, optionalSurcharges, select) {
			var self = this;

			//přidáme všechny volby
			if (select && select.length) {
				//volba "přidat příplatek"
				select.empty().append(
						//první hodnota "Přidat příplatek"
						$('<option>', {
							value : -1,
							html : cesys.ts('Add surcharge')
						})
					);

				//povinné
				if (requiredSurcharges.length) {
					var optGroupRequired = $('<optgroup>', {
						'label' : cesys.ts('Required surcharges'),
						'class' : 'required'
					});

					$.each(requiredSurcharges, function(index, surcharge){
						optGroupRequired.append(
							$('<option>', {
								value : surcharge.id,
								html : surcharge.name + ' (' + cesys.utils.formatPrice(surcharge.price, surcharge.currencyId) + ')'
							})
						);
					}); 

					select.append(optGroupRequired);
				}

				//nepovinné
				if (optionalSurcharges.length) {
					var optGroupOptional = $('<optgroup>', {
						'label' : cesys.ts('Optional surcharges'),
						'class' : 'optional'
					});

					$.each(optionalSurcharges, function(index, surcharge){
						optGroupOptional.append(
							$('<option>', {
								value : surcharge.id,
								html : surcharge.name + ' (' + cesys.utils.formatPrice(surcharge.price, surcharge.currencyId) + ')'
							})
						);
					});

					select.append(optGroupOptional);
				}
			}
		},

		/**
		 * Dostane příplatek ze vstupních dat a umísté ho na správné místo do selektu příplatků
		 * @param {object} surcharge
		 */
		_addSurchargeSelectOption : function(surcharge){
			var self = this;

			//podle příplatku se orzhodneme do které skupiny vložit
			var destination = null;
			if (surcharge.required) {
				destination = self._surchargeSelect.find('optgroup.required');
			}else{
				destination = self._surchargeSelect.find('optgroup.optional');
			}

			destination.show().append($('<option>', {
				value : surcharge.id,
				html : surcharge.name + ' (' + cesys.utils.formatPrice(surcharge.price, surcharge.currencyId) + ')'
			}));
		},


		/**
		 * Přídání volitelného příplatku do tabulky
		 * @param {int} surchargeIndex Index příplatku v datech
		 */
		_addSurchargeRow : function(surcharge, deletable){
			var self = this;

			//jen pokud příplatek najdeme, tak ho přidáme (blblo v IE 8, protože se spouštěla událost change i když byla ručně vybraná volba -1)
			if (surcharge) {
				var priceRow = self._createPriceRow(surcharge, deletable);
				self._elementSurchargesTable.append(priceRow);

				//přepočítat ceny
				self._calculate();
			}

		},


		/**
		 * Vytvoří jeden řádek do tabulky s cenami
		 * @param  {object} price jedna ceníková položka
		 * @param  {bool} deletableSurcharge Vložit odkaz pro odebrání ceny? - pro volitelné příplatky
		 * @return {object}       Jquery objekt s řádkem tabulky
		 */
		_createPriceRow : function(price, deletableSurcharge) {
			var self = this,
                options = self.options;

			var output = $('<tr>', {
				'class' : 'price-row',
				'data-id' : price.id,
				'data-currency-id' : price.currencyId,
				'data-per-day' : price.perDay,
				'data-value' : price.price,
				'data-storno' : price.storno,
				'data-name' : $("<i>").html(price.name).text(), //zbavíme se html entit
			});

			var descriptionCell = $('<td>', {
				'class' : 'description',
				html : price.name
			});
			
			//pokud je deletableSurcharge true (volitelný příplatek), přidáme odkaz na odebrání řádku
			if (deletableSurcharge) {
				output.addClass('deletable');
				descriptionCell.prepend(' ', 
					$('<a>', {
						html : '&times;',
						'class' : 'remove-surcharge',
						title : cesys.ts('Remove surcharge')
					})
					.bind('click', function(){
						$(this).closest('tr').fadeOut('fast', function(){
							var surchargeId = $(this).data('id');
							var surcharge = self._getSurchargeById(surchargeId);

							//vrátíme příplatek do selektu
							self._addSurchargeSelectOption(surcharge);

							//smažeme debug a samotný příplatek
							$(this).next('.debug').remove();
							$(this).remove();
						});
					})
				);
			}

			var priceCell = $('<td>', {
				'class' : 'price',
				html : self._formatPrice(price.price, price.currencyId)
			});

			var countCell = $('<td>', {
				'class' : 'count'
			});

			//selekt jen pokud je cena ve správné měně
			if (price.currencyId == self._targetCurrencyId){
                var disabled = false;
                if (options.disableChanges == 1) {
                    disabled = true;
                }
				countCell.append(
					$('<select>')
						.append(self._getPriceCountOptions())
						.val(price.count)
                        .prop('disabled', disabled)
						.bind('change', function(){
							self._calculate();
						})
				);
			}

			var totalCell = $('<td>', {
					'class' : 'total'
				})
				.append(
					$('<span>', {
						'class' : 'item-sum',
						'text' : '0'
					})
				);

			output
				.append(descriptionCell)
				.append(priceCell)
				.append(countCell)
				.append(totalCell);

			//debug přidáme pokud je vyplněn
			if (typeof price.debug == "string" && price.debug.length > 1){
				output = output.add('<tr class="debug"><td class="debug" colspan="4">' + price.debug + '</td></tr>');
			}

			return output;
		},


		/**
		 * Vyplní ke každé service tabulku
		 */
		_fillServices : function(services) {
			var self = this;

			//původní servicy odstraníme
			self.element.find('.section.service').remove();

			//nové projdeme a přidáme
			if (services) {
				$.each(services, function(index, service){
					//wrapper div služby s hlavičkou
					var sectionDiv = $('<div>', {
						'class' : 'section service ' + service.id,
						'data-service-id' : service.id
					})
					.append($('<div>', {
						'class' : 'section-header',
						html : service.name
					}));

					//přidáme popis služby
					var sectionDescription = $('<div>', {
						'class' : 'clearfix description',
						html : service.description
					}).appendTo(sectionDiv);

					//pokud má služba obrázek, vložíme ho do popisu
					if (service.image && service.image.length){
						sectionDescription.prepend($('<img>', {
							src : service.image,
							alt : service.name
						}));
					}

					//přidáme ceník služby
					var priceTable = $('<table>', {
						'class' : 'prices'
					})
					.append('<tr> \
						<th class="description">' + cesys.ts('Description') + '</th> \
						<th class="price">'       + cesys.ts('Price')       + '</th> \
						<th class="count"></th> \
						</tr>')
					.appendTo(sectionDiv);
					
					//naplnění ceníku
					$.each(service.prices, function(index, price){
						priceTable.append(self._createServicePriceRow(price, service.id ));
					});

					//na změnu v inputech přepočteme ceny
					priceTable.find('input').bind('change', function(){
						self._calculate();
					});

					//doplňkový text služby pokud je
					if (service.text && service.text.length){
						sectionDiv.append($('<div>', {
							'class' : 'clearfix text',
							html : service.text
						}));
					}

					//vyplňujeme na konec za nepovinné příplatky
					self._elementSurcharges.after(sectionDiv);
					
				});
			}
			
		},


		/**
		 * Vytvoří jeden řádek do tabulky s cenami služeb
		 * @param  {object} price jedna ceníková položka
		 * @param  {string} serviceId Id služby
		 * @return {object}       Jquery objekt s řádkem tabulky
		 */
		_createServicePriceRow : function(price, serviceId) {
			var self = this;

			var output = $('<tr>', {
				'class' : 'service-price-row',
				'data-type-id' : price.id,
				'data-currency-id' : price.currencyId,
				'data-per-day' : price.perDay,
				'data-value' : price.price,
				'data-storno' : price.storno,
				'data-name' : $("<i/>").html(price.name).text(), //zbavíme se html entit
				'data-service-id' : serviceId
			});

			var descriptionCell = $('<td>', {
				'class' : 'description',
				html : price.name
			});

			var priceCell = $('<td>', {
				'class' : 'price',
				html : self._formatPrice(price.price, price.currencyId)
			});

			var radioCell = $('<td>', {
				'class' : 'count'
			}).append($('<input>', {
				type : 'radio',
				value : 1,
				name : serviceId,
				checked : price.checked //v datech by měl být boolean
			}));

			output
				.append(descriptionCell)
				.append(priceCell)
				.append(radioCell);

			return output;
		},

		/**
		 * Obnoví volby v selektech u ceníkových položek podle počtu dospělých a dětí
		 */
		_refreshPriceSelectOptions : function(){
			var self = this;

			//sezna options do selektu
			var options = self._getPriceCountOptions();

			//projdeme selekty, vyplníme volby a pokusíme se znovu nastavit původní hodnotu
			var selects = self._elementRoomTable
								.add(self._elementSurchargesTable)
								.find('select');			

			$.each(selects, function(index, select){
				var oldValue = $(select).val();
				$(select).html(options.clone()).val(oldValue);
			});

		},

		/**
		 * Vrátí seznam <option> pro selekt u ceníkové položky - podle počtu dětí a dospělých
		 * @return {object} Jquery kolekce options pro selekt
		 */
		_getPriceCountOptions : function(){
			var self = this;

			var personsCount = parseInt(self.options.adultCount) + parseInt(self.options.childCount);
			var selectOptions = $();

			for (var i = 0; i <= personsCount; i++) {
				selectOptions = selectOptions.add($('<option>', {
					'value' : i,
					'text' : i
				}));
			}

			return selectOptions;
		},

		/**
		 * Přepočtení všech cen dle vybraných počtů
		 */
		_calculate : function(){
			var self = this;

			var totalPrice = 0;
			var totalStornoPrice = 0;

			//standardní ceníkové položky - pokoj + příplatky
			var prices = self._elementRoomTable
							.add(self._elementSurchargesTable)
							.find('tr.price-row');

			$.each(prices, function(index, priceRow){
				priceRow = $(priceRow);
				var price = parseFloat(priceRow.data('value'));
				var stornoPrice = parseFloat(priceRow.data('storno'));
				var perDay = priceRow.data('per-day');
				var currency = priceRow.data('currency-id');
				var count = priceRow.find('select').val();
				var currentPriceTotal = 0;
				var currentStornoPrice = 0;
					
				//počítáme jen když je položka ve správné měně
				if (currency == self._targetCurrencyId){
					//výpočet hodnoty aktuální položky
					if (perDay){
						currentPriceTotal = count * price * self._duration;
						currentStornoPrice = count * stornoPrice * self._duration;
					}else{
						currentPriceTotal = count * price;
						currentStornoPrice = count * stornoPrice;
					}

					//vyplnění ceny aktuální položky
					priceRow.find('.item-sum').html(self._formatPrice(currentPriceTotal, currency));
					
					//přidání do celkové ceny
					totalPrice += currentPriceTotal;
					totalStornoPrice += currentStornoPrice;
				}

			});

			//služby
			prices = self.element.find('.service .service-price-row input:checked');
			$.each(prices, function(index, input){
				var priceRow = $(input).closest('tr');
				var price = parseFloat(priceRow.data('value'));
				var stornoPrice = parseFloat(priceRow.data('storno'));
				var perDay = priceRow.data('per-day');
					
				//výpočet přírůstku aktuální položky
				if (perDay){
					totalPrice += price * self._duration;
					totalStornoPrice += stornoPrice * self._duration;
				}else{
					totalPrice += price;
					totalStornoPrice += stornoPrice;
				}
				
			});

			//pokud tam chekbox je (jen HU)
			if (self._elementStornoPriceCheckbox && self._elementStornoPriceCheckbox.length){
				//u nulove ceny disable a odskrtnuti
				if (totalStornoPrice == 0){
					self._elementStornoPriceCheckbox
					 	.prop('checked', false)
					 	.prop('disabled', true)
				}else{
					//jinak checkbox povolime (u jiz povoleneho nema zadny efekt)
					self._elementStornoPriceCheckbox.prop('disabled', false);

					//pokud je i zaškrtnutý, připočteme do hlavní ceny
					if (self._elementStornoPriceCheckbox.is(':checked')){
						totalPrice += totalStornoPrice;						
					}
				}

			}

			this._elementTotalPrice.html(self._formatPrice(totalPrice, self._targetCurrencyId));
			this._elementStornoPrice.html(self._formatPrice(totalStornoPrice, self._targetCurrencyId));

		},

		/**
		 * Naformátuje a vyplní cenu včetně altPrice
		 * @param  {float} value Cena
		 */
		_formatPrice : function(price, currency) {
			var priceText = window.cesys.utils.formatPrice(price, currency);
			var altPriceText = window.cesys.utils.formatAltPrice(price, currency, this._dataSource);
			if (altPriceText){
				priceText = priceText + ' (' + altPriceText + ')';
			}
			return priceText;
		},

		/**
		 * Ověření dostupnosti u pokoje - pokud se neověřuje, skryje span pro výsledek dostupnosti
		 * @param  {int} roomId Id pokoje
		 */
		_checkRoomAvailability : function(roomId){
			var self = this;

			//loadovací obrázek
			var img = $('<img>', {
				src : window.config.fileServer + "/img/availability/loading.gif",
				'class' : 'img-availability img-loading'
			});
			var text = $('<span>', {
				'class' : 'text-availability',
				text : cesys.ts('Room availability') + ': '
			});
			self._elementRoomAvailability.empty().append(text).append(img).show();

			//ověříme dostupnost pokoje - trvalý stav on-reqest se u pokoje neřeší
			if (self._availability == 1 && typeof self._hashAvailability !== 'undefined' && self._hashAvailability !== ''){
				window.cesys.service.availability.configuration.lang = window.config.cesysLang;
				var childAgesAsString = '';
				if (typeof self.options.childAges === 'Array') {
					childAgesAsString = self.options.childAges.join();
				}
				window.cesys.service.availability.doCheckRoom(
					{
						'cid'  : self._dateId,
						'hash' : self._hashAvailability,
						'mode' : 'room',
						'adultCount' : self.options.adultCount,
						'childCount' : self.options.childCount,
						'childAges'  : childAgesAsString,
						'rid'  : roomId
					},
					function(status) {
						img.removeClass('img-loading');
						switch (status) {
							case 'OK':
								img.addClass('img-ok availability-small')
									.attr('src', window.config.fileServer + "/img/availability/ok.svg")
									.attr('alt', cesys.ts('Volná kapacita'))
									.attr('title', cesys.ts('Volná kapacita'));
								text.append(cesys.ts('VOLNO'));
								break;

							case 'RQ':
							case 'ER':
								img.addClass('img-rq availability-small')
									.attr('src', window.config.fileServer + "/img/availability/rq.svg")
									.attr('alt', cesys.ts('Na dotaz'))
									.attr('title', cesys.ts('Na dotaz'));
								text.append(cesys.ts('NA DOTAZ'));
								break;

							case 'NK':
							case 'SO':
								//sold out stav zpraucjeme dle nastavení
								if (window.config.settings.availability_allow_order_sold_out) {
									img.addClass('img-rq availability-small')
										.attr('src', window.config.fileServer + "/img/availability/rq.svg")
										.attr('alt', cesys.ts('Na dotaz'))
										.attr('title', cesys.ts('Na dotaz'));
									text.append(cesys.ts('NA DOTAZ'));
								}else{
									img.addClass('img-so availability-small')
										.attr('src', window.config.fileServer + "/img/availability/fail.svg")
										.attr('alt', cesys.ts('Obsazená kapacita'))
										.attr('title', cesys.ts('Obsazená kapacita'));
									text.append(cesys.ts('OBSAZENO'));
								}
								break;
						}
					}
				);

			}else{
				self._elementRoomAvailability.hide();
			}
		},

		// veřejné metody ----------------------------------------------------------------------
		
		/**
		 * Nastaví obsazenost
		 * @param {int} adultCount Počet dospělých
		 * @param {int} childCount Počet dětí
		 * @param {int} childAges  Pole s věky dětí
		 */
		setOccupancy : function(adultCount, childCount, childAges){
			this.options.adultCount = parseInt(adultCount);
			this.options.childCount = parseInt(childCount);
			this.options.childAges = childAges;
			
			//obnovení počtu voleb v selektech u ceníkových položek
			this._refreshPriceSelectOptions();

			this._loadData();
		},

		/**
		 * Vrátí vyplněný ceník - vybraný pokoj s vybranými ceníkovými položkami + služby
		 * - služby vrací v property services pod id služby, hodnota je null pokud služba nebyla vybraná, nebo je objekt s vybranou položkou
		 * @return {object} 
		 */
		getData : function(){
			var self = this;

			var output = {};
			output.room = self._getRoomByIndex((self._roomSelect && self._roomSelect.length) ? self._roomSelect.val() : 0);
			output.room.prices = {}; //přepíšeme původní seznam cen k pokoji - dáme jen vyplněné
			output.room.surcharges = {}; //přepíšeme původní seznam příplatků k pokoji - dáme jen vyplněné
			output.services = {};
			output.storno = false;

			//zpracování cen k pokoji - vybíráme z tabulky v html
			var selectedPrices = self._elementRoomTable.find('.price-row select');
			selectedPrices = selectedPrices.filter(function(){
				return $(this).val() > 0;
			}).closest('tr');

			$.each(selectedPrices, function(index, element){
				output.room.prices[index] = {
					count : $(element).find('select').val(),
					id : $(element).data('id'),
					value : $(element).data('value'),
					perDay : $(element).data('per-day'),
					storno : $(element).data('storno'),
					name : $(element).data('name'),
					currencyId : $(element).data('currency-id')
				};
			});

			//zpracování příplatků - vybíráme z tabulky v html - ukládáme pod pokoj
			selectedPrices = self._elementSurchargesTable.find('.price-row select');
			selectedPrices = selectedPrices.filter(function(){
				return $(this).val() > 0;
			}).closest('tr');

			$.each(selectedPrices, function(index, element){
				output.room.surcharges[index] = {
					id : $(element).data('id'),
					count : $(element).find('select').val(),
					value : $(element).data('value'),
					perDay : $(element).data('per-day'),
					storno : $(element).data('storno'),
					name : $(element).data('name'),
					currencyId : $(element).data('currency-id')
				};
			});

			//zpracování services - projdeme všechny tabulky služeb
			var services = self.element.find('.service');
			$.each(services, function(index, service){
				//do návratové proměnné přidáme null položku s názvem služby - i když nebyla vybraná, budeme vědět že byla v nabídce
				var serviceId = $(service).data('service-id');
				output.services[serviceId] = null;

				//najdeme vybranou volbu služby
				var selectedOption = $(service).find('input:checked').closest('tr');

				//pokud byla vybraná validní položka, přidáme ji do dat
				if (selectedOption.data('value')){
					output.services[serviceId] = {
						value : selectedOption.data('value'),
						perDay : selectedOption.data('per-day'),
						storno : selectedOption.data('storno'),
						currencyId : selectedOption.data('currency-id'),
						typeId : selectedOption.data('type-id'),
						name : selectedOption.data('name'),
						count : 1
					};
				}
			});

			//storno ano / ne
			if (self._elementStornoPriceCheckbox && self._elementStornoPriceCheckbox.length) {
				output.storno = self._elementStornoPriceCheckbox.is(':checked');
			}
			
			//odjezdy
			output.departure = null;
			if (self.element.find('#departure').length){
				output.departureId  = parseInt(self.element.find('#departure').val());
			}
			return output;
		}

	});

})(jQuery, window, document);
