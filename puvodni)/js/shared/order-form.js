
;(function ($, window, document, undefined) {
	$.widget('cesys.orderForm', {
		/**
		 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
		 * $(selector).orderForm({
		 * 		optionName : optionValue
		 * });
		 */
		options: {

			//výchozí počet dospělých
			adultCount : 2,

			//výchozí počet dětí
			childCount : 0,

			//validační pravidla
			validationRules : []
		},

		/**
		 * Konstruktor - načtení seznamů, bindování funkcí
		 */
		_create : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn

			//počet dospělých možno předat atributem
			if (element.data('adult-count')) {
				options.adultCount = element.data('adult-count');				
			}

			//počet dospělých možno předat atributem
			if (element.data('child-count')) {
				options.childCount = element.data('child-count');				
			}

			//validační pravidla možno předat atributem
			if (element.data('validation-rules')) {
				options.validationRules = element.data('validation-rules');

				//pokud se obsah nenaparsuje jako json, zkusíme to manuálně
				if (typeof options.validationRules == "string"){
					try	{
						options.validationRules = $.parseJSON(options.validationRules);
					}catch(error){
						console.log("Validation rules in attribute are not valid: " + error.message);
					}
				}
            }

            // Vyhledávací parametry z masky base64(serialize())
            options.searchParams = element.data('search-params');

			//povinní účastníci? - potřebujeme při změně obsazenosti při přegenerování inputů
			//vyplněno v applyValidationRules
			self._areParticipantsRequired = false;

			//elementy
			self._elementParticipantsTable = element.find('.booking-participants');

			//naplnění tabulky s účastníky
			self._fillParticipantsTable();

			//odkaz "kopírovat údaje objednatele ..."
			self._elementParticipantsTable.find('.copy').bind('click', function(event){
				var firstParticipant = self._elementParticipantsTable.find('.participant-row').first();

				if (firstParticipant.length){
					firstParticipant.find('.first-name input').val(
						element.find('#firstName').val()
					);

					firstParticipant.find('.last-name input').val(
						element.find('#lastName').val()
					);
				}

				event.preventDefault();
			});

			//na submit vlastní událost s daty formuláře
			element.bind('submit', function(event){
				event.preventDefault();
				element.trigger('orderSubmit', self.getData());
			});

			//aplikace validačních pravidel
			self._applyValidationRules();

		},

		/**
		 * Aplikuje validační pravidla  - výchozí nebo předaná atributem, nastaví proměnnou _areParticipantsRequired
		 */
		_applyValidationRules : function(){
			var self = this;
			var element = self.element;
			var validationRules = self.options.validationRules;

			$.each(validationRules, function(index, rule){
				if (!rule.input){
					return true; //ekvivalent continue
				}
				//speciální případ pokud příjde klíč "participants" - účastníky nastavíme jako povinné, nastavíme proměnnou _areParticipantsRequired
				else if(rule.input === "participants"){
					self._areParticipantsRequired = true;
					self._elementParticipantsTable.find('.participant-row input').each(function(){
						$(this).attr('required', true);
					});
					self._elementParticipantsTable.find('.colnames th').each(function(){
						$(this).append(' (*)');
					});
					return true
				}

				//najdeme input a přidáme atributy
				var input = element.find('#' + rule.input);
				if (input.length){
					if (rule.required){
						input.attr('required', true);
						element.find('label[for='+rule.input+']').append(' (*)');
					}
					if (rule.pattern){
						input.attr('pattern', rule.pattern);
					}
					if (rule.message){
						input.attr('title', rule.message);
						input.attr('x-moz-errormessage', rule.message);
					}
				}
			});

		},

		/**
		 * Vyplnění tabulky s účastníky - dle počtu dospělých a dětí
		 */
		_fillParticipantsTable : function(){
			var self = this;

			self._elementParticipantsTable.find('tr.participant-row').remove();

			for (var i = 0; i < self.options.adultCount; i++) {
				self._elementParticipantsTable.append(self._createParticipantRow(false));
			};

			for (var i = 0; i < self.options.childCount; i++) {
				self._elementParticipantsTable.append(self._createParticipantRow(true));
			};
		},


		/**
		 * Vytvoří jeden řádek do tabulky s účastníky
		 * @param  {bool} child True pokud má být řádek pro dítě
		 * @return {object}       Jquery objekt s řádkem tabulky
		 */
		_createParticipantRow : function(child) {
			var self = this;

			var output = $('<tr>', {
				'class' : 'participant-row',
				'data-child' : child
			});

			var titleCell = $('<td>', {
				'class' : 'title'
			});

			if (child) {
				titleCell.append(cesys.ts('Child'));
				titleCell.append('<input type="hidden" value="3" />');
			}else{
				titleCell.append(
					$('<select>', {
						'class' : 'form-control size-small', //pro adminu
						html : self._getTitleOptions()
					})
				);				
			}

			var nameCell = $('<td>', {
				'class' : 'first-name',
			})
			.append($('<input>', {
				'class' : 'form-control size-small', //pro adminu
				type : 'text',
				required : self._areParticipantsRequired
			}));

			var surnameCell = $('<td>', {
				'class' : 'last-name',
			})
			.append($('<input>', {
				'class' : 'form-control size-small', //pro adminu
				type : 'text',
				required : self._areParticipantsRequired
			}));

			var birthCell = $('<td>', {
				'class' : 'birth'
			})
			.append($('<input>', {
				'class' : 'form-control size-small', //pro adminu
				type : 'text',
				title : cesys.ts('//gdpr booking birthday title//'),
				required : self._areParticipantsRequired,
				pattern : '(([\\d]{4}-?[0|1]?[\\d]-?[0|1|2|3]?[\\d])|([0|1|2|3]?[\\d]\\.[0|1]?[\\d]\\.[\\d]{4}))'
			}));

			output
				.append(titleCell)
				.append(nameCell)
				.append(surnameCell)
				.append(birthCell);

			return output;
		},

		/**
		 * Vrátí seznam options do selektů s oslovením - jen pro dospělé
		 */
		_getTitleOptions : function() {
			options = $().add($('<option>', {
				value : 0,
				text : cesys.ts('Mr')
			}))
			.add($('<option>', {
				value : 1,
				text : cesys.ts('Mrs')
			}))
			.add($('<option>', {
				value : 2,
				text : cesys.ts('Miss')
			}));

			return options;
		},


		/**
		 * Sestavení json objektu s daty z vyplněného formuláře
		 */
		getData : function() {
			var self = this;
			var element = self.element;
			var data = {};
			
			//hlavní data formuláře
			data.firstName = element.find('#firstName').val();
			data.lastName = element.find('#lastName').val();
			data.phone = element.find('#phone').val();
			data.email = element.find('#email').val();
			data.street = element.find('#street').val();
			data.city = element.find('#city').val();
			data.postCode = element.find('#postCode').val();
			data.country = element.find('#country').val();
			data.note = element.find('#text').val();
			data.CsClientId = element.find('#CsClient').val();
			data.businessCaseId = element.find('#business_case_id').val();
            data.searchParams = self.options.searchParams;

			//účastníci
			data.participants = [];
			self._elementParticipantsTable.find('.participant-row').each(function(idnex, row){
				var participant = {};
				participant.title = parseInt($(row).find('.title select, .title input').val());
				participant.firstName = $(row).find('.first-name input').val();
				participant.lastName = $(row).find('.last-name input').val();
				participant.birth = $(row).find('.birth input').val();
				data.participants.push(participant);
			});

			//zaškrtávací podmínka
			if (element.find('#checkCondition').length){
				data.checkCondition = element.find('#checkCondition').prop('checked');
			}

			return data;
		},

		/**
		 * Nastavení nové skladby osob a přenastavení tabulky s účastníky
		 * @param {int} adultCount Počet dospělých
		 * @param {int} childCount Počet dětí
		 */
		setOccupancy : function(adultCount, childCount){
			this.options.adultCount = parseInt(adultCount);
			this.options.childCount = parseInt(childCount);

			this._fillParticipantsTable();
		}

	});

})(jQuery, window, document);

