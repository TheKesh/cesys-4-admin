/**
 * @deprecated - původně v topical offers - nahrazeno Ondrovou verzí
 *             - momentálně se verze pluginu nikde nepoužívá
 *
 * 
 *	Vzor pro vytvoření jQuery pluginu stavějící na prototypech.
 *
 *  Konstrukční funkce je funkce vytvářející instanci třídy objektu.
 *	Instanční funkce je funkce vytváčející instanci objektu. Obvykle ji pojmenováváme 
 *		init, a volá se podle zvyklostí, v případě, kdy jako první parametr je 
 *		tabulka nastavení, případně je prázdný.
 *	Option funkce je funkce, která se volá jménem uvedeným jako první parametr. 
 *		V případě, že druhý parametr je prázdný, funguje jako getter, v případě, 
 *		že je vyplněn funguje jako setter.
 *
 *  Licensed under both the MIT license and the GNU GPLv2 (same as jQuery: http://jquery.org/license)
 */
if (jQuery)(function($) {



	$.fn.multiSelectBoxNE = (function ()
	{

		/**
		 * Odstranění duplicit
		 *
		 * @param Array a
		 * @return Array
		 */
		function arrayUnique(a)
		{
			var unique = [];
			$.each(a, function(i, el){
			    if($.inArray(el, unique) === -1) unique.push(el);
			});

			return unique;
		}



		/**
		 * Pokud v poli prvek je, tak jej vyjme, pokud není, tak jej přidá.
		 *
		 * @param Array arr
		 * @param mixin m
		 * @return Array
		 */
		function arrayToggle(arr, m)
		{
		    if (arr.indexOf(m) < 0) {
		        arr.push(m);
		    }
		    else {
		    	while(arr.indexOf(m) >= 0) {
		    		arr.splice(arr.indexOf(m), 1);
		    	}
		    }

		    return arr;
		}



		/**
		 * Construct Function
		 *
		 * @param string | object method Název akce, nebo nastavení konstruktoru.
		 */
		function multiSelectBoxNE(method)
		{
			/**
			 *	Defaultní konfigurace pluginu.
			 */
			this.defaults = {
				select0: 'not selected',
				selects: 'Select rows: ',
				'klass': 'subtype-ne',
				permanentCtrlKey: true,
				updateLabel: multiSelectBoxNE.prototype.updateLabel,
				updateOptions: multiSelectBoxNE.prototype.updateOptions,
				buildOption: multiSelectBoxNE.prototype.buildOption,
				toggleOption: multiSelectBoxNE.prototype.toggleOption,
				filterData: multiSelectBoxNE.prototype.filterData,

				_doneInit: function(control) {

					/**
					 * vyhledávání potomků.
					 */
					function findChildrenX(a, control, val)
					{
						control.find('option[data-parent="' + val + '"]').each(function(){
							a.push($(this).val());
							a = findChildrenX(a, control, $(this).val());
						});
						return a;
					}

					var maskedcontrol = control.data('selectBox-control'),
						maskedoptions = maskedcontrol.data('selectBox-options');

					$.each($(control).val() || [], function(i,m) {
						var a = findChildrenX([], $(control), m);
						$.each(a, function(i, m) {
							var el = $(maskedoptions).find('input[value="' + m + '"]');
							el.attr('checked', 'checked');
							el.parent().parent().parent().addClass('selectBox-selected');
						})
					});
				},
				age: 42
			};


			/**
			 *	Zpracování všech elementů selektoru.
			 */
			var _this = this;
			return this.each(function(index, el) {
				//	Instantion method
				if (typeof method === 'object' || !method) {
					return multiSelectBoxNE.prototype.init.call(this, $.fn.extend(_this.defaults, method || {}));
				}
				//	Option method
				else if (multiSelectBoxNE.prototype[method]) {
					return multiSelectBoxNE.prototype[method].apply(this, Array.prototype.slice.call(arguments, 1));
				}
				else {
					$.error('Method ' + method + ' does not exist on jQuery.multiSelectBoxNE');
				}
			});
		}



		/**
		 * Inicializate function for instantion.
		 *
		 * @param el jQuery selector for one element.
		 * @param setting Object liter object with setting.
		 */
		multiSelectBoxNE.prototype.init = function(settings) 
		{
			$(this)
				.selectBox(settings);
			return this;
		};



		/**
		 * Obsah hlavičky selectboxu.
		 * Pokud 0 => nevybráno
		 * Pokud nevybrána skupina států, tak zobrazit název skupiny (Egypt)
		 * Jinak použít název.
		 * Pokud je vybráno více jak 2, tak zobrazit součet, a ne názvy.
		 *
		 * @param label Control with text's label.
		 * @param select Control with options.
		 */
		multiSelectBoxNE.prototype.updateLabel = function(label, select)
		{
			var text = this.select0,
				selected = select.find('OPTION:selected');
			
			if (selected.length) {
				if (selected.length == 1 && $(selected[0]).val() == 0) {
					//	eh
				}
				else {
					var count = selected.length,
						a = [];
					if (select.find('OPTION:selected[value="0"]').length) {
						count = count - 1;
					}

					selected.each(function () {
						if (a.indexOf($(this).val()) === -1) {
							a.push($(this).val());
						}
					});

					//	Do tří slovně, pak číselně.
					if (a.length < 3) {
						text = $.map(a, function(m) {
							var opt = select.find('OPTION[value="' + m + '"]');

							//	Pokud se jedná o "nerozhoduje" u názvu skupiny = stát.
							var parent = opt.parents('OPTGROUP').first();
							if (parent.find('OPTION').first().val() == opt.val()) {
								return parent.attr("label");
							}
							return select.find('OPTION[value="' + m + '"]').html();
						})
						.join(", ");

					}
					else {
						text = this.selects + a.length;
					}
				}
			}
			
			label.text(text);
		};



		/**
		 * Buď je zakšrtnutý 0, nebo ostatní - exklusivně.
		 * Pokud je zaškrtnutý parent skupiny, nesmí být zaškrtnutý žádný potomek skupiny a naopak.
		 * Pokud je začkrtnutý parent, ale nejedná se o parent/stát, tak se vizuelně zaškrtnou všechny potomci.
		 *
		 * @param HTMLElement options Wraper selecboxu
		 * @param HTMLSelectElement select Original select = model.
		 */
		multiSelectBoxNE.prototype.updateOptions = function(options, select)
		{
			//	Reset původních hodnot
			options.find('LI').removeClass('selectBox-selected');

			//	Nastavení podle modelu
			var values = $(select).val() || [];
			if (values.length && values != ["0"]) {
				$.each(values, function(i, m) {
					var a = options.find('A[rel="' + m + '"]')
					a.parents('LI').addClass('selectBox-selected');
					a.find('INPUT').attr('checked', 'checked');
				});
			}
			else {
				var a = options.find('A[rel="0"]')
				a.parents('LI').addClass('selectBox-selected');
				a.find('INPUT').attr('checked', 'checked');
			}
		};



		/**
		 * Generování jednoho řádku.
		 * @param HTMLOptionElement option
		 */
		multiSelectBoxNE.prototype.buildOption = function(option)
		{
			var _this = this,
				indent = $(option).attr('data-indent'),
				select = $(option).parents('select'),
				li = $('<li />'),
				a = $('<a />'),
				label = $('<label />'),
				input = $('<input />', {
						'type': 'checkbox',
						'value': option.val(),
						change: function(event) {
							return false;
						},
						click: function(event) {
							return false;
						}
						});
			li.addClass(option.attr('class'));
			li.data(option.data());

			a.attr('rel', option.val())
				.text(option.text())

			//	Vizuelní odsazení
			if (indent) {
				a.css({'padding-left': (parseInt(indent) * 1) + 'em'});
			}

			/*/	Zaškrtnutí.
			if (select.val()) {
				$.each(select.val(), function(index, value) {
					if (value != 0 && (value == $(option).val())) {
						input.attr('checked', 'checked');
					}
				});
			}
			else {

			}*/

			//	elementy do DOM
			a.prepend(input);
			label.append(a);
			li.append(label);

			if (option.attr('disabled')) {
				li.addClass('selectBox-disabled');
			}
			/*
			if (option.attr('selected')) {
				if (option.val() == 0) {
					var v = option.parent().val();
					if (v && v.length == 1) {
						li.addClass('selectBox-selected');
					}
				}
				else {
					li.addClass('selectBox-selected');
				}
			}
			*/
			return li;								
		};




		/**
		 * Buď je zakšrtnutý 0, nebo ostatní - exklusivně.
		 * Pokud je zaškrtnutý parent skupiny, nesmí být zaškrtnutý žádný potomek skupiny a naopak.
		 * Pokud je zaškrtnuý parent/stát, tak je možné zaškrtnout potomka, a tím zaškrtnutí parent/stát zrušit.
		 * Pokud je zaškrtnutý parent/skupiny, tak není možné zaškrtnout potomka.
		 * Pokud je zaškrtnutý potomek, tak zaškrtnutím parent/skupiny, nebo parent/stát se tento zruší a 
		 *	zaškrtně se požadovaný parent/skupiny,nebo parent/stát
		 *
		 * @param HTMLSelectElement control Model, do kterého zapisujeme prvky.
		 * @param int val Který prvek byl změněn.
		 * @param object triggerData 
		 * @param array values Hodnoty zvolených prvků.
		 *
		 * @return [] of ids
		 */
		multiSelectBoxNE.prototype.filterData = function(control, val, triggerData, values)
		{
			/**
			 * vyhledávání potomků.
			 * @param array a IO
			 * @param Element control Zdroj dat.
			 * @param int val base, ze které vyhledáváme potomky.
			 */
			function findChildren(a, control, val)
			{
				control.find('option[data-parent="' + val + '"]').each(function(){
					a.push($(this).val());
					a = findChildren(a, control, $(this).val());
				});
				return a;
			}

			//	Odstranění 0
			values = $.fn.selectBox.prototype.filterData(control, val, triggerData, values);

			var maskedcontrol = control.data('selectBox-control'),
				maskedoptions = maskedcontrol.data('selectBox-options');

			//	Zkontrolujeme, zda není parent zaškrtnutý, to bychom to zakázali. Stačí vizuelní zaškrtnutí.
			var parentid = control.find('option[value="' + val + '"]').attr('data-parent');
			var checked = maskedoptions.find('input[value="' + parentid + '"]').attr('checked');
			if (parentid) {
				var rootid = control.find('option[value="' + val + '"]').parents("OPTGROUP").first().find("OPTION").first();
				rootid = rootid.val();
			}

			//	Nepovolit odškrtnutí, ledaže by parent byl "nerozhoduje"
			if (checked && parentid != rootid) {
		    	while(values.indexOf(val) >= 0) {
		    		values.splice(values.indexOf(val), 1);
		    	}
			}

			//	Pokud nejsme parent AND jsou zaškrtnuté nějací potomci -> potomky vyhodit
			//	Všechny potomky odškrtnout, protože to se dělá vizuelně.
			var a = findChildren([], control, val);
			$.each(a, function(i, m) {
		    	while(values.indexOf(m) >= 0) {
		    		values.splice(values.indexOf(m), 1);
		    	}
			});

			//	Odškrtnutí parenta
			if (rootid && rootid != val && values.indexOf(rootid) >= 0) {
	    		values.splice(values.indexOf(rootid), 1);				
			}

			return values;	
		}



		/**
		 * Vizuelní přepnutí stavu optionu.
		 *
		 * @param HTMLElementSelect control Zdrojový model.
		 * @param HTMLLiElement li
		 * @param object opts
		 */
		multiSelectBoxNE.prototype.toggleOption = function(control, li, opts)
		{
			
			/**
			 * vyhledávání potomků.
			 * @param array a IO
			 * @param Element control Zdroj dat.
			 * @param int val base, ze které vyhledáváme potomky.
			 */
			function findChildren(a, control, val)
			{
				control.find('option[data-parent="' + val + '"]').each(function(){
					a.push($(this).val());
					a = findChildren(a, control, $(this).val());
				});
				return a;
			}

			$.fn.selectBox.prototype.toggleOption(control, li, opts);

			var selections = arrayUnique(control.val() || []),
				val = $(li).find('a').attr('rel'),
				checked = false;

			//	Konkrétní zvolený.
			if (!selections || selections.indexOf(val) < 0) {
				$(li).parent().find('input[value="' + val + '"]').attr('checked', null);
			}
			else {
				$(li).parent().find('input[value="' + val + '"]').attr('checked', 'checked');
				checked = true;
			}

			//	Všechny
			if (! selections || val == 0) {
				//	Všechny odškrtnout
				$(li).parent().find('input').attr('checked', null);
				//	a 0 zaškrtnout.
				$(li).parent().find('input[value="0"]').attr('checked', 'checked');
			}
			//	Odškrtnutí nuly.
			else if (selections.length < 2) {
				$(li).parent().find('input[value="0"]').attr('checked', null);
			}

			//	Stromové zaškrtávání.
			var option = control.find('option[value="' + val + '"]');

			//	Jsme uprostřed stromu, najít kořen 
			if (option.attr('data-parent')) {

				//	Najít kořenový 0 prvek a odznačit jej.
				function findRoot(control, val)
				{
					var option = control.find('option[value="' + val + '"]');
					if (option.attr('data-parent')) {
						option = findRoot(control, option.attr('data-parent'));
					}
					return option;
				}
				var root = findRoot(control, option.attr('data-parent'));
				if (root) {
					root = root.val();

			    	while (selections.indexOf(root) >= 0) {
			    		selections.splice(selections.indexOf(root), 1);
			    	}

			    	//	aktualizovat control
					control.val(selections);

					//	Překreslit
					var el = $(li).parent().find('input[value="' + root + '"]');
					el.attr('checked', null);
					el.parent().parent().parent().removeClass('selectBox-selected');
				}

				//	Pokud zaškrtáváme, Označit všechny potomky, ale jen visuelně.
				if (checked) {
					var a = findChildren([], control, option.val());
					$.each(a, function(i, m) {
						var el = $(li).parent().find('input[value="' + m + '"]');
						el.attr('checked', 'checked');
						el.parent().parent().parent().addClass('selectBox-selected');
					})
				}
				//	Odškrtáváme
				else {
					//	Je parent (stačí vizuelní) zaškrtnut?, Zakázat odšrtnutí.
					var parentid = control.find('option[value="' + option.attr('data-parent') + '"]');
					if (parentid) {
						parentid = parentid.val();
					}

					if ($(li).parent().find('input[value="' + parentid + '"]').attr('checked')) {
						var el = $(li).parent().find('input[value="' + option.val() + '"]')
						el.attr('checked', 'checked');
						el.parent().parent().parent().addClass('selectBox-selected');
					}
					else {
						//	Odškrtnut potomky.
						var a = findChildren([], control, option.val());
						$.each(a, function(i, m) {
							var el = $(li).parent().find('input[value="' + m + '"]');
							el.attr('checked', null);
							el.parent().parent().parent().removeClass('selectBox-selected');
						})
					}
				}
			}
			//	Jsme kořen, změnit subtree
			else {
				//	Pouze pokud nulák začkrtáváme, tak resetujem obsah.
				if (checked) {

					//	Najít všechny potomky
					var a = findChildren([], control, val);

					//	Odznačit
					$(li).parent().find('input').each(function() {
						var m = $(this).val();
						if (a.indexOf(m) >= 0) {
							$(this).attr('checked', null);
							$(this).parent().parent().parent().removeClass('selectBox-selected');

					    	while(selections.indexOf(m) >= 0) {
					    		selections.splice(selections.indexOf(m), 1);
					    	}
						}
					});

					//	Vyhodit ze seznamu.
					control.val(selections);
				}
			}
		}



		return multiSelectBoxNE;
	})();


})(jQuery);


