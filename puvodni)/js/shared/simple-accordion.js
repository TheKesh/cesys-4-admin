
/**
 * Plugin pro zprovoznění accordionu v elementu simple-accordion - záložky a obsah je provázán atributem rel
 * Při zobrazení oddílu vyvolá událost simleaccordionshow
 */

(function ( $, window, document, undefined ) {
    $.widget( "cesys.simpleAccordion" , {

        //Defaultní options
        options: {
            rememberSelected: true,          //pamatovat v hashi v adrese vybranou záložku?
            activatePageByContentHash: true, //přepnout na vybranou záložku při inicializaci i podle obsahu záložek
            selectedPagePrefix: ''           //prefix který se propisuje do hashe, pokud je zapnuté pamatování vybrané záložky
        },

        //Setup widget
        _create: function () {
        	var self = this;
            var options = self.options;

            //element accordionu
        	this._accordionElement = this.element;

            //odkazy na záložky  přepínače
        	this._openers = this._accordionElement.find('> a');

            //kontejner s jednotlivými divy s obsahem
        	this._contents = this._accordionElement.find('> div');

            //divům obsahu nastavíme fixně počáteční šířku, jinak nefungují pořádně animace
            this._contents.width(this._accordionElement.width());

            //počáteční skrytí záložek, které nejsou otevřené
            this._contents.not('.opened').each(function(index, element){
                $(element).hide();
            });

            //přepínání stránek
            //lze přepínat také libovolně umístěným odkazem s třídou change-structured-content - v rel atributu musí být id požadovaného tabu
            this._openers.add('a.change-structured-content').each(function(index, element){
                $(element).bind('click', function(clickEvent){
                    var toShow = $(this).attr('rel');
                    self.showContent(toShow);
                    clickEvent.preventDefault();
                });
            });


            //počáteční načtení
            if (options.rememberSelected || options.activatePageByContentHash){
                //id obsahu z kotvy - odstraňujeme znaky, které by mohly vést k chybě:
                //"uncaught exception: Syntax error, unrecognized expression: =8&fi-lm="
                //TODO: do budoucna pravděpodobně použití nějakého pluginu na url (URI.js)
                var selected = location.hash;
                selected = selected.replace(options.selectedTabPrefix, '');
                selected = selected.replace(/[#=%;,\/]/g,"");

                //pokud je vůbec něco v hashi, zkusíme to najít v tabech
                if (selected.length){
                    //pokusíme se kotvu najít přímo v tabech
                    var tab = this._contents.filter('[id="'+selected+'"]').first();

                    //pokud jsme nenalezli a je zapnuté aktivace tabu dle obsahu
                    //prohledáme i obsah záložek na přítomnost kotvy
                    if (!tab.length && options.activatePageByContentHash){
                        tab = this._contents.find('#' + selected).closest('.accordion-content');
                    }
                    //pokud jsme záložku našli, přepneme se na ni
                    if (tab.length){
                        var tabId = tab.attr('id');
                        this.showContent(tabId);
                    }
                }

            }
        },


        /**
         * Zobrazení záložky - automaticky řeší kliknutí na již otevřenou záložku, není třeba řešit zvenčí
         * @param  {string} pageId Id stránky, který chceme zobrazit - bez # na začátku
         */
        showContent: function (pageId){
        	var self = this;
            var options = self.options;
        	var toHide = this._contents.filter('.opened');
        	var toShowElement = this._contents.filter('#' + pageId);
            var link = this._openers.filter('[rel="' + pageId + '"]'); 
            var isClosingCurrent = link.hasClass('opened');

            //odstranění třídy ze záložek
   			this._openers.removeClass('opened');

            //skrytí viditelné záložky
			toHide.slideUp(300, function(){
                //po dokončení animace odebereme třídu
				toHide.removeClass('opened');
            });

            //pokud zavíráme aktuální stránku, skončíme po zavření
            if (isClosingCurrent){
                return;
            }

            //zapsání vybrané záložky do hashe před zobrazením obsahu
            //díky tomu, že ještě není obsah s kotvou vidět, nescrolluje prohlížeč k elementu
            if (options.rememberSelected){
                location.hash = options.selectedPagePrefix + toShowElement.attr('id');
            }

            //zobrazení správného obsahu
            toShowElement.slideDown(300, function() {
                //přidáme třídu na záložce a odkazu
                $(this).addClass('opened');
                link.addClass('opened');

                //spustíme událost
                self._trigger('show', null, {
                    'page' : $(this)
                });
            });


        } //end of showContent

    });

})( jQuery, window, document );


