var cesys = cesys || {};
$.extend(true, cesys, {
    utils: {
        frontx: function () {
            var next = null,
                each_lock = null;

            /**
             *    Projede pole a nad každým z nich provede akci a zároveň ukončí zpracování předchozí fronty, pokud ještě nedoběhla
             *
             *    @param rows Pole záznamů.
             *    @param invoker callback(index, row) pro zpracování jednoho řádku.
             *    @param delay pauza mezi zpracováním.
             */
            this.each = function (rows, invoker, delay) {
                if (each_lock) {
                    window.clearTimeout(each_lock);
                }

                var i = 0,
                    length = rows.length,
                    isObj = length === undefined || jQuery.isFunction(rows);

                if (isObj) {
                    throw "not implemented"
                }
                else if (rows.length > 0) {
                    //	Rutina pro spustění dalšího prvku.
                    next = function (status) {
                        if (i < length) {
                            each_lock = window.setTimeout(cb, delay);
                        }
                    }

                    //	Zavolání zpracování řádku.
                    var cb = function () {
                        if (invoker.call(rows[i], i, rows[i++]) === false) {
                            //					return;
                        }
                    }
                    cb();
                }
            },

                /**
                 * Zrušení aktuální fronty
                 */
                this.destroy = function () {
                    window.clearTimeout(each_lock);
                    next = null;
                },


                /**
                 *    Přetížení originálního ajaxu, aby zajistil následné volání obsluhy fronty
                 *
                 *    @param options Standardní nastavení jquery.ajax
                 */
                this.ajax = function (options) {
                    var orig_success = options.success,
                        orig_error = options.error,
                        utls = cesys.utils;
                    options.success = function (status) {
                        orig_success.call(this, status);
                        if (next) {
                            next.call(this, status);
                        }
                    }
                    options.error = function (status) {
                        orig_error.call(this, status);
                        if (next) {
                            next.call(this, status);
                        }
                    }
                    return $.ajax(options);
                },

                /**
                 *  počkáme dokud nejsou všechny termíny ze skupiny ověřené a potom pustíme další skupinu
                 *  
                 *  @param group Aktuální skupina v procesu ověřování
                 *  @param checkReturnStatus true - pokud je alespoň jeden termín OK tak další skupina se neověřuje
                 */
                this.while = function (group, checkReturnStatus) {
                    var interval = setInterval(function () {
                        allOk = 1;
                        isReturnStatusOk = false;
                        $.each(group, function (k, v) {
                            if (v.checked == false) {
                                allOk = 0;
                            }
                            if (v.status == 'OK') {
                                isReturnStatusOk = true;
                            }
                        });
                        if (allOk == 1) {
                            if (next && (isReturnStatusOk == false || checkReturnStatus == false)) {
                                next.call(this, 'OK');
                            }
                            clearInterval(interval);
                        }
                    }, 100);
                }

            return this;
        }
    }
});


