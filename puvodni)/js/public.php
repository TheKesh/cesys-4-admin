<?php

/**
 * BC: neudržujeme předchozí verze
 */


$files_v2 = array(
	'beforeLocales' => array(
			'./shared/jquery-1.6.4.min.js'
		),
	'afterLocales'	=> array(
		'./shared/jquery-ui-1.8.16.custom.min.js',
		'./shared/jquery.selectCombo.js',
		'./shared/dialog-destinations-selector.js',
		'./public/jquery.superfish-1.3.1.js',
		'./public/URI.min.js',							//plugin pro práci s url, viz /core/vendors/javascript/URI.js-gh-pages

		//'./shared/jquery.colorbox-1.3.17.js',
		'./shared/jquery.colorbox-1.5.14.js',
		'./public/bind-colorboxes.js', 					//přidání funkce do jquery pro bindování colorboxu na ajaxově nahraném obsahu

	//	'./public/jquery.bgiframe-2.1.1.js',			
		'./public/layout-inline.js',
		'./public/simple-tabs.js',						//vlastní plugin pro záložky
		'./shared/simple-accordion.js',					//vlastní plugin pro accordion
		'./public/ajax-paging.js',						//funkce pro ajaxové stránkování ve výsledcích hledání
		'./public/search-result-templates.js',			//ostatní funkce výsledků vyhledávání
		'./public/jquery.easySlider-1.7.custom.js',
		'./public/searchmasks.js',						
		'./shared/jquery.selectBox.js',
		'./shared/jquery.multiSelectBox.js',		
		'./shared/utils.frontx.js',						//ajaxová fronta
		'./shared/utils.price.js',						//formátování ceny
		'./public/google-maps.js', 						//funkce pro načtení mapy v elementu google_map.ctp
		'./public/jquery-ui-accommodation-tabs.js', 	//rozšíření pluginu jquery ui o funkce používané v detailu hotelu
		'./public/simple-accordion-accommodation.js', 	//rozšíření pluginu simple accordion o funkce používané v detailu hotelu
		'./public/simple-tabs-accommodation.js', 		//rozšíření pluginu simple tabs o funkce používané v detailu hotelu
		'./public/templates-plugins-init.js', 			//automatická inicializace pluginů v šablonách hotelu
		'./public/jquery.calculation.min.js',			//matematické výpočty
		'./public/ui.selectmenu.js',					//jquery select (výběr pokoje v ceníku...)
		'./shared/availability.js',						//online ověření dostupnosti
		'./shared/jquery.jqGrid.4.5.2.min.js',			//nejnovejsi verze jqGrid
		'./public/traffics-dates-jqgrid.js', 			//funkce pro načtení německých nabídek v elementu traffics_dates_table_grid.ctp
		'./public/amadeus-dates-jqgrid.js', 			//funkce pro načtení německých nabídek v elementu amadeus_dates_table_grid.ctp
		'./public/cl-dates-jqgrid.js',					//inicializace jqgridu pro termíny vlastních nabídek - cl_offers_dates_table_jqgrid.ctp
		'./public/dates-jqgrid.js',					    //inicializace jqgridu pro standardní termíny - dates_table_jqgrid.ctp
		'./public/dates-table-simple.js',				//ověření temrínů v html tabulce termínů - dates_table_simple.ctp
		'./public/dates-view.js',					    //objednávkový formulář
		'./shared/price-list.js',					    //obsluha ceníku
		'./shared/occupancy-form.js',					//výběr skladby osob
		'./shared/order-form.js',					    //objednávkový formulář
		'./public/local-booking-form.js',				//kompletní objednávka na frontu - složeno z occupancy formu, price listu a order formu
        './public/jquery.tooltip.min.js',               //tooltip (na ikony parametrů ubytování)
		'./public/swiper.min.js',						//slider
	)
);


$locales =array(
	'cs' => array(
		'./shared/grid.locale-cs.js',
		'./shared/jquery-ui.datepicker-cs.js'
	),
	'sk' => array(
		'./shared/grid.locale-sk.js',
		'./shared/jquery-ui.datepicker-sk.js'
	),
	'hu' => array(
		'./shared/grid.locale-hu.js',
		'./shared/jquery-ui.datepicker-hu.js'
	),
	'vi' => array(
		'./shared/grid.locale-vi.js',
		'./shared/jquery-ui.datepicker-vi.js'
	),
	'sr' => array(
		'./shared/grid.locale-sr-latin.js',
		'./shared/jquery-ui.datepicker-sr-SR.js'
	),
	'hr' => array(
		'./shared/grid.locale-hr.js',
		'./shared/jquery-ui.datepicker-hr.js'
	),
	'en' => array(
		'./shared/grid.locale-en.js',
		'./shared/jquery-ui.datepicker-en-GB.js'
	)
);


//zjistime jazyk
$lang = require __dir__ . '/libs/locale.php';

//pokud existuje definice souboru s lokalizaci v pozadovanem jazyce, pouzijeme ji, jinak prazdne pole
if (isset($locales[$lang])){
	$locale_file = $locales[$lang];
} else {
	$locale_file = array();
}


switch (@$_GET['v']) {
	case 2:
	default:
		//pokud se neodesle/nezpracuje verze, pro jistotu nastavime vychozi, aktualne verze 2
		$files = array_merge($files_v2['beforeLocales'], $locale_file, $files_v2['afterLocales']);
		$version = 2;
}


require_once __dir__ . '/js.inc.php';

