<?php
/**
 *	Minifikator pro js.
 *
 *	@param $_GET['d'] Uroven ladeni. 
 *		NULL: žádné/product - kód se komrimuje
 *		1: kód se komprimuje, v komentáři se uvádí které soubory jsou použity.
 *		2: kód se NEkomprimuje, v komentáři se uvádí které soubory jsou použity.
 *		3: devel
 *	@param $_GET['r'] flag jen pro prohížeče
 *	@param $file Jméno scriptu, který se načítá.
 *
 *	@dependence ./jsmin.php
 */
 

date_default_timezone_set('Europe/Prague');

$cache = true;
$debug = 0;

if (isset($_GET['d']) AND in_array($_GET['d'], array('1', '2'))) {
	$debug = $_GET['d'] + 0;
	$cache = false;
}

$filename = $_GET['file'] . '.js';
$filepath = './cache/' . $filename;

if (!isset($content)) {
	$content = '';
}

//	Pokusíme se použít kešovanou verzi.
if ($debug == 0 && file_exists($filepath)) {
	$content = file_get_contents($filepath);
	$lastModified = filemtime($filepath);
}
else {
	//	Script sloužící k minifikaci js.
	require_once './jsmin.php';

	if ($debug > 0) {
		$content .= "\n\n\n/* file: $filename */\n";
	}

	if ($debug == 2){
		$content .= file_get_contents($filename);
	}
	else {
		$content .= JSMin::minify(file_get_contents($filename));
	}

	if ($debug == 0 
			&& strlen($content) > 0 
			&& (($fp = @fopen($filepath, 'w')) !== false)) {
		fwrite($fp, $content);
		fclose($fp);
	}
	$lastModified = time();
}

if ($cache) {
	$ifModifiedSince = isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? stripslashes($_SERVER['HTTP_IF_MODIFIED_SINCE']) : false;
	if ($ifModifiedSince && strtotime($ifModifiedSince) >= $lastModified) {
		header('HTTP/1.1 304 Not Modified');
		exit; // stop processing
	}
	header("Last-Modified: " . gmdate("D, j M Y G:i:s ", $lastModified) . 'GMT');
	header("Content-Type: text/javascript");
	header("Expires: " . gmdate("D, j M Y H:i:s", time() + 60*60*24) . " GMT");
	header("Cache-Control: cache"); // HTTP/1.1
	header("Pragma: cache");		// HTTP/1.0
}
else {
	header("Content-Type: text/javascript");
	header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
	header("Expires: Sat, 10 Jan 2000 05:00:00 GMT"); // Date in the past
	header('Pragma: no-cache');
}
echo $content;
