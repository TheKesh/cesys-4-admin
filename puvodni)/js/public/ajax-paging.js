 /**
  * Funce pro stárnkování zabalné do objektu
  * - public funkce: handlePageLoad pro zpracování paginatorových odkazů a handlePageLoad pro zpracování hashe po načtení stránky
  * - ostatní funkce private uvnitř objektu
  */
 $(function( ajaxPaging, $, undefined ){
	
 	/**
 	 * Uložení stavu stránkování - probíhá ajax/načítáme novou stránku?
 	 */
	var isLoading = false;

	/**
	 * Zpracování kliku na ajaxové paginatorové odkazy
	 *  - provede request na další zájezdy a ty buď přilepí na konec zájezdů (místo placeholderu s předem daným ID), nebo nahradí celý element se zájezdy (pevně dané ID)
	 */
	ajaxPaging.clickHandler = function (clickEvent){
			var link = $(clickEvent.target);
			var offers = $('#ajax-offers-result');
			var linkHref = link.attr('href');
			var loadMore = link.hasClass('load-next');
			var orderClasses = ['price_asc', 'price_desc', 'priority_desc'];

			//pokud již načítáme předchozí ajax, neděláme nic
			if (isLoading){
				clickEvent.preventDefault();
				return;
			}

			//zpracování pokud jde o řadící odkazy
			$.each(orderClasses, function(index, orderClass){
				if (link.hasClass(orderClass)){
					updateHashOrder(orderClass);	
				}
			});

			//pokud bylo kliknuto na "načíst další", upravíme text tlačítka
			if (loadMore){
				link.text(cesys.ts('Loading ...'));
			}

			//načteme výsledky
			loadPages(linkHref, loadMore);
			clickEvent.preventDefault();
	};


	/**
	 * Zpracování hashe při načtení stárnky
	 * @param  {int} ajaxPagingInitialPageFrom Počáteční hodnota page_from = načtená stránka.
	 *                                         Předává se globální javascriptovou proměnnou přímo z layoutu, v budoucnu do html5 atributu
	 */
	ajaxPaging.handlePageLoad = function(ajaxPagingInitialPageFrom){
		var uri = new URI();
		var locHash = uri.fragment();
		var params = URI.parseQuery(locHash);
		var orderClasses = ['price_asc', 'price_desc', 'priority_desc'];
		var page_from = null;
		var	page_to = null;
		var order = null;

		//načtení parametrů z hashe
		if ("page_from" in params){
			page_from = parseInt(params.page_from);
		}
		if ("page_to" in params){
			page_to = parseInt(params.page_to);
		}
		if ("sort_by" in params){
			if (orderClasses.indexOf(params.sort_by) > -1){
				order = params.sort_by;
			}
		}

		//parametry z hashe přeneseme do getu
		var newUrl = new URI();
		var doReload = false;
		if (!isNaN(page_from) && page_from !== null && !isNaN(page_to) && page_to !== null){
			newUrl.setSearch('page_from', page_from);
			newUrl.setSearch('page_to', page_to);
			doReload = true;
		}
		if (!isNaN(order) && order !== null){
			newUrl.setSearch('sort_by', order);
			doReload = true;
		}

		//pokud byly v hashi nějaké parametry, které jsme přidali do get parametrů
		//reloadujeme výsledky
		if (doReload){
			loadPages(newUrl.valueOf(), false);
		}
		//pokud žádné parametry v hashi nebyly, potřebujeme tam přidat počáteční parametr page_from
		else if (typeof ajaxPagingInitialPageFrom != 'undefined'){
			newUrl.removeFragment('page_from').addFragment('page_from', ajaxPagingInitialPageFrom);
			location.hash = newUrl.fragment();
		}
	};


	/**
	 * Načtení nových zájezdů
	 * @param  {string} linkHref Odkaz paginatoru
	 * @param  {bool} loadMore   Bylo kliknuto na "načíst další" ?
	 */
	var loadPages = function(linkHref, loadMore){
		var content = $('.search-results-tmpl');
		var offers = content.find('#ajax-offers-result');
		var paging = content.find('.paging');
		var clTrips = content.find('#offers-cl');
		var placeholder = content.find('#placeholder-load-next');
		var offersCount = parseInt(offers.find('.sm-result').length);

		//zprůhledníme nabídky a označíme css třídou, že načítáme ajax
		content.animate({'opacity': 0.4}, 400);
		isLoading = true;

		//DEBUG -----------------
		//var start = new Date().getTime();

		$.ajax({
			url: linkHref,
			dataType: "json",
			data: { 'ajaxPaging': true, 'loadMore' : loadMore, 'currentOffersCount' : offersCount}
		})
		.done(function(data, status, resultObject) {
			if (!data){
				//console.log('error: no data returned, status: ' + status);
				return;
			}

			var newOffers = $(data.htmlResult);
			$(newOffers).bindColorboxes();
			$(newOffers).datesTableSimple();

			//načíst další - vyměníme paging, placeholder nahradíme nabídkami
			if (loadMore){
				paging.replaceWith(data.paging);
				placeholder.replaceWith(newOffers);
				updateHashPages(data.pageFrom, data.pageTo, false);
			}
			//nahrazení všech výsledků - nahradíme clTrips, nahradíme všechny nabídky, nahradíme stránkováni
			else{
				clTrips.html(data.clTrips);
				offers.html(newOffers);
				paging.replaceWith(data.paging);				
				updateHashPages(data.pageFrom, data.pageTo, true);
			}

		})
		.fail(function(resultObject, status){
			//console.log('error: ' + status);
		})
		.always(function(){
			//vypneme zprůhlednění a odebereme třídu značící probíhající ajax
			content.animate({'opacity': 1}, 100);
			isLoading = false;

			//DEBUG ---------------
			// var end = new Date().getTime();
			// var time = end - start;
			// alert('Execution time: ' + time);
			//DEBUG ---------------
		});
	};


	/**
	 * Update pageFrom a pageTo v hashi
	 * @param  {int} pageFrom   Počáteční stránka
	 * @param  {int} pageTo     Poslední stránka
	 * @param  {bool} singlePage Načtena pouze jedna stránka, nikoliv rozsah ?  
	 *                           - při akci "načíst další" musíme přepisovat jen PageTo
	 */
	var updateHashPages = function(pageFrom, pageTo, singlePage){
		var uri = new URI();
		var params = URI.parseQuery(uri.fragment());

		//nastavujeme odkaz na jednotlivou stránku
		//vložíme číslo aktuální stránky
		if (singlePage){
			uri.removeFragment('page_from').addFragment('page_from', pageFrom);
			uri.removeFragment('page_to').addFragment('page_to', pageTo);
		}
		//parametr už je nastaven, nebo chceme vkládat rozsah při load-next
		else{
			//při load next nastavujeme jen pageTo
			if (!('page_from' in params)){
				uri.removeFragment('page_from').addFragment('page_from', pageFrom);
			}
			uri.removeFragment('page_to').addFragment('page_to', pageTo);
		}

		location.hash = uri.fragment();

	};


	/**
	 * Hastavování řazení v hashi
	 * @param  {string} order Typ řazení
	 */
	var updateHashOrder = function(order){
		var uri = new URI();
		var params = URI.parseQuery(uri.fragment());

		uri.removeFragment('sort_by').addFragment('sort_by', order);
		location.hash = uri.fragment();
	};

	

}( window.ajaxPaging = window.ajaxPaging || {}, jQuery ));



$(document).ready(function(){
	//po načtení dokumentu zpracujeme hash
	//řešíme jen pokud jsme na stránce s šablonovými výsledky
	//div #ajax-offers-result se vykresluje jen pokud jsme našli nějaké zájezdy - pokud ne, nemá cenu se snažit ajaxem přenačítat
	if ($('#ajax-offers-result').length){
		ajaxPaging.handlePageLoad(ajaxPagingInitialPageFrom);
	}

	//na ajaxové paginatorové odkazy nabindujeme zpracování události
	$(document).delegate('.sm-ajax-paging', 'click', function(clickEvent){
		ajaxPaging.clickHandler(clickEvent);
	});
});