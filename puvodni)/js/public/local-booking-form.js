
;(function ($, window, document, undefined) {
	$.widget('cesys.localBookingForm', {
		/**
		 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
		 * $(selector).localBookingForm({
		 * 		optionName : optionValue
		 * });
		 */
		options: {
			//adresa kam posílat data k uložení objednávky
			savelUrl : '/LocalBookings/save_order'
		},

		/**
		 * Konstruktor - načtení seznamů, bindování funkcí
		 */
		_create : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn

			//html elementy
			self._elementPriceList = element.find('.price-list');
			self._elementOccupancyForm = element.find('.occupancy-form');
			self._elementBookingResult = element.find('.booking-result');
			self._elementOrderForm = element.find('.order-form');
			
			//inicializace price listu
			self._elementPriceList
				.priceList({
					loadData : false //při vytvoření pluginu nebudeme načítat data ceníku, počkáme až na událost změny obsazenosti
				})
				.bind('dataLoading', function(){
					self._showOverlay();
				})
				.bind('dataLoaded', function(){
					self._hideOverlay();
				});

			//order form
			self._elementOrderForm
				.orderForm()
				.bind('orderSubmit', function(event, data){
					data.priceList = self._elementPriceList.priceList('getData');
					data.occupancy  = self._elementOccupancyForm.occupancyForm('getData');
					data.dataSource = element.data('data-source');
					data.onlineBooking = element.data('online-booking');
					data.dateId = element.data('date-id');
					data.accept = element.find('#localBookingAccept').is(':checked');
					data.acceptMarketing = element.find('#localBookingAcceptMarketing').is(':checked');

                    // pro delta se termíny z traffiscu nejdříve zobrazí v dialogovém oknu
                    if (data.onlineBooking == 1 && data.dataSource == 'traffics_connector') {
                        self._openReviewDialog(data);
                    } else {
                        self._showOverlay();
                        self._postData(data);
                    }
				});

			self._elementOccupancyForm
				.bind('occupancyChanged', function(event, data){
					//změna obsazenosti - předáme info do pricelistu a orderFormu
					self._elementPriceList.priceList('setOccupancy', data.adultCount, data.childCount, data.childAges);
					self._elementOrderForm.orderForm('setOccupancy', data.adultCount, data.childCount, data.childAges);
				})
				//zavoláme až po bindu occupancyChanged, díky tomu se po načtení ihned aktualizují správně seznamy
				.occupancyForm();
		
			
		},

        /**
         * Odešle data na server k uložení
         * @private
         */
        _postData : function (data) {
            var self = this,    //instance pluginu
                options = self.options; //options

            $.ajax({
            	type: "POST",
            	url: options.savelUrl + '/' + data.dateId,
            	dataType : 'json',
            	data : JSON.stringify(data)
            })
            .done(function(response){
            	self._handleOrderResponse(response);
            })
            .fail(function(jqXHR, textStatus, errorThrown){
            	self._handleOrderResponseError(cesys.ts('Server error occurred while saving the order') + ': ' + textStatus + '. ' + cesys.ts('Please try again.'));
            });
        },

        /**
         * Otevře dialogové okno pro kontrolu zadaných údajů
         * @param data
         * @private
         */
        _openReviewDialog : function (data) {
            var self = this,
                element = self.element;

            if ($('#reviewDialogWindow').length == 0) {
                var dialog = $('<div id="reviewDialogWindow"></div>');
                element.append(dialog);
            }

            var reviewDialog = $('#reviewDialogWindow');
            self._fillReviewData(reviewDialog, data);
            reviewDialog.dialog({
                title: cesys.ts('Review order'),
                modal: true,
                buttons: [
                    {
                        text: cesys.ts('Edit'),
                        click: function () {
                            $(this).dialog('close');
                        }
                    },
                    {
                        text: cesys.ts('Send'),
                        click : function () {
                            self._postData(data);
                        }
                    }
                ]
            });
        },

        /**
         * Vyplnění dialogového okna
         * @param dialog
         * @param data
         * @private
         */
        _fillReviewData : function (dialog, data) {

            // Informace o objednavateli
            html = '<strong>' + cesys.ts('Firstname') + ':</strong> ' + data.firstName + '<br \>';
            html += '<strong>' + cesys.ts('Lastname') + ':</strong> ' + data.lastName + '<br \>';
            html += '<strong>' + cesys.ts('Street') + ':</strong> ' + data.street + '<br \>';
            html += '<strong>' + cesys.ts('City') + ':</strong> ' + data.city + '<br \>';
            html += '<strong>' + cesys.ts('Post Code') + ':</strong> ' + data.postCode + '<br \>';
            html += '<strong>' + cesys.ts('Country') + ':</strong> ' + data.country + '<br \>';
            html += '<strong>' + cesys.ts('Phone') + ':</strong> ' + data.phone + '<br \>';
            html += '<strong>' + cesys.ts('E-mail') + ':</strong> ' + data.email + '<br \>';

            // Účastníci
            html += '<br \><strong>' + cesys.ts('Participants') + ':</strong><br \>';
            $.each(data.participants, function(k, participant){
                var participantNumber = k+1;
                html += participantNumber + '. ' + cesys.ts('Participant') + ': ' + participant.firstName + ' ' + participant.lastName + '<br \>';
            });

            // Ceník
            html += '<br \><strong>' + cesys.ts('Price list') + ':</strong><br \>';
            $.each(data.priceList.room.prices, function(k, price) {
                html += price.name + ': ' + price.count + 'x ' + price.value + window.config.currencies[price.currencyId] + ' ('+(price.count*price.value) + window.config.currencies[price.currencyId] + ')<br \>';
            });

            // Služby
            html += '<br \><strong>' + cesys.ts('Services') + ':</strong><br \>';
            if (data.priceList.services.c2c !== undefined) {
                html += data.priceList.services.c2c.name + ': ' + data.priceList.services.c2c.value + window.config.currencies[data.priceList.services.c2c.currencyId] + '<br \>';
            }
            if (data.priceList.services.erv !== undefined) {
                html += data.priceList.services.erv.name + ': ' + data.priceList.services.erv.value + window.config.currencies[data.priceList.services.erv.currencyId] + '<br \>';
            }

            dialog.html(html);
        },

		/**
		 * Nastavení vráceného stavu objednávky
		 * @param array data
		 */
		_handleOrderResponse : function (response) {
			if (response) {
				//vrátil se objekt s odpovědí serveru, zpracujeme podle úspěchu/neúspěchu
				if (response.success) {
					this._handleOrderResponseSuccess(response.message, response.data, response.redirect);
				}else{
					this._handleOrderResponseError(response.message, response.data);
				}
			}else{
				//nevrátily se  žádná data, vypíšeme obecnou lášku o chybě serveru
				this._handleOrderResponseError(cesys.ts('Server error occurred while saving the order') + '. ' + cesys.ts('Please try again.'));
			}
		},

		/**
		 * Nastavení chybového stavu
		 * @param string status Textový message
		 * @param array  data   Dodatečná data chyby
		 */
		_handleOrderResponseError : function (message, data) {
			var self = this;
			self._elementBookingResult
				.empty()
				.removeClass('success')
				.addClass('error')
				.append('<h3>' + message + '</h3>');

			//jsou li data, projdeme a vypiseme
			if (typeof data == "array" || (typeof data == "object" && data != null)) {
				var ul = $('<ul>').appendTo(self._elementBookingResult);
				$.each(data, function(index, error){
					ul.append('<li>' + error + '</li>');
				});
			}

			//zobrazit a scrolovat k chybám
			self._elementBookingResult.show('fast', function(){

				var topOffset = self._elementBookingResult.offset().top;
				if (topOffset){
					$('html, body').animate({
					    scrollTop: self._elementBookingResult.offset().top - 50
					}, 400);
				}
			});

			//skryjeme overlay
			self._hideOverlay();

		},

		/**
		 * Nastavení úspěšného stavu vytváření objednávky
		 * @param string message Textová zpráva
		 * @param array  data   Dodatečná data - u úspěšného stavu zatím nepoužité
		 */
		_handleOrderResponseSuccess : function(message, data, redirect) {
			var self = this;

			//pokud je redirect tak nic nevypisujeme a rovnou presmerujeme (overlay neskryvame)
			if (redirect && redirect.length){
				location.href = redirect;
			}else{
				//jinak zobrazime info message
				self._elementBookingResult
					.empty()
					.removeClass('error')
					.addClass('success')
					.append('<h3>' + message + '</h3>')
					.show('fast');
				self._elementOrderForm.get(0).reset();

				//skryjeme overlay
				self._hideOverlay();
			}
		},

		/**
		 * Přidá overlay při zpracování ajaxu
		 */
		_showOverlay : function(){
			var element = this.element;
			var overlay = $('<div class="overlay"></div>');
			element.append(overlay);
		},

		/**
		 * Odstraní overlay po dokončení ajaxu
		 */
		_hideOverlay : function(){
			var element = this.element;	
			$(element).find('> .overlay').fadeOut('fast', function(){
				$(this).remove();
			});
		}

	});

})(jQuery, window, document);

/**
 * Po načtení stránky spustit
 */
$(function(){
	$('.local-booking-form').localBookingForm();
});

