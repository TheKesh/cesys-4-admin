/**
 * Načtení tabů s detailnimi informacemi pro konkrétní accomodation id
 */
$(document).delegate('.load-acm-details', 'click', function(clickEvent){
	var link = $(this);
	var linkHref = link.attr('href');
	var target = $(link.attr('rel'));
	var offer = link.closest('.sm-result');
	var moreLabel = link.find('.show');
	var lessLabel = link.find('.hide');
	var loadingLabel = link.find('.loading');

	//pokud právě neprobíhá načítání
	if (!link.hasClass('loading')){
		//info ještě nebylo načtené - ajaxem ho získáme
		if (!target.hasClass('loaded')){
			//označíme načítání třídou na nabídce a přepneme text odkazu
			offer.addClass('loading');
			link.animate({'opacity': 0.4}, 400).addClass('loading');
			moreLabel.removeClass('active');
			loadingLabel.addClass('active');
	 		$.ajax({
				url: linkHref
			})
			.done(function(data, status, resultObject){
				//nad načtenými daty načteme colorbox
				var description = $(data);
				description.bindColorboxes();

				//apendujeme data do stránky a rozbalíme je animací
				//na nabídce nastavíme třídu expanded = je rozbalená
				//na placeholderu k načtení dat nastavíme třídu loaded = data jsou již načtená
				target.append(description).slideDown(function(){
					offer.removeClass('loading').addClass('expanded');
				}).addClass('loaded');

				//přepnutí textu odkazu
				moreLabel.add(loadingLabel).removeClass('active');
				lessLabel.addClass('active');
				link.animate({'opacity': 1}, 400).removeClass('loading');
			});
		}
		//info bylo načtené ale bylo znovu skryté
		else if(!target.is(':visible')){
			target.slideDown(function(){
				offer.addClass('expanded');
			});

			//přepnutí textu odkazu
			moreLabel.removeClass('active');
			lessLabel.addClass('active');
		}
		//skrytí informací
		else{
			target.slideUp(function(){
				offer.removeClass('expanded');
			});

			//přepnutí textu odkazu
			lessLabel.removeClass('active');
			moreLabel.addClass('active');
		}
	}

	//potlačení defaultního chování odkazu
	clickEvent.preventDefault();
});