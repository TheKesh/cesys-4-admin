/**
 * Auto inicializace pluginů v šablonách hotelů
 */
$(document).ready(function(){
	//inicializace pluginů v detailu hotelu
    $("#acm-tabs.jquery-ui-tabs").tabsAccommodation();
    $("#acm-tabs.simple-tabs").simpleTabsAccommodation();
    $("#acm-accordion.simple-accordion").simpleAccordionAccommodation();

    //inicializace pluginů v detailu termínu
    $("#date-tabs.jquery-ui-tabs").tabsAccommodation();
    $("#date-tabs.simple-tabs").simpleTabsAccommodation();
    $("#date-accordion.simple-accordion").simpleAccordionAccommodation();
});