$(document).ready(function(){

	/**
	 * Otevírání selektorů a zpracování všech souvisejících událostí
	 * - iterujeme přes kolekci selektorů a konkrétní id elementu bereme z atributu 'rel'
	 */
	$('.dest-selector-opener').each(function(){
		var target = $(this).attr('rel'), //id cílového selectoru
			opener = $(this), //element s otevíracím tlačítkem
			selector = $('#'+target), 
			confirm = selector.find('.confirm'),
			reset = selector.find('.reset'),
			anchor = $(this);


		/**
		 * Přesunutí selectoru na konec dokumentu - jinak ho mohly zakrývat části layoutu s overflow:hidden
		 */
		selector.appendTo('body');


		/**
		 * Zavření/otevření selectoru . parametrem forceClose lze vynutit zavření
		 * @param  bool forceClose Nastavením na true vynutí zavření, neřeší se, jestli byl otevřen nebo zavřen
		 */
		var performToggleSelector = function(forceClose){
			if (opener.hasClass('opened') || forceClose == true){
				opener.removeClass('opened');
				selector.slideUp('fast', function(){
					$(this).removeClass('rightAligned');
				});
			}else{
				//vzdalenost od kraje k leve straně
				var offsetLeft = anchor.offset().left;

				//vzdalenost zprava k pravé straně
				var offsetRight = ($(window).width() - (offsetLeft + anchor.outerWidth()));

				//vzdalenost zhora
				var positionTop = anchor.offset().top + anchor.outerHeight();

				//pokud je otevírací tlačítko napravo od středu, zobrazujeme zarovnané k pravému okraji, jinak vlevo
				if (offsetLeft > offsetRight){
					selector.css('right', offsetRight);
				}else{
					selector.css('left', offsetLeft);
				}

				selector.css('top', positionTop);

				opener.addClass('opened');
				selector.slideDown('fast');
			}
		}


		/**
		 * Otevření dialogu kliknutím na tlačítko ve vyhledávači
		 */
		opener.click(function(){
			performToggleSelector();
		});


		/**
		 * Klik na OK
		 */
		confirm.click(function(event){
			performToggleSelector();
			event.preventDefault();
		});


		/**
		 * Klik na reset - vyčištění selektoru
		 */
		reset.click(function(event){
			selector.destinationsSelector('clear');
			event.preventDefault();
		});


		/**
		 * Zavření selectoru při kliknutí mimo
		 */
		$(document).mouseup(function (event){
		    var elements = selector.add(opener);

		    //pokud není cílem kliku ani selector, otevírací tlačítko (má vlastní click handler), ani jejich potomek, skryjeme ho
		    if (!elements.is(event.target) && elements.has(event.target).length === 0){
		        performToggleSelector(true);
		    }
		});		
	}); //end each


	var swiper = new Swiper('.search-swiper-container', {
		nextButton: '.search-swiper-button-next',
		prevButton: '.search-swiper-button-prev',
	});

	
});
