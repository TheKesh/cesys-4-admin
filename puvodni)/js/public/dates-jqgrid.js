/**
 * Plugin pro grid s termíny. Příklad volání: $('#terminy').datesGrid('nazevMetody'), volitelně s daty $('#terminy').datesGrid('nazevMetody', data)
 * - spouštět nado obalovacím elementem gridu a filtru
 * 
 * Public metody:
 * create - inicializace
 * resize - nastavení zadané šířky, šířku předat druhým parametrem
 * 
 */
$.fn.datesGrid = function(method, data) {
	//definice dále neměnných proměnných
    var self = this;

    //element gridu - tabulka
    var gridElement = $(this).find('#grid');
    var searchmaskId = gridElement.data('searchmask-id');
    var masterId = gridElement.data('master-id');
    var displayTOcolumn = gridElement.data('display-to');

    //proměná pro setTimout na postupné načítání dostupností
	var availabilityTimeout;

    /**
     * Konstrukce gridu - zpracuje obsah location.hash a případně přednastaví hodnoty do filtru
     *  - s nastaveným filtrem inicializuje grid
     */
    var create = function()
    {	
    	//nad načteným gridem neinicializujeme
        if (isLoaded()){
        	return;
        }

        //přednasaví filtr
        handleHashParams();

		//info pro grid
		var params = getParams();
	    var sortOrder = gridElement.data('sort-order');
	    var sortName = gridElement.data('sort-name');
	    var limit = gridElement.data('limit');

        //názvy sloupců
        var columns = [
			cesys.ts('Termín'),
			cesys.ts('Délka'),
			cesys.ts('Strava'),
			cesys.ts('Doprava'),
			'<sup>'+cesys.ts('FM')+'</sup>&frasl;<sub>'+cesys.ts('LM')+'</sub>',
			cesys.ts('Price from (inc. discount)')
		];
		if (displayTOcolumn) {
			columns.push(cesys.ts('TO'));
		}
		columns.push('');

		//model sloupců
		var columnModel = [
			{name:'date_from', index: 'HeapSearch.date_from', width: 8 },
			{name:'duration', index: 'HeapSearch.duration', width: 4, align:'center'},
			{name:'boarding_id', index: 'HeapSearch.boarding_id', width: 9},
			{name:'transport_id', index: 'HeapSearch.transport_id', width: 8},
			{name:'last_minute', index: 'HeapSearch.last_minute', width: 4, align:'center'},
			{name:'price', index: 'HeapSearch.price', width: 17, align:'right'}
		];
		if (displayTOcolumn) {
			columnModel.push({name:'accommodation_id', width: 9, sortable:false, align:'center'});
		}
		columnModel.push({name:'link', index:'link', width: 6, sortable:false, align:'center'});

		//inicializace pluginu
		// - colModel array describes the model of the column.
		// - name is the name of the column,
		// - index is the name passed to the server to sort data
		// - note that we can pass here nubers too.
		// - width is the width of the column
		// - align is the align of the column (default is left)
		// - sortable defines if this column can be sorted (default true)
        $(gridElement).jqGrid({
			//width: gridwidth,
			autowidth: true,
			shrinkToFit: true,
			resizable: false,
			mtype: "POST",
			url: "/Searchmasks/master_dates/" + searchmaskId + "/" + masterId + "/" + displayTOcolumn+ "/?" + params,
			datatype: "JSON",
			colNames: columns,
			colModel:columnModel,
			pager: '#pager',
			rowNum: limit,
			rowList:[10,20,30,50],
			
			sortname: sortName,
			sortorder: sortOrder,
			//viewsortcols : [true, 'vertical', true],  //povoluje zobrazení ikon řazení u všech sloupců, kde je řazení povolené
			height: "100%",
			caption: false,
			beforeRequest : function(){
				//loadovací overlay
				$(self).append($('<div class="overlay"></div>'));
			},
			loadComplete: function(data){
				//skrytí overlaye
				$(self).children('.overlay').fadeOut('fast', function(){
					$(this).remove();
				});

				//zrušíme načasované ověřování, z předchozích stránek
				clearTimeout(availabilityTimeout);

				//lichým řádkům přidáme třídu
				$(gridElement).find("tr:odd").addClass("alt");

				//nahradíme názvy tour operatorů
				replaceTourOperators();

				//když nejsou žádná data - zobrazíme upozornění
				if ($(this).getGridParam("records") == 0) {
					//jen pokud ještě nebyla zpráva vložena
					if ($(this).parent().find('#no-dates-message').length == 0) {
				     	$(this).after($('<div>',{
				     		'id' : 'no-dates-message',
				     		'text' : cesys.ts('No dates were found')
				     	})); 
			     	}   
		     	}else{
     		        //jinak odstraníme zprávu o prázdných výsledcích
			        $(this).parent().find('#no-dates-message').remove();
		     	}

				//seznam dates kde ověřujeme dostupnost
				var dates = $.makeArray($(this).find('span.date-availability[data-availability-hash]'));

				//do konfigu ověřováku vyplníme language
				window.cesys.service.availability.configuration.lang = window.config.cesysLang;

				//funkce pro ověření dostupnosti
				//zavolá se nad polem dates ze kterého odebere první, ověří ho a rekurzivně se zpožděním spustí ověření nad dalším prvkem v poli
				//spustíme na seznamu termínů k ověření
				;(function checkAvailability(datesQueue){
					if (!datesQueue || !datesQueue.length){
						return;
					}
					
					//první date z fronty
					var date = $(datesQueue.shift());
						
					//ověří date
					if (date) {
						window.cesys.service.availability.doCheckRoom(
							{
								cid: date.data('availability-date-id'),
								hash : date.data('availability-hash')
							},
							function(status) {
								var dateLoadingImage = date.find('img').removeClass('img-loading');
								var dateLink = date.closest('tr').find('.grid-date-link');
								var dateLinkLoadingImage = dateLink.siblings('img').remove();
								switch (status) {
									case 'OK':
										dateLoadingImage
											.addClass('img-ok availability-small')
											.attr('src', window.config.fileServer + "/img/availability/ok.svg")
											.attr('alt', cesys.ts('Volná kapacita'))
											.attr('title', cesys.ts('Volná kapacita'));
										dateLink.show('slow');
										break;

									case 'RQ':
									case 'ER':
										dateLoadingImage
											.addClass('img-rq availability-small')
											.attr('src', window.config.fileServer + "/img/availability/rq.svg")
											.attr('alt', cesys.ts('Na dotaz'))
											.attr('title', cesys.ts('Na dotaz'));
										dateLink.show('slow');
										break;

									case 'NK':
									case 'SO':
										//sold out stav zpraucjeme dle nastavení
										if (window.config.settings.availabilityAllowOrderSoldOut) {
											dateLoadingImage
												.addClass('img-rq availability-small')
												.attr('src', window.config.fileServer + "/img/availability/rq.svg")
												.attr('alt', cesys.ts('Na dotaz'))
												.attr('title', cesys.ts('Na dotaz'));
											dateLink.show('slow');
										}else{
											dateLoadingImage
												.addClass('img-so availability-small')
												.attr('src', window.config.fileServer + "/img/availability/fail.svg")
												.attr('alt', cesys.ts('Obsazená kapacita'))
												.attr('title', cesys.ts('Obsazená kapacita'));
										}
										break;
								}

							}
						);
					}
					
					//se zpožděním spustí na zbytku fronty
					if (datesQueue.length){
						availabilityTimeout = setTimeout(function(){
							checkAvailability(datesQueue);
						}, 1200);
					}
				})(dates);


			}
		});

		$(gridElement).data('datesLoaded', true);

    };//end of create


    /**
     * Refreshne grid s aktualne nastavenymi parametry ve filtru
     */
    var refresh = function(){
        var params = getParams();

        // Refresh gridu
        $(gridElement)
            .setGridParam({
                url: "/Searchmasks/master_dates/" + searchmaskId + "/" + masterId + "/" + displayTOcolumn+ "?" + params,
            })
            .setGridParam({
                page: 1
            })
            .trigger("reloadGrid");
    };


    /**
	 * Nahrazení id operátorů ve sloupci info jejich názvy ze selectu (v selectu buď anonymní očíslování nebo název TO)
	 */
	var replaceTourOperators = function() {
		//pokud je v selectu více tour operátorů a pokud máme co přepisovat (přepisujeme span s třídou tourOperatorReplace)
		//nahradíme id jejich názvy
		var toReplace = gridElement.find('.tourOperatorReplace');
		var toSelect = self.find('#fi-to');

		if (toReplace.length > 0 && toSelect.length > 0){
			//naplníme překladovou mapu operátorů ze selectu
			var operators = {};
			toSelect.find('option').each(function(index, element){
				operators[element.value] = $(element).text();
			});

			//nahradíme texty v buňkách
			//celý span k výměně nahradíme názvem operatora
			toReplace.each(function(index, element){
				//pokud je operator v selectu, nahradíme názvem
				if (operators[$(element).text()] !== undefined)
				{
					$(element).replaceWith(
						operators[$(element).text()]
					);
				}
				//jinak nahradíme prázdným stringem
				else{
					$(element).replaceWith('');
				}
			});
		}
	};

    /**
     * Je grid vytvořen?
     */
    var isLoaded = function(){
        return $(gridElement).data('datesLoaded') == true;
    };

    /**
     * Upraví grid podle velikosti rodiče
     */
    var resize = function(width){
        if (isLoaded(gridElement)){
            $(gridElement).setGridWidth(width).trigger("resize");            
        }else{
            create(gridElement);
        }
    };

    /**
     * Zpracuje parametry v location.hash a přednastaví filtr
     */
    var handleHashParams = function(){
    	var possibleFilterParams = [
			'fi-dm',
			'fi-du',
			'fi-ti',
			'fi-bi',
			'fi-ai',
			'fi-lm',
			'fi-fm',
			'fi-dp',
			'fi-to'
        ];
        
        //získání parametrů z adresy
        var uri = new URI();
        var hashParams = URI.parseQuery(URI.decode(uri.fragment()));

        //projdeme všechny možné filtrovací parametry a zkusíme je přednastavit
        $.each(possibleFilterParams, function(){
            //název aktuálního parametru
            var paramName = this;

            //najdeme k němu input
            var input = $('#'+paramName);

            //pokud input na stránce existuje
            if (input.length){
                //nabindujeme na něm change událost na přenačtení zájezdů
                input.change(
                    function(){          
                        refresh();
                    }
                );

                //zkusíme jestli je v nastavená hodnota v parametrech z hashe
                //a případně ji nastavíme
                if (paramName in hashParams){
                    if (input.prop("tagName") == 'SELECT'){
                        input.val(hashParams[paramName]);
                    }
                    //checkbox fi-lm/fi-fm musíme nastavit dle hodnoty
                    else if (input.attr('type') == 'checkbox' && paramName == 'fi-lm'){
                        switch (parseInt(hashParams[paramName])){
                            case 1:
                                $('#fi-lm').prop('checked', true);
                                break;
                            case 2:
                                $('#fi-fm').prop('checked', true);
                                break;
                        }
                    }
                }
            }
        });
	};


    /**
     * Z filtru získá nastavené hodnoty, spojí je s url params z php a vrátí je v query stringu, např.: fi-lm=1&fi-bi=2 
     */
    var getParams = function(){
        var params = gridElement.data('params');
        var filterParams = {
        	'fi-dm' : $('#fi-dm').val(),
			'fi-du' : $('#fi-du').val(),
			'fi-ti' : $('#fi-ti').val(),
			'fi-bi' : $('#fi-bi').val(),
			'fi-ai' : $('#fi-ai').val(),
			'fi-dp' : $('#fi-dp').val(),
			'fi-to' : $('#fi-to').val()
        }
        var lm = $("#fi-lm:checked").val();
        var fm = $("#fi-fm:checked").val();
        if(typeof lm == "undefined") lm = 0;
        if(typeof fm != "undefined") lm = 2;
        filterParams["fi-lm"] = lm;

        
        // slozeni filtrovacich parametru
        var tmp = [];
        $.each(filterParams, function(param, value){
            if (value == 'undefined'){
                return true; //ekvivalent continue
            }
            if (value > 0 || param == 'fi-lm'){
                tmp.push(param + "=" + value);
            }
        });

        // Pokud neni prazdny params tak pridam &
        if (params != "") {
            params = params+"&";
        }
        params = params + tmp.join('&');

        return params;
    }; //end of get params

    /**
     * Volání jednotlivých public metod
     */
    if (typeof method == 'undefined'){
        method = 'create';
    }
    switch (method){
        case 'create':
            create();
            break;
        case 'resize':
            resize(data);
            break;
        default:
            console.log('Undefined method in datesGrid');
    }
}


/**
 * Najdeme-li na stránce element s vlastními nabídkami, inicializujeme
 */
$(window).load(function(){
    $('#terminy').each(function(){
        $(this).datesGrid();
    });
});