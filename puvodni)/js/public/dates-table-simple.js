/**
 * Javascript k html tabulce termínů (dates_table_simple.ctp) - vyhledá termíny k ověření a sekvenčně ověří dostupnost
 * - pokud se zavolá nad všemi tabulkami na stránce, ověřuje dostupnost sekvenčně napříč všemi tabulkami termínů
 */
$.fn.datesTableSimple = function(){
	//spustíme test na online dostupnost
	var dates = $(this).find('span.date-availability[data-availability-hash]');

	//do konfigu language
	window.cesys.service.availability.configuration.lang = window.config.cesysLang;

	//ajaxová fronta
	var front = new cesys.utils.frontx();
	
	//frontou zpracujeme jednotlivé termíny
	front.each(dates, function(index, date){
			if (date) {
				window.cesys.service.availability.doCheckRoom(
					{
						cid: $(date).data('availability-date-id'),
						hash : $(date).data('availability-hash')
					},

					function(status) {
						var dateLoadingImage = $(date).find('img').removeClass('img-loading');
						var dateLink = $(date).closest('tr').find('.date-detail-link');
						var dateLinkLoadingImage = dateLink.siblings('img').remove();
						switch (status) {
							case 'OK':
								dateLoadingImage
									.addClass('img-ok availability-small')
									.attr('src', window.config.fileServer + "/img/availability/ok.svg")
									.attr('alt', cesys.ts('Volná kapacita'))
									.attr('title', cesys.ts('Volná kapacita'));
								dateLink.show('slow');
								break;

							case 'RQ':
							case 'ER':
								dateLoadingImage
									.addClass('img-rq availability-small')
									.attr('src', window.config.fileServer + "/img/availability/rq.svg")
									.attr('alt', cesys.ts('Na dotaz'))
									.attr('title', cesys.ts('Na dotaz'));
								dateLink.show('slow');
								break;

							case 'NK':
							case 'SO':
								//sold out stav zpraucjeme dle nastavení
								if (window.config.settings.availabilityAllowOrderSoldOut) {
									dateLoadingImage
										.addClass('img-rq availability-small')
										.attr('src', window.config.fileServer + "/img/availability/rq.svg")
										.attr('alt', cesys.ts('Na dotaz'))
										.attr('title', cesys.ts('Na dotaz'));
									dateLink.show('slow');
								}else{
									dateLoadingImage
										.addClass('img-so availability-small')
										.attr('src', window.config.fileServer + "/img/availability/fail.svg")
										.attr('alt', cesys.ts('Obsazená kapacita'))
										.attr('title', cesys.ts('Obsazená kapacita'));
								}
								break;
						}

					},
					front.ajax
				);
			}
	});
};

/**
 * Automaticky ověří nad tabulkami ve stránce
 */
$(function(){
	$('.dates-table-simple').datesTableSimple();
});