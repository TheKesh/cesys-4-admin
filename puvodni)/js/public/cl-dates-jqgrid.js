/**
 * Plugin pro grid s termíny vlastních nabídek. Příklad volání: $('#gridElement').clDateGrid('nazevMetody'), volitelně s daty $('#gridElement').clDateGrid('nazevMetody', data)
 * 
 * Public metody:
 * create - inicializace
 * resize - nastavení zadané šířky, šířku předat druhým parametrem
 * 
 */
$.fn.clDatesGrid = function(method, data) {
    var self = this;

    /**
     * Konstrukce gridu
     */
    var create = function()
    {
        if (!isLoaded()){
            var possibleFilterParams = [
                'cl-fi-dm',
                'cl-fi-du',
                'cl-fi-ti',
                'cl-fi-bi',
                'cl-fi-ai',
                'cl-fi-lm',
                'cl-fi-fm',
                'cl-fi-dp'
            ];
        
            //získání parametrů z adresy
            var uri = new URI();
            var hashParams = URI.parseQuery(URI.decode(uri.fragment()));
            var filterParams = {};

            //projdeme všechny možné filtrovací parametry a zkusíme je přednastavit
            $.each(possibleFilterParams, function(){
                //název aktuálního parametru
                var paramName = this;

                //najdeme k němu input
                var input = $('#'+paramName);

                //pokud input na stránce existuje
                if (input.length){
                    //nabindujeme na něm change událost na přenačtení zájezdů
                    input.change(
                        function(){          
                            refresh();
                        }
                    );

                    //zkusíme jestli je v nastavená hodnota v parametrech z hashe (v hashi je bez prefixu)
                    //a případně ji nastavíme
                    paramName = paramName.replace('cl-', '');
                    if (paramName in hashParams){
                        if (input.prop("tagName") == 'SELECT'){
                            input.val(hashParams[paramName]);
                        }
                        //checkbox fi-lm/fi-fm musíme nastavit dle hodnoty
                        else if (input.attr('type') == 'checkbox' && paramName == 'fi-lm'){
                            switch (parseInt(hashParams[paramName])){
                                case 1:
                                    $('#ci-fi-lm').prop('checked', true);
                                    break;
                                case 2:
                                    $('#ci-fi-fm').prop('checked', true);
                                    break;
                            }
                        }
                    }
                }
            });

            // Nacteni informaci pro grid
            var params = getParams();
            var limit = $(self).data('limit');
            var tripId = $(self).data('cltrip-id');
            var searchmaskId = $(self).data('searchmask-id');

            // Vytvorim grid
            $(self).jqGrid({
                autowidth: true,
                shrinkToFit: true,
                resizable: false,
                mtype: "POST",
                url: "/ClDates/grid_dates/" + searchmaskId + "/" + tripId + "?" + params,
                datatype: "json",
                colNames:[
                        cesys.ts('Date'),
                        cesys.ts('Duration'),
                        cesys.ts('Boarding'),
                        cesys.ts('Transport'),
                        '<sup>' + cesys.ts('FM') + '</sup>&frasl;<sub>' + cesys.ts('LM') + '</sub>',
                        cesys.ts('Price'),
                        cesys.ts('Link')
                    ],

                colModel:[
                    {name:'date', index: 'ClHeapSearch.date_from', width: 7 },
                    {name:'duration', index: 'ClHeapSearch.duration', width: 5, align:'center'},
                    {name:'boarding', index: 'ClHeapSearch.boarding_id', width: 11},
                    {name:'transport', index: 'ClHeapSearch.transport_id', width: 7, sortable:true},
                    {name:'last_minute', index: 'ClHeapSearch.last_minute', width: 4, align:'center'},
                    {name:'price', index:'ClHeapSearch.price', width: 14, sortable:true, align:'center'}, 
                    {name:'link', index:'link', width: 6, sortable:false, align:'center'}
                ],
                pager: "#cl-dates-pager",
                rowList:[10,20,30,50],
                viewrecords: false, 
                sortname: "ClHeapSearch.date_from",
                sortorder: 'asc',
                height: "100%",
                rowNum: limit,
                beforeRequest : function(){
                    //loadovací overlay
                    $(self).closest('.overlay-parent').append($('<div class="overlay"></div>'));
                },
                loadComplete: function (data) {
                    //skrytí overlaye
                    $(self).closest('.overlay-parent').children('.overlay').fadeOut('fast', function(){
                        $(this).remove();
                    });

                    //lichým řádkům přidáme třídu
                    $(self).find("tr:odd").addClass("alt");

                    //když nejsou žádná data - zobrazíme upozornění
                    if ($(this).getGridParam("records") == 0) {
                        //jen pokud ještě nebyla zpráva vložena
                        if ($(this).parent().find('#no-dates-message').length == 0) {
                            $(this).after($('<div>',{
                                'id' : 'no-dates-message',
                                'text' : cesys.ts('No dates were found')
                            })); 
                        }   
                    }else{
                        //jinak odstraníme zprávu o prázdných výsledcích
                        $(this).parent().find('#no-dates-message').remove();
                    }
                }
            });
            $(self).data('clDatesLoaded', true);
        }//if loaded
    };//end of create


    /**
     * Refreshne grid s aktualne nastavenymi parametry ve filtru
     */
    var refresh = function(){
        var tripId = $(self).data('cltrip-id');
        var searchmaskId = $(self).data('searchmask-id');
        var params = getParams();

        // Refresh gridu
        $(self)
            .setGridParam({
                url: "/ClDates/grid_dates/" + searchmaskId + "/" + tripId + "?" + params,
            })
            .setGridParam({
                page: 1
            })
            .trigger("reloadGrid");
    };//end of refresh

    /**
     * Je grid vytvořen?
     */
    var isLoaded = function(){
        return $(self).data('clDatesLoaded') == true;
    }

    /**
     * Upraví grid podle velikosti rodiče
     */
    var resize = function(width){
        if (isLoaded(self)){
            $(self).setGridWidth(width).trigger("resize");            
        }else{
            create(self);
        }
    }


    /**
     * Z filtru získá nastavené hodnoty, spojí je s url params z php a vrátí je v query stringu, např.: fi-lm=1&fi-bi=2 
     */
    var getParams = function(){
        var params = $(self).data('params');
        var filterParams = {
            "fi-dm" : $("#cl-fi-dm").val(),
            "fi-du" : $("#cl-fi-du").val(),
            "fi-bi" : $("#cl-fi-bi").val(),
            "fi-ti" : $("#cl-fi-ti").val(),
            "fi-ai" : $("#cl-fi-ai").val(),
            "fi-dp" : $("#cl-fi-dp").val()
        }
        var lm = $("#cl-fi-lm:checked").val();
        var fm = $("#cl-fi-fm:checked").val();
        if(typeof lm == "undefined") lm = 0;
        if(typeof fm != "undefined") lm = 2;
        filterParams["fi-lm"] = lm;

        
        // slozeni filtrovacich parametru
        var tmp = [];
        $.each(filterParams, function(param, value){
            if (value == 'undefined'){
                return true; //ekvivalent continue
            }
            if (value > 0 || param == 'fi-lm'){
                tmp.push(param + "=" + value);
            }
        });

        // Pokud neni prazdny params tak pridam &
        if (params != "") {
            params = params+"&";
        }
        params = params + tmp.join('&');

        return params;
    }; //end of get params

    /**
     * Volání jednotlivých public metod
     */
    if (typeof method == 'undefined'){
        method = 'create';
    }
    switch (method){
        case 'create':
            create();
            break;
        case 'resize':
            resize(data);
            break;
        default:
            console.log('Undefined method in clDatesGrid');
    }
}


/**
 * Najdeme-li na stránce element s vlastními nabídkami, inicializujeme
 */
$(window).load(function(){
    $('#cl-offers-grid').each(function(){
        $(this).clDatesGrid();
    });
});