/**
 * 
 * Inicializace colorboxů nad html - očekává se volání na celém obsahu, nikoliv na samostatných odkazech - to ani nebude fungovat,
 * protože find() hledá v potomcích, nikoliv přímo v kolekci elementů
 *
 * Použítí hlavně na ajaxem načtený obsah - místo vkládání celého kódu do načteného view, stačí v callbacku ajaxu zavolat tuto funkci
 * 
 */
$.fn.bindColorboxes = function(){

	// základní konfigurace colorboxu
	var config = {
		transition:"elastic",
		preloading:true,
		current: cesys.ts('COLORBOX image {current} of {total}'),
		previous:  cesys.ts('COLORBOX previous'),
		next:  cesys.ts('COLORBOX next'),
		close:  cesys.ts('COLORBOX close')
	};


	// pro verzi frame speciální konfigurace - rozlišujeme podle přítomnosti třídy 'version-frame' na elementu body
	if ($('body').hasClass('version-frame')){
		config.maxWidth = 550;
		config.top =  100;
		config.opacity = 0;
	}	

	//inicializace s předpřipraveným configem
	$(this).find("a[rel='lightbox'], a[rel='colorbox'], a.lightbox, a.colorbox").colorbox(config);

	//galerii se vnuti ze je vzdy obrazek
	$.extend(config, {photo: true});
	$(this).find("a[rel='gallery']").colorbox(config);

}