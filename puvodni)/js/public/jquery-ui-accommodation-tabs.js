/**
 * Rozšíření jQuery UI tabs pluginu pro použití hlavně v šablonách hotelu a termínu
 *  - pamatuje vybrané záložky
 *  - dle nastavení parametru activateTabByContentHash automaticky zobrazí záložku s vybraným obsahem
 *  - automaticky inicializuje mapu na záložce a refreshne grid
 *
 *  @param {bool} activateTabByContentHash Pokud je true, vyhledá při načtení záložku obsahující element uložený v location.hash a záložku zobrazí
 */


$.widget( "cesys.tabsAccommodation", $.ui.tabs, {

    //callback metody
    options: {
        //pokud je true, vyhledá při načtení záložku obsahující element uložený v location.hash a záložku zobrazí
        activateTabByContentHash: true, 

    	/**
         *  Při vybrání testujeme na přítomnost mapy, kterou lze načítat, jen pokud je element viditelný
         */
        show: function(event, ui) {
            //ošetření načtení mapy
            if ($(ui.panel).find('.mapElement').length){
               $(ui.panel).find('.mapElement').loadGoogleMap();
            }
            //ošetření načtení německých CK na kliknuti na tab
            if ($(ui.panel).find('#trafficsDates').length){
               $(ui.panel).find('#trafficsDates').createTrafficsGrid();
            }
            //ošetření načtení německých CK na kliknuti na tab
            if ($(ui.panel).find('#amadeusDates').length){
                $(ui.panel).find('#amadeusDates').createAmadeusGrid();
            }
            //ošetření načtení gridu
            if ($(ui.panel).find('#terminy').length){
               $(ui.panel).find('#terminy').datesGrid('resize', $(ui.panel).width());
            }
            //ošetření načtení gridu
            if ($(ui.panel).find('#cl-offers-grid').length){
               $(ui.panel).find('#cl-offers-grid').clDatesGrid('resize', $(ui.panel).width());
            }
            //překreslení grafu cen
            if ($(ui.panel).find('.price-histories-chart').length){
               var redrawFunction = $(ui.panel).find('.price-histories-chart').data('redrawPlotFunction');
               redrawFunction();
            }
        },

        /**
         *  Poznačíme zvolenou záložku do location.hash
         */
        select: function(event, ui) {
            location.hash = ui.panel.id;
        }
    },

    //rozšíření konstruktoru - pokud je zaplé vybrání záložky dle obsahu,
    //pokusíme se přepnout na položku obsahující kotvu z location.hash
    _create : function(){
        //parent constructor (nenašel jsem jak to zavolat hezčeji)
        $.ui.tabs.prototype._create.call(this);

        var self = this;
        var options = this.options;
        var tabs = this.element.find('.ui-tabs-panel');

        //id obsahu z kotvy - odstraňujeme znaky, které by mohly vést k chybě, např.:
        //"uncaught exception: Syntax error, unrecognized expression: =8&fi-lm="
        //TODO: do budoucna pravděpodobně použití nějakého pluginu na url (URI.js)
        var contentId = location.hash.replace(/[#=%;,\/]/g,"");

        if (contentId.length && tabs.length && options.activateTabByContentHash){
            //vyfiltrujeme tab, který obsahuje zadané id z location.hash
            //selector na atribut id je bezpecnejsi nez pouziti .has()
            var tab = tabs.has('[id="'+contentId+'"]').first();
           
            //pokud jsme záložku našli, přepneme se na ni přes její index a metodusetOption
            if (tab.length){
                var tabIndex = tabs.index(tab); 
                this._setOption('selected', tabIndex );  
            }
        
        }

        //taby lze přepínat také libovolně umístěným odkazem s třídou change-structured-content - v rel atributu musí být id požadovaného tabu
        $('a.change-structured-content').each(function(index, element){
            $(element).bind('click', function(clickEvent){
                //vyfiltrujeme tab, který obsahuje zadané id z rel odkazu
                var contentId = $(this).attr('rel');
                var tab = tabs.filter('[id="'+contentId+'"]').first();

                //pokud jsme záložku našli, přepneme se na ni přes její index a metodusetOption
                if (tab.length){
                    var tabIndex = tabs.index(tab); 
                    self._setOption('selected', tabIndex );  
                }
                clickEvent.preventDefault();
            });
        });
    }

});

