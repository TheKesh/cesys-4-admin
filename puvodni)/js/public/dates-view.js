/**
 * Funkce v detailu termínu - ověření dostupnosti, schránka zájezdů apod...
 */
$(function() {

	/**
	 *	Schránka zájezdů - přidání/odebrání termínu
	 */
	$('.favourites-links').click(function(a,b,c){
		var id = $(this).attr('data-id');
		$.ajax({
			url : $(this).attr('href'),
			data : { 'data[Date][id]': id },
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				var el = $('.favourites-links');
				
				el.hide('fade');
				$('#favourites-number').html(data.count);

				if (el.attr('href').indexOf('js_favourites_add') > 0) {
					var	re = /js_favourites_add/;
					el.text(cesys.ts('Odebrat ze schránky'));
					el.removeClass('link-add').addClass('link-delete');
					el.attr('href', (el.attr('href').replace(re, 'js_favourites_delete')));
				}
				else {
					var	re = /js_favourites_delete/;
					el.text(cesys.ts('Vložit zájezd do schránky'));
					el.removeClass('link-delete').addClass('link-add');
					el.attr('href', (el.attr('href').replace(re, 'js_favourites_add')));
				}
				el.show('fade');
			},
			error : function() {}
		});			
		
		return false;
	});


	/**
	 * Kontrola dostupnosti termínu - pustí se jen na obrázek s atributem data-date-id a data-hash, které jsou jen u termínů s možností ověření
	 */
	$('#group-availability[data-date-id][data-hash]').each(function(){
		var availabilityImage = $(this).removeClass('img-lodaing');
		var dateId = availabilityImage.data('date-id');
		var hash = availabilityImage.data('hash');

		window.cesys.service.availability.configuration.lang = window.config.cesysLang;
		window.cesys.service.availability.doCheckRoom(
			{
				'cid'  : dateId,
				'hash' : hash
			},
			function(status) {
				switch (status) {
					case 'OK':
						availabilityImage
							.addClass('img-ok')
							.attr('src', window.config.fileServer + "/img/availability/ok.svg")
							.attr('alt', cesys.ts('Volná kapacita'))
							.attr('title', cesys.ts('Volná kapacita'))
							.after('<p id="group-availability-text">' + cesys.ts('VOLNO') + '</p>');
						break;

					case 'RQ':
					case 'ER':
						availabilityImage
							.addClass('img-rq')
							.attr('src', window.config.fileServer + "/img/availability/rq.svg")
							.attr('alt', cesys.ts('Na dotaz'))
							.attr('title', cesys.ts('Na dotaz'))
							.after('<p id="group-availability-text">' + cesys.ts('NA DOTAZ') + '</p>');
						break;

					case 'NK':
					case 'SO':
						if (window.config.settings.availabilityAllowOrderSoldOut) {
							availabilityImage
								.addClass('img-rq')
								.attr('src', window.config.fileServer + "/img/availability/rq.svg")
								.attr('alt', cesys.ts('Na dotaz'))
								.attr('title', cesys.ts('Na dotaz'))
								.after('<p id="group-availability-text">' + cesys.ts('NA DOTAZ') + '</p>');
						}else{
							availabilityImage
								.addClass('img-so')
								.attr('src', window.config.fileServer + "/img/availability/fail.svg")
								.attr('alt', cesys.ts('Obsazená kapacita'))
								.attr('title', cesys.ts('Obsazená kapacita'))
								.after('<p id="group-availability-text">' + cesys.ts('OBSAZENO') + '</p>');
						}
						break;
				}
			}
		); //doCheckRoom
	});

});
