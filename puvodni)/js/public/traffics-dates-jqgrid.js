
$.fn.createTrafficsGrid = function() {
	if ($(this).data('trafficsDatesLoaded') !== true ){
		// Nacteni informaci pro grid
		var self = this;
		var gridElement = $(this).find('#trafficsDatesElement');
		var giataid = gridElement.data('giataid');
		var params = gridElement.data('params');
		var limit = gridElement.data('limit');
		var searchmaskId = gridElement.data('searchmaskid');
		var acmDataSource = gridElement.data('acmsource');

		//proměná pro setTimout na postupné načítání dostupností
		var availabilityTimeout;

		//inicializujeme filtr
        var monthsFilter = $(self).find('#trafficsFilterMonth');
		var airportFilter = $(self).find('#trafficsFilterAirport');
		var durationFilter = $(self).find('#trafficsFilterDuration');
		var toFilter = $(self).find('#trafficsFilterTourOperator');
		var occupancyFilter = $(this).find('.occupancy-form');

		//inicializujeme occupancy form
		occupancyFilter
			.bind('occupancyChanged', function(event, data){
				console.log("occupancyChanged", data);
				$(self).refreshTrafficsGrid();
			})
			.occupancyForm();

		var occupancy = occupancyFilter.occupancyForm('getData');
		if (occupancy.childAges === null){
			delete occupancy.childAges;
		}

		airportFilter.add(durationFilter).add(toFilter).add(monthsFilter).change(function(){
			$(self).refreshTrafficsGrid();
		});

		// Vytvorim grid
		$(gridElement).jqGrid({
			autowidth: true,
			shrinkToFit: true,
			resizable: false,
			mtype: "POST",
			url: "/TrafficsMasters/grid_dates/"+searchmaskId+"/"+giataid+"/"+acmDataSource+"?"+params + '&' + $.param(occupancy),
			datatype: "json",
			colNames:[
					cesys.ts('Date'),
					cesys.ts('Days'),
					cesys.ts('Boarding'),
					cesys.ts('Airport'),
                '<sup>'+cesys.ts('FM')+'</sup>&frasl;<sub>'+cesys.ts('LM')+'</sub>',
					cesys.ts('Room'),
					cesys.ts('TO'),
					cesys.ts('Price'),
					cesys.ts('Link')
				],

			colModel:[
				{name:'date', index: 'dateFrom', width: 8 },
				{name:'duration', index: 'duration', width: 4, align:'center'},
				{name:'boarding', index: 'boarding', width: 9},
				{name:'airport', index: 'airport', width: 8, sortable:true},
                {name:'lm', index:'lm', width: 6, sortable:true, align:'center'},
				{name:'room', index: 'room_type', width: 7, sortable:false, align:'center',title:false},
				{name:'to', index: 'tourOperator', width: 10, sortable:false, align:'center'},
				{name:'price', index:'price', width: 6, sortable:true, align:'center'},
				{name:'link', index:'link', width: 6, sortable:false, align:'center',title:false}
			],
			pager: "#traffics-pager",
			rowList:[10,20,30,50],
			viewrecords: false, 
			sortname: "price",
			sortorder: 'asc',
			height: "100%",
			rowNum: limit,
			beforeRequest : function(){
				//loadovací overlay
				$(self).append($('<div class="overlay"></div>'));
			},
			loadComplete: function (data) {
				//skrytí overlaye
				$(self).children('.overlay').fadeOut('fast', function(){
					$(this).remove();
				});

				//když nejsou žádná data - zobrazíme upozornění
				if ($(this).getGridParam("records") == 0) {
					//jen pokud ještě nebyla zpráva vložena
					if ($(this).parent().find('#no-dates-message').length == 0) {
				     	$(this).after($('<div>',{
				     		'id' : 'no-dates-message',
				     		'text' : cesys.ts('No dates were found')
				     	})); 
			     	}   
		     	}else{
     		        //jinak odstraníme zprávu o prázdných výsledcích
			        $(this).parent().find('#no-dates-message').remove();
		     	}

				// naplnim filtr letistema
				var listAirport = data['listAirport'];
				if (!$.isEmptyObject(listAirport) && airportFilter.children().length == 1) {
					$.each(listAirport, function(v, k){
						airportFilter.append("<option value='"+k+"'>"+v+"</option>");
					});
					airportFilter.closest('div').show();
				}

				// naplnim filtr tour operatorama
				var listTourOperator = data['listTourOperator'];
				if (!$.isEmptyObject(listTourOperator) && toFilter.children().length == 1) {
					$.each(listTourOperator, function(v, k){
						toFilter.append("<option value='"+k+"'>"+v+"</option>");
					});
					toFilter.closest('div').show();
				}

				// // naplnim filtr poctem dni
				var listDuration = data['listDuration'];
				if (!$.isEmptyObject(listDuration) && durationFilter.children().length == 1) {
					$.each(listDuration, function(v, k){
						durationFilter.append("<option value='"+k+"'>"+k+"</option>");
					});
					durationFilter.closest('div').show();
				}

				//lichým řádkům přidáme třídu
				$(this).find("tr:odd").addClass("alt");

                $('.check-date').click(function(){
                    var self = $(this);
                    var selfTr = self.closest('tr');
                    var availability = selfTr.find('.date-availability');
                    var dateLink = selfTr.find('.grid-date-link');
                    var datePrice = selfTr.find('.grid-date-price');
                    var loadingImg = $('<img>')
                        .addClass('img-availability img-loading')
                        .attr('src', window.config.fileServer + "/img/availability/loading_small.gif")
                        .attr('alt', cesys.ts('Loading ...'))
                        .attr('title', cesys.ts('Loading ...'));
                    availability.prepend(loadingImg);
                    selfTr.find('.check-date').html(loadingImg.clone());
                    $.ajax({
                        url: "/online/availability/cesys/check_date_traffics.php",
                        data: {
                            cid: self.data('cid'),
                            code : self.data('offercode'),
                            countAdult : self.data('countadult'),
                            countChild : self.data('countchild'),
                            childAges : self.data('childages')
                        },
                        dataType: 'json',
                        success: function(response) {
                            switch (response.code) {
                                case "OK":
                                    selfTr.find('img.img-loading')
										.addClass('availability-small')
                                        .attr('src', window.config.fileServer+"/img/availability/ok.svg")
                                        .attr('alt', cesys.ts('Volná kapacita'))
                                        .attr('title', cesys.ts('Volná kapacita'));
                                    dateLink.show('slow');
                                    datePrice.html(response.price_per_person);
                                    break;
                                case 'RQ':
                                    selfTr.find('img.img-loading')
										.addClass('availability-small')
                                        .attr('src', window.config.fileServer+"/img/availability/rq.svg")
                                        .attr('alt', cesys.ts('Na dotaz'))
                                        .attr('title', cesys.ts('Na dotaz'));
                                    dateLink.show('slow');
                                    datePrice.html(response.price_per_person);
                                    break;
                                case 'XX':
                                case 'NK':
                                    selfTr.find('img.img-loading')
										.addClass('availability-small')
                                        .attr('src', window.config.fileServer+"/img/availability/fail.svg")
                                        .attr('alt', cesys.ts('Obsazená kapacita'))
                                        .attr('title', cesys.ts('Obsazená kapacita'));
                                    break;
                            }
                            selfTr.find('.check-date').remove();
                        }
                    });
                });

			}
		});
		$(this).data('trafficsDatesLoaded', true);
	}
};


$.fn.refreshTrafficsGrid = function() {
	// Nacteni informaci
	var self = this;
	var gridElement = $(this).find('#trafficsDatesElement');
	var giataid = gridElement.data('giataid');
	var params = gridElement.data('params');
	var searchmaskId = gridElement.data('searchmaskid');
    var acmDataSource = gridElement.data('acmsource');

	// Zadani do filtru
	var filter = {};
	filter.Airport = $(this).find('#trafficsFilterAirport').val();
	filter.TourOperator = $(this).find('#trafficsFilterTourOperator').val();
	filter.Duration = $(this).find('#trafficsFilterDuration').val();
    filter.Month = $(this).find('#trafficsFilterMonth').val();

	//zpracování obsazenosti
	var occupancy = $(this).find('.occupancy-form').occupancyForm('getData');
	if (occupancy.childAges === null){
		delete occupancy.childAges;
	}

	// Pokud neni prazdny params tak pridam &
	if (params != "") {
		params = params+"&";
	}
	
	// Vytvoreni filtru
	var tmp = [];
	var $i = 0;
	$.each(filter, function(k, v){
		tmp[$i++] = "gridFilter" + k + "=" + v;
	});
	params = params+tmp.join('&');


	// Refresh gridu
	gridElement
		.setGridParam({
			url: "/TrafficsMasters/grid_dates/"+searchmaskId+"/"+giataid+"/"+acmDataSource+"?"+params + '&' + $.param(occupancy)
		})
		.setGridParam({
			page: 1
		})
		.trigger("reloadGrid");
};

/**
 * Najdeme-li na stránce element s nemeckymi terminy a je viditelný, inicializujeme
 */
$(window).load(function(){
    $('#trafficsDates').each(function(){
        if ($(this).is(':visible')){
            $(this).createTrafficsGrid();
        }
    });
});