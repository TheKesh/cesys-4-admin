/**
 * Inicializace mapy nad elementem. Předpokládá se, že element má v property data uložené hodnoty latitude, longitude, name
 */
$.fn.loadGoogleMap = function(){
    //init jen pokud nebyla mapa ještě načtená
    if ($(this).data('mapLoaded') !== true ){
        var latitude = $(this).data('latitude');
        var longitude = $(this).data('longitude');
        var name = $(this).data('name');

        //vlastnosti mapy
        var mapOptions = {
          center: new google.maps.LatLng(latitude, longitude),
          zoom: 16,
          scaleControl: true,
          mapTypeId: google.maps.MapTypeId.HYBRID
        };

        //vytvoření mapy a uložení do dat elementu
        var map = new google.maps.Map(
            $(this).get(0), //získání DOM elementu z jQuery kolekce
            mapOptions
        );
        $(this).data('mapInstance', map);

        //značka na mapě
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: map,
            title: name
        });
        
        //info window po kliknutí na značku
        var infowindow = new google.maps.InfoWindow({content: name, size: new google.maps.Size(50,50)});
        google.maps.event.addListener(marker, 'click', function() {infowindow.open(map,marker); });

        //uložíme si, že byla mapa již načtená
        $(this).data('mapLoaded', true);
    }
    //pokud již byla načtená provedeme jen refresh, který zajistí správné zobrazení
    else{
        //instanci google mapy vyčteme z dat elementu
        var map = $(this).data('mapInstance');
        if (map){
            google.maps.event.trigger(map, 'resize');
        }
    }
}


/**
 * Najdeme-li na stránce element s mapu a je viditelný, inicializujeme
 */
$(window).load(function(){
    $('.mapElement').each(function(){
        if ($(this).is(':visible')){
            $(this).loadGoogleMap();
        }
    });
});
