$(document).ready(function(){

    /**
     * Inicializace superfish na hlavním menu
     */
    $(".nav").superfish({
        animation : { opacity:"show", height:"show" }
    });


    /**
     * Incializace colorboxů - vlastní funkce, viz bind-colorbox.js
     */
    $('body').bindColorboxes();
	

    /**
     * Obsluha rozbalovacího textu (původně element elementu show_more_text)
     * - po kliku na odkaz k rozbalení textu se vezme id v atributu rel, vyhledá se element dle id, zobrazí se a odkaz sám sebe odstraní
     */
    $('a.show-more-text[rel]').click(function(event){
        $('#' + $(this).attr('rel')).show();
        $(this).parent().remove();
        event.preventDefault();
    });


    /**
     * Pro URI plugin nastavíme globálně fragment prefix na prázdný string - defaultně otazník
     */
    URI.fragmentPrefix = "";


    /**
     * Inicializace tooltipu
     */
    $('.tooltip').tooltip();

});

