
/**
 * Plugin pro zprovoznění tabů v elementu simple-tabs - hlavičky tabů a obsah je provázán atributem rel
 * Při zobrazení tabu vyvolá událost simpletabsshow
 */

(function ( $, window, document, undefined ) {
    $.widget( "cesys.simpleTabs" , {

        //Defaultní options
        options: {
            rememberSelected: true,         //pamatovat v hashi v adrese vybranou záložku?
            activateTabByContentHash: true, //přepnout na vybranou záložku při inicializaci i podle obsahu záložek
            selectedTabPrefix: ''           //prefix který se propisuje do hashe, pokud je zapnuté pamatování vybrané záložky
        },

        //Setup widget
        _create: function () {
        	var self = this;
            var options = self.options;

            //element tabů
        	this._tabsElement = this.element;

            //odkazy na záložky  přepínače
        	this._openers = this._tabsElement.find('.simple-tab-header a');

            //kontejner s jednotlivými divy s obsahem
        	this._contents = this._tabsElement.find('.simple-tab-content .simple-tab');

            //počáteční skrytí záložek, které nejsou otevřené
            this._contents.not('.opened').each(function(index, element){
                $(element).hide();
            });

            //přepínání tabů
            this._openers.add('a.change-structured-content').each(function(index, element){
                $(element).bind('click', function(clickEvent){
                    var toShow = $(this).attr('rel');
                    self.showTab(toShow);
                    clickEvent.preventDefault();
                });
            });

            //počáteční načtení
            if (options.rememberSelected || options.activateTabByContentHash){
                //id obsahu z kotvy - odstraňujeme znaky, které by mohly vést k chybě:
                //"uncaught exception: Syntax error, unrecognized expression: =8&fi-lm="
                //TODO: do budoucna pravděpodobně použití nějakého pluginu na url (URI.js)
                var selected = location.hash;
                selected = selected.replace(options.selectedTabPrefix, '');
                selected = selected.replace(/[#=%;,\/]/g,"");

                //pokud je vůbec něco v hashi, zkusíme to najít v tabech
                if (selected.length){
                    //pokusíme se kotvu najít přímo v tabech
                    var tab = this._contents.filter('[id="'+selected+'"]').first();

                    //pokud jsme nenalezli a je zapnuté aktivace tabu dle obsahu
                    //prohledáme i obsah záložek na přítomnost kotvy
                    if (!tab.length && options.activateTabByContentHash){
                        tab = this._contents.find('#' + selected).closest('.simple-tab');
                    }

                    //pokud jsme záložku našli, přepneme se na ni
                    if (tab.length){
                        var tabId = tab.attr('id');
                        this.showTab(tabId);
                    }
                }
            }
        },


        /**
         * Zobrazení tabu
         * @param  {string} tabId Id tabu, který chceme zobrazit - bez # na začátku
         */
        showTab: function (tabId){
        	var self = this;
            var options = self.options;
        	var toHide = this._contents.filter('.opened');
            var link = this._openers.filter('[rel="' + tabId + '"]');
        	var toShowElement = this._contents.filter('#' + tabId);
          
            //pokud je záložka už otevřená, nic neděláme
            if (link.hasClass('opened')){
                return;
            }

            //odstranění třídy ze záložek
   			this._openers.removeClass('opened');

            //zapsání vybrané záložky do hashe před zobrazením obsahu
            //díky tomu, že ještě není obsah s kotvou vidět, nescrolluje prohlížeč k elementu
            if (options.rememberSelected){
                location.hash = options.selectedTabPrefix + toShowElement.attr('id');
            }

            //skrytí viditelné záložky
            toHide.hide(0, function(){
                //po dokončení animace odebereme třídu
                toHide.removeClass('opened');

                //spustíme animaci na zobrazení nové záložky
                toShowElement.show(0, function() {
                    //přidáme třídu na záložce a odkazu
                    $(this).addClass('opened');
                    link.addClass('opened');

                    //spustíme událost
                    self._trigger('show', null, {
                        'tab' : $(this)
                    });
                });
            }); //end of fadeOut


        } //end of showTab

    });

})( jQuery, window, document );


