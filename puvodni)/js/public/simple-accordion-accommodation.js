/**
 * Rozšíření simple accordionu pro použití hlavně v šablonách hotelu a termínu
 *  - pamatuje vybrané záložky
 *  - automaticky inicializuje pluginy, které vyžadují viditelnost elementu (nesmějí být display:none)
 *
 */

$.widget( "cesys.simpleAccordionAccommodation", $.cesys.simpleAccordion, {

    //callback metody
    options: {
        //pokud je true, vyhledá při načtení záložku obsahující element uložený v location.hash a záložku zobrazí
        activatePageByContentHash: true, 

    	/**
         *  Při vybrání testujeme na přítomnost mapy, kterou lze načítat, jen pokud je element viditelný
         */
        show: function(event, data) {
            //zobrazená záložka
            var content = data['page'];

            //pokud obsahuje mapu, načteme ji
            if (content.find('.mapElement').length){
                content.find('.mapElement').loadGoogleMap();
            }
            //ošetření načtení německých CK na kliknuti na tab
            if (content.find('#trafficsDates').length){
                content.find('#trafficsDates').createTrafficsGrid();
            }
            //ošetření načtení německých CK na kliknuti na tab
            if (content.find('#amadeusDates').length){
                content.find('#amadeusDates').createAmadeusGrid();
            }
            //ošetření načtení gridu
            if (content.find('#terminy').length){
               content.find('#terminy').datesGrid('resize', content.width());
            }
            //ošetření načtení gridu
            if (content.find('#cl-offers-grid').length){
               content.find('#cl-offers-grid').clDatesGrid('resize', content.width());
            }
            //překreslení grafu cen
            if (content.find('.price-histories-chart').length){
               var redrawFunction = content.find('.price-histories-chart').data('redrawPlotFunction');
               redrawFunction();
            }
        }

    }

});

