<?php

class Logger
{
	private $logfile;
	
	
	/**
	 *	@param string $logfile Umístění logu.
	 */
	public function __construct($logfile)
	{
		$this->logfile = $logfile;
	}


	/**
	 *	Zalogování
	 *	@param string $addr Kdo zapisoval.
	 *	@param string $addr ?
	 *	@param string $location ?
	 *	@param string $browser ?
	 *	@param string $file ?
	 *	@param string $line ?
	 *	@param string $msg ?
	 */
	public function log($addr, $location, $browser, $file, $line, $msg)
	{
		$this->write($this->format($addr, $location, $browser, $file, $line, $msg));
		return $this;
	}



	/**
	 *	Zapsání do souboru.
	 *	@param string $content Obsah zápisu.
	 */
	private function write($content)
	{
		file_put_contents($this->logfile, $content, FILE_APPEND | LOCK_EX);
		return $this;
	}



	/**
	 *	Formátování obsahu.
	 */
	private function format($addr, $location, $browser, $file, $line, $msg)
	{
		return '[' . date('Y-m-d H:i:s') . ']'
    		. "\tremote: [{$addr}]"
    		. "\tlocation: [{$location}]"
    		. "\tbrowser: [{$browser}]"
    		. "\tfile: {$file} ({$line})"
    		. "\tmesssage: {$msg}"
    		. PHP_EOL;
	}


}


#define('WWW_ROOT', rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/'); //kvuli includu v global_configu
#require_once __dir__ . '/../global_config.inc.php';

#print_r(':' . FILE_SERVER);
#die('++');


$log = new Logger(__dir__ . '/../../../../logs/errorlogger.log');

if (isset($_POST['msg'], $_POST['file'], $_POST['line'], $_POST['location'])) {
	$log->log($_SERVER['REMOTE_ADDR'], $_POST['location'], $_SERVER['HTTP_USER_AGENT'], $_POST['file'], $_POST['line'], $_POST['msg']);
	echo 'post';
}
else if (isset($_GET['msg'], $_GET['file'], $_GET['line'], $_GET['location'])) {
	$log->log($_SERVER['REMOTE_ADDR'], $_GET['location'], $_SERVER['HTTP_USER_AGENT'], $_GET['file'], $_GET['line'], $_GET['msg']);
	header('Content-Type: image/gif');
}
else {
	echo 'fail';
}
