<?php


$jsFiles = array(
	'beforeLocales' => array(
		'./mobile/libs/jquery-1.11.1.min.js',                 
		'./mobile/libs/jquery.mobile-1.4.5.js',           //jquery mobile v neminifikované verzi, jinak je to po druhe minifikaci rozbite
		'./mobile/libs/jqm-datebox.core.js',              //datepicker core
		'./mobile/libs/jqm-datebox.mode.datebox.js',	  //datepicker datebox verze
	),
	'afterLocales' => array(
		'./mobile/libs/jqm-spinbox.js',                   //výber počtu v masce TODO zkusit nahradit spinnerem
		'./mobile/libs/xdate.js',                         //práce s datumem
		'./mobile/libs/xdate.i18n.js',                    //jazykové verze pro xdate
		'./mobile/libs/idangerous.swiper.min.js',       //slider v detailu
		'./mobile/libs/spinner.js',                       //výber počtu v detailu nabídky
		'./mobile/libs/jquery.raty.js',                   //hvezdickove hodnoceni


		//vlastní widgety a kód
		'./mobile/widgets/selectmenu_overload.js',       //override defaultního selectmenu
		'./mobile/widgets/filterable_overload.js',       //override filterable widgetu

		'./mobile/widgets/filterableSelect.js',          //vlastní select s filtrováním
		'./mobile/widgets/specific/countrySelect.js',    //vlastní selectmenu na výběr země
		'./mobile/widgets/specific/destinationSelect.js',//vlastní selectmenu na výběr destinace
		'./mobile/widgets/specific/locationSelect.js',   //vlastní selectmenu na výběr lokace
		'./mobile/widgets/specific/tripTypeSelect.js',   //vlastní selectmenu na výběr typu zájezdu
		'./mobile/widgets/specific/availableDates.js',   //vlastní selectmenu na výběr dostupných termínů
		'./mobile/widgets/specific/bookingForm.js',      //widget objednávkového formu

		'./mobile/widgets/specific/offersWidget.js',     //widget view seznamu nabídek
		'./mobile/widgets/specific/offerDetailWidget.js',//widget detailu nabídky
		'./mobile/widgets/specific/searchmaskWidget.js', //widget vyhledávací masky
		'./mobile/widgets/app.js',                       //widget s komplet aplikací

		'./shared/availability.js',                      //service pro online ověření
		'./mobile/utils.js',                             //obecné utility
		'./mobile/init.js',                              //inicializace
	)
);

//překaldy k pluginům
$locales =array(
	'cs' => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.cs.utf8.js'
		),
	'sk'   => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.sk.utf8.js'
		),
	'hu'  => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.hu.utf8.js'
		),
	'sr'  => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.sr.utf8.js'
		),
	'hr'  => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.hr.utf8.js'  
		),
	'en'  => array(
			'./mobile/libs/jquery.mobile.datebox.i18n.en.utf8.js'
		)
);

//zjistime jazyk
$lang = require __dir__ . '/libs/locale.php';

//existuje definice souboru s lokalizaci v pozadovanem jazyce, pouzijeme ji
if (isset($locales[$lang])){
	$jsLocaleFile = $locales[$lang];
} else {
	$jsLocaleFile = array();
}

$files = array_merge($jsFiles['beforeLocales'], $jsLocaleFile, $jsFiles['afterLocales']);

require_once __dir__ . '/js.inc.php';

