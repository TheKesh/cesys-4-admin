<?php

/**
 * BC: neudržujeme předchozí verze javascriptu 1-3
 */


$files_v4 = array(
	'beforeLocales' => array(
		'./admin/jquery-1.10.2.min.js',				//nejvyssi jQuery s podporou IE 8
		'./admin/jquery-migrate-1.2.1.min.js',			//plugin pro prechod pres jQuery 1.9
		'./admin/moment.js'					//knihovna pro práci s datumy a časy (nova verze 2.13.0) + jazyky prilozeny nize
		),
	'afterLocales'	=> array(

		'./admin/assets/console.js',				//osetreni neexistence konzole
		#'./admin/assets/errorlogger.js',
		'./admin/bootstrap.min.js',				//bootstrap framework - !musi byt pred jquery ui!
		'./admin/jquery-ui-1.10.3.min.opraveno-vs.js',		//plná verze jquery ui s ručně opravenou komponentou draggable (oprava bugu ticket #9315), v dalších verzích bude opravená
														//ručně minifikovaná pomocí http://refresh-sf.com/yui - původní verze po druhé minifikaci obsahovala chyby

		'./admin/jquery.multiselect.1.13.js',			//multiselect - nejnovejsi verze
		'./admin/jquery.multiselect.filter1.5pre.js',		//aktualni verze k prepracovanemu multiselect pluginu
		'./admin/jquery.multiselect.presets.js',		//vlastni plugin k pridani predvoleb do multiselectu
		'./shared/jquery.jqGrid.4.5.2.min.js',			//nejnovejsi verze jqGrid
		'./admin/jquery-calendar.pack.js',			//deprecated - pouziva se ale mel by se nahradit datepickerem z ui
		'./admin/jquery-calendar-cs.js',			//deprecated - pouziva se ale mel by se nahradit datepickerem z ui
		'./shared/jquery.flot.js',				//kresleni grafu
		'./admin/easySlider.js',				//slider - galerie ve vyhledavaci
		'./admin/jquery.farbtastic-1.2.js',			//výběr barvy - colorpicker

		'./shared/jquery.selectBox.js',				//pluginy pro selekty/multiselekty
		'./shared/jquery.multiSelectBox.js',
		'./shared/jquery.selectCombo.js',			//nahrávání obsahu selectu v reakci na výběr možnosti v jiném

		'./admin/searches_grid.js',				//grid do vyhledavace

		'./admin/multifile_compressed.js',			//plugin pro výběr více souborů při uploadu
		'./admin/jquery.slicker.js',				//vlastní miniplugin na skrývání obsahu
		'./shared/jquery.colorbox-1.5.14.js',		//fotogalerie
		'./admin/globalFunctions.js',				//incializace pluginů a dalších funkcí
		'./admin/jtip.js',					//nápověda v nastavení apod. - loadovaná ajaxem
		'./admin/jquery.tooltip.pack.js',			//tooltip v kalendáři - pouziva se ale mel by se nahradit bootstrap tootlipem
		'./shared/utils.frontx.js',	 			//ajaxová fronta
		'./shared/utils.price.js',				//formátování ceny
		'./admin/forum.scripts.js',

		//nové v cesys 3
		'./shared/simple-accordion.js',				//vlastní plugin pro accordion
		'./admin/cesys.menu.js',				//funkce pro obsluhu main menu a submenu
		'./admin/login.js',					//funkce uplatněné v login layoutu
		'./admin/form.filter.js',				//sbalování/rozbalování filtrů
		'./admin/listLimit.js',					//přepínač pro počet záznamů na stránce
		'./admin/removeDiacriticsPlugin.js', 			//plugin s funkcí removeDiacriticsFromString(str) pro odstranění diakritiky ze stringu
		'./admin/bootstrap.init.js',				//inicializace věcí týkajících se bootstrapu, javascript fallbacky bootstrapu
		'./shared/dialog-destinations-selector.js',		//selektor destinací ve vyhledávači
		'./shared/availability.js',				//service pro ověření dostupnosti zájezdu
		'./shared/price-list.js',				//obsluha ceníku
		'./shared/occupancy-form.js',				//výběr skladby osob
		'./shared/order-form.js',				//objednávkový formulář
		'./admin/local-booking-form.js',			//adminovská objednávka


		'./admin/angularjs/angular.js',				//init angularjs
		'./admin/angularjs/ng-sanitize.js',			//knihovna pro práci s řetězci
		'./admin/angularjs/safe-string.js',			//knihovna pro práci s řetězci
		'./admin/angularjs/ng-route.js',			//knihovna pro práci s routováním
		'./admin/angular-apps/newsletter-dialog.js',		//aplikace pro dialog s newsletterem
		'./admin/angular-apps/cesys-directives.js',		//souhrn všech menších directiv
		'./admin/angular-apps/todo.js',				//todo

		//aplikace průvodce newletteru
		'./admin/angular-apps/newsletter-campaign/NewsletterCampaignModule.js',
		'./admin/angular-apps/newsletter-campaign/Services/DatabaseAdapter.js',
		'./admin/angular-apps/newsletter-campaign/Services/StackService.js',
		'./admin/angular-apps/newsletter-campaign/Services/ValidatorService.js',
		'./admin/angular-apps/newsletter-campaign/Factories/FlashMessageFactory.js',
		'./admin/angular-apps/newsletter-campaign/Factories/NavButtonService.js', //todo - jméno na zamyšlenou
		'./admin/angular-apps/newsletter-campaign/Directives/FileDirective.js',
		'./admin/angular-apps/newsletter-campaign/Controllers/RecipientsController.js',
		'./admin/angular-apps/newsletter-campaign/Controllers/PreferencesController.js',
		'./admin/angular-apps/newsletter-campaign/Controllers/ContentController.js',
		'./admin/angular-apps/newsletter-campaign/Controllers/AttachmentsController.js',
		'./admin/angular-apps/newsletter-campaign/Controllers/SummaryController.js',

		'./admin/bootstrap-editable.min.js',			//in line editace textu
		'./admin/select2-4.0.3.min.js',					//select s filtrem
		'./admin/toastr-2.1.3.min.js',					//notifikacni knihovna
		'./admin/document-template-dialog.js',
		'./admin/jquery.fileDownload.js',
        './admin/magicware-booking.js',
		'./admin/clipboard-copy.js',				//gdpr funkce
		'./admin/table-sort.js',				//šipky pro řazení
		'./admin/file-upload.js',				//drag & drop
		)

	);

$locales =array(
	 'cs'	=> array(
	 			'./shared/grid.locale-cs.js'
	 			, './shared/jquery-ui.datepicker-cs.js'
				, './admin/checkboxesSelector-cs.js'				//přidává tlačítka pro zaškrtnutí/odškrtnutí checkboxů v tabulkách
				, './admin/moment-locale/cs.js'
	 		)
	, 'sk'	=> array(
	 			'./shared/grid.locale-sk.js'
	 			, './shared/jquery-ui.datepicker-sk.js'
				, './admin/checkboxesSelector-sk.js'
				, './admin/moment-locale/sk.js'
	 		)
	 , 'hu'	=> array(
	 			'./shared/grid.locale-hu.js'
	 			, './shared/jquery-ui.datepicker-hu.js'
				, './admin/checkboxesSelector-hu.js'
				, './admin/moment-locale/hu.js'
 			)
	 , 'vi'	=> array(
	 			'./shared/grid.locale-vi.js'
	 			, './shared/jquery-ui.datepicker-vi.js'
				, './admin/checkboxesSelector-vi.js'
				, './admin/moment-locale/vi.js'
	 		)
	 , 'sr'	=> array(
	 			'./shared/grid.locale-sr-latin.js'
	 			, './shared/jquery-ui.datepicker-sr-SR.js'
				, './admin/checkboxesSelector-sr.js'
				, './admin/moment-locale/sr.js'
	 		)
	 , 'hr'	=> array(
	 			'./shared/grid.locale-hr.js'
	 			, './shared/jquery-ui.datepicker-hr.js'
				, './admin/checkboxesSelector-hr.js'
				, './admin/moment-locale/hr.js'
	 		)
	 , 'en'	=> array(
	 			'./shared/grid.locale-en.js'
	 			, './shared/jquery-ui.datepicker-en-GB.js'
				, './admin/checkboxesSelector-en.js'
				// locale v anglictine pro momentjs neni potreba
	 		)
	);

//zjistime jazyk
$lang = require __dir__ . '/libs/locale.php';

//existuje definice souboru s lokalizaci v pozadovanem jazyce, pouzijeme ji
if (isset($locales[$lang])){
	$locale_file = $locales[$lang];
} else {
	$locale_file = array();
}

switch (@$_GET['v']) {
	case 4:
		$files = array_merge($files_v4['beforeLocales'], $locale_file, $files_v4['afterLocales']);
		$version = 4;
		break;	
	default:
	    //pokud se neodesle/nezpracuje verze, pro jistotu nastavime vychozi
		$files = array_merge($files_v4['beforeLocales'], $locale_file, $files_v4['afterLocales']);
		$version = 4;
}


require_once './js.inc.php';

