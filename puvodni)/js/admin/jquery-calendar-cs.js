/* Czech initialisation for the jQuery calendar extension. */
/* Written by Tomas Muller (tomas@tomas-muller.net). */
$(document).ready(function(){
	popUpCal.regional['cs'] = {clearText: 'Smazat', closeText: 'Zavřít', 
		prevText: '&lt;&lt;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', nextText: '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&gt;&gt;', currentText: 'Dnes',
		dayNames: ['Ne','Po','Út','St','Čt','Pá','So'],
		monthNames: ['Leden','Únor','Březen','Duben','Květen','Červen',
		'Červenec','Srpen','Září','Říjen','Listopad','Prosinec'],
		dateFormat: 'DMY.'};
	popUpCal.setDefaults(popUpCal.regional['cs']);
});
