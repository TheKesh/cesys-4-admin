/*
 * Plugin pro vložení předvoleb do multiselectů vytvořených pluginem multiselect
 * - jako parametr při inicializaci musíme předat options se seznamem voleb selectu - hodnoty value a text
 * - ke každé hodnotě value musí odpovídat stejný data atribut na selectu nad kterým inicializujeme, např.:
 *
 * <select id="example" data-volba-1="[1,2]" data-volba-2="[2,3]">
 *   <option value="1">letecky</option>
 *   <option value="2">busem</option>
 *   <option value="3">autem</option>
 * </select>
 *
 * $('#example')
 *   .multiselect(...)
 *   .multiselectpresets({
 *    options : [
 *      {
 *        value : 'volba-1',
 *        text : 'předvybrat volbu 1'          
 *      },
 *      {
 *        value : 'volba-2',
 *        text : 'předvybrat volbu 2'
 *      }
 *    ]
 *  });
 */
(function($) {
  $.widget('ech.multiselectpresets', {

    options: {
      //placeholder selectu s předvolbami
      placeholder: cesys.ts('Presets')+':',

      //text při načítání předvoleb
      textSelecting: cesys.ts('Selecting'),

      //prodleva než nastavíme zpět text placeholderu
      selectingTextDelay : 1200
    },

    _create: function() {
      var input = $(this.element);
      var instance = (input.data('echMultiselect') || input.data("multiselect") || input.data("ech-multiselect"));
      var widgetFilter = instance.header.find('.ui-multiselect-filter');
      var options = this.options;

      //v parametrech konstruktoru nebyly předané hodnoty pro předvolby, končíme
      if (!options.options) return;

      //příprava voleb pro select, první přidáme placeholder
      var selectValues = [];
      selectValues.push(
        $('<option>', {
          'value' : 'placeholder',
          'html' : options.placeholder
        })
      );
      
      $.each(options.options, function(index, value){
        var option = $('<option>', {
          'value' : value.value,
          'html' : value.text
        });
        selectValues.push(option);
      });


      //sestavení selectu
      var filterSelect = $('<select>', {
        'class' : 'ui-helper-reset'
      })
      //append voleb selectu
      .append(selectValues)
      //na změně vyčteme id z atributu selectu a přednastavíme je
      //atribut selectu musí mít odpovídající název k atributu value volby selectu
      .on('change', function(){
        var filterSelect = $(this);
        var selectedOption = filterSelect.val();
        var valuesToSet = null;

        //vybraný placeholder - končíme
        if (selectedOption == 'placeholder') return;

        //k vybrané hodnotě z předvoleb musí existovat atribut data na selectu a v něm sezname idček, které se mají vybrat
        valuesToSet = input.data(selectedOption);

        //pokud je co nastavovat
        if (valuesToSet){
          var optionValue = null;

          //projdeme vsechny hodnoty selectu a bud option vybereme nebo odvybereme
          input.find('option').each(function(){
            optionValue = parseInt($(this).val());
            if (valuesToSet.indexOf(optionValue) !== -1){
              $(this).prop('selected', true);
            }else{
              $(this).prop('selected', false);
            }           
          });

          //refreshneme plugin, aby se změna výběru projevila i vizuelně v checkboxech
          //nastavíme default hodnotu selectu
          filterSelect.val('placeholder');
          input.multiselect('refresh');
          filterSelect.find('[value="placeholder"]').text(options.textSelecting);
          setTimeout(function(){
            filterSelect.find('[value="placeholder"]').text(options.placeholder);
          }, options.selectingTextDelay);
          
        }       
      });
      
      //vložení za textový filtr
      widgetFilter.append(filterSelect);

    }
  });

})(jQuery);
