
;(function ($, window, document, undefined) {
	$.widget('cesys.localBookingForm', {
		/**
		 * Defaultní options, všechny jdou přepsat při inicializaci pluginu
		 * $(selector).localBookingForm({
		 * 		optionName : optionValue
		 * });
		 */
		options: {
			//adresa kam posílat data k uložení objednávky
			savelUrl : '/admin/LocalBookings/save_order',

			//počty dospělých a dětí
			adultCount : 2,
			childCount : 0,

			// target v linku na editaci objednávky
            linkTarget: 'self'
		},

		/**
		 * Konstruktor - načtení seznamů, bindování funkcí
		 */
		_create : function(){
			var self = this,  //instance pluginu
				options = self.options, //options
				element = self.element; //element, na kterém je plugin spuštěn

			//html elementy
			self._elementPriceList = element.find('.price-list');
			self._elementOccupancyForm = element.find('.occupancy-form');
			self._elementOrderForm = element.find('.order-form').hide();
			self._elementOrderFormToggler = element.find('.toggle-order-form');
			self._elementBookingResult = element.find('.booking-result');
			self._elementButtonContract = element.find('.cs-contract');
			self._elementButtonCalculation = element.find('.send-calculation');
			self._elementButtonPriceList = element.find('.send-price-list');

			//pomocné proměnné
			self._dateId = element.data('date-id');
			self._dataSource = element.data('data-source');
			self._hideForm = element.data('hide-booking-form');

			//inicializace price listu
			self._elementPriceList
				.bind('dataLoading', function(){
					self._showOverlay();
				})
				.bind('dataLoaded', function(){
					self._hideOverlay();
				})
				.priceList({
					adultCount : self.options.adultCount,
					childCount : self.options.childCount,
					loadData : false //při vytvoření pluginu nebudeme načítat data ceníku, počkáme až na událost změny obsazenosti
				});

			//order form
			self._elementOrderForm
				.orderForm({
					adultCount : self.options.adultCount,
					childCount : self.options.childCount
				})
				.bind('orderSubmit', function(event, data){

					data.priceList = self._elementPriceList.priceList('getData');
					data.dataSource = self._dataSource;
					data.dateId = self._dateId;
					if (data.dataSource === 'traffics_connector') { // pro traffics odešlu i pojištění a parkování
                        data.insurance = self.element.data('insurance');
                        data.parking = self.element.data('parking');
                    } else if (data.dataSource === 'ypsilon_package') { // pro dnamický balíček odešlu pojištění, parkování a lety tam/zpět
                        data.insurance = self.element.data('insurance');
                        data.parking = self.element.data('parking');
                        data.flightToDestination = self.element.data('flightToDestination');
                        data.flightFromDestination = self.element.data('flightFromDestination');
					}

					data.checkCondition = true; //pro adminu checkcondition fixně na true
					data.occupancy = {
						adultCount : self.options.adultCount,
						childCount : self.options.childCount,
						childAges : null
					};

                    // pro delta se termíny z traffiscu nejdříve zobrazí v dialogovém oknu
                    if (data.dataSource === 'traffics_connector') {
                        self._openReviewDialog(data);
                    } else {
                        self._postData(data);
                    }
				});		

			//formulář obsazenosti
			self._elementOccupancyForm
				.bind('occupancyChanged', function(event, data){
					//změna obsazenosti - předáme info do pricelistu a orderFormu
					self._elementPriceList.priceList('setOccupancy', data.adultCount, data.childCount, data.childAges);
					self._elementOrderForm.orderForm('setOccupancy', data.adultCount, data.childCount, data.childAges);
				})
				//zavoláme až po bindu occupancyChanged, díky tomu se po načtení ihned aktualizují správně seznamy
				.occupancyForm();

			//sbalování / rozbalování objednávky
			self._elementOrderFormToggler.click(function () {
				self._elementOrderForm.slideToggle();
			});

			//vytváření smlouvy
			self._elementButtonContract.click(function () {
				self._createCsContract();
			});

			//odeslání kalkulace
			self._elementButtonCalculation.click(function(event){
				event.preventDefault();
				self._sendPriceListByEmail(true);
			});

			//odeslání ceníku
			self._elementButtonPriceList.click(function(event){
				event.preventDefault();
				self._sendPriceListByEmail(false);
			});
			
		},

        /**
         * Odešle data na server k uložení
         * @private
         */
        _postData : function (data) {
            var self = this,
                options = self.options;

            self._showOverlay();

            $.ajax({
                type: "POST",
                url: options.savelUrl + '/' + data.dateId,
                dataType : 'json',
                data : JSON.stringify(data)
            })
            .done(function(response){
                if (self._hideForm == true) {
                    self._hideBookingForm();
                }
                self._handleOrderResponse(response);
            })
            .fail(function(jqXHR, textStatus, errorThrown){
                self._handleOrderResponseError(cesys.ts('Server error occurred while saving the order') + ': ' + textStatus + '. ' + cesys.ts('Please try again.'));
            })
            .always(function(){
                self._hideOverlay();
            });
        },

        _openReviewDialog : function (data) {
            var self = this,
                element = self.element;

            if ($('#reviewDialogWindow').length == 0) {
                var dialog = $('<div id="reviewDialogWindow"></div>');
                element.append(dialog);
            }

            var reviewDialog = $('#reviewDialogWindow');
            self._fillReviewData(reviewDialog, data);
            reviewDialog.dialog({
                title: cesys.ts('Review order'),
                modal: true,
                buttons: [
                    {
                        text: cesys.ts('Edit'),
                        click: function () {
                            $(this).dialog('close');
                        }
                    },
                    {
                        text: cesys.ts('Send'),
                        click : function () {
                            $(this).dialog('close');
                            self._postData(data);
                        }
                    }
                ]
            });
        },

        /**
         * Vyplnění dialogového okna
         * @param dialog
         * @param data
         * @private
         */
        _fillReviewData : function (dialog, data) {

            // Informace o objednavateli
            html = '<strong>' + cesys.ts('Firstname') + ':</strong> ' + data.firstName + '<br \>';
            html += '<strong>' + cesys.ts('Lastname') + ':</strong> ' + data.lastName + '<br \>';
            html += '<strong>' + cesys.ts('Street') + ':</strong> ' + data.street + '<br \>';
            html += '<strong>' + cesys.ts('City') + ':</strong> ' + data.city + '<br \>';
            html += '<strong>' + cesys.ts('Post Code') + ':</strong> ' + data.postCode + '<br \>';
            html += '<strong>' + cesys.ts('Country') + ':</strong> ' + data.country + '<br \>';
            html += '<strong>' + cesys.ts('Phone') + ':</strong> ' + data.phone + '<br \>';
            html += '<strong>' + cesys.ts('E-mail') + ':</strong> ' + data.email + '<br \>';
            html += '<strong>' + cesys.ts('Insurance') + ':</strong> ' + data.insurance + '<br \>';

            // Účastníci
            html += '<br \><strong>' + cesys.ts('Participants') + ':</strong><br \>';
            $.each(data.participants, function(k, participant){
                var participantNumber = k+1;
                html += participantNumber + '. ' + cesys.ts('Participant') + ': ' + participant.firstName + ' ' + participant.lastName + ' (' + participant.birth + ')<br \>';
            });

            dialog.html(html);
        },

		/**
		 * Nastavení vráceného stavu objednávky
		 * @param array data
		 */
		_handleOrderResponse : function (response) {
			var self = this;

			if (response) {
				//vrátil se objekt s odpovědí serveru, zpracujeme podle úspěchu/neúspěchu
				if (response.success) {
					self._handleOrderResponseSuccess(response.message, response.data, response.redirect);
				}else{
					self._handleOrderResponseError(response.message, response.data);
				}
			}else{
				//nevrátily se  žádná data, vypíšeme obecnou lášku o chybě serveru
				self._handleOrderResponseError(cesys.ts('Server error occurred while saving the order') + '. ' + cesys.ts('Please try again.'));
			}

			//scrollovat k výsledku
			self._elementBookingResult.show('fast', function(){
				var topOffset = self._elementBookingResult.offset().top;
				if (topOffset){
					$('html, body').animate({
					    scrollTop: topOffset - 50
					}, 400);
				}
			});
		},

		/**
		 * Nastavení chybového stavu
		 * @param string status Textový message
		 * @param array  data   Dodatečná data chyby
		 */
		_handleOrderResponseError : function (message, data) {
			var self = this;
			self._elementBookingResult
				.empty()
				.removeClass('success')
				.addClass('error')
				.append('<h3>' + message + '</h3>');

			//jsou li data, projdeme a vypiseme
			if (typeof data == "array" || (typeof data == "object" && data != null)) {
				var ul = $('<ul>').appendTo(self._elementBookingResult);
				$.each(data, function(index, error){
					ul.append('<li>' + error + '</li>');
				});
			}

		},

		/**
		 * Nastavení úspěšného stavu vytváření objednávky
		 * @param string message Textová zpráva
		 * @param array  data   Dodatečná data - u úspěšného stavu zatím nepoužité
		 * @param array  redirect   Odkaz k editaci poptávky
		 */
		_handleOrderResponseSuccess : function(message, data, redirect) {
			var self = this;
			var buttonText = cesys.ts('Go to local booking edit');

			if (data !== null && typeof data.buttonText !== "undefined") {
                buttonText = data.buttonText
			}
			//zobrazime info message
			self._elementBookingResult
				.empty()
				.removeClass('error')
				.addClass('success')
				.append('<h4>' + message + '</h4>');

			if (redirect){
				var paragraph = $('<p>');
				var button = $('<a>', {
					'class' : 'btn btn-blue btn-small',
					href : redirect,
					target: self.options.linkTarget,
					html : buttonText
				});

				self._elementBookingResult.append(paragraph.append(button));
			}

			self._elementBookingResult.show('fast');
			self._elementOrderForm.get(0).reset();
			
		},

		/**
		 * Vytvoření smlouvy / evidence - s POSTem přesměruje na add formulář
		 */
		_createCsContract : function(){
			var self = this;
			
			var data = JSON.stringify(self._elementPriceList.priceList('getData'));
			var form = $('<form action="' + self._elementButtonContract.data('url') + '" method="post">' +
						  '<input type="text" name="priceList" value=\'' + data + '\' />' +
						  '</form>');
			
			form.appendTo($('body')).submit();

		},

		/**
		 * Odeslání ceníku mailem
		 * !! pozor na závislost na vnější funkci createEmailDialog() !!
		 * @param  {bool} type True = kalkulace, false = ceník
		 */
		_sendPriceListByEmail : function(type){
			var self = this;

			//typ mailu
			if (type) {
				type = 'calculation';
			}else{
				type = 'pricelist';
			}

			//sestavení dat k odeslání - ceník + obsazenost
			var content = {
				'priceList' : JSON.stringify(self._elementPriceList.priceList('getData')),
				'occupancy' : JSON.stringify(self._elementOccupancyForm.occupancyForm('getData'))
			}; 

			//odeslání vyplněného ceníku, id zájezdu a typu
			$.ajax('/admin/Searches/offer_to_email/' + self._dateId + '/' + type + '/' + self._dataSource, {
					'type' : 'POST', 
					'data' : content, 
					success : function(data){ 
						//vytvoření náhledového okna
						createEmailDialog(); 
						$('#email-dialog').html(data);
					}
				}
			);

		},		


		/**
		 * Přidá overlay při zpracování ajaxu
		 */
		_showOverlay : function(){
			var element = this.element;
			var overlay = $('<div class="overlay"></div>');
			overlay.append($('<img src="'+config.fileServer+'/img/loader/calendar.gif" title="'+cesys.ts('Loading ...')+'" alt="'+cesys.ts('Loading ...')+'">').attr('style', 'position: relative;top: 25%;max-height: 50%;max-width: 90%;'));
			element.append(overlay);
		},

		/**
		 * Odstraní overlay po dokončení ajaxu
		 */
		_hideOverlay : function(){
			var element = this.element;	
			$(element).find('> .overlay').fadeOut('fast', function(){
				$(this).remove();
			});
		},

        /**
         * Schová objednávkový formulář
         * @private
         */
        _hideBookingForm: function() {
            this.element.find('.order-form').fadeOut('fast', function () {
                $(this).remove();
            });
        }

	});

})(jQuery, window, document);

