$(document).ready(function(){

  	/**
  	 *	Skrývací alert messages
  	 */
  	 $(".sessionFlash.alert").alert();

	/**
	 * Disabled tlačítka 
	 */
	$('.btn.disabled, button[disabled="disabled"]').click(function(e){
		e.preventDefault();
	});

});