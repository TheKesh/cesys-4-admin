/**
 * Modul pro ukládání do localStorage
 *
 * @author Vojta Tůma<ja@vojta-tuma.cz>
 */

var storage = angular.module("LocalStorageModule",[]);

/**
 * Prefix klíčů, aby se nepřepisovali aktuální
 */
storage.value("prefix" ,"als_");


storage.service('LocalStorageService',[
	'prefix'
	,function(prefix){


	/**
	 * Předání objektů s localStorage od window
	 */
	localStorage = window.localStorage;


	//zjistí se jaký engine používat
	if(isLocalStorageSupported()){
		localStorage = window.localStorage;
	}else{
		console.log("LocalStorageService not supported.")
	}




	/**
	 * Zjišťuje zda je localStorage v prohlížeči podpoŕováno
	 * @return {[type]} [description]
	 */
	var isLocalStorageSupported = function(){
		try{
			return ('localStorage' in window && window['localStorage'] !== null)
		}catch(e){
			logger(e);
			return false;
		}
	};

	/**
	 * Logger pro exceptions
	 * @param  Exception e vyjímka
	 */
	var logger = function(e){
		console.log(e.message);
	};




	/**
	 * Vpíště proměnnou do localStorage
	 * @param  String key   klíč 
	 * @param  Object|String value hodnota
	 * @return boolean       podařilo se zapsat
	 */
	var setValue = function(key, value){
		if(angular.isObject(value)){
			value = angular.toJson(value);
		}

		try{
			localStorage.setItem(prefix + key, value);
		}catch(e){
			logger(e);
			return false;
		}
		return true;
	};

	/**
	 * Čte hodnotu z localStorage
	 * @param  String	 key klíč
	 * @return Object|String     přečtená hodnota
	 */
	var readValue = function(key){
		var value = localStorage.getItem(prefix + key);
		if(value.charAt(0) === "{" || value.charAt(0) === "["){
			return angular.fromJson(value);
		}else{
			return value;
		}
	};



	/**
	 * Jak se bude tvářit service na venek
	 */
	return {
		setValue : setValue
		,readValue : readValue
	};



}]);
//}).call(this);
