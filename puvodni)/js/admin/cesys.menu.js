$(document).ready(function(){

	//počáteční zobrazení aktivního menu bez animace
	$('.sub-navbar  ul.main-level.active').show(0,function(){
		ChangeContentPadding();				
	});

	/**
	 * Odsazování obsahu podle výšky horního menu
	 */
	function ChangeContentPadding(){	
		menuHeight = $('.topFix').height(); //ziskani vysky menu
		$('.main-content').stop(true).animate({'padding-top': menuHeight }, 'fast');
	}
	$(window).resize(function() {
		ChangeContentPadding();		
	});
	$(window).load(function() {
		ChangeContentPadding();		
	});

	
	/**
	 * Obsluha přepínání submenu dle výběru v hlavním menu
	 */
	$('#top-navbar .main-navbar-link[data-toggle], li.favourites-link > a, li.info-link > a').click(function(event){
		event.preventDefault();

		//přepnutí active na položce v hlavním menu nebo odkazu na oblíbené
		$('#top-navbar .nav > li, li.favourites-link, li.info-link').removeClass('active');
		$(this).parent().addClass('active');

		//id submenu, které náleží k položce
		var subnav = $(this).attr('data-toggle');
	
		//zastavení animací
		$('.sub-navbar  ul.main-level').stop(true);

		//pokud je nejake submenu zobrazene tak ho schovame a po dokonceni zobrazime jine submenu
		if ($('.sub-navbar  ul.main-level.active').length){		
			$('.sub-navbar  ul.main-level.active').removeClass('active').slideUp('fast', function(){
				$('#'+subnav).addClass('active').slideDown('fast', function(){
					ChangeContentPadding();				
				});
				
			});
		}
		//jinak rovnou zobrazime nove submenu
		else{
			$('#'+subnav).addClass('active').slideDown('fast', function(){
				ChangeContentPadding();		
			});	
		}		
	});


	/**
	 * Obsluha přepínání třídy .active na spodním menu
	 */
	$('#bottom-navbar .main-navbar-link[data-toggle]').click(function(event){
		event.preventDefault();

		// přepnutí active na položce ve spodním menu
		$('#bottom-navbar .nav > li').removeClass('active');
		$(this).parent().addClass('active');
	});
	// odstranění active při kliknutí mimo dropdown (zavření nabídky)
	$(window).click(function() {
		$('#bottom-navbar .nav > li').removeClass('active');
	});

	/**
	 * Zabránění zavření .dropdown-content spodního menu při kliknutí uvnitř
	 */
	$(document).on('click', '.bottom-navbar .dropdown-menu', function(e) {
	  e.stopPropagation();
	});
	
	/**
	 * Zobrazení/skrytí formuláře pro přidání úkolu
	 */
	$("#new-task-link").on("click", function(){
		$(".tasks-new").addClass("show");
	});
	$("#close-new-task-link").on("click", function(){
		$(".tasks-new").removeClass("show");
	});
	/**
	 * Zobrazení/skrytí formuláře pro přidání události
	 */
	$("#new-event-link").on("click", function(){
		$(".event-new").addClass("show");
	});
	$("#close-new-event-link").on("click", function(){
		$(".event-new").removeClass("show");
	});

	/**
	 * Zobrazení submenu třetí úrovně na dotyk u dotykovách zařizení
	 * U nedotykových přes css hover
	 */
	var is_touch_device = (('ontouchstart' in window) || (window.DocumentTouch && document instanceof DocumentTouch) ||  window.navigator.msMaxTouchPoints > 0);
	if (is_touch_device){
		//přidání třídy na body pro odlišné stylování
		$('body').addClass('touch-device');
		//přepínání dropdownů na touch události
		$('.sub-navbar .dropdown .submenu-link').bind('touchstart pointerdown mspointerdown', function(e) {
			//zavření otevřeného dropdownu kliknutím znovu na stejnou položku
			if ($(this).parent().hasClass('open')){
				$(this).parent().removeClass('open');
			}else{
				$('.sub-navbar .dropdown.open').removeClass('open');
				$(this).parent().addClass('open');
			}
	  	    //zastavení propagace události a defaultních akcí
	        e.preventDefault();
	        e.stopPropagation(); 
	    });
	    //skrytí všech dropdownů při dotyku mimo dropdwon a mimo submenu link
	    $('body').bind('touchstart pointerdown mspointerdown', function(e) {
	        $('.sub-navbar .dropdown').removeClass('open');
	    });
	    //při kliknutí na dropdown zastavit propagaci události k handleru na <body> - viz výše
	    $('.sub-navbar .dropdown-menu').bind('touchstart pointerdown mspointerdown', function(e) {
	        e.stopPropagation(); 
	    });
	}


	/**
	 * přepínání menu při scrollování
	 */
	$(window).scroll(function () {
		topScroll = $(document).scrollTop();
		 if (topScroll > 50){
	     	$('.main-navbar').addClass('collapsed');
	     }else{
	     	$('.main-navbar').removeClass('collapsed');
	     	ChangeContentPadding();
	     }
	});

});