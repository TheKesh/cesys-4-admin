$(document).ready(function(){

	/**
	 * Obsluha selectů pro nastavení počtu záznamů na stránku
	 */
	$('.list-limit-selector').each(function(){
		$(this).change(function(){
			window.location.href = $(this).attr('data-location')+'?set-limit='+$(this).val();
		});
	});

});