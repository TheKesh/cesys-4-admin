/**
* Kód pro ajaxové nahrávání codemirror v settings
*/


function ajaxSave(id,postData){
    if(!postData){
        postData = editor.getValue();
    }
    if(!checkTempDomain(postData)){
        return false;
    }
    
    var loader = $('<div>',{
        id: 'full-screen-loader'
    });
    $('body').append(loader);
    $.ajax('/admin/settings/ajax_save/' + id,{
        data : {
          "data[Setting][value_text]" : postData 
        },
        type : "POST",
        success: function(){
            $('#full-screen-loader').remove();
        }
    });
}

function checkTempDomain(data, alertMessage){
    if(!alertMessage){
        alertMessage = 'Nastavení obsahuje adresu na dočasnou doménu. Chcete pokračovat ?';
    }
    if(data.match(/mojcesys\.sk/) || data.match(/mujcesys\.cz/)){
        if(confirm(alertMessage)){
            return true;
        }
        return false;
    }
    return true;
}
