;(function ($, window, document, undefined) {
    $.widget('cesys.magicwareBooking', {

        options: { // výchozí hodnoty
            magicwareObjects: { // objekty MW API
                client: null,
                booking: null,
            },
            dynamicPackage: false, // zda se jedná o dynamický balíček
            username: false, // uživatelskí jméno
            password: false, // heslo
            productId: false, // id ubytování od MW
            termId: false, // id termínu od MW
            cesysDateId: false, // ID termínu
            dateFrom: false, // datum odjezdu
            dateTo: false, // datum návratu
            inumber: false, // ičo
            organizationNumber: false,
            adultCount: 2, // počet dospělých
            childCount: 0, // počet dětí
            childAges: [], // věky dětí
            customer: false, // info o objednavateli
            clientId: -1, // nevytvářet klienta
            businessCaseId: -1, // nevytvářet obchodní případ
            stationId: false // ID letiště z dat MW
        },

        build: function () {
            var self = this,
                element = self.element;

            self.options.magicwareObjects.booking
                .setUser(self.options.username, self.options.password)
                .setProductId(self.options.productId)
                .setStationId(self.options.stationId)
                .setOnLoadStart(self.onLoadStart)
                .setOnLoadEnd(self.onLoadEnd)
                .setOnLoadError(self.onLoadError)
                .setOnAuthException(self.onAuthException)
                .setOnUpdate(self.onUpdate)
                .setOnBooked(self.onBooked)
                .setTravelersCount(self.options.adultCount, self.options.childAges)
            ;

            // nastaveni čísla organizace pokud je nastaveno
            if (typeof self.options.organizationNumber === 'number' && self.options.organizationNumber > 1) {
                self.options.magicwareObjects.booking.setOrgNumber(self.options.organizationNumber);
            } else {
                self.options.magicwareObjects.booking.setIdentificationNumber(self.options.inumber);
            }

            if (self.options.customer !== false) {
                var birthDate = moment(self.options.customer.birthDate, 'DD.MM.YYYY');
                // předvyplneni objednavatele
                self.options.magicwareObjects.client
                    .setFirstName(self.options.customer.firstName)
                    .setLastName(self.options.customer.lastName)
                    .setEmail(self.options.customer.email)
                    .setStreet(self.options.customer.street)
                    .setZipCode(self.options.customer.postCode)
                    .setCity(self.options.customer.city)
                    .setMobile(self.options.customer.phone)
                    .setBirthDate(new Date(birthDate.toDate()))
                    // .setGender(MagicWare.Gender.Male)
                    // .setTelephone("654321")
                ;
                self.options.magicwareObjects.booking.setClientData(self.options.magicwareObjects.client);
            }

            // pokud není dynamický balíček, tak máme termId, jinak musíme zadat datum od-do
            if (self.options.dynamicPackage === false) {
                self.options.magicwareObjects.booking.setTermId(self.options.termId);
            } else {
                self.options.magicwareObjects.booking.setDates(self.options.dateFrom, self.options.dateTo);
            }

            self.options.magicwareObjects.booking.render('.' + self.element.attr('class'));

            // nevím jak jinak dostat informace do callback funkce "onBooked"
            window.config.cesysDateId = self.options.cesysDateId;
            window.config.customer = self.options.customer;
            window.config.adultCount = self.options.adultCount;
            window.config.childCount = self.options.childCount;
            window.config.elementClass = self.element.attr('class');
            window.config.businessCaseId = self.options.businessCaseId;
            window.config.clientId = self.options.clientId;

        },

        /**
         * nastavení mw bookovacího objektu
         * @param object
         */
        setMwBookingObject: function (object) {
            this.options.magicwareObjects.booking = object;
        },

        /**
         * nastavení mw objektu klienta
         * @param object
         */
        setMwClientObject: function (object) {
            this.options.magicwareObjects.client = object;
        },

        /**
         * hláška při obecné chybě na straně MW
         */
        onLoadError: function () {
            var errorMsg = cesys.ts('Term can not be booked. Please choose another term.'),
                elementForMessage = $('.' + window.config.elementClass);

            if (elementForMessage.find('.alert-danger').length === 0) {
                elementForMessage.append(
                    $('<div>', {
                        'class': 'alert alert-danger',
                        'html': errorMsg
                    })
                        .fadeIn()
                );
            }
        },

        /**
         * Chybová hláška pokud agentura nemá oprávnění k online rezervacím
         * @param msg
         */
        onAuthException: function(msg) {
            var elementForMessage = $('.' + window.config.elementClass)
                // , errorMsgRegex = /([\w\W]*) ([0-9]{6,}) ([\w\W]*)/.exec(msg)
            ;

            if (elementForMessage.find('.alert-danger').length === 0) {
                // errorMsg = cesys.ts(errorMsgRegex[1] + ' %i ' + errorMsgRegex[3]);
                // errorMsg = errorMsg.replace('%i', errorMsgRegex[2]);

                elementForMessage.append(
                    $('<div>', {
                        'class': 'alert alert-danger',
                        'html': msg
                    })
                        .fadeIn()
                );
            }
        },

        /**
         * akce při začátku nahrávání
         */
        onLoadStart: function () {
            $('.overlay-parent').append(
                $('<div>', {
                    'class': 'overlay'
                })
                    .append($('<img>', {
                        'src': config.fileServer + '/img/loader/calendar.gif'
                    }))
            );
        },

        /**
         * akce při konci nahrávání
         */
        onLoadEnd: function () {
            $('.overlay').remove();
        },

        /**
         * akce po aktualizaci
         * @param bookingForm
         */
        onUpdate: function (bookingForm) {
            var persons = bookingForm.getSummary().getPersons();

            // zaktualizujeme počty dospělých/dětí
            window.config.adultCount = persons.getAdultsCount();
            window.config.childCount = persons.getChildrenCount();
        },

        /**
         * akce po zabukování
         * @param bookingResult
         */
        onBooked: function (bookingResult) {
            // poskládám ceník
            var items = [];
            $.each(bookingResult.getItems(), function (k, bookingResultItem) {
                var item = { // jedna ceníková položka
                    'name': bookingResultItem.getName(),
                    'count': bookingResultItem.getCount(),
                    'unitPrice': bookingResultItem.getUnitPrice(),
                    'totalPrice': bookingResultItem.getTotalPrice(),
                    'discount': []
                };
                if (bookingResultItem.getDiscounts().length > 0) { // existují nějaké slevy
                    $.each(bookingResultItem.getDiscounts(), function (k, bookingResultItemDiscount) {
                        item.discount[item.discount.length] = { // sleva
                            'name': bookingResultItemDiscount.getName(),
                            'price': bookingResultItemDiscount.getPrice()
                        }
                    })
                }
                items[items.length] = item;
            });
            // odeslání rezervace k uložení
            $.ajax({
                'type': 'POST',
                'url': '/admin/RbBookings/save_mw',
                'dataType': 'json',
                'data': {
                    'expirationDate': moment(bookingResult.getExpirationDate()).format('YYYY-MM-DD HH:mm:ss'),
                    'reservationNumber': bookingResult.getNumber(),
                    'totalPrice': bookingResult.getTotalPrice(),
                    'adultCount': window.config.adultCount,
                    'childCount': window.config.childCount,
                    'currency': bookingResult.getCurrency(),
                    'items': items,
                    'cesysDateId': window.config.cesysDateId,
                    'customer': window.config.customer,
                    'businessCaseId': window.config.businessCaseId,
                    'clientId': window.config.clientId
                }
            }).success(function (data) {
                var messageDiv = $('<div>', {
                    'class': 'alert',
                    'html' : data.message
                });

                switch (data.code) {
                    case 200:
                        messageDiv.addClass('alert-success');
                        break;
                    case 500:
                        messageDiv.addClass('alert-danger');
                        break;
                    default:
                        messageDiv.addClass('alert-warning');
                        break;

                }
                $('.' + window.config.elementClass).append(
                    messageDiv.fadeIn()
                );
            }).error(function (data) {
                var messageDiv = $('<div>', {
                    'class': 'alert alert-danger',
                    'html' : data.message
                });

                $('.' + window.config.elementClass).append(
                    messageDiv.fadeIn()
                );
            });
        }

    });
})(jQuery, window, document);