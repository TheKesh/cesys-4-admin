/**
 * Reportování client-side chyb JavaScriptu
 *
 *	@author Martin Takáč
 */
window.onerror = function (msg, url, line)
{
	//	Adresa logovadla.
	var logger = 'http://' + location.host + '/js/errorlog.php';
	
	var params = 'msg=' + encodeURIComponent(msg) 
			+ '&file=' + encodeURIComponent(url) 
			+ '&line=' + encodeURIComponent(line)
			+ '&location=' + encodeURIComponent(window.location.href)
			;
	var xhr = null;

	//	Získání AJAX objektu.
	try {
		xhr = new XMLHttpRequest();
	}
	catch (e) {}

	try {
		xhr = new ActiveXObject('Msxml2.XMLHTTP.6.0');
	}
	catch (e) {}

	try {
		xhr = new ActiveXObject('Msxml2.XMLHTTP.3.0');
	}
	catch (e) {}

	try {
		xhr = new ActiveXObject('Msxml2.XMLHTTP');
	}
	catch (e) {}

	if (!xhr) {
		//	Posíláme přes image.
		var i = new Image;
		i.src = logger + "?" + params;
	}
	else {
		xhr.open('POST', logger, true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
//		xhr.setRequestHeader('Content-length', params.length);
//		xhr.setRequestHeader('Connection', 'close');
		xhr.send(params);
	}
	
	return true;
}
