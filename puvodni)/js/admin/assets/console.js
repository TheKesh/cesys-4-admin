		/**
		 *	Ošetření neexistence console.
		 *
		 *	@author Martin Takáč
		 */
		if (window.console == undefined || window.console.log == undefined) {
			console = $.extend(true, {}, { 
				log: function() {}
			});
		}

