/**
 * Za tabulku s atributem "data-js='select-all-checkboxes']" vloží tlačítka pro
 * zaškrtnutí nebo odškrtnutí všech checkboxů
 * Tlačítka jsou svázaná s jednou konkrétní tabulkou - může jich být více na stránce
 * @author Martin Takáč
 * @return void
 */
$(document).ready(function() {
	$("[data-js='select-all-checkboxes']").each(function() {
		var table = this;
		$(table).after($('<div/>',{
			'class': 'btn-group listCheckboxsSelector',
		})
			.append($('<a>', {
				text: 'Mindent kiválasztani',
				'class': 'btn btn-default btn-xs',
				click: function() {
						$("input[type=checkbox]", table).each(function() {
							this.checked = "checked";
						});
					}
				}))
	   		.append($('<a>', {
				text: 'Kiválasztás megszüntetése',
				'class': 'btn btn-default btn-xs',
				click: function() {
						$("input[type=checkbox]", table).each(function() {
							this.checked = "";
						});
					}
				}))
   		);

	});


});