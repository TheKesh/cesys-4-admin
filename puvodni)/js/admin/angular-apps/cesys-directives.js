var CesysDirectives = angular.module('CesysDirectives',[]);

CesysDirectives.directive('ajaxInput',function($http){
	
	//kód klávesy enter
	var ENTER_KEY_CODE = 13;


	var transformToNormalPost =  function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    }


	var saveInput = function($scope){
		if(angular.isUndefined($scope.value)){
			alert(cesys.ts('Please enter a value.'));
			return false;
		}


		var data = {};
		data[$scope.name] = $scope.value;
		$scope.loader = true;
		$http.post($scope.url
			,data
			,{
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				,transformRequest : transformToNormalPost  
			})
		.success(function(answer){
			console.log(answer);
			$scope.loader = false;
		})
		.error(function(answer){
			alert(answer);
			$scope.loader = false;
		})
	}

	var linkFn = function($scope, $element, attrs, controller)
	{
		
		$scope.loader = false;

		$scope.handleKeyUp = function($event){
			if($event.keyCode == ENTER_KEY_CODE){
				$event.preventDefault();
				saveInput($scope);
			}
		}

		$scope.showLoader = function(){

		}

	};

	var templ = '<div class="form-group">' +
					'<input ng-hide="loader" type="text" class="form-control size-{{size}}" ng-model="value" name="{{name}}" ng-keydown="handleKeyUp($event)" />' + 
					'<div class="loader-gif" ng-show="loader"></div>' +
				'</div>';

	return{
		restrict : 'E'
		,link : linkFn
		,scope:{url:'@' , name:'@' , size:'@' , value:'@'}
		,transclude: true
		,template : templ

	}
});