//inicializace celkového modulu
var NewsletterCampaign = angular.module("NewsletterCampaign",['ngRoute']);


//routování a šablony
NewsletterCampaign.config(function($routeProvider){
	$routeProvider.when('/recipients',{controller:'RecipientController', 'templateUrl' : '/admin/NewsletterCampaigns/ngtemplate/recipient'}); //1.krok
	$routeProvider.when('/preferences',{controller:'PreferencesController', 'templateUrl' : '/admin/NewsletterCampaigns/ngtemplate/preferences'}); //2.krok
	$routeProvider.when('/content',{controller:'ContentController', 'templateUrl' : '/admin/NewsletterCampaigns/ngtemplate/content'}); //3.krok
	$routeProvider.when('/attachments',{controller:'AttachmentsController', 'templateUrl' : '/admin/NewsletterCampaigns/ngtemplate/attachments'}); //4.krok
	$routeProvider.when('/summary',{controller:'SummaryController', 'templateUrl' : '/admin/NewsletterCampaigns/ngtemplate/summary'}); //5.krok
	$routeProvider.otherwise({redirectTo:'/recipients'}); //přesměrování při neexistujicím kroku
});