/**
 * Factory, která se stará o ukládání aktuálního stavu dat
 */
NewsletterCampaign.factory('stackService',function($location, navButtonService, $http, dataProvider){
	
	var Service = {};
	//nastavení aktuálního kroku, kde se nachází průvodce
	var _setStep = function(step){	
		if(step > Service.maxStep){
			Service.maxStep = step;
		}
		navButtonService.changeState(step, Service.maxStep);
	}

	//kontrola, zda je přechod na další krok povolen
	var _isStepAllowed = function(step)
	{

		if(step > Service.maxStep){
			$location.url('/');
		}else{
			return true;
		}
	}


	//úřechod na další krok, pokud je povolen
	var _setCurrentStep = function(step)
	{
		if(_isStepAllowed(step)){
			_setStep(step);
		}

	}

	//přesměruje průvodce do posledního povoleného kroku
	var _redirectoToMaxStep = function()
	{

		switch(Service.maxStep){
			case 1:
			case 2:
				$location.url("/preferences");
				break;
			case 4:
			case 3:
			case 5:
				$location.url("/content");
				break;
		}
	}


	//inicializace při prvním spuštění
	Service.firstInit = function()
	{
		Service.subject = dataProvider.value_subject;
		Service.content = dataProvider.value_content;
		Service.senderEmail = dataProvider.value_sender_email;
		Service.backColor = dataProvider.value_back_color;
		Service.fontColor = dataProvider.value_font_color;
	}


		// ** Atributy, které je možné stacku nastavit ** ///

		//příjemci
		Service.groups = [];
		Service.addressArea = "";
		Service.filter = "crop";
		//předvolby
		Service.subject = "";
		Service.sender = "";
		Service.senderEmail = "";
		Service.name = "";
		//obsah
		Service.content = "";
		Service.backColor = "#ffffff";
		Service.fontColor = "#000000";
		//soubory
		Service.files = {};
		//globální věci
		Service.maxStep = 1;
		Service.isStepAllowed = _isStepAllowed;
		Service.setStep = _setStep;
		Service.setCurrentStep = _setCurrentStep;
		Service.redirectoToMaxStep= _redirectoToMaxStep;
		Service.reloaded = false;

		//čas odeslání
		Service.sendFrom = "";

		return Service;
	// 	}
});