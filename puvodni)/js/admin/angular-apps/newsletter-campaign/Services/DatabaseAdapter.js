

/**
 * Komunikace aplikace s CESYSem resp databází
 * -obstarává ukládání a nahrávání
 */
NewsletterCampaign.service('DatabaseAdapter', function($http, stackService, $location, $rootScope, flashMessageFactory, dataProvider){
	
	var campaignId = 0;

	//konstanty
	var SAVE_URL = "/admin/NewsletterCampaigns/ajax_save_from_wizzard/";
	var LOAD_URL = "/admin/NewsletterCampaigns/ajax_load_to_wizzard/";
	var TEST_URL = "/admin/NewsletterCampaigns/ajax_send_test_email/";

	//defaultní nastavení $http factory
	$http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
	

	//před odesláním newsletteru se obsah zprávy vyescapuje. Opačná akce se musí udělat v modelu na straně cake
	var escapeHtmlContent = function(content)
	{
		content = content.replace(/=/g,'\\##rovn##;');
		content = content.replace(/&/g,'\\##amp##;');
		content = content.replace(/\+/g,'\\##plus##;');
		return content;
	}


	//provede uložení obsahu stacku do databáze
	this.save = function(stack){
		console.log("save");
		var content = stack.content;
		//vyescapování potřebných dat
		stack.content = escapeHtmlContent(stack.content);
		stack.subject = escapeHtmlContent(stack.subject);
		stack.sender = escapeHtmlContent(stack.sender);
		$http.post(SAVE_URL + '/' + campaignId, "data=" + JSON.stringify(stack))
		.success(function(data,status){
			campaignId = data.id;
			$rootScope.campaignId = campaignId;
            dataProvider.countRecipients = data.countRecipients;
			flashMessageFactory.printOkFlash(dataProvider.text_saved);
		})
		.error(function(data,status){
			flashMessageFactory.printBadFlash(dataProvider.text_fail);
		});
		stack.content = content;
	}


	//odeslání testovacího e-mailu
	this.sendTest = function(stack, testAddress){
		var content = stack.content;
		stack.content = escapeHtmlContent(stack.content);
		stack.subject = escapeHtmlContent(stack.subject);
		stack.sender = escapeHtmlContent(stack.sender);
		$http.post(TEST_URL + '/' + campaignId, "data=" + JSON.stringify(stack) + "&address=" + testAddress)
		.success(function(data,status){
			flashMessageFactory.printOkFlash(dataProvider.text_sent);
		})
		.error(function(data,status){
			flashMessageFactory.printBadFlash(dataProvider.text_fail);
		});
		stack.content = content;
	}





	//nahraje newsletter z DB. Například při klonování
	this.load = function(id)
	{
		if(stackService.reloaded == false){
			campaignId = id;
			$http.get(LOAD_URL + "/" + id)
			.success(function(data,status){
				
				//uložení do stacku
				stackService.maxStep = Math.min(3,data.maxStep);
				stackService.addressArea = data.addressArea;
				stackService.backColor = data.backColor;
				stackService.content = data.content;
				stackService.name = data.name;
				stackService.filter = data.filter;
				stackService.fontColor = data.fontColor;
				stackService.sender = data.sender;
				stackService.senderEmail = data.senderEmail;
				stackService.subject = data.subject;
				stackService.groups = data.groups;
				stackService.reloaded = true;
				stackService.redirectoToMaxStep();
			})
		}
	}

	//vrací id aktuální kampaně
	this.getCampaignId = function()
	{
		return campaignId;
	}

	//vyresetuje id kampaně
	this.resetCampaignId = function()
	{
		campaignId = 0;
		return campaignId;
	}
});