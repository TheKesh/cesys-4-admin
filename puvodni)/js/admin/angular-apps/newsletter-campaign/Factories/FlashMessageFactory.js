/**
 * Vytváří flash message zprávy
 */
NewsletterCampaign.factory('flashMessageFactory', function(){
	
	var _printOkFlash = function(message)
	{
		var message = $('<a class="btn btn-green size-default disabled" style="margin-left:10px"></a>').html(message).insertAfter('button[name="submit"]');
		setTimeout(function(){
			message.fadeOut();
		},1000)
	}	

	var _printBadFlash = function(message)
	{
		var message = $('<a class="btn btn-red size-default disabled" style="margin-left:10px"></a>').html(message).insertAfter('button[name="submit"]');
		setTimeout(function(){
			message.fadeOut();
		},1000)
	}


	return {
		printOkFlash : _printOkFlash
		,printBadFlash : _printBadFlash
	};
})