/**
 * Service ovládá horní panel s navigací
 */
NewsletterCampaign.factory('navButtonService', function($rootScope){
	
	var Service = {};
	//třída vypnutí
	var DISABLE_CLASS = "disabled";
	//aktivní třída
	var ACTIVE_CLASS = "btn-green";

	//mění stavi navigačních tlačítek
	Service.changeState  = function(step, maxStep)
	{

		//aktivní
		$rootScope.button1 = ( step == 1)?ACTIVE_CLASS:''; 
		$rootScope.button2 = ( step == 2)?ACTIVE_CLASS:''; 
		$rootScope.button3 = ( step == 3)?ACTIVE_CLASS:''; 
		$rootScope.button4 = ( step == 4)?ACTIVE_CLASS:''; 
		$rootScope.button5 = ( step == 5)?ACTIVE_CLASS:'';
		//vypnuté
		$rootScope.button2 = ( maxStep < 2)?$rootScope.button2 + " " + DISABLE_CLASS:$rootScope.button2; 
		$rootScope.button3 = ( maxStep < 3)?$rootScope.button3 + " " + DISABLE_CLASS:$rootScope.button3; 
		$rootScope.button4 = ( maxStep < 4)?$rootScope.button4 + " " + DISABLE_CLASS:$rootScope.button4; 
		$rootScope.button5 = ( maxStep < 5)?$rootScope.button5 + " " + DISABLE_CLASS:$rootScope.button5; 
	}

	return Service;
});