/**
 * Kontroller schovává a skrývá přílohy
 */
NewsletterCampaign.controller("AttachmentsController",function($scope, stackService, $location, DatabaseAdapter, $rootScope){

	//další krok
	var NEXT_STEP_URL = "/summary";
	//id kroku
	var STEP_ID = 4;

	//potvrzení formuláře
	$scope.submit = function($event){
		$event.preventDefault();
		$('.secret-files').appendTo('.original-files-position');
		stackService.setStep(STEP_ID + 1);
		DatabaseAdapter.save(stackService);
		$location.url(NEXT_STEP_URL);
	}

	//změna adresy = uložení příloh 
	$scope.$on('$locationChangeStart',function(){
		$('.secret-files').appendTo('.original-files-position');
	});

	//spouští se vždy na začátku
	var onInit = function(){
		//skování příloh, ty se použijí až v posledním kroku formuláře
		$('.secret-files').appendTo('.temp-attachments');
		stackService.setCurrentStep(STEP_ID);
	};onInit();
});