/**
 * Kontroller se stará o položku příjemci
 */
NewsletterCampaign.controller("RecipientController",function($scope, stackService, $location, validatorService, DatabaseAdapter, dataProvider){
	
	//počet skupin
	var GROUPS_COUNT = 4;
	//kam dál přesměrovat
	var NEXT_STEP_URL = '/preferences';
	//id kroku
	var STEP_ID = 1;

	/**
	 * Submit formuláře
	 */
	$scope.submit = function($event){
		$event.preventDefault();
		stackService.groups = getGroups();
		stackService.addressArea = $scope.addressArea;
		stackService.filter = $scope.filter;
		//validace emailů a skupin
		if(validatorService.multipleEmails(stackService.addressArea)){
			if( ($scope.filter == "crop") && areGroupsEmpty()  && (stackService.addressArea.length == 0)){
				alert(dataProvider.alert_NoAddresses);
			}else{
				stackService.setStep(STEP_ID + 1);
				$location.url(NEXT_STEP_URL);
			}
		}
	}


	//změna vybrání adres. Když jsou vybrány všechny adresy, tak se zneaktivní pole pro výběr skupin a textarea
	$scope.radioChanged = function(state)
	{

		if(state == "all"){
			$('.step-1 select').attr('disabled','disabled');
			$('.step-1 textarea').attr('disabled','disabled');
		}else{
			$('.step-1 select').removeAttr('disabled');
			$('.step-1 textarea').removeAttr('disabled');
		}
	}


	/**
	 * Nastavuje skupiny podle stackService
	 */
	var setGroups = function()
	{
		if(stackService.groups.length == GROUPS_COUNT){
			$scope.group1 = stackService.groups[0];
			$scope.group2 = stackService.groups[1];
			$scope.group3 = stackService.groups[2];
			$scope.group4 = stackService.groups[3];
			return true;
		}
		return false;
	}


	//kontrola, zda jsou všechny polo pro skupiny prázdné
	var areGroupsEmpty = function ()
	{	
		if(
			$scope.group1 == null
			&& $scope.group2 == null
			&& $scope.group3 == null
			&& $scope.group4 == null
			){
			return true;
		}
		return false;
	}
	

	/**
	 * Vrací pole zvolených skupin
	 */
	var getGroups = function(){
		return [$scope.group1, $scope.group2, $scope.group3, $scope.group4];
	}

	//init nastavení
	var onInit = function(){
		if(dataProvider.value_campaign_id > 0){ 
			DatabaseAdapter.load(dataProvider.value_campaign_id);
		}else if(dataProvider.value_duplicate_id > 0){ //jedná se klonování newsletteru
			DatabaseAdapter.load(dataProvider.value_duplicate_id);
			DatabaseAdapter.resetCampaignId();
		}else if(stackService.maxStep == 1){ //vytváření nového newsletteru
			stackService.firstInit();
		}
		setGroups(); //nastavení hodnot do selectů skupin
		$scope.filter = stackService.filter;
		$scope.radioChanged($scope.filter);
		$scope.addressArea = stackService.addressArea;
		stackService.setCurrentStep(STEP_ID);
	};onInit();

});


