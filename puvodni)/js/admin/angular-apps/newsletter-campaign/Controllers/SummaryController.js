/**
 * Shrnutí newsletteru
 */
NewsletterCampaign.controller("SummaryController",function($scope, stackService, $location, dataProvider ,DatabaseAdapter, validatorService, dataProvider){
	
	//id kroku
	var STEP_ID = 5;

	var firstInit = true;

	//vrací seznam příloh jako string
	var getAttachments = function(){
		var files = [];
		angular.forEach(stackService.files, function(value,key){
			files.push(value);
		});
		return files.join(", ");
	}

	var getRecipients = function(){
		if(stackService.filter == "all"){
			return dataProvider.text_AllEmail;
		}
		var groups  = "";
		angular.forEach(stackService.groups, function(value, key){
			if(typeof value != "undefined" && value){
				groups += dataProvider.groups[value] + ", ";
			}	
		});
		return groups + stackService.addressArea;
	}

	$scope.saveStack = function($event)
	{
		$event.preventDefault();
		DatabaseAdapter.save(stackService);
	}


	//změna adresy = uložení příloh 
	$scope.$on('$locationChangeStart',function(){
		stackService.files = $scope.param;
		$('.attachments-hidden-inner').appendTo('.attachments-hidden');
		$('.secret-files').removeClass('hidden');
	});

	$scope.submit = function($event)
	{
        var countEmails = dataProvider.sent_emails+dataProvider.countRecipients;
        if (countEmails > dataProvider.max_free_emails) {
            if (validatorService.question(cesys.ts('This newsletter will be paid. Do you really want to send?')) === false) {
                $event.preventDefault();
            }
        }
		//kontrola odesílacích časů
		if(angular.isUndefined($scope.sendFromDate)){
			validatorService.fault(cesys.ts('Send date can not be empty.'));
			$event.preventDefault();
			return false;
		}else if(angular.isUndefined($scope.sendFromHour)){
			validatorService.fault(cesys.ts('Send time can not be empty.'));
			$event.preventDefault();
			return false;
		}
	}


	var onInit = function(){
		$scope.subject = stackService.subject;
		$scope.content = stackService.content;
		$scope.sender = stackService.sender + " <" + stackService.senderEmail + ">";
		$scope.attachments = getAttachments();
		$scope.recipients = getRecipients();
		$scope.sendType = 'now';

		$scope.sendFromDate = dataProvider.value_current_date;
		$scope.sendFromHour = dataProvider.value_current_hour;

		$('.attachments-hidden-inner').appendTo('.final-form');
		$('.secret-files').addClass('hidden');
		stackService.setCurrentStep(STEP_ID);

		if(firstInit){
			console.log("první inicializace");
			firstInit = false;
		}else{
			console.log("ukládám v pátem kroku");
			DatabaseAdapter.save(stackService);
		}
		$("input[class^='datepicker']").datepicker({
			minDate : 0
			,maxDate : "+10d"
		});
		
	};onInit();

});