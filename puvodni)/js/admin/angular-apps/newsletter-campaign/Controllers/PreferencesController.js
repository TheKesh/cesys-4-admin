/**
 * Kontroller se stará o nastavení předvolebe
 */
NewsletterCampaign.controller("PreferencesController",function($scope, stackService, $location, validatorService, dataProvider, DatabaseAdapter){
	
	//url dalšího kroku
	var NEXT_STEP_URL = "/content";
	//id kroku
	var STEP_ID = 2;

	$scope.submit = function($event){
		$event.preventDefault();
		updateStack();
		if(validate()){
			stackService.setStep(STEP_ID + 1);
			$location.url(NEXT_STEP_URL);
		}
	}
	//uložení aktuální nastavení newsletteru
	$scope.saveStack = function($event)
	{
		$event.preventDefault();
		updateStack();
		if(validate()){
			DatabaseAdapter.save(stackService);
		}
	}

	//validace obsahu před uložením
	var validate = function()
	{
		return validatorService.notEmpty(dataProvider.name_subject, stackService.subject)
			&& validatorService.email(stackService.senderEmail);
	}

	//aktualizuje stack o informace odesílatel, jméno odesílatele, odesílatelův e-mail
	var updateStack = function()
	{
		stackService.subject = $scope.subject;
		stackService.sender = $scope.sender;
		stackService.senderEmail = $scope.senderEmail;
		stackService.name = ($scope.name.length != 0)?$scope.name:$scope.subject;
	}

	//spouští se při inicializace controlleru
	var onInit = function(){
		$scope.subject = stackService.subject;
		$scope.sender = stackService.sender;
		$scope.senderEmail = stackService.senderEmail;
		$scope.name = stackService.name;
		stackService.setCurrentStep(STEP_ID);
	};onInit();
});