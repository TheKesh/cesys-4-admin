/**
 * Kontroller se stará o obsah zprávy
 */
NewsletterCampaign.controller("ContentController",function($scope, stackService, $location, validatorService, dataProvider, DatabaseAdapter){
	
	//další krok
	var NEXT_STEP_URL = "/attachments";
	//id kroku
	var STEP_ID = 3;
	
	//potvrzení formuláře
	$scope.submit = function($event){
		$event.preventDefault();
		updateStack();
		if(validate()){
			stackService.setStep(STEP_ID + 1);
			$location.url(NEXT_STEP_URL);
		}
	}
	//zobrazení panelu s barvami
	$scope.showColors = function($event)
	{
		$event.preventDefault();
		$('#colorPanel').toggle();
		$('#NewsletterBgColor').keyup();
		$('#NewsletterFontColor').keyup();

	}

	//nastavení barev
	function setColors(){
	    var editor = $('#NewsletterContent').tinymce(); // získání instance tinyMCE
	    $(editor.getBody()).css('background-color',$('#NewsletterBgColor').val()); // obarvení body iframu tinyMCE
	    $(editor.getBody()).css('color',$('#NewsletterFontColor').val()); // color pro body iframu tinyMCE
	}

	//aktualizace z DOM do Angularu z věcí, které neřídí angular
	var updateStack = function()
	{
		stackService.testEmail = $scope.testEmail;
		stackService.content = $('#NewsletterContent').tinymce().getContent();
		stackService.backColor = $('#NewsletterBgColor').val()
		stackService.fontColor = $('#NewsletterFontColor').val()
	}

	//uložení aktuálního nastavení newsletteru
	$scope.saveStack = function($event)
	{
		$event.preventDefault();
		updateStack();
		DatabaseAdapter.save(stackService);
	}

	//validace, zda není obsah zprávy prázdný
	var validate = function()
	{
		return validatorService.notEmpty(dataProvider.name_content, stackService.content)
	}

	//kliknutí na tlačítko odeslat testovací zprávu
	$scope.sendTest = function($event)
	{
		updateStack();
		DatabaseAdapter.sendTest(stackService, $scope.testEmail);
		$event.preventDefault();
	}

	//inicializace
	var onInit = function(){
		$scope.content = stackService.content;
		$scope.testEmail = stackService.testEmail;
		$scope.backColor = stackService.backColor;
		$scope.fontColor = stackService.fontColor;
		$scope.testEmail = dataProvider.value_sender_email;
		//color pickery
		$('#colorpicker1').farbtastic('#NewsletterBgColor');
		$('#colorpicker2').farbtastic('#NewsletterFontColor');
		$('#color_update').click(function(){setColors()});
		$('#colorPanel').hide();
		stackService.setCurrentStep(STEP_ID);
	};onInit();
});