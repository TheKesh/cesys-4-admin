/**
 * Dialog pro pridavani lidi do newsltteru z ruznych mist
 *
 * @author Vojta Tůma <ja@vojta-tuma.cz>
 *
 * ** MARKDOWN comment --start--
 *  K čemu slouží?
 * ---------------
 * Direktiva zobrazí tlačítko, pomocí kterého se otevře dialog, kde jsou předvyplněné údaje podle atributů.
 *
 *  = přidávání kontaktů do newsletteru odkudkoliv
 * 
 * Jak se používá?
 * ---------------
 * Kamkoliv v administraci stačí napsat:
 *
 *    <add-newsletter-dialog firstname="Vojta" surname="Tůma" email="vt@darkmay.cz" />
 *
 * Předvyplněné hodnoty se propíší do dialogového okna.
 *
 * Nastavení
 * ----------
 * - DIALOG_ID - název jQuery selectoru, ze ktérého elementu se má vytvořit dialog
 * - POST_URL - kam se bude post požadavek odesílat
 * - GET_EXISTS_URL - na jaké adrese se zjišťuje jestli kontakt existuje 
 * 
 * ** MARKDOWN comment --end--
 */

var NewsletterDialog = angular.module('NewsletterDialog',[]);

// nastavení defaultního chování http, aby se tvářil jako Ajax request
NewsletterDialog.config(function($httpProvider){
	$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
})

//definice direktivy dialogu a tlačítek pro jeho otevření
NewsletterDialog.directive('addNewsletterDialog',function($http){
	return{
		restrict : 'E' //jedná se o element <add-newsletter-dialog>
		,transclude : true
		,scope : {surname: '@', firstname: '@', email : '@', sex : '@', vocative : '@', div: '@'} //parametry předané z atributů elementů
		,replace : true
		,link: function($scope, $element){
			//id elementu, ze kterého seb ude tvořit dialog
			var DIALOG_ID = '#' + $scope.div;
			//url kam se bude odesílat post
			var POST_URL = '/admin/NewsletterContacts/ajax_add';

			//url kde se dotazuje na existujicí kontakt
			var GET_EXISTS_URL = '/admin/NewsletterContacts/check_contact_exists/';

			//class for dialog to be hidden
			$scope.dialogOpen = "hidden";

			//počáteční nastavení zobrazení tlačítek
			$scope.createButton = "";
			$scope.existsButton = "hidden";

			$scope.groups = {};

			//otevření dialogu
			$scope.open = function()
			{
				$scope.dialogOpen = "";
				$(DIALOG_ID).dialog({'title' : cesys.ts('Add address to newsletter'), width: '430', height: 'auto'});
			}

			//kliknutí na vytvoření kontaktu
			$scope.createContact = function(event)
			{
				event.preventDefault();
				//odesláné požadavku na vytvoření kontaktu
				$http.post(POST_URL,{
					'firstname' : $scope.firstname
					,'surname' : $scope.surname
					,'email' : $scope.email
					,'sex' : $scope.sex
					,'vocative' : $scope.vocative
					,'groups' : $scope.groups
				})
				.success(function(data){
					$(DIALOG_ID).dialog('close');
					$('<div>' + data + '</div>').dialog({
						buttons : {
							"OK" : function(){$(this).dialog("close")}
						}
					});
					$scope.createButton = "hidden";
					$scope.existsButton = "";
				})
				.error(function(data){
					$('<div>' + data + '</div>').dialog({
						buttons : {
							"OK" : function(){$(this).dialog("close")}
						}
					});
				})

			}
			//zkontroluje se existence kontaktu. Pokud kontakt existuje, nezobrazí se tlačítko pro zobrazení dialogu
			var onInit = function()
			{
				$scope.DIALOG_ID = $scope.div;//odstraním # z id
				$http.get(GET_EXISTS_URL + $scope.email)
				.success(function(data){
					if(data.exists){
						$scope.createButton = "hidden";
						$scope.existsButton = "";
					}else{
						$scope.createButton = "";
						$scope.existsButton = "hidden";
					}
				});
			};onInit();

		}
		//adresa templatu, odkud se bude stahovat
		,templateUrl: '/admin/NewsletterContacts/ajax_add_dialog_template/'
			
	}
});