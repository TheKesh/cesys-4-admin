$(document).ready(function(){

    /**
     * Po reloadu se input pro heslo renderuje jako password - checkbox musíme odškrtnout, aby souhlasil stav s inputem
     */
    $('#UserViewPassword').prop('checked', false);

    /**
     * Login - zobrazení hesla
     */
    $('#UserViewPassword').change(function(){
    	//kvuli restrikci na zmenu atributu type v IE je treba vymenit cely input
    	if ($('#UserPassword').attr('type') == 'password'){
    		originalInput = $('#UserPassword');
    		newInput = $('<input />',{
    					'class': $(originalInput).attr('class'),
    					'id': $(originalInput).attr('id'),
    					'name': $(originalInput).attr('name'),
    					'type':'text'
    		});
    		$(originalInput).replaceWith(newInput);
    	}
    	//typ text - menime na password a zachovame value
    	else{

    		originalInput = $('#UserPassword');
    		newInput = $('<input />',{
    					'class': $(originalInput).attr('class'),
    					'id': $(originalInput).attr('id'),
    					'name': $(originalInput).attr('name'),
    					'type':'password'							
    		});
    		value = $(originalInput).val();
    		$(originalInput).replaceWith(newInput);
    		$(newInput).val(value);
    	}
    });

    /**
     * Testování stisknutého caps locku
     */
    function isCapslock(e){
        kc = e.keyCode?e.keyCode:e.which;
         sk = e.shiftKey?e.shiftKey:((kc == 16)?true:false);
         if(((kc >= 65 && kc <= 90) && !sk)||((kc >= 97 && kc <= 122) && sk))
            return true;
         else
            return false;
    }

    /**
    * Login - upozornění na caps lock
    */
    $('#UserPassword').keypress(function(e){
        if (isCapslock(e)){
            if (!$('.password-group').next().hasClass('in')){
                $('.password-group').popover('show');
            }
        }else{
            $('.password-group').popover('hide');
        }
    });

    $('.password-group').focusout(function(e){
        $('.password-group').popover('hide');
    });

});