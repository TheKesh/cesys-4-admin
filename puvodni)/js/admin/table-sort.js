$(document).ready(function() {
	//šipky pro řazení tabulek v administrace
	$('th a[href*="direction"]').each(function () {
		//šipky nejsou u hvězdičky
		if ($(this).attr('href').indexOf('star') == -1) {
			if (
				$(this).attr('href').indexOf('.' + $('#orderspan .name').html() + '/') !== -1
				|| $(this).attr('href').indexOf(':' + $('#orderspan .name').html() + '/') !== -1
			) {
				if ($('#orderspan .direction').html() == 'asc') {					
					$(this).html($(this).html() + '&nbsp;<span class="glyphicon glyphicon glyphicon-sort-by-attributes"></span>');
				} else {
					$(this).html($(this).html() + '&nbsp;<span class="glyphicon glyphicon-sort-by-attributes-alt"></span>');
				}
			} else {
				$(this).html($(this).html() + '&nbsp;<span style="color: Silver" class="glyphicon glyphicon-sort"></span>');
			}
		}
	});
});