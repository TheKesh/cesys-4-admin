/**
 * Globální funkce pro celý CeSYS
 * Funkce specifické pro jednotlivé sekce raději do jednotlivých složek a includovat jen když je potřeba
 */

$(document).ready(function() {
    //použití zastaralého kalendáře
    //není jasný výskyt - možná v remote bookingu, raději zachováme
    $('#OfferDateFrom').calendar({firstDay: 1, changeFirstDay: false, changeMonth: false, changeYear: false, dateFormat: 'YMD-'});
    $('#OfferDateTo').calendar({firstDay: 1, changeFirstDay: false, changeMonth: false, changeYear: false, dateFormat: 'YMD-'});
    $('#OfferValidityTo').calendar({firstDay: 1, changeFirstDay: false, changeMonth: false, changeYear: false, dateFormat: 'YMD-'});
    
    //inicializace selektů ve správě obsahu a dalších
    //TODO: inicializace pro TOMa do vlastního souoru
    $('#ContentMenuTableName').selectCombo('/admin/content_menus/js_table_list','#ContentMenuTableId', {hidetarget: false});
    $('#ContentContentColumnId').selectCombo('/admin/contents/js_element_list','#ContentContentElementId', {hidetarget: false});
    $('#ContentContentElementId').selectCombo('/admin/contents/js_element_id_list','#ContentElementId', {hidetarget: false});
    $('#ClTripCountryId').selectCombo('/admin/destinations/dilist','#ClTripDestinationId', {hidetarget: false});
    $('#ContentMenuContentActionId').selectCombo('/admin/content_menus/js_target_list','#ContentMenuTargetId', {hidetarget: false});
    $('#ContentSideboxContentActionId').selectCombo('/admin/content_sideboxes/js_target_list','#ContentSideboxTargetId', {hidetarget: false});
    $('#ContentBannerContentActionId').selectCombo('/admin/content_banners/js_target_list','#ContentBannerTargetId', {hidetarget: false});
    $('#FsImageContentActionId').selectCombo('/admin/FsImageLocations/js_target_list','#FsImageTargetId', {hidetarget: false});
    $('#FsImageLocationContentActionId').selectCombo('/admin/FsImageLocations/js_target_list','#FsImageLocationTargetId', {hidetarget: false});
    $('#MAccommodationCountryId').selectCombo('/admin/m_accommodations/js_destination_list','#MAccommodationDestinationId', {hidetarget: false});
    $('#AccommodationMasterCountryId').selectCombo('/admin/destinations/dilist/1','#AccommodationMasterDestinationId', {hidetarget: false});
    $('#MTripCountryId').selectCombo('/admin/m_accommodations/js_destination_list','#MTripDestinationId', {hidetarget: false});
    $('#MFeeCountryId').selectCombo('/admin/m_accommodations/js_destination_list','#MFeeDestinationId', {hidetarget: false});

    /*
     * Incializace slickboxu - skrávání/zobrazování libovolného obsahu obsahu
     * Obsah musí mít id slickbox, tlačítka pro přepínání odkazy s id slick-show/slick-hide
     */
    $('#slickbox').hide();
    $('a#slick-show').click(function() {
        $('#slickbox').show('slow');
        return false;
    });
    $('a#slick-hide').click(function() {
        $('#slickbox').hide('slow');
        return false;
    });

    /* 
     * Incializace tooltipu
     * @deprecated ??????????
     */
    $('.tooltip').tooltip();


    /*
     * Inicializace colorboxu
     */
    $("a[rel='colorbox']").colorbox({transition:"elastic", preloading:true});


	/**
	 *	Ajaxové odebírání omezení ve filtrovacích kritériících
	 */
	$('.liveaction').click((liveaction_cb = function () {
		var $parent = $(this).parent();
		$.ajax($(this).attr('href'))
			.done(function (data) {
				if (data == 'true'){
					$parent.hide(500, function(){
						$(this).remove();
						$(document).trigger('liveActionRemoveEvent');
					});
				}
				else {
					$parent.html(data);
					$parent.find('.liveaction').click(liveaction_cb);
				}
			}
		);
		return false;
	}));


	/**
	 *	Ajaxové přidávání omezení ve filtrovacích kritériích
	 */
	$('.liveform').submit(function() {
		var $parent = $(this).parent();
		$.post($(this).attr("action"), $(this).serialize(), function(data) {
			$parent.append(data);
			$parent.find('.liveaction').click(liveaction_cb);
		})
		return false;
	});


	/**
	 * Ajaxová změna parametru active
	 * Jako odpověď očekává pole v jsonu {"error":true, "message":"Záznam se nepodařilo načíst"}
	 * V error je info o úspěšnosti volání, v message chybová zpráva nebo text, který se má zobrazit po úspěšném přepnutí
	 */
	$('.change-bool-ajax').click(function(e){
		//reference na kliknutý odkaz
		var $link = $(this);

		$.ajax($(this).attr('href'))
			.done(function (data) {
				//parsování odpovědi
				$result = JSON.parse(data);

				//skrytí odkazu - po dokončení animace následuje zpracování odpovědi
				$link.fadeOut('fast', function(){
					//při chybové odpovědi nahraď odkaz textem s návratovou zprávou
					if ($result.error === true){
						$link.replaceWith(
							$('<span>', {
								'class'	: 'change-active-ajax-error',
								'text'	: $result.message
							})
						);

					}
					//jinak změňí text odkazu na zprávu předanou v odpovědi 
					else{
						$link.text($result.message);
					}
				});

				//opětovné zobrazení objektu
				$link.fadeIn('fast');
			}
		);
		return false;
	});


	/**
	 *  Inicializace datepickerů 
	 */
	$('.datepicker').datepicker(); // obyčejný datepicker
	$('.datepicker-today').datepicker({minDate:0}); // nejdou zadat starší datumy než ten dnešní
	$('.datepicker-three-months').datepicker({numberOfMonths: 3}); //datepicker se třemi měsíci
	
	/**
	 * Select s filtrem
	 */
	$('.select2').select2();
	
	//den v tydnu a odpocet dni
	$('span.countdownFromNowWithDayOfWeek').each(function () {
		$(this).html(
			moment($(this).attr('rel')).format('dddd')
			+ ', '
			+ moment($(this).attr('rel')).startOf('day').fromNow()
		);
	});
	
	//nastavení pro toasty	
	toastr.options = {
		"closeButton": true,
		"positionClass": "toast-top-center",
		"timeOut": "3000"
	}
});
