/**
 * Grid zobrazující výsledky výhledávání, ale také je napojen na seznam z axelerátoru.
 *
 * @author Martin Takáč
 */
$(function() {


		var tabswidget = $( "#tabs" );

		//fronta na ajaxové ověřování dostupnosti
		var front = null;

        // Uložení kódů pro traffics
        var trafficsDates = [];

		/**
		 *	Grid
		 */
		$("#grid").jqGrid({
			//	Obsah přejímá z nastavení formuláře.
//			datatype: "JSON",
			datatype: "local",
			/*mtype:'POST',*/
			colNames:[
				'&nbsp;',
				cesys.ts('Ava'),
				cesys.ts('ID'),
				cesys.ts('TO'),
				cesys.ts('Type'),
				cesys.ts('Country'),
				cesys.ts('Destination'),
				cesys.ts('Hotel'),
				cesys.ts('Cesys rating //SHORT//'),
				cesys.ts('Date'),
				cesys.ts('Duration'),
				cesys.ts('Boarding'),
				cesys.ts('Transport'),
				cesys.ts('Airport //SHORT//') + ' / ' + cesys.ts('Departure //SHORT//'),
				cesys.ts('Pokoje'),
				cesys.ts('Discount'),
				cesys.ts('Price')],
			colModel:[
				{name:'select', sortable: false, width:18 },
				{name:'ava', sortable: false, width:30 },
				{name:'id',index:'HeapSearch.id', width:55, align: 'right'},
				{name:'tour_operator_id',index:'HeapSearch.tour_operator_id', width:85},
				{name:'trip_type_id',index:'HeapSearch.trip_type_id', width:40, align: 'center'},
				{name:'country_id',index:'HeapSearch.country_id', width:85},
				{name:'destination_id',index:'HeapSearch.destination_id', width:90},
				{name:'accommodation_master_id',index:'HeapSearch.accommodation_master_id', width:180},
				{name:'overall_rating', sortable: false, width: 40, align: 'center'},
				{name:'date_from',index:'HeapSearch.date_from', width:65, align: 'center'},
				{name:'duration',index:'HeapSearch.duration', width:40},
				{name:'boarding_id',index:'HeapSearch.boarding_id', width:40, align: 'center'},
				{name:'transport_id',index:'HeapSearch.transport_id', width:42, align: 'center'},
				{name:'airport_id',index:'HeapSearch.airport_id', width:40, align: 'center'},
				{name:'rooms',sortable: false, width:35}, 
				{name:'discount',index:'HeapSearch.discount', width:35, align: 'right'},
				{name:'price', index:'HeapSearch.price', width:85, align: 'right'}
			],
			useJSON: true,
			rowNum: 30,
			autowidth: false,
//			shrinkToFit: false,
            loadui: 'disable',
			width: tabswidget.width() - 10,
			height: "100%",
			rownumbers: false,
			rowList:[30,50,100],
			pager: jQuery('#pager'),
			sortname: 'HeapSearch.date_from',
			viewrecords: true,
			sortorder: "asc",
			caption: false,
            beforeRequest : function(){
                //loadovací overlay
                $('.overlay-parent').append(
                    $('<div>', {
                        'class': 'overlay'
                    })
                    .append($('<img>', {
                        'src': config.fileServer + '/img/loader/calendar.gif'
                    }))

                );
                $('#searches-error').remove();
            },
			loadComplete: function(data) {
                $('.overlay').remove();
				if (!data){
					return;
				}

				// vypsání chybových hlášek
				if (typeof data.error !== 'undefined' && data.error.length > 0) {
				    var errorHtml = '<ul>';
				    $.each(data.error, function (k, v) {
                        errorHtml = errorHtml + '<li>' + v + '</li>';
                    });
                    errorHtml = errorHtml + '</ul>';
				    $('#tabs-searchmasks').after(
				        $('<div>', {
				            'id': 'searches-error',
                            'class': 'alert alert-danger',
				            'html' : errorHtml
                            })
                            .fadeIn()
                    );
                }
				
				//zrušení fronty z předchozího kroku a vytvoření nové
				if (front) {
					front.destroy();					
				}
				front = new cesys.utils.frontx();

				controller = $("#grid").jqGrid('getGridParam', 'controller');

				//	Systém odhlásil uživatele.
				if (data.code && data.code == -1) {
                    window.location.href = "/admin";
				}
	
				//	Spustit frontu dotazů na dostupnost.
				var table = this;

                // pro trafficsu jiné výchozí hodnoty
                if (controller === 'TrafficsDates') {
                    maxGroupLength = 1;
                    checkOkStatus = false;
                    cesys.service.availability.configuration.url.check = '/online/availability/cesys/check_date_traffics.php';
                } else if (controller === 'YpsilonPackages') {
                    maxGroupLength = 1;
                    checkOkStatus = false;
                    cesys.service.availability.configuration.url.check = '/online/availability/cesys/check_date_ypsilon.php';
                } else {
                    maxGroupLength = 1;
                    checkOkStatus = false;
                    cesys.service.availability.configuration.url.check = '/online/availability/cesys/check_date.php';
                }

                //	Pouze ty, které jsou online.
                var frowsTmp = $(data.rows).filter(function(i, row) {
					try {
						return (row.hash != false);
					}
					catch (e) {
						return false;
					}
				});

                // rozdělíme termíny z tabulky do tzv skupin
                // termíny ve skupině se ověřují paralelně
                // jednotlivé skupiny běží sériově
                var frowsGroup = [];
                var groupCount = frowsGroup.length;
                frowsGroup[groupCount] = [];
                $.each(frowsTmp, function (k, data) {
                    if (typeof frowsGroup[groupCount] !=='undefined' && ((groupCount == 0 && frowsGroup[groupCount].length >= maxGroupLength) || groupCount > 0)) {
                        groupCount = groupCount+1;
                        frowsGroup[groupCount] = [];
                    }

                    var Row = function (data) {
                        this.checked = false;
                        this.status = '';

                        var self = this;

                        // metoda na online ověření
                        this.check = function (url) {

                            // Než začneme ověřovat přidáme obrázek
                            var img = $('<img />', {
                                'src': config.fileServer+'/img/availability/loading_small.gif',
                                'alt': cesys.ts('Ava'),
                                'css': { 'margin-top': '1px', 'position' : 'absolute' }
                            });
                            $('#' + data.id + ' td:nth-child(2)', table).append(img);

                            // element ceny
                            var priceElement = $('#' + data.id + ' td:nth-child(17) span.price');

                            // data pro GET požadavek na obsazenost
                            var getData = {
                                cid: data.id,
                                hash: data.hash,
                                lang: config.cesysLang,
                                cache: true,
                                code: typeof data.offerCode != 'undefined' ? data.offerCode : null,
                                countAdult: typeof data.countAdult != 'undefined' ? data.countAdult : null,
                                countChild: typeof data.countChild != 'undefined' ? data.countChild : null,
                                childAges: typeof data.childAges != 'undefined' ? data.childAges : null
                            };

                            // požadavek na stav termínu
                            $.get(url, getData, function(returnData) {

                                // obrázek kolečka
                                var img = $('#' + data.id + ' td:nth-child(2) img', table);

                                // rozlišíme o jaký stav se jedná
                                switch (returnData.status) {
                                    // stavy volno
                                    case 'OK':
                                        img.attr({
                                            'src': config.fileServer+'/img/availability/ok.svg',
                                            'title': cesys.ts('Volná kapacita'),
                                            'alt': cesys.ts('VOLNO')
                                            })
											.addClass('availability-small');
                                        var price_per_person = priceElement.html();
                                        if (typeof returnData.price_per_person != 'undefined') { // vrácená cena za osobu, tak jí načtu protože se mohla změnit
                                            price_per_person = returnData.price_per_person;
                                        }
                                        if (typeof returnData.total_price !== 'undefined') { // vrácena i celkévá cena ....zobrazíme jí za cenou v závorce
                                            priceElement.hide(500);
                                            priceElement.html(price_per_person + ' (' + returnData.total_price + ')');
                                            priceElement.show(500);
                                        } else if (controller === 'YpsilonPackages') {
                                            priceElement.hide(500);
                                            priceElement.html(price_per_person);
                                            priceElement.show(500);
                                        }
                                        break;

                                    // stavy na dotaz
                                    case 'RQ':
                                    case 'ER':
                                        img.attr({
                                            'src': config.fileServer+'/img/availability/rq.svg',
                                            'title': cesys.ts('Kapacita na dotaz'),
                                            'alt': cesys.ts('NA DOTAZ')
                                            })
											.addClass('availability-small');
                                        var price_per_person = priceElement.html();
                                        if (typeof returnData.price_per_person != 'undefined') { // vrácená cena za osobu, tak jí načtu protože se mohla změnit
                                            price_per_person = returnData.price_per_person;
                                        }
                                        if (typeof returnData.total_price != 'undefined') { // vrácena i celkévá cena ....zobrazíme jí za cenou v závorce
                                            priceElement.hide(500);
                                            priceElement.html(price_per_person + ' (' + returnData.total_price + ')');
                                            priceElement.show(500);
                                        }
                                        if (controller == 'YpsilonPackages') {
                                            priceElement.hide(500);
                                            priceElement.html(price_per_person);
                                            priceElement.show(500);
                                        }
                                        break;

                                    // stavy obsazeno
                                    case 'NK':
                                    case 'SO':
                                    case 'XX':
                                        img.attr({
                                            'src': config.fileServer+'/img/availability/fail.svg',
                                            'title': cesys.ts('Obsazená kapacita'),
                                            'alt': cesys.ts('OBSAZENO')
                                            })
											.addClass('availability-small');
                                        if (controller == 'TrafficsDates' || controller == 'YpsilonPackages') { // pro traffics celý řádek proškrtneme (není volný)
                                            $("#grid").find("#" + data.id).addClass('occupied');
                                        }
                                        break;
                                }

                                // uložíme jaký stav je a že je už tento řádek překontrolován
                                self.status = returnData.status;
                                self.checked = true;
                            });
                        }
                    };

                    // uložím jednotlivě řadky jednak do sktuální skupiny a jednak do polo pro ověřování na kliknutí na řádek (pouze traffics)
                    trafficsDates[data.id] = frowsGroup[groupCount][frowsGroup[groupCount].length] = new Row(data);
                });

                // sériově projdeme jednotlivé skupiny (pokud nějaké existují)
                if (frowsGroup.length) {
                    front.each(frowsGroup, function (i, group) {
                        if (group != undefined) {
                            cesys.service.availability.doCheckGroupRooms(group, front.while, checkOkStatus);
                        }
                    }, 500);

                }
			},
//			gridComplete: function() {
//				console.log('gridComplete');
//			},
			loadError: function(x, status, error) {
			    switch (x.status) {
                    case 500: // Internal server error - dotaz trval příliš dlouho
                    case 504: // Gateway timeout - dotaz trval příliš dlouho
                        var errorHtml = '<ul>' +
                            '<li>' + cesys.ts('Connection timeout. Please specify the assignment.') + '</li>' +
                            '</ul>';
                        $('#tabs-searchmasks').after( // za masku vypíšeme červeně chybovou hlášku
                            $('<div>', {
                                'id': 'searches-error',
                                'class': 'alert alert-danger',
                                'html' : errorHtml
                            })
                                .fadeIn()
                        );
                        break;
                }
                $("#grid").jqGrid("clearGridData");
                $('.overlay').remove();
			},
			onSelectRow: function(rowid, a, b, c) {

				//	Jiná barva pro zvolenou řádku. Asi to mělo být, že řádky, které jsou v seznamu, tak mají jinou barvu. Asi.
//				$("#grid").setRowData(rowid, false, {
//						background:'#FAFAFA'
//						});
				controller = $("#grid").jqGrid('getGridParam', 'controller');
				occupancy = $("#grid").jqGrid('getGridParam', 'userData');
				if (typeof occupancy == "undefined"){
					occupancy = null;
				}

                // Pro traffics budeme ověřovat dostupnost až po prvním kliknutí na řádek, druhý klik už termín otevře
                if (controller === 'TrafficsDates') {
                    var row = trafficsDates[rowid];
                    if (row.checked == false) {
                        row.check('/online/availability/cesys/check_date_traffics.php');
                        return;
                    } else if (row.status !== 'OK' && row.status !== 'RQ') {
                        return;
                    }
                }

                // Pro dynamické balíčky budeme ověřovat dostupnost až po prvním kliknutí na řádek, druhý klik už termín otevře
                if (controller == 'YpsilonPackages') {
                    var row = trafficsDates[rowid];
                    if (row.checked == false) {
                        row.check('/online/availability/cesys/check_date_ypsilon.php');
                        return;
                    } else if (row.status !== 'OK' && row.status !== 'RQ') {
                        return;
                    }
                }

				var data = $("#grid").getRowData(rowid),
					i = tabswidget.tabs_searches('add', rowid, data.accommodation_master_id, occupancy, controller);

					//	Nastavení ho jako aktivního
					tabswidget.tabs_searches('option', 'active', i);

					//	Poznamenat otevření odkazu do url.
					tabswidget.tabs_searches('persist');
			}
		});


		/**
		 *	Odkazy na hromadné označení a odoznačení.
		 */
		$('#pager_left').append('<span class="left"><a class="selectallcheckboxes">'+cesys.ts('Select')+'</a>&nbsp;/&nbsp;<a class = "deselectallcheckboxes">'+cesys.ts('Deselect')+'</a> '+cesys.ts('all')+'</span>');

		$(".selectallcheckboxes").click(function(){
			$("#grid-box input[type=checkbox]").each(function() {
				this.checked = "checked";
			});
		});
		$(".deselectallcheckboxes").click(function() {
			$("#grid-box input[type=checkbox]").each(function() {
				this.checked = "";
			});
		});
		
		
});

