;(function ($, window, document, undefined) {
    $.widget('cesys.documentTemplateDialog', {

        options: {
            documentTemplates: [],
            showCheckboxTourOperator: true,
            showCheckboxContact: true,
            showCheckboxImage: true,
            showCheckboxQrCode: true,
            exportData: []
        },

        setExportData: function (data) {
            this.options.exportData = data;
            $(this._dialog).find('#DocumentTemplatesDateData').val(JSON.stringify(data));
        },

        setDocumentTemplates: function (data) {
            this.options.documentTemplates = data;
        },

        closeDialog: function () {
            var self = this;
            self._dialog.dialog('close');
        },

        _create: function() {
            var self = this,
                element = self.element;

            element.bind('click', function (event) {

                self._openDialog();

                // po kliknutí na tlačítko odešleme formulář
                self._formular.find('input[type="submit"]').bind('click', function (ev) {
                    self._sendDialog(this, ev);
                });

                event.preventDefault();
            })

        },

        _openDialog: function () {
            // připravím div pro dialog
            var self = this,
                divDialog = $('<div>').attr('class', 'dialog-div'),
                divDialogForm = $('<form>').attr('id', 'document-template-form').attr('method', 'post'),
                divDialogBody = $('<div>'),
                divDialogBodyWrapper = $('<div>').attr('class', 'well form-wrapper'),
                selectTemplate = $('<select>').attr('class', 'form-control size-small select-template').attr('id', 'selectDocumentTemplate'),
                checkboxes = [];

            // povolen checkbox CK?
            if (self.options.showCheckboxTourOperator) {
                checkboxes[checkboxes.length] = {
                    label: 'tourOperator',
                    name: cesys.ts('Insert TO'),
                    checkbox: $('<input>').attr('type', 'checkbox').attr('class', 'show-tour-operator').attr('checked', true).attr('id', 'document-tourOperator').attr('name', 'data[checkboxOptions][tourOperator]')
                };
            }

            // povolen checkbox kontakt?
            if (self.options.showCheckboxContact) {
                checkboxes[checkboxes.length] = {
                    label: 'contact',
                    name: cesys.ts('Insert contact'),
                    checkbox: $('<input>').attr('type', 'checkbox').attr('class', 'show-contact').attr('checked', true).attr('id', 'document-contact').attr('name', 'data[checkboxOptions][contact]')
                };
            }

            // Zakázáno dne 1.12.2016
            // povolen checkbox obrázek?
            // if (self.options.showCheckboxImage) {
            //     checkboxes[checkboxes.length] = {
            //         label: 'picture',
            //         name: cesys.ts('Insert picture'),
            //         checkbox: $('<input>').attr('type', 'checkbox').attr('class', 'show-image').attr('checked', true).attr('id', 'document-picture').attr('name', 'data[checkboxOptions][picture]')
            //     };
            // }

            // povolen checkbox QR code?
            if (self.options.showCheckboxQrCode) {
                checkboxes[checkboxes.length] = {
                    label: 'qrcode',
                    name: cesys.ts('Insert qrcode'),
                    checkbox: $('<input>').attr('type', 'checkbox').attr('class', 'show-qr').attr('checked', true).attr('id', 'document-qrcode').attr('name', 'data[checkboxOptions][qrcode]')
                };
            }

            // K selectu přidám options
            $.each(self.options.documentTemplates, function (optgroup, options) {
                var optgroup = $('<optgroup>').attr('label', optgroup);
                $.each(options, function (k, v) {
                    var option = $('<option>').attr('name', v[0]).html(v[1]);
                    optgroup.append(option);
                });
                selectTemplate.append(optgroup);
            });
            // Do wrapperu dáme požadovaný obsah
            divDialogBodyWrapper.append($('<div>')
                .append($('<input>')
                    .attr('type', 'hidden')
                    .attr('id', 'DocumentTemplatesDateData')
                    .attr('name', 'data[dateData]')
                    .attr('value', JSON.stringify(self.options.exportData))));

            divDialogBodyWrapper.append($('<div>')
                .attr('class', 'row')
                .append($('<div>')
                    .attr('class', 'form-group col-sm-12')
                    .append($('<label>').html(cesys.ts('Type')))
                    .append(selectTemplate)));

            // přidám povolený checkboxy
            var row = $('<div>').attr('class', 'row');
            $.each(checkboxes, function (k, checkbox) {
                row.append($('<div>')
                    .attr('class', 'form-group col-sm-6')
                    .append($('<label>').attr('for', 'document-' + checkbox.label).html(checkbox.name))
                    .append($('<br />'))
                    .append(checkbox.checkbox));

                if (k%2 == 1) {
                    divDialogBodyWrapper.append(row);
                    row = $('<div>').attr('class', 'row');
                }
            });
            divDialogBodyWrapper.append(row);

            //přidáme tlačitka formuláře
            exportButton = $('<input>').attr('type', 'submit').attr('class', 'btn size-medium btn-blue').attr('data-button-type', 'export').val(cesys.ts('Export'));
            editButton = $('<input>').attr('type', 'submit').attr('class', 'btn size-medium btn-blue').attr('data-button-type', 'edit').val(cesys.ts('Edit and export'));
            divDialogBodyWrapper.append($('<div>').addClass('text-right row').append(editButton).append('&nbsp;').append(exportButton));

            // Wrapper přidám do těla dialogu
            divDialogBody
                .append($('<p>').html(cesys.ts('PDF/DOCX dialog Header')))
                .append(divDialogBodyWrapper);

            divDialogForm.append(divDialogBody);
            divDialog.append(divDialogForm);

            // Dialog přidám do stránky
            $('body').append(divDialog);

            // a otevřu dialog
            divDialog.dialog({
                width: 370,
                title: cesys.ts('Export (PDF, XLS)'),
                modal: true
            });

            self._formular = divDialogForm;

            self._dialog = divDialog;


        },

        /**
         * Odeslání formuláře
         * @param clickedButton
         * @param ev
         * @private
         */
        _sendDialog: function (clickedButton, ev) {
            var self = this,
                idDocumentTemplate = self._formular.find('#selectDocumentTemplate').find(':selected').attr('name');

            switch ($(clickedButton).data('button-type')) {
                case 'export':
                    self._formular.attr('action', '/admin/DocumentTemplates/view/'+idDocumentTemplate);
                    break;
                case 'edit':
                    self._formular
                        .attr('target', '_blank')
                        .attr('action', '/admin/DocumentTemplates/edit/'+idDocumentTemplate);
                    break;
                case 'close':
                    self._dialog.dialog('close');
                    ev.preventDefault();
                    break;
            }
        }
    })
})(jQuery, window, document);