

//obsluhuje nápis zkopírováno tam kde se kopíruje do schránky
function besideMouse() {
	//na chvíly zobrazí zelený nápis zkopírováno
	$('.copy-span').click(function () {
		$("#besideMouse").toggle();
		$('#besideMouse').offset(currentMousePos);
		setTimeout('$("#besideMouse").toggle()', 2000);
	});
	//udržuje v proměnné pozici myši
	var currentMousePos = { top: -1, left: -1 };
	$(document).mousemove(function(e){
		currentMousePos = { top: e.pageY + 10, left: e.pageX + 10 };
		$('#besideMouse').offset(currentMousePos);
	});
}