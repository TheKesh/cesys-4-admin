/*
 * js pro drag and drop pro nahrání souborů do fs_files
 * @param string url url kam se odesílají soubory ajaxem 
 */
function fileDragAndDrop (url) {	
	$('#filedrag').on(
		"dragover drop", 
		function(e) {
			e.preventDefault();  // allow dropping and don't navigate to file on drop
			FileDragHover(e);
		}
	).on(
		"drop", 
		function(e) {
			//při dropu se soubory rovnou pošlou přez ajax, není možné je přidat do file inputu (v lišce)
			var ajaxData = new FormData();
			var droppedFiles = e.originalEvent.dataTransfer.files;
			if (droppedFiles) {
				$.each( droppedFiles, function(i, file) {
					ajaxData.append('data[FsFile][file][]', file);
				});
				$.ajax({
					url: url,
					type: 'POST',
					data: ajaxData,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,
					complete: function() {
					},
					success: function(data) {
						if (data.success) {
							toastr.success(data.successTranslated);
							//funkce reloadFiles musí být definovaná ve view
							reloadFiles();
						} else {
							toastr.warning(data.errors.join('<br />'));
						}
					},
					error: function(data) {
						toastr.warning(data.errors.join('<br />'));
					}
				});
			}
		}
	).on('dragleave', function(e) {
		FileDragHover(e);
	});
}

//třída pro hover se souborem
function FileDragHover(e) {
	e.target.className = (e.type == "dragover" ? "hover" : "");
}